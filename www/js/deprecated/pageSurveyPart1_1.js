﻿var db = window.openDatabase("Database", "1.0", "Survey", 200000);

function errorCB(err) {
    alert("Error processing SQL: " + err.code);
}

function successCB() {
    //alert("success!");
}

function submitBtnSP1_1test() {
    console.log($('#satisfyStatus :radio:checked').val());
}

function submitBtnSP1_1() {

    var Person = {
        id: $('#IDNumber_sp1_1').val(),
        title: $('#title_sp1_1').val(),
        firstName: $('#firstName_sp1_1').val(),
        lastName: $('#lastName_sp1_1').val(),
        passport: $('#passport_sp1_1').val(),
        
        dob: $('#dob_sp1_1').val(),
        nation: $('#nation_sp1_1').val(),
        race: $('#race_sp1_1').val(),
        religion: $('#religion_sp1_1').val(),
        marriageStatus: $('#marriageStatus_sp1_1').val(),
        staIDNumber: $("#staIDNumber").val(),

        recorderRelation: $("#relation").val()
    };

    var Address = {

        homenumber: $('#homeNumber_sp1_1').val(),
        addressType: $('#addressType_sp1_1').val(),
		address:  $('#address_sp1_1').val(), 
		tumbon:	$('#tumbon_sp1_1').val(), 
		distint: $('#distint_sp1_1').val(),

		province: $('#province_sp1_1').val(),
		numpost: $('#numpost_sp1_1').val(),
		tel: $('#tel_sp1_1').val(),
		birthProvince: $('#birthProvince_sp1_1').val(),
		numVictim: $('#numVictim_sp1_1').val(),
		
		addressSatisfy: $('#satisfyStatus :radio:checked').val()
    };

    //value form Survey page
    var txtIDNumber = $("#IDNumber").val();

    //for (var key in Person) {
    //    if (Person.hasOwnProperty(key)) {
    //        console.log(key + " -> " + Person[key]);
    //    }
    //}

    //for (var key in Address) {
    //    if (Address.hasOwnProperty(key)) {
    //        console.log(key + " -> " + Address[key]);
    //    }
    //}

    var chk = document.getElementById("informantCB").checked;
    var chk2 = document.getElementById("useInformantCB").checked;

    if (chk == true) {
        console.log('update informant');

        //--------- อัพเดทตาราง Person ในกรณีที่เป็นผู้ให้ข้อมูล -------------
        db.transaction(function (tx) {
            tx.executeSql('UPDATE Person SET ' +
                'Passport = "' + Person.passport + '", DOB = "' + Person.dob + '", Nation = "' + Person.nation + '", ' +
                'Race = "' + Person.race + '", Religion = "' + Person.religion + '", MarriageStatus = "' + Person.marriageStatus + '", ' +
                'AddressSatisfy = ' + Address.addressSatisfy + ', BirthProvince = "' + Address.birthProvince + '" ' +
                'WHERE PID = ' + txtIDNumber + ' ');
        }, errorCB, successCB);

        //--------- อัพเดทตาราง Address --------------
        var AddressID;
        db.transaction(function (tx) { //หา AddressID ของ Person
            tx.executeSql('SELECT A.AddressID FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = ' + txtIDNumber + ' ', [],
                (function (tx, response) {
                    AddressID = response.rows.item(0).AddressID;
                }), errorCB);
        });       

        db.transaction(function (tx) {
            tx.executeSql('UPDATE Address SET AddressType = "' + Address.addressType + '", HomeCode = "'+ Address.homenumber +'" ' +
                ' WHERE AddressID = "' + AddressID + '" ');
        }, errorCB, successCB);
        //------------------------------------------

    }
    else {
        console.log('insert new person or update old person');

        var queryStr = "SELECT PID FROM Person WHERE PID = '" + Person.id + "' "; //เช็คว่ามีบุคคลนี้ใน Person หรือยัง
        db.transaction(function (tx) {
            tx.executeSql(queryStr, [],
                (function (tx, response) {
                    if (response.rows.length == 0 && Person.id != "") { // ---- ถ้ายังไม่มีข้อมูล -> Insert บุคคลไปใหม่
                        console.log("ยังไม่มีข้อมูลคนนี้");

                        // insert ลงตาราง person
                        db.transaction(
                            function (tx) {
                                tx.executeSql('INSERT INTO Person ( PID, Title, FirstName, LastName, Passport, DOB, Nation, Race, Religion, MarriageStatus, AddressSatisfy, BirthProvince, RecordStaffPID )'
                                    + ' VALUES ( ' + Person.id + ', "' + Person.title + '", "' + Person.firstName + '", "' + Person.lastName + '", "' + Person.passport + '", "' + Person.dob + '", "' + Person.nation + '", "' + Person.race + '", "' + Person.religion + '", "' + Person.marriageStatus + '", "' + Address.addressSatisfy + '", "' + Address.birthProvince + '", ' + Person.staIDNumber + ' ) ');
                            }, errorCB, successCB);
                        
                        //สร้างความเกี่ยวข้องกับผู้ให้ข้อมูล
                        db.transaction(
                            function (tx) {
                                tx.executeSql('INSERT INTO PersonRecordBy ( PID, RecordPID, Relation )'
                                    + ' VALUES ( ' + Person.id + ',' + txtIDNumber + ', "' + Person.recorderRelation + '" ) ');
                            }, errorCB, successCB);

                        //ใช้ที่อยู่เดียวกับผู้ให้ข้อมูล
                        if (chk2 == true) {
                            console.log('ใช้ที่อยู่เดียว กับ ผู้ให้ข้อมูล');

                            var AddressID;
                            db.transaction(function (tx) { //หา AddressID ของ Person
                                tx.executeSql('SELECT A.AddressID FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = ' + txtIDNumber + ' ', [],
                                    (function (tx, response) {
                                        AddressID = response.rows.item(0).AddressID;
                                    }), errorCB);
                            });

                            db.transaction(function (tx) {
                                tx.executeSql('INSERT INTO PersonVSAddress ( PID, AddressID )'
                                    + ' VALUES ( ' + Person.id + ', "' + AddressID + '" )');
                            }, errorCB, successCB);
                        } // ใช้ที่อยู่คนละที่กับผู้ให้ข้อมูล
                        else {
                            console.log('ใช้ที่อยู่คนละที่ กับ ผู้ให้ข้อมูล');

                            var AddressID = guidGenerator();

                            //เพิ่ม address ของผู้ให้ข้อมูล
                            db.transaction(function (tx) {
                                tx.executeSql('INSERT INTO Address ( AddressID, AddressData, Tumbon, Amphor, Province, Postcode, Phone, AddressType, HomeCode )'
                                    + ' VALUES ( "' + AddressID + '", "' + Address.address + '", "' + Address.tumbon + '", "' + Address.distint + '", "' + Address.province + '", "' + Address.numpost + '", "' + Address.tel + '", "' + Address.addressType + '", "' + Address.homenumber + '" )');
                            }, errorCB, successCB);

                            // สร้างความเกี่ยวข้องระหว่าง บุคคล กับ ที่อยู่
                            db.transaction(function (tx) {
                                tx.executeSql('INSERT INTO PersonVSAddress ( PID, AddressID )'
                                    + ' VALUES ( ' + Person.id + ', "' + AddressID + '" )');
                            }, errorCB, successCB);
                        }

                    }
                    else if (Person.id != "" && response.rows.length != 0) { // --- ถ้ามีบุคคลนี้แล้วให้แจ้งเตือนว่าจะอัพเดท หรือใช้ข้อมูลเฉยๆ

                        var r = confirm("พบเลขบัตรประชาชนนี้แล้ว \nต้องการอัพเดทข้อมูล หรือไม่");

                        if (r == true) {
                            
                            //--------- อัพเดทตาราง Person --------------
                            db.transaction(function (tx) {
                                tx.executeSql('UPDATE Person SET ' +
                                    'Title = "' + Person.title + '", FirstName = "' + Person.firstName + '", LastName ="' + Person.lastName + '" , Passport = "' + Person.passport + '", ' +
                                    'DOB = "' + Person.dob + '", Nation = "' + Person.nation + '", Race = "' + Person.race + '", Religion = "' + Person.religion + '", ' +
                                    'MarriageStatus = "' + Person.marriageStatus + '", AddressSatisfy = "' + Address.addressSatisfy + '", BirthProvince = "' + Address.birthProvince + '" ' +
                                    'WHERE PID = '+ Person.id +' ');
                            }, errorCB, successCB);
                            //-----------------------------------------

                            //--------- อัพเดทตาราง Address --------------
                            var AddressID;
                            db.transaction(function (tx) { //หา AddressID ของ Person
                                tx.executeSql('SELECT A.AddressID FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = ' + Person.id + ' ', [],
                                    (function (tx, response) {
                                        AddressID = response.rows.item(0).AddressID;
                                    }), errorCB);
                            });

                            db.transaction(function (tx) {
                                tx.executeSql('UPDATE Address SET ' +
                                    'AddressData = "' + Address.address + '", Tumbon = "' + Address.tumbon + '", Amphor = "' + Address.distint + '", ' +
                                    'Province = "' + Address.province + '", Postcode = "' + Address.numpost + '", Phone = "' + Address.tel + '", ' +
                                    'AddressType = "' + Address.addressType + '", HomeCode = "' + Address.homenumber + '" ' +
                                    'WHERE AddressID = "' + AddressID + '" ');
                            }, errorCB, successCB);
                            //-----------------------------------------

                            console.log("อัพเดท");
                        }
                        else {
                            console.log("ยกเลิก");
                        }

                    } else { // --- ยังไม่ได้กรอก CID

                        console.log("ยังไม่ได้กรอกเลขบัตรประชาชน");

                    }

                }), errorCB, successCB);
        });
    }
}

function checkboxCB() {
    clearData_sp1_1();
    var chk = document.getElementById("informantCB").checked;
    $('input[name="useInformantCB"]').prop("checked", false).checkboxradio('refresh');
    if (chk == true) {    
        if ($('#dupPerson').is(':visible') && $('#dupAddress').is(':visible')) {
            $('#dupPerson').hide();
            $('#dupAddress').hide();
            $('#IDNumber_sp1_1').val($('#IDNumber').val());
        }

    } else {
        
        clearData_sp1_1();
        if ($('#dupPerson').is(':hidden') && $('#dupAddress').is(':hidden')) {
            $('#dupPerson').show();
            $('#dupAddress').show();
        }        

    }
}

function useInformantCB() {
    clearData_sp1_1();
    if ($('#dupPerson').is(':hidden') && $('#dupAddress').is(':hidden')) {
        $('#dupPerson').show();
        $('#dupAddress').show();
    }    
    
    var chk = document.getElementById("useInformantCB").checked;
    var txtIDNumber = $("#IDNumber").val();
    $('input[name="informantCB"]').prop("checked", false).checkboxradio('refresh');

    if (chk == true) {
        
        var AddressID;
        db.transaction(function (tx) { //หา AddressID ของ Person
            tx.executeSql('SELECT A.AddressID FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = ' + txtIDNumber + ' ', [],
                (function (tx, response) {
                    AddressID = response.rows.item(0).AddressID;
                }), errorCB);
        });

        db.transaction(function (tx) { //หา AddressID ของ Person
            tx.executeSql('SELECT * FROM Address WHERE AddressID = "' + AddressID + '" ', [],
                (function (tx, response) {

                    $('#homeNumber_sp1_1').val(response.rows.item(0).HomeCode);
                    $('#addressType_sp1_1').val(response.rows.item(0).AddressType); $('#addressType_sp1_1').selectmenu("refresh");
                    $('#address_sp1_1').val(response.rows.item(0).AddressData);
                    $('#tumbon_sp1_1').val(response.rows.item(0).Tumbon);
                    $('#distint_sp1_1').val(response.rows.item(0).Amphor);
                    $('#province_sp1_1').val(response.rows.item(0).Province); $('#province_sp1_1').selectmenu("refresh");
                    $('#numpost_sp1_1').val(response.rows.item(0).Postcode);
                    $('#tel_sp1_1').val(response.rows.item(0).Phone);

                }), errorCB);
        });

    }
    else {
        clearData_sp1_1();
    }
}

function clearData_sp1_1() {    
    $('#IDNumber_sp1_1').val("");
    $('#title_sp1_1').val("");
    $('#firstName_sp1_1').val("");
    $('#lastName_sp1_1').val("");
    $('#passport_sp1_1').val("");

    $('#relation').val("");

    $('#dob_sp1_1').val("");
    $('#nation_sp1_1').val("");
    $('#race_sp1_1').val("");
    $('#religion_sp1_1').val("");
    $('#marriageStatus_sp1_1').val("");

    $('#homeNumber_sp1_1').val("");
    $('#addressType_sp1_1').val("");
    $('#address_sp1_1').val("");
    $('#tumbon_sp1_1').val("");
    $('#distint_sp1_1').val("");

    $('#province_sp1_1').val("");
    $('#numpost_sp1_1').val("");
    $('#tel_sp1_1').val("");
    $('#birthProvince_sp1_1').val("");
    $('#numVictim_sp1_1').val("");

    $('#survey_part1_1').find('select').selectmenu('refresh');
    //$('#survey_part1_1').find('input').val(''); // clear all input in div //have bug on radio
    //$('#survey_part1_1').find('select').val('');  // clear all select in div

    //$('#satisfyStatus :radio:checked').val(""); 
    
}

// เอาไว้คลิกแล้ว auto fill ข้อมูล
$('#lvIDNumber_sp1_1').on('click', 'li', function () {
    //alert("Works"); // id of clicked li by directly accessing DOMElement property
    //console.log($(this).text());
    var val = $(this).text();
    $('#IDNumber_sp1_1').val(val);
    $('#lvIDNumber_sp1_1').toggle();

    var txtIDNumber_sp1_1 = $(this).text();
    //console.log(txtStaIDNumber);

    //var queryStr = "SELECT Title, FirstName, LastName, Job, Position FROM Staff WHERE StaffID = '" + txtStaIDNumber + "' ";
    //db.transaction(function (tx) {
    //    tx.executeSql(queryStr, [],
    //        (function (tx, response) {
    //            //console.log(response.rows.item(0).Title);
    //            $('#staTitle').val(response.rows.item(0).Title);
    //            $('#staFirstName').val(response.rows.item(0).FirstName);
    //            $('#staLastName').val(response.rows.item(0).LastName);
    //            $('#staJob').val(response.rows.item(0).Job);
    //            $('#staPosition').val(response.rows.item(0).Position);
    //        }), errorCB, successCB);
    //});

    //var queryAddr = "SELECT AddressData, Tumbon, Amphor, Province, Phone FROM Address INNER JOIN PersonVSAddress ON Address.AddressID = PersonVSAddress.AddressID WHERE PersonVSAddress.PID = '" + txtStaIDNumber + "' ";
    //db.transaction(function (tx) {
    //    tx.executeSql(queryAddr, [],
    //        (function (tx, response) {
    //            //console.log(response.rows.item(0).AddressID);
    //            $('#staAddress').val(response.rows.item(0).AddressData);
    //            $('#staTumbon').val(response.rows.item(0).Tumbon);
    //            $('#staAmphor').val(response.rows.item(0).Amphor);
    //            $('#staProvince').val(response.rows.item(0).Province);
    //            $('#staPhone').val(response.rows.item(0).Phone);
    //        }), errorCB, successCB);
    //});

});

function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

