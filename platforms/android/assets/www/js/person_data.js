﻿var db;

var cid = "";

function errorCB(err) {
    alert("Error processing SQL: " + err.code);
}

function successCB() {
    //alert("success!");
}

$(document).on("pagecreate", "#blank", function (event) {
    //sleep(3000);
    //window.location.href = 'home.html';
    $('#blank_next').click(function () {
        window.location.href = 'home.html';
    });

});

document.addEventListener("backbutton", function (e) {
    if ($.mobile.activePage.is('#blank')) {
        e.preventDefault();
        window.location.href = 'home.html';
        //navigator.app.exitApp();
    }
    else {
        navigator.app.backHistory()
    }
}, false);

//--------------- informant page ---------------

$(document).on("pagebeforecreate", "#informant", function (event) {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);
    $("#headerInformant").empty(); $("#subHeaderInformant").empty();
    $("#bankDIV").hide(); $("#relation_informant").hide();
    if ($('#title').children('option').length < 3) { insertDropdown('informant'); lvCreate('informant'); }

    //console.log(sessionStorage.getItem('PersonCID') + " " + sessionStorage.getItem('EditType'));

    if (sessionStorage.getItem('PersonCID') === null) {
        console.log('null');
        document.getElementById("navbarEditInfomant").style.display = "none";
        $('#headerInformant').append('เพิ่มข้อมูลบุคคลใหม่');
        $("#subHeaderInformant").append('ผู้ให้ข้อมูล');
        //$("#relation_informant").show();        
    }
    else if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'standIn') {
        console.log('standin');
        document.getElementById("navbarInfomant").style.display = "none";
        $('#headerInformant').append('แก้ไขข้อมูลผู้ดำเนินการแทน');
        $("#subHeaderInformant").append('ผู้ดำเนินการแทน');
        $("#relation_informant").show(); $("#bankDIV").show();

        db.transaction(function (tx) {
            tx.executeSql('SELECT P.*, PS.StandInRole, PS.StandIn FROM Person P INNER JOIN PersonStandIn PS ON P.PID = PS.StandIn WHERE PS.PID = "' + sessionStorage.getItem('PersonCID') + '" ', [],
                (function (tx, results) {
                    if (results.rows.length != 0 && results.rows[0].StandIn != "") {
                        //console.log(results.rows[0]);
                        $('#relation').val(results.rows[0].StandInRole);
                        loadInformantData(results.rows[0].PID);

                        //$('select').selectmenu().selectmenu('refresh', true);
                    }
                }), errorCB);
        }, errorCB, function () {
            //$('#informant select').selectmenu('refresh');
        });
    }
    else if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'emergency') {
        console.log('emergency');
        document.getElementById("navbarInfomant").style.display = "none";
        $('#headerInformant').append('แก้ไขข้อมูลผู้ติดต่อฉุกเฉิน');
        $("#subHeaderInformant").append('ผู้ติดต่อฉุกเฉิน');
        $("#relation_informant").show();

        db.transaction(function (tx) {
            tx.executeSql('SELECT P.*, PEC.EmergencyContactRole, PEC.EmergencyContactPID FROM Person P INNER JOIN PersonEmergencyContact PEC ON P.PID = PEC.EmergencyContactPID WHERE PEC.PID = "' + sessionStorage.getItem('PersonCID') + '" ', [],
                (function (tx, results) {
                    if (results.rows.length != 0 && results.rows[0].EmergencyContactPID != "") {
                        //console.log(results.rows[0]);
                        $('#relation').val(results.rows[0].EmergencyContactRole);
                        loadInformantData(results.rows[0].PID);

                        //$('select').selectmenu().selectmenu('refresh', true);
                    }
                }), errorCB);
        }, errorCB, function () {
            //$('#informant select').selectmenu('refresh');
        });
    }
    else {
        console.log('else');
        document.getElementById("navbarEditInfomant").style.display = "none";
        $('#headerInformant').append('เพิ่มข้อมูลบุคคลใหม่');
    }

});

$(document).on("pagecreate", "#informant", function (event) {
    //db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);    
    //if (navigator.onLine) {
    //    initMap();
    //} else { }


    $('#informant_next').click(function () {
        //window.location.href = 'home.html';



        var queryStr = "SELECT PID FROM Person WHERE PID = '" + $('#IDNumber').val() + "' ";
        db.transaction(function (tx) {
            tx.executeSql(queryStr, [], (function (tx, response) {
                if (response.rows.length == 0 && $('#IDNumber').val() != "" && $('#IDNumber').val().length == 13 && /^\d+$/.test($('#IDNumber').val())) {
                    replaceNull("informant");

                    var informant = {
                        CID: $('#IDNumber').val(),
                        Title: $('#title').val(),
                        FName: $('#firstName').val(),
                        LName: $('#lastName').val(),
                        Phone: $('#tel').val(),

                        AddressID: guidGenerator(),
                        AddressType: "9999",
                        HouseNumber: $('#HouseNumber').val(),
                        MooNumber: $('#MooNumber').val(),
                        VillageName: $('#VillageName').val(),
                        Alley: $('#Alley').val(),
                        Street: $('#Street').val(),
                        Province: $('#province').val(),
                        Amphor: $('#amphor').val(),
                        Tumbon: $('#tumbon').val(),
                        Postcode: $('#postcode').val(),

                        recordStaffCID: sessionStorage.getItem('CID')
                    };

                    var personalTypeInfo = {
                        CID: $('#IDNumber').val(),
                        income: 0,
                        outcome: 0,
                        inDept: 0,
                        exDept: 0
                    };

                    //alert("ยังไม่มีข้อมูลคนนี้");
                    var r = confirm("ต้องการไปหน้าต่อไป ใช่หรือไม่");
                    if (r == true) {

                        console.log("ใช่");
                        insertPerson(informant, 'informant');

                        //insertPersonalType(personalTypeInfo, 'insert');

                        $(":mobile-pagecontainer").pagecontainer("change", "#info", { transition: "slide" });
                    } else {
                        console.log("ยกเลิก");
                    }

                }
                else if ((($('#IDNumber').val() != "" && $('#IDNumber').val() != "-") && $('#IDNumber').val().length == 13 && /^\d+$/.test($('#IDNumber').val())) && response.rows.length != 0) {
                    var r = confirm("พบเลขบัตรประชาชนนี้แล้ว \nต้องการใช้ข้อมูลบุคคลนี้ หรือไม่");
                    if (r == true) {
                        console.log("ใช่");
                        //window.location.href = 'person_data.html#info';
                        $(":mobile-pagecontainer").pagecontainer("change", "#info", { transition: "slide" });

                    } else {
                        console.log("ยกเลิก");
                    }
                }
                else if ($('#IDNumber').val() == "") {
                    //alert("ยังไม่ได้กรอกเลขบัตรประชาชน");
                    var r = confirm("ยังไม่ได้กรอกเลขบัตรประชาชน \nผู้รับบริการต้องการ 'ให้ข้อมูลด้วยตนเอง' หรือไม่");
                    if (r == true) {
                        console.log("ใช่");
                        //window.location.href = 'person_data.html#info';
                        $(":mobile-pagecontainer").pagecontainer("change", "#info", { transition: "slide" });
                    } else {
                        console.log("ยกเลิก");
                    }
                }
                else { alert("เลขบัตรประชาชนไม่ถูกต้อง"); }
            }), errorCB, successCB);
        });

    });

    $('#informant_edit').click(function () {

        if (sessionStorage.getItem('EditType') == 'standIn' || sessionStorage.getItem('EditType') == 'emergency') {



            db.transaction(function (tx) {
                tx.executeSql("SELECT PID FROM Person WHERE PID = '" + $('#IDNumber').val() + "' ", [], function (tx, rs) {
                    if (rs.rows.length == 0 && ($('#IDNumber').val() != "" && $('#IDNumber').val() != "-") && /^\d+$/.test($('#IDNumber').val())) {
                        replaceNull("informant");

                        var informant = {
                            CID: $('#IDNumber').val(),
                            Title: $('#title').val(),
                            FName: $('#firstName').val(),
                            LName: $('#lastName').val(),
                            Phone: $('#tel').val(),

                            AddressID: guidGenerator(),
                            AddressType: "9999",
                            HouseNumber: $('#HouseNumber').val(),
                            MooNumber: $('#MooNumber').val(),
                            VillageName: $('#VillageName').val(),
                            Alley: $('#Alley').val(),
                            Street: $('#Street').val(),
                            Province: $('#province').val(),
                            Amphor: $('#amphor').val(),
                            Tumbon: $('#tumbon').val(),
                            Postcode: $('#postcode').val(),

                            Relation: $('#relation').val(),
                            recordStaffCID: sessionStorage.getItem('CID')
                        };

                        var personalTypeInfo = {
                            CID: $('#IDNumber').val(),

                            income: 0,
                            outcome: 0,
                            inDept: 0,
                            exDept: 0,

                            bankName: $('#bank_informant').val(),
                            bankBranch: $('#branch_informant').val(),
                            accountNum: $('#accountNum_informant').val(),
                            accountName: $('#accountName_informant').val()
                        };

                        if (sessionStorage.getItem('EditType') == 'standIn') {

                            if (document.getElementById('copyInfoAddrCB').checked) {
                                //alert("checked");

                                db.transaction(function (tx) { //หา AddressID ของ Person
                                    tx.executeSql('SELECT A.AddressID FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = "' + sessionStorage.getItem('PersonCID') + '" ', [],
                                        (function (tx, response) {
                                            informant.AddressID = response.rows.item(0).AddressID;
                                            insertPerson(informant, 'standInExistAddr');

                                        }), errorCB);
                                });

                            } else {
                                insertPerson(informant, 'standIn');
                            }

                            //insertPerson(informant, 'standIn');

                            var insertPersonBank = 'INSERT INTO PersonBankAccount ( PID, BankName, BankBranch, AccountNo, AccountName )'
                            + ' VALUES ( "' + $('#IDNumber').val() + '", "' + $('#bank_informant').val() + '", "' + $('#branch_informant').val() + '", "' + $('#accountNum_informant').val() + '", "' + $('#accountName_informant').val() + '" )';
                            tx.executeSql(insertPersonBank, null, function () { console.log('PersonBankAccount insert success!'); }, errorCB);

                            //insertPersonalType(personalTypeInfo, 'insert');

                        }
                        else if (sessionStorage.getItem('EditType') == 'emergency') {

                            if (document.getElementById('copyInfoAddrCB').checked) {
                                //alert("checked");

                                db.transaction(function (tx) { //หา AddressID ของ Person
                                    tx.executeSql('SELECT A.AddressID FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = "' + sessionStorage.getItem('PersonCID') + '" ', [],
                                        (function (tx, response) {
                                            informant.AddressID = response.rows.item(0).AddressID;
                                            insertPerson(informant, 'emergencyExistAddr');

                                        }), errorCB);
                                });

                            } else {
                                insertPerson(informant, 'emergency');
                            }
                            //insertPerson(informant, 'emergency');


                        } else {
                            console.log('ไม่พบ EditType');
                        }
                    }
                    else if (rs.rows.length != 0 && ($('#IDNumber').val() != "" && $('#IDNumber').val() != "-") && /^\d+$/.test($('#IDNumber').val())) {
                        replaceNull("informant");
                        db.transaction(function (tx) {

                            var updateInformant = 'UPDATE Person SET Title = "' + $('#title').val() + '", FirstName = "' + $('#firstName').val() + '", LastName = "' + $('#lastName').val() + '", '
                            + 'Phone = "' + $('#tel').val() + '" WHERE PID = "' + $('#IDNumber').val() + '" ';

                            var updateInformantAddr = 'UPDATE Address SET Tumbon = "' + $('#tumbon').val() + '", '
                            + 'Amphor = "' + $('#amphor').val() + '", Province = "' + $('#province').val() + '", Postcode = "' + $('#postcode').val() + '", '
                            + 'HouseNumber = "' + $('#HouseNumber').val() + '", MooNumber = "' + $('#MooNumber').val() + '", VillageName = "' + $('#VillageName').val() + '", '
                            + 'Alley = "' + $('#Alley').val() + '", StreetName = "' + $('#Street').val() + '" '
                            + 'WHERE AddressID = "' + $('#addrID').val() + '" ';

                            var updateInformantBank = 'UPDATE PersonBankAccount SET '
                            + 'BankName = "' + $('#bank_informant').val() + '", BankBranch = "' + $('#branch_informant').val() + '", '
                            + 'AccountNo = "' + $('#accountNum_informant').val() + '", AccountName = "' + $('#accountName_informant').val() + '" '
                            + 'WHERE PID = "' + $('#IDNumber').val() + '" ';

                            var insertPersonBank = 'INSERT INTO PersonBankAccount ( PID, BankName, BankBranch, AccountNo, AccountName )'
                            + ' VALUES ( "' + $('#IDNumber').val() + '", "' + $('#bank_informant').val() + '", "' + $('#branch_informant').val() + '", "' + $('#accountNum_informant').val() + '", "' + $('#accountName_informant').val() + '" )';

                            tx.executeSql(updateInformant, null, function () {
                                console.log('Person update success!');

                                if (sessionStorage.getItem('EditType') == 'standIn') {

                                    tx.executeSql(updateInformantAddr, null, function () { console.log('Old address update success!'); }, errorCB);


                                    tx.executeSql('SELECT PID FROM PersonBankAccount WHERE PID = "' + $('#IDNumber').val() + '" ', [], function (tx, rs) {
                                        if (rs.rows.length != 0) {
                                            tx.executeSql(updateInformantBank, null, function () { console.log('PersonBankAccount update success!'); }, errorCB);
                                        }
                                        else {
                                            tx.executeSql(insertPersonBank, null, function () { console.log('PersonBankAccount insert success!'); }, errorCB);
                                        }
                                    }, errorCB);

                                    tx.executeSql('UPDATE PersonStandIn SET StandIn = ' + $('#IDNumber').val() + ', StandInRole = "' + $('#relation').val() + '"'
                                    + ' WHERE PID = "' + sessionStorage.getItem('PersonCID') + '" ', null, function () { console.log('PersonStanIn update success!'); }, errorCB);


                                }
                                else if (sessionStorage.getItem('EditType') == 'emergency') {
                                    tx.executeSql(updateInformantAddr, null, function () { console.log('Old address update success!'); }, errorCB);

                                    tx.executeSql('UPDATE PersonEmergencyContact SET EmergencyContactPID = ' + $('#IDNumber').val() + ', EmergencyContactRole = "' + $('#relation').val() + '"'
                                    + ' WHERE PID = "' + sessionStorage.getItem('PersonCID') + '" ', null, function () { console.log('PersonEmergencyContact update success!'); }, errorCB);

                                } else {
                                    console.log('ไม่พบ EditType');
                                }

                            }, errorCB);


                        }, errorCB, function () {
                            window.location.href = 'home.html#profile';
                        });
                    }
                    else { alert("เลขบัตรประขาขนไม่ถูกต้อง"); }
                }, errorCB);
            }, errorCB, successCB);

        }
        else {
            console.log('No table update');
            window.location.href = 'home.html#profile';
        }

    });

    $('#informant a[title="Clear text"]').click(function () {
        // this function will be executed on click of X (clear button)
        $("#personList").empty();
        $('#informant :input').val("");
        $('#informant select').val("0000").selectmenu('refresh');
        $('#IDNumber').css('border', '1px solid #ff0000');
    });

    $('#IDNumber').on('keyup', function (e) {
        FormValidation('IDNumber');
        var theEvent = e || window.event;
        var keyPressed = theEvent.keyCode || theEvent.which;
        if ($('#IDNumber').val() == "") {
            $("#personList").empty();
            $('#informant :input').val("");
            $('#informant select').val("0000").selectmenu('refresh');
        } else {
            //var query = "SELECT * FROM Person WHERE (PID LIKE '%" + $('#searchData').val() + "%' OR FirstName LIKE '%" + $('#searchData').val() + "%' OR LastName LIKE '%" + $('#searchData').val() + "%') LIMIT 10";
            //if (keyPressed == 13) {
            db.transaction(function (tx) {
                $("#personList").empty();
                tx.executeSql("SELECT PID FROM Person WHERE PID LIKE '%" + $('#IDNumber').val() + "%' LIMIT 5", [], function (tx, results) {
                    //alert(results);
                    if (results.rows.length != 0) {
                        for (var i = 0; i < results.rows.length; i++) {
                            $("#personList").append("<li id='" + results.rows[i].PID + "'><a>" + results.rows[i].PID + "</a></li>");
                        }
                        $("#personList").listview('refresh');
                    }
                    else {
                    }
                }, errorCB);
            }, errorCB);
        }
    });

    $('#personList').on('click', 'li', function () {
        //$(this).attr('id');
        $('#IDNumber').val($(this).attr('id')); $('#IDNumber').css('border', '1px solid #00ff00');
        var informantCID = $(this).attr('id');
        var queryGetInformant = "SELECT P.*, A.*, PBA.* FROM Person P"
            + " LEFT JOIN PersonVSAddress PVA ON P.PID = PVA.PID"
            + " LEFT JOIN Address A ON PVA.AddressID = A.AddressID"
            + " LEFT JOIN PersonBankAccount PBA ON PBA.PID = P.PID"
            + " WHERE P.PID = '" + informantCID + "' ";
        console.log(queryGetInformant);
        db.transaction(function (tx) {
            tx.executeSql(queryGetInformant, [], (function (tx, results) {
                if (results.rows.length != 0) {
                    console.log(results.rows[0]);
                    $('#title').val(results.rows[0].Title);
                    $('#firstName').val(results.rows[0].FirstName);
                    $('#lastName').val(results.rows[0].LastName);
                    $('#tel').val(results.rows[0].Phone);

                    $('#HouseNumber').val(results.rows[0].HouseNumber);
                    $('#MooNumber').val(results.rows[0].MooNumber);
                    $('#VillageName').val(results.rows[0].VillageName);
                    $('#Alley').val(results.rows[0].Alley);
                    $('#Street').val(results.rows[0].StreetName);
                    $('#province').val(results.rows[0].Province);
                    autoLoadCityAndTumbon('informant', 'tumbon', 'amphor', results.rows[0].Tumbon, results.rows[0].Amphor, results.rows[0].Province);
                    $('#postcode').val(results.rows[0].Postcode);

                    if (results.rows[0].BankName != null) {
                        $('#bank_informant').val(results.rows[0].BankName);
                        $('#branch_informant').val(results.rows[0].BankBranch);
                        $('#accountNum_informant').val(results.rows[0].AccountNo);
                        $('#accountName_informant').val(results.rows[0].AccountName);
                    }

                }
                //$('#informant select').selectmenu('refresh');
            }), errorCB);
        }, errorCB);
        $("#personList").empty();
    });

    $('#copyInfoAddrCB').change(function () {
        if (this.checked) {
            var queryGetAddr = "SELECT * FROM Address A "
                + "LEFT JOIN PersonVSAddress PVA ON PVA.AddressID = A.AddressID "
                + "WHERE PVA.PID = '" + sessionStorage.getItem('PersonCID') + "' ";
            db.transaction(function (tx) {
                tx.executeSql(queryGetAddr, [], function (tx, results) {
                    $('#HouseNumber').val(results.rows[0].HouseNumber);
                    $('#MooNumber').val(results.rows[0].MooNumber);
                    $('#VillageName').val(results.rows[0].VillageName);
                    $('#Alley').val(results.rows[0].Alley);
                    $('#Street').val(results.rows[0].StreetName);
                    $('#province').val(results.rows[0].Province);
                    autoLoadCityAndTumbon('address_informant_div', 'tumbon', 'amphor', results.rows[0].Tumbon, results.rows[0].Amphor, results.rows[0].Province);
                    $('#postcode').val(results.rows[0].Postcode);
                }, errorCB);
            }, errorCB, function () {
                //$('#address_informant_div select').selectmenu('refresh');
            });
        }
        else {
            //$('#address_informant_div :input').val("");
            //$('#address_informant_div select').val("0000").selectmenu('refresh');
            var queryGetAddr = "SELECT * FROM Address A "
                + "LEFT JOIN PersonVSAddress PVA ON PVA.AddressID = A.AddressID "
                + "WHERE PVA.PID = '" + $("#IDNumber").val() + "' ";
            db.transaction(function (tx) {
                tx.executeSql(queryGetAddr, [], function (tx, results) {
                    $('#HouseNumber').val(results.rows[0].HouseNumber);
                    $('#MooNumber').val(results.rows[0].MooNumber);
                    $('#VillageName').val(results.rows[0].VillageName);
                    $('#Alley').val(results.rows[0].Alley);
                    $('#Street').val(results.rows[0].StreetName);
                    $('#province').val(results.rows[0].Province);
                    autoLoadCityAndTumbon('address_informant_div', 'tumbon', 'amphor', results.rows[0].Tumbon, results.rows[0].Amphor, results.rows[0].Province);
                    $('#postcode').val(results.rows[0].Postcode);
                }, errorCB);
            }, errorCB, function () {
                //$('#address_informant_div select').selectmenu('refresh');
            });
        }
    });

    document.getElementById('province').onchange = function () {
        var txtProvinceID = $(this).val();
        $('#amphor').empty();
        $('#amphor').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#amphor').selectmenu("refresh");
        $('#tumbon').empty();
        $('#tumbon').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#tumbon').selectmenu("refresh");
        insertCity(txtProvinceID, 'amphor');
    };

    document.getElementById('amphor').onchange = function () {
        var txtAmphorID = $(this).val();
        $('#tumbon').empty();
        $('#tumbon').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#tumbon').selectmenu("refresh");
        insertTumbon(txtAmphorID, 'tumbon');
    };

    function initMap() {
        var map;
        document.getElementById('mapInformant').innerHTML = "";
        map = new google.maps.Map(document.getElementById('mapInformant'), {
            center: new google.maps.LatLng(7.887241364, 98.3904953), //{ lat: -34.397, lng: 150.644 },
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.HYBRID
        });
    }

});

function FormValidation(inputID) {
    var fn = document.getElementById('' + inputID).value;
    if (fn == "") {
        $('#' + inputID).css('border', '1px solid #ff0000');
        return false;
    } else {
        $('#' + inputID).css('border', '1px solid #00ff00');
    }
    if (fn.length < 13) {
        $('#' + inputID).css('border', '1px solid #ff0000');
        return false;
    } else {
        if (/^\d+$/.test($('#' + inputID).val())) {
            $('#' + inputID).css('border', '1px solid #00ff00');
        }
        else {
            $('#' + inputID).css('border', '1px solid #ff0000');
        }

    }
}

//----------------------------------------------

//----------------- info page -------------------
var imgH = 0, imgW = 0;
$(document).on("pagebeforecreate", "#info", function (event) {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);
    $("#headerInfo").empty();
    if ($('#title_info').children('option').length < 3) { insertDropdown('info'); lvCreate('info'); }

    if (sessionStorage.getItem('PersonCID') === null) {
        //console.log('null'); imgDIV
        document.getElementById("navbarEditInfo").style.display = "none";
        document.getElementById("imgDIV").style.display = "block";
        $('#headerInfo').append('เพิ่มข้อมูลบุคคลใหม่');
    }
    else if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') === null) {
        //console.log('editPerson');
        document.getElementById("navbarInfo").style.display = "none";
        document.getElementById("imgDIV").style.display = "block";
        var elements = document.getElementsByClassName("HideOnUpdateInfo");
        for (var i = 0; i < elements.length; i++) {
            elements[i].style.display = "none";
        }
        $('#headerInfo').append('แก้ไขข้อมูลบุคคล');
        loadPersonData(sessionStorage.getItem('PersonCID'));

    }
    else if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'standIn') {
        //console.log('standin');
        document.getElementById("navbarInfo").style.display = "none";
        $('#headerInfo').append('แก้ไขข้อมูลบุคคล');

        db.transaction(function (tx) {
            tx.executeSql('SELECT P.*, PS.StandInRole FROM Person P INNER JOIN PersonStandIn PS ON P.PID = PS.StandIn WHERE PS.PID = "' + sessionStorage.getItem('PersonCID') + '" ', [],
                (function (tx, results) {
                    if (results.rows.length != 0) {
                        loadPersonData(results.rows[0].PID);
                        $('#relation_info').val(results.rows[0].StandInRole);
                        $('#info select').selectmenu('refresh');
                    }

                }), errorCB);
        });
    }
    else if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'newFamily') {
        //console.log('newFamily');        
        document.getElementById("navbarEditInfo").style.display = "none";
        $('#headerInfo').append('เพิ่มครอบครัว');
    }
    else if (sessionStorage.getItem('FamilyCID') !== null && sessionStorage.getItem('EditType') == 'famPerson') {
        console.log('editPersonFam');
        document.getElementById("navbarInfo").style.display = "none";
        var elements = document.getElementsByClassName("HideOnUpdateInfo");
        elements[0].style.display = "none";
        $('#headerInfo').append('แก้ไขข้อมูลบุคคล');

        db.transaction(function (tx) {
            tx.executeSql('SELECT * FROM PersonFamily WHERE PID = "' + sessionStorage.getItem('FamilyCID') + '" ', [],
                (function (tx, results) {
                    if (results.rows.length != 0) {

                        loadPersonData(sessionStorage.getItem('FamilyCID'));
                        $('#relation_info').val(results.rows[0].FamilyRole);
                        $('#info select').selectmenu('refresh');
                    }
                    else {
                        console.log('ไม่มีคนนี้ในครอบครัว');
                        loadPersonData(sessionStorage.getItem('FamilyCID'));
                    }
                }), errorCB);
        });


    }
    else if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'newMate') {
        console.log('newMate');
        document.getElementById("navbarEditInfo").style.display = "none";
        $('#relation_info_div').hide();
        $('#headerInfo').append('เพิ่มคู่สมรส');
    }
    else {
        document.getElementById("navbarEditInfo").style.display = "none";
        $('#headerInfo').append('เพิ่มข้อมูลบุคคลใหม่');
    }

});

$(document).on("pagecreate", "#info", function (event) {
    //alert(sessionStorage.getItem('PersonCID'));    

    $('#info_next').click(function () {

        if (sessionStorage.getItem('PersonCID') !== null && (sessionStorage.getItem('EditType') == 'newFamily' || sessionStorage.getItem('EditType') == 'newMate')) {
            //console.log(sessionStorage.getItem('EditType'));

            var queryStr = "SELECT PID FROM Person WHERE PID = '" + $('#IDNumber_info').val() + "' ";
            db.transaction(function (tx) {
                tx.executeSql(queryStr, [], (function (tx, response) {
                    if (response.rows.length == 0 && $('#IDNumber_info').val() != "" && $('#IDNumber_info').val().length == 13 && /^\d+$/.test($('#IDNumber_info').val())) {
                        replaceNull("info");
                        $('#IDNumber').val(sessionStorage.getItem('PersonCID'));
                        $(":mobile-pagecontainer").pagecontainer("change", "#address_info", { transition: "slide" });
                    }
                    else if ($('#IDNumber_info').val() != "" && $('#IDNumber_info').val().length == 13 && response.rows.length != 0 && /^\d+$/.test($('#IDNumber_info').val())) {
                        replaceNull("info");
                        var r = confirm('กดตกลงหากต้องการ "ใช้ข้อมูล" ของเลขบัตร : "' + $('#IDNumber_info').val() + '" ');
                        if (r == true) {
                            if (sessionStorage.getItem('EditType') == 'newFamily') {
                                var personInfo = {
                                    CID: $('#IDNumber_info').val()
                                };
                                insertPerson(personInfo, 'familyExistAddr');
                            }
                            else if (sessionStorage.getItem('EditType') == 'newMate') {
                                var personInfo = {
                                    CID: $('#IDNumber_info').val()
                                };
                                insertPerson(personInfo, 'mateExistAddr');
                            }
                            else {
                                console.log('Not found : Sesion Storage("EditType").');
                            }
                        }
                        else {
                            console.log('ยกเลิก');
                        }
                    }
                    else {
                        alert("เลขบัตรประชาชนไม่ถูกต้อง");
                    }
                }), errorCB)
            }, errorCB, successCB);


        }
        else {
            var queryStr = "SELECT P.PID, PRB.RecordPID FROM Person P LEFT JOIN PersonRecordBy PRB ON PRB.PID = P.PID WHERE P.PID = '" + $('#IDNumber_info').val() + "' ";
            db.transaction(function (tx) {
                tx.executeSql(queryStr, [], (function (tx, response) {
                    if (response.rows.length == 0 && $('#IDNumber_info').val() != "" && $('#IDNumber_info').val().length == 13 && /^\d+$/.test($('#IDNumber_info').val())) {
                        replaceNull("info");
                        console.log("ยังไม่มีข้อมูลคนนี้");
                        //insertPerson(informant, 'informant');
                        //window.location.href = 'person_data.html#address_info';
                        imgH = $('#myImage').height(); imgW = $('#myImage').width();
                        $(":mobile-pagecontainer").pagecontainer("change", "#address_info", { transition: "slide" });
                    }
                    else if ($('#IDNumber_info').val() != "" && $('#IDNumber_info').val().length == 13 && response.rows.length != 0 && response.rows[0].RecordPID != null && /^\d+$/.test($('#IDNumber_info').val())) {
                        console.log(response.rows[0]);
                        alert("ไม่สามารถดำเนินการได้ เนื่องจากมีเลขบัตรประชาชน : '" + $('#IDNumber_info').val() + "' เป็นผู้รับบริการอยู่แล้ว");

                    }
                    else if ($('#IDNumber_info').val() != "" && $('#IDNumber_info').val().length == 13 && response.rows.length != 0 && response.rows[0].RecordPID == null && /^\d+$/.test($('#IDNumber_info').val())) {
                        console.log(response.rows[0]);
                        var r = confirm("ท่านต้องการเพิ่มประชาชน : '" + $('#IDNumber_info').val() + "' เป็นผู้รับบริการใช่หรือไม่ \n***หมายเหตุพบประชาชนนี้อยู่ในระบบแล้ว แต่ไม่ได้เป็นผู้รับบริการ");
                        if (r) {
                            console.log("yes");
                            var rPID = $('#IDNumber').val();
                            if (rPID == "") {
                                rPID = $('#IDNumber_info').val();
                            }
                            else { }
                            console.log(rPID + " / ");
                            db.transaction(function (tx) {
                                tx.executeSql("INSERT INTO PersonRecordBy VALUES ('" + $('#IDNumber_info').val() + "', '" + rPID + "', '" + $('#relation_info').val() + "') ", [], (function (tx, response) {
                                    window.location.replace('home.html');
                                }), errorCB)
                            }, errorCB);
                        }
                        else { console.log("no"); }

                    }
                    else {
                        alert("เลขบัตรประชาชนไม่ถูกต้อง");
                    }
                }), errorCB, successCB);
            });
        }

    });

    $('#info a[title="Clear text"]').click(function () {
        // this function will be executed on click of X (clear button)
        $("#personInfoList").empty();
        $('#IDNumber_info').css('border', '1px solid #ff0000');
    });

    $('#IDNumber_info').on('keyup', function (e) {
        var theEvent = e || window.event;
        var keyPressed = theEvent.keyCode || theEvent.which;
        FormValidation('IDNumber_info');
        db.transaction(function (tx) {
            $("#personInfoList").empty();
            tx.executeSql("SELECT P.PID, PRB.RecordPID FROM Person P LEFT JOIN PersonRecordBy PRB ON PRB.PID = P.PID WHERE P.PID = '" + $('#IDNumber_info').val() + "' ", [], function (tx, results) {
                //alert(results);
                if ($('#IDNumber_info').val() != "" && $('#IDNumber_info').val().length == 13 && results.rows.length != 0 && results.rows[0].RecordPID == null) {
                    for (var i = 0; i < results.rows.length; i++) {
                        $("#personInfoList").append("<li id='" + results.rows[i].PID + "'><a>" + results.rows[i].PID + "</a></li>");
                    }
                    $("#personInfoList").listview('refresh');
                    $('#IDNumber_info').css('border', '1px solid #ffff00');
                }
                else if (results.rows.length != 0) {
                    for (var i = 0; i < results.rows.length; i++) {
                        $("#personInfoList").append("<li id='" + results.rows[i].PID + "'><a>" + results.rows[i].PID + "</a></li>");
                    }
                    $("#personInfoList").listview('refresh');
                    $('#IDNumber_info').css('border', '1px solid #ff0000');
                }
                else { }
            }, errorCB);
        }, errorCB);

    });

    $('#info_edit').click(function () {
        var queryStr = "";

        if (sessionStorage.getItem('EditType') == 'standIn') {

            var queryCheckCID = "SELECT PID FROM Person WHERE PID = '" + $('#IDNumber_info').val() + "' ";
            db.transaction(function (tx) {
                tx.executeSql(queryCheckCID, [], (function (tx, response) {
                    if (response.rows.length == 0 && $('#IDNumber_info').val() != "" && $('#IDNumber_info').val().length == 13 && /^\d+$/.test($('#IDNumber_info').val())) {
                        replaceNull("info");
                        var r = confirm('ไม่พบเลขบัตรประชาชน ต้องการเพิ่มใหม่ หรือไม่ ?');
                        if (r == true) {
                            var personInfo = {
                                CID: $('#IDNumber_info').val(),
                                title: $('#title_info').val(),
                                firstName: $('#firstName_info').val(),
                                lastName: $('#lastName_info').val(),
                                tel: $('#tel_info').val(),


                                passport: $('#passport_info').val(),
                                dob: $('#dob_info').val(),
                                nation: $('#nation_info').val(),
                                race: $('#race_info').val(),
                                religion: $('#religion_info').val(),

                                marriageStatus: $('#marriageStatus_info').val(),
                                addressSatisfy: $('#satisfyStatus').val(),
                                birthProvince: $('#birthProvince_info').val(),


                                recordStaffCID: sessionStorage.getItem('CID'),

                                recorderRelation: $("#relation_info").val(),
                                //-------------------- address

                                AddressID: guidGenerator(),
                                homenumber: 0,
                                addressType: "0000",
                                address: "ไม่ระบุ",
                                tumbon: "00",

                                distint: "00",
                                province: "00",
                                numpost: 0

                            };

                            var personalTypeInfo = {
                                CID: $('#IDNumber_info').val(),
                                personType: $("#personal_sp1_2 :selected").val(),
                                income: 0,
                                outcome: 0,
                                inDept: 0,
                                exDept: 0
                            };

                            insertPerson(personInfo, 'person');
                            insertPersonalType(personalTypeInfo, 'insert');
                            updateTable('UPDATE PersonStandIn SET StandIn = ' + $('#IDNumber_info').val() + ', StandInRole = "' + $("#relation_info").val() + '" '
                            + 'WHERE PID = "' + sessionStorage.getItem('PersonCID') + '" ');

                        } else {
                            console.log('ยกเลิก');
                        }
                        //insertPerson(informant, 'informant');
                        //window.location.href = 'person_data.html#address_info';
                    }
                    else if ($('#IDNumber_info').val() != "" && $('#IDNumber_info').val().length == 13 && response.rows.length != 0 && /^\d+$/.test($('#IDNumber_info').val())) {
                        replaceNull("info");
                        var r = confirm("ต้องการ 'ใช้ข้อมูล' ของบุคคลนี้ข้อมูล ใช่หรือไม่");
                        if (r == true) {
                            updateTable('UPDATE PersonStandIn SET StandIn = ' + $('#IDNumber_info').val() + ', StandInRole = "' + $("#relation_info").val() + '" '
                            + 'WHERE PID = "' + sessionStorage.getItem('PersonCID') + '" ');
                        } else {
                            console.log('ยกเลิก');
                        }
                    }
                    else {
                        alert("เลขบัตรประชาชนไม่ถูกต้อง");
                    }
                }), errorCB, successCB);
            });

        } //-------- End of Edit _Staindin -----------
        else { //---------- Edit Person Zone -----------
            var editPID = "";
            if (sessionStorage.getItem('FamilyCID') !== null && sessionStorage.getItem('EditType') == 'famPerson') {
                editPID = sessionStorage.getItem('FamilyCID');
            }
            else {
                editPID = sessionStorage.getItem('PersonCID');
            }
            replaceNull("info");
            //console.log(editPID);
            queryStr = 'UPDATE Person SET Title = "' + $('#title_info').val() + '", FirstName = "' + $('#firstName_info').val() + '", LastName = "' + $('#lastName_info').val() + '", '
            + 'Phone = "' + $('#tel_info').val() + '", Passport = "' + $('#passport_info').val() + '", DOB = "' + $('#dob_info').val() + '", Nation = "' + $('#nation_info').val() + '", '
            + 'Race = "' + $('#race_info').val() + '", Religion = "' + $('#religion_info').val() + '", MarriageStatus = "' + $('#marriageStatus_info').val() + '", AddressSatisfy = "' + $('#satisfyStatus').val() + '", '
            + 'BirthProvince = "' + $('#birthProvince_info').val() + '" WHERE PID = "' + editPID + '" ';

            console.log(queryStr);
            if ($('#IDNumber_info').val() != "" && $('#IDNumber_info').val().length == 13 && /^\d+$/.test($('#IDNumber_info').val())) {

                var r = confirm("ต้องการ 'แก้ไข' ของบุคคลนี้ข้อมูล ใช่หรือไม่");
                if (r == true) {
                    //var updateStatus = updateTable(queryStr);
                    db.transaction(function (tx) {
                        tx.executeSql(queryStr, null, function () {
                            if (sessionStorage.getItem('EditType') == 'famPerson') {
                                //var updateFamStatus = updateTable('UPDATE PersonFamily SET FamilyRole = "' + $('#relation_info').val() + '" WHERE PID = "' + sessionStorage.getItem('FamilyCID') + '" ');
                                db.transaction(function (tx) {
                                    tx.executeSql('UPDATE PersonFamily SET FamilyRole = "' + $('#relation_info').val() + '" '
                                        + ' WHERE PID = "' + sessionStorage.getItem('FamilyCID') + '" ');
                                }, errorCB, function () {
                                    window.location.href = 'home.html#profileFam';
                                });
                            }
                            else {
                                if (navigator.onLine) {
                                    var uploadImg = $('#myImage').attr('src');
                                    getBase64Img(uploadImg, 1);
                                } else { window.location.href = 'home.html#profile'; }

                            }

                        }, errorCB);
                    }, errorCB);

                    /*
                    , function () {
                        if (sessionStorage.getItem('EditType') == 'famPerson') {
                            //var updateFamStatus = updateTable('UPDATE PersonFamily SET FamilyRole = "' + $('#relation_info').val() + '" WHERE PID = "' + sessionStorage.getItem('FamilyCID') + '" ');

                            db.transaction(function (tx) {
                                tx.executeSql('UPDATE PersonFamily SET FamilyRole = "' + $('#relation_info').val() + '" '
                                    + ' WHERE PID = "' + sessionStorage.getItem('FamilyCID') + '" ');
                            }, errorCB,
                                function () {
                                    window.location.href = 'home.html#profileFam';
                                });
                        }
                        else {
                            window.location.href = 'home.html#profile';
                        }
                    });
                    */

                }
                else {
                    console.log('ยกเลิก');
                }
                //window.location.href = 'person_data.html#address_info';
            }
            else {
                alert("เลขบัตรประชาชนไม่ถูกต้อง");
            }

        }

    });

    $('#selfInfo').click(function () {
        //window.location.href = 'person_data.html#info';
        $(":mobile-pagecontainer").pagecontainer("change", "#info", { transition: "slide" });
    });

    $('#choosePhotoBtn').click(function () {
        //alert(sessionStorage.getItem('imagepath'));
        var options = {
            quality: 50,
            targetWidth: 500,
            targetHeight: 500,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            correctOrientation: true
        };
        navigator.camera.getPicture(onPhotoSuccess, onPhotoFail, options);
    });

    $('#myImage').click(function () {
        var nHeight = $('#myImage').prop('naturalHeight'), nWidth = $('#myImage').prop('naturalWidth');
        alert($('#myImage').attr('src') + "\nWidth: " + nHeight + " px/Height: " + nWidth + " px");
        //if (nHeight < nWidth) {
        //    alert('landscape');
        //    $('#myImage').css('width', '150px'); $('#myImage').css('height', 'auto');
        //}
        //else {
        //    alert('portrait');
        //    $('#myImage').css('width', 'auto'); $('#myImage').css('height', 'auto');
        //}

        //var c = document.getElementById("myCanvas");
        //c.width = $('#myImage').width(); c.height = $('#myImage').height();
        //var ctx = c.getContext("2d");
        //var img = document.getElementById("myImage");        
        //ctx.clearRect(0, 0, c.width, c.height);        
        //ctx.drawImage(img, 0, 0, $('#myImage').width(), $('#myImage').height());
        //console.log(c.width + "x" + c.height);

        //getBase64Img($('#myImage').attr('src'));
    });

    $('#takePhotoBtn').click(function () {
        sessionStorage.removeItem('imagepath');
        //var options = { quality: 50, destinationType: Camera.DestinationType.DATA_URL };
        var options = {
            quality: 50,
            targetWidth: 500,
            targetHeight: 500,
            destinationType: Camera.DestinationType.FILE_URI,
            correctOrientation: true
        };
        //navigator.camera.getPicture(onPhotoSuccess, onPhotoFail, options);
        navigator.camera.getPicture(onPhotoSuccess, onPhotoFail, options);
    });

    function onPhotoSuccess(ImageData) {
        var myImage = document.getElementById('myImage');
        //myImage.style.display = 'block';
        //myImage.src = 'dataimage/jpeg;base64,' + imageData;
        //alert(ImageData);
        //sessionStorage.setItem('pathIMG', ImageData);
        myImage.src = ImageData;

    }

    function onPhotoFail(message) {
        alert('Failed because: ' + message);
    }

    //loadPersonData(sessionStorage.getItem('PersonCID'));

});

//------------------------------------------------

function getBase64Img(ImageData, typeCallback) {
    /*
    var myImage = document.getElementById('myImage');
    myImage.style.display = 'block';
    //myImage.src = 'dataimage/jpeg;base64,' + imageData;
    //alert(ImageData);
    sessionStorage.setItem('pathIMG', ImageData);
    myImage.src = ImageData;
    */
    if ($('#myImage').attr('src') != "img/avatar-placeholder.png") {

        var c = document.getElementById("myCanvas");
        //c.width = $('#myImage').width(); c.height = $('#myImage').height();

        if (typeCallback == 1 && sessionStorage.getItem('PersonCID') !== null) {
            c.width = $('#myImage').width(); c.height = $('#myImage').height();
            //imgW = $('#myImage').width(); imgH = $('#myImage').height();
        } else if (typeCallback == 2) {
            c.width = imgW; c.height = imgH;
        } else { }
        //console.log(c.width + "x" + c.height + " " + typeCallback);
        var ctx = c.getContext("2d");
        var img = document.getElementById("myImage");
        ctx.drawImage(img, 0, 0, c.width, c.height);
        //ctx.drawImage(img, 0, 0, imgW, imgH);
        //ctx.drawImage(img, 0, 0, $('#myImage').width(), $('#myImage').height());
        //console.log(c.width + "x" + c.height);

        //var c = document.getElementById("myCanvas");
        //var ctx = c.getContext("2d");
        //var img = document.getElementById("myImage");
        //ctx.drawImage(img, 0, 0, 150, 150);

        var dataURL = c.toDataURL("image/png");

        var str = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

        var parts = str.match(/[\s\S]{1,500}/g) || [];

        if (typeCallback == 1 && sessionStorage.getItem('PersonCID') !== null) {
            parts.push(sessionStorage.getItem('PersonCID'));
        } else if (typeCallback == 2) {
            parts.push($('#IDNumber_info').val());
        } else { }

        //console.log(parts); console.log(parts.length);
        //$('#base64txt').text(JSON.stringify(parts));

        startAjaxLoader();
        $.ajax({
            type: "POST",
            //url: "http://localhost:1722/Service1.svc/UploadPicture",
            url: "http://seniorservice.azurewebsites.net/Service1.svc/UploadPicture",
            data: JSON.stringify(parts),
            contentType: "application/json; charset=utf-8",
            dateType: 'json',
            success: function (msg) {
                //saveData_callback(msg);
                stopAjaxLoader();
                console.log(msg);
                ctx.clearRect(0, 0, c.width, c.height);

                if (typeCallback == 1) { window.location.href = 'home.html#profile'; }
                else if (typeCallback == 2) { $(":mobile-pagecontainer").pagecontainer("change", "#blank", { transition: "slide" }); }
                else { }

            },
            error: function (request, status, error) {
                stopAjaxLoader();
                alert("Upload fail กรุณาลองใหม่อีกครั้ง\nError: " + error);
                ctx.clearRect(0, 0, c.width, c.height);
            }
        });

        //return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

    }
    else {
        if (typeCallback == 1) { window.location.href = 'home.html#profile'; }
        else if (typeCallback == 2) { $(":mobile-pagecontainer").pagecontainer("change", "#blank", { transition: "slide" }); }
    }

}

//----------- address_info page --------------
var addrLat = 0.0, addrLng = 0.0;
$(document).on("pagebeforecreate", "#address_info", function (event) {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);
    $("#headerAddress").empty();

    if ($('#province_sp1_1').children('option').length < 3) { insertDropdown('address_info'); lvCreate('address_info'); }

    if (sessionStorage.getItem('PersonCID') === null) {
        console.log('new person address');
        document.getElementById("navbarEditAddr").style.display = "none";
        $('#headerAddress').append('เพิ่มข้อมูลบุคคลใหม่');
        if (navigator.onLine) { initMapInfo(0.0, 0.0); } else { }
    }
    else if (sessionStorage.getItem('PersonCID') !== null && (sessionStorage.getItem('EditType') == 'newFamily' || sessionStorage.getItem('EditType') == 'newMate')) {
        console.log(sessionStorage.getItem('EditType'));
        document.getElementById("navbarEditAddr").style.display = "none";
        if (sessionStorage.getItem('EditType') == 'newFamily') {
            $('#headerAddress').append('เพิ่มครอบครัว');
        }
        else if (sessionStorage.getItem('EditType') == 'newMate') {
            $('#headerAddress').append('เพิ่มคู่สมรส');
        } else { }

    }
    else {
        console.log('edit person address');
        document.getElementById("navbarAddr").style.display = "none";
        document.getElementById("addrCbDIV").style.display = "none";
        $('#headerAddress').append('แก้ไขข้อมูลที่อยู่');

        var editPID;
        if (sessionStorage.getItem('FamilyCID') !== null && sessionStorage.getItem('EditType') == 'famAddress') {
            editPID = sessionStorage.getItem('FamilyCID');
            console.log("fam : " + editPID);
        }
        else {
            editPID = sessionStorage.getItem('PersonCID');
            console.log("person : " + editPID);
        }

        loadPersonAddress(editPID);
    }

});

$(document).on("pagecreate", "#address_info", function (event) {

    $("#addressCB").change(function () {
        if (this.checked) {
            //window.location.href = 'person_data.html#person_type';        
            $(":mobile-pagecontainer").pagecontainer("change", "#person_type", { transition: "slide" });
        }
    });

    $('#addr_next').click(function () {
        replaceNull("address_info");
        $(":mobile-pagecontainer").pagecontainer("change", "#person_type", { transition: "slide" });
    });

    $('#addr_edit').click(function () {
        replaceNull("address_info");
        var editPID;
        if (sessionStorage.getItem('FamilyCID') !== null && sessionStorage.getItem('EditType') == 'famAddress') {
            editPID = sessionStorage.getItem('FamilyCID');
            console.log("fam : " + editPID);
        }
        else {
            editPID = sessionStorage.getItem('PersonCID');
            console.log("person : " + editPID);
        }
        //"AddressID, LiveHomeStatusID, HouseNumber, MooNumber, VillageName, Alley, StreetName, Tumbon, Amphor, Province, Postcode"
        db.transaction(function (tx) { //หา AddressID ของ Person
            tx.executeSql('SELECT A.* FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = "' + editPID + '" ', [],
                (function (tx, response) {

                    var queryStr = 'UPDATE Address SET '
                        + 'LiveHomeStatusID = "' + $('#addressType_sp1_1').val() + '", HouseNumber= "' + $('#HouseNumber_sp1_1').val() + '", MooNumber= "' + $('#MooNumber_sp1_1').val() + '", '
                        + 'VillageName= "' + $('#VillageName_sp1_1').val() + '", Alley= "' + $('#Alley_sp1_1').val() + '", StreetName= "' + $('#Street_sp1_1').val() + '", '
                        + 'Tumbon= "' + $('#tumbon_sp1_1').val() + '", Amphor= "' + $('#distint_sp1_1').val() + '", Province= "' + $('#province_sp1_1').val() + '", '
                        + 'Postcode= "' + $('#numpost_sp1_1').val() + '", HomeCode = "' + $('#homeNumber_sp1_1').val() + '" ';
                    if (addrLat != 0.0 && addrLng != 0.0) {
                        queryStr += ',addrLat = ' + addrLat + ', addrLng = ' + addrLng + ' ';
                    } else { }
                    queryStr += 'WHERE AddressID = "' + response.rows.item(0).AddressID + '" ';

                    console.log(queryStr);

                    var r = confirm("ต้องการอัพเดทข้อมูลใช่ หรือไม่?");
                    if (r == true) {
                        //updateTable(queryStr);
                        db.transaction(function (tx) { tx.executeSql(queryStr); }, errorCB, function () {
                            if (sessionStorage.getItem('FamilyCID') !== null && sessionStorage.getItem('EditType') == 'famAddress') {
                                window.location.href = 'home.html#profileFam';
                            }
                            else {
                                window.location.href = 'home.html#profile';
                            }
                        });

                    } else {
                        console.log('ยกเลิก');
                    }

                }), errorCB);
        });

    });

    document.getElementById('province_sp1_1').onchange = function () {
        var txtProvinceID = $(this).val();
        $('#distint_sp1_1').empty();
        $('#distint_sp1_1').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#distint_sp1_1').selectmenu("refresh");
        $('#tumbon_sp1_1').empty();
        $('#tumbon_sp1_1').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#tumbon_sp1_1').selectmenu("refresh");
        insertCity(txtProvinceID, 'distint_sp1_1');
    };

    document.getElementById('distint_sp1_1').onchange = function () {
        var txtAmphorID = $(this).val();
        $('#tumbon_sp1_1').empty();
        $('#tumbon_sp1_1').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#tumbon_sp1_1').selectmenu("refresh");
        insertTumbon(txtAmphorID, 'tumbon_sp1_1');
    };

    document.getElementById('tumbon_sp1_1').onchange = function () {
        if (navigator.onLine) {
            db.transaction(function (tx) {
                tx.executeSql("SELECT * FROM Tumbon WHERE TumbonID = '" + $('#tumbon_sp1_1').val() + "'", [], function (tx, rs) {
                    if (rs.rows.length != 0) {
                        //console.log(map);
                        if (map != null) {
                            map.setCenter({ lat: rs.rows[0].Latitude, lng: rs.rows[0].Longtitude });

                            if (myMarker.length != 0) {
                                myMarker[0].setMap(null);
                            }
                            myMarker = [];
                            var marker = new google.maps.Marker({
                                position: new google.maps.LatLng(rs.rows[0].Latitude, rs.rows[0].Longtitude)
                            });
                            marker.addListener('click', function () {
                                this.setMap(null);
                                addrLat = 0.0; addrLng = 0.0;
                            });
                            addrLat = rs.rows[0].Latitude; addrLng = rs.rows[0].Longtitude;
                            myMarker.push(marker);
                            myMarker[0].setMap(map);
                        }

                    } else { }
                }, errorCB);
            }, errorCB);
        } else { }
    }

    if ($('#IDNumber').val() != "" && $('#IDNumber').val() != "-") {
        $('#addressCB').checkboxradio('enable');
        //console.log('enable');
    } else {
        $('#addressCB').checkboxradio('disable');
        //console.log('disable');
    }

    $('#getCurLocation').click(function () {
        if (navigator.onLine) {
            navigator.geolocation.getCurrentPosition(
                function (pos) {
                    console.log('lat: ' + pos.coords.latitude + '\n' + 'lng: ' + pos.coords.longitude);
                    map.setCenter({ lat: pos.coords.latitude, lng: pos.coords.longitude });

                    if (myMarker.length != 0) {
                        myMarker[0].setMap(null);
                    }
                    myMarker = [];
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude)
                    });
                    marker.addListener('click', function () {
                        this.setMap(null);
                        addrLat = 0.0; addrLng = 0.0;
                    });
                    addrLat = pos.coords.latitude; addrLng = pos.coords.longitude;
                    myMarker.push(marker);
                    myMarker[0].setMap(map);

                },
                function (err) {
                    alert('code: ' + err.code + '\n' + 'message: ' + err.message);
                });
        }
        else { alert("กรุณาเชื่อมต่อ internet"); }
    });

});

var map; var myMarker = [];
function initMapInfo(aLat, aLng) {
    console.log("initMap");
    $('.myMaps').css("display", "block");

    var isEdit = false;

    if (aLat == 0.0 && aLng == 0.0) {
        isEdit = false;
        aLat = 13.76486874;
        aLng = 100.5382767; //bkk
    } else { isEdit = true; }

    document.getElementById('mapAddressInfo').innerHTML = "";
    map = new google.maps.Map(document.getElementById('mapAddressInfo'), {
        center: new google.maps.LatLng(aLat, aLng), //{ lat: -34.397, lng: 150.644 },
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.HYBRID
    });

    // Create the search box and link it to the UI element.
    var input = document.getElementById('addrInfo');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

        // Clear out the old markers.
        markers.forEach(function (marker) {
            marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
                map: map,
                icon: icon,
                title: place.name,
                position: place.geometry.location
            }));

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
    });

    google.maps.event.addListener(map, 'click', function (event) {
        if (myMarker.length != 0) {
            myMarker[0].setMap(null);
        }
        myMarker = [];
        var marker = new google.maps.Marker({
            position: event.latLng,
            //draggable: true
            //animation: google.maps.Animation.DROP
        });
        marker.addListener('click', function () {
            this.setMap(null);
            addrLat = 0.0; addrLng = 0.0;
        });

        addrLat = event.latLng.lat(); addrLng = event.latLng.lng();
        //console.log(addrLat + " " + addrLng);
        myMarker.push(marker);
        myMarker[0].setMap(map);
        //map.setCenter(event.latLng);
    });

    //console.log(isEdit);
    if (isEdit) {
        var aMarker = new google.maps.Marker({
            position: new google.maps.LatLng(aLat, aLng),
            icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
        });
        aMarker.addListener('click', function () {
            this.setMap(null);
            addrLat = 0.0; addrLng = 0.0;
        });
        aMarker.setMap(map);
    } else { }

}

//---------------------------------------------

//----------- person_type page --------------

$(document).on("pagebeforecreate", "#person_type", function (event) {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);
    $('#headerType').empty();

    if ($('#personal_sp1_2').children('option').length < 3) { insertDropdown('person_type'); lvCreate('person_type'); }

    if (sessionStorage.getItem('PersonCID') === null) {
        console.log('new person type');
        $('#jobDIV').hide(); $('#studentDIV').hide(); $('#academyDIV').hide();
        document.getElementById("navbarEditType").style.display = "none";
        $('#headerType').append('เพิ่มข้อมูลบุคคลใหม่');
    }
    else if (sessionStorage.getItem('PersonCID') !== null && (sessionStorage.getItem('EditType') == 'newFamily' || sessionStorage.getItem('EditType') == 'newMate')) {
        console.log(sessionStorage.getItem('EditType'));
        $('#jobDIV').hide(); $('#studentDIV').hide(); $('#academyDIV').hide();
        document.getElementById("navbarEditType").style.display = "none";
        if (sessionStorage.getItem('EditType') == 'newFamily') {
            $('#headerAddress').append('เพิ่มครอบครัว');
        }
        else if (sessionStorage.getItem('EditType') == 'newMate') {
            $('#headerAddress').append('เพิ่มคู่สมรส');
        } else { }

    }
    else {
        console.log('edit person type');
        $('#jobDIV').show(); $('#studentDIV').hide(); $('#academyDIV').show();
        document.getElementById("navbarType").style.display = "none";
        $('#headerType').append('แก้ไขข้อมูลอาชีพ');

        var editPID;
        if (sessionStorage.getItem('FamilyCID') !== null && sessionStorage.getItem('EditType') == 'famPersonalType') {
            editPID = sessionStorage.getItem('FamilyCID');
            console.log("fam : " + editPID);
        }
        else {
            editPID = sessionStorage.getItem('PersonCID');
            console.log("person : " + editPID);
        }

        var queryStr = "SELECT OccupationID FROM PersonOccupation WHERE PID = '" + editPID + "' ";
        db.transaction(function (tx) {
            tx.executeSql(queryStr, [], (function (tx, response) {
                if (response.rows.length != 0) {
                    $('#personal_sp1_2').val(response.rows[0].OccupationID);
                    $('#person_type select').selectmenu('refresh');
                    if (response.rows[0].OccupationID == 7) {
                        startAjaxLoader();
                        loadPersonType(editPID, 7);
                        loadPersonAddress(sessionStorage.getItem('PersonCID')); //this has some error on refresh listview
                        $('#jobDIV').hide(); $('#studentDIV').show(); $('#academyDIV').show();
                    } else {
                        startAjaxLoader();
                        loadPersonType(editPID, 0);
                        $('#academyHead').empty(); $('#academyHead').append("7.จบการศึกษา");
                    }
                }

            }), errorCB, successCB);
        });

    }

});

$(document).on("pagecreate", "#person_type", function (event) {

    //$("#headerType").empty();
    $('#person_type a[title="Clear text"]').click(function () {
        // this function will be executed on click of X (clear button)
        $("#parentList").empty();
        $('#informantDIV_sp1_2 :input').val("");
        $('#informantDIV_sp1_2 select').val("0000").selectmenu('refresh');
        $('#idParent').css('border', '1px solid #ff0000');
    });

    $('#idParent').on('keyup', function (e) {
        var theEvent = e || window.event;
        var keyPressed = theEvent.keyCode || theEvent.which;
        FormValidation('idParent');

        if ($('#idParent').val() == "") {
            $("#parentList").empty();
            $('#informantDIV_sp1_2 :input').val("");
            $('#informantDIV_sp1_2 select').val("0000").selectmenu('refresh');
        }
        else {
            //var query = "SELECT * FROM Person WHERE (PID LIKE '%" + $('#searchData').val() + "%' OR FirstName LIKE '%" + $('#searchData').val() + "%' OR LastName LIKE '%" + $('#searchData').val() + "%') LIMIT 10";
            //if (keyPressed == 13) {
            db.transaction(function (tx) {
                $("#parentList").empty();
                tx.executeSql("SELECT PID FROM Person WHERE PID LIKE '%" + $('#idParent').val() + "%' LIMIT 5", [],
                    (function (tx, results) {
                        //alert(results);
                        for (var i = 0; i < results.rows.length; i++) {
                            $("#parentList").append("<li id='" + results.rows[i].PID + "'><a>" + results.rows[i].PID + "</a></li>");
                        }
                        $("#parentList").listview('refresh');
                    }), errorCB);
            }, errorCB);
        }
    });

    $('#parentList').on('click', 'li', function () {
        //$(this).attr('id');
        $('#idParent').val($(this).attr('id')); $('#idParent').css('border', '1px solid #00ff00');
        var informantCID = $(this).attr('id');
        var queryGetInformant = "SELECT P.*, A.* FROM Person P"
            + " INNER JOIN PersonVSAddress PVA ON P.PID = PVA.PID"
            + " INNER JOIN Address A ON PVA.AddressID = A.AddressID"
            + " WHERE P.PID = '" + informantCID + "' ";
        db.transaction(function (tx) {
            tx.executeSql(queryGetInformant, [], (function (tx, results) {
                if (results.rows.length != 0) {

                    $('#titleParent').val(results.rows[0].Title);
                    $('#nameParent').val(results.rows[0].FirstName);
                    $('#surnameParent').val(results.rows[0].LastName);
                    $('#telPr').val(results.rows[0].Phone);

                    $('#HouseNumberPr').val(results.rows[0].HouseNumber);
                    $('#MooNumberPr').val(results.rows[0].MooNumber);
                    $('#VillageNamePr').val(results.rows[0].VillageName);
                    $('#AlleyPr').val(results.rows[0].Alley);
                    $('#StreetPr').val(results.rows[0].StreetName);
                    $('#provincePr').val(results.rows[0].Province);
                    autoLoadCityAndTumbon('person_type', 'tumbonPr', 'distinctPr', results.rows[0].Tumbon, results.rows[0].Amphor, results.rows[0].Province);
                    $('#numpostPr').val(results.rows[0].Postcode);
                }
                $('#person_type select').selectmenu('refresh');
            }), errorCB);
        }, errorCB);
        $("#parentList").empty();
        $("#useInfoAddrCB").prop("checked", false);

    });

    $("#useInfoAddrCB").change(function () {
        if (this.checked) {
            $('#HouseNumberPr').val($('#HouseNumber_sp1_1').val());
            $('#MooNumberPr').val($('#MooNumber_sp1_1').val());
            $('#VillageNamePr').val($('#VillageName_sp1_1').val());
            $('#AlleyPr').val($('#Alley_sp1_1').val());
            $('#StreetPr').val($('#Street_sp1_1').val());
            $('#provincePr').val($('#province_sp1_1').val());
            autoLoadCityAndTumbon('addrParentDIV', 'tumbonPr', 'distinctPr', $('#tumbon_sp1_1').val(), $('#distint_sp1_1').val(), $('#province_sp1_1').val());
            $('#numpostPr').val($('#numpost_sp1_1').val()); numpost_sp1_1
            //$('#addrParentDIV select').selectmenu('refresh');
        }
        else {

            db.transaction(function (tx) { //หา AddressID ของ Person
                tx.executeSql('SELECT A.* FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = "' + $('#idParent').val() + '" ', [],
                    (function (tx, response) {
                        if (response.rows.length != 0) {
                            $('#HouseNumberPr').val(response.rows[0].HouseNumber);
                            $('#MooNumberPr').val(response.rows[0].MooNumber);
                            $('#VillageNamePr').val(response.rows[0].VillageName);

                            $('#AlleyPr').val(response.rows[0].Alley);
                            $('#StreetPr').val(response.rows[0].StreetName);
                            $('#provincePr').val(response.rows[0].Province);
                            autoLoadCityAndTumbon('addrParentDIV', 'tumbonPr', 'distinctPr', response.rows[0].Tumbon, response.rows[0].Amphor, response.rows[0].Province);
                            $('#numpostPr').val(response.rows[0].Postcode);

                        }
                        else {
                            $('#addrParentDIV :input').val("");
                            $('#addrParentDIV select').selectmenu('refresh');
                        }
                        //$('#address_info select').selectmenu('refresh');

                    }), errorCB);
            });

        }

    });

    $('#person_type_next').click(function () {
        if ($('#IDNumber').val() == "" || $('#IDNumber').val() == "-") {
            $('#IDNumber').val($('#IDNumber_info').val());
        }

        var areYouSure = confirm("ต้องการบันทึกข้อมูลใช่หรือไม่");
        if (areYouSure == true) {
            replaceNull("person_type");

            var personInfo = {
                CID: $('#IDNumber_info').val(),
                title: $('#title_info').val(),
                firstName: $('#firstName_info').val(),
                lastName: $('#lastName_info').val(),
                tel: $('#tel_info').val(),

                passport: $('#passport_info').val(),
                dob: $('#dob_info').val(),
                nation: $('#nation_info').val(),
                race: $('#race_info').val(),
                religion: $('#religion_info').val(),

                marriageStatus: $('#marriageStatus_info').val(),
                addressSatisfy: $('#satisfyStatus').val(),
                birthProvince: $('#birthProvince_info').val(),


                recordStaffCID: sessionStorage.getItem('CID'),

                recorderRelation: $("#relation_info").val(),
                //-------------------- address

                AddressID: guidGenerator(),
                homenumber: $('#homeNumber_sp1_1').val(),
                addressType: $('#addressType_sp1_1').val(),
                //address: $('#address_sp1_1').val(),
                houseNumber: $('#HouseNumber_sp1_1').val(),
                mooNumber: $('#MooNumber_sp1_1').val(),
                villageName: $('#VillageName_sp1_1').val(),
                alley: $('#Alley_sp1_1').val(),
                street: $('#Street_sp1_1').val(),
                tumbon: $('#tumbon_sp1_1').val(),

                distint: $('#distint_sp1_1').val(),
                province: $('#province_sp1_1').val(),
                numpost: $('#numpost_sp1_1').val()

            };

            var personalTypeInfo = {
                CID: $('#IDNumber_info').val(),

                eduStatus: "",
                eduLevel: $('#graduateLevel').val(),
                academy: $('#academy').val(),
                academyProvince: $('#province_academy').val(),
                academyDistinct: $('#distint_academy').val(),
                academyTumbon: $('#tumbon_academy').val(),
                eduYear: $('#endyear').val(),

                personType: $("#personal_sp1_2 :selected").val(),
                position: $('#position_sp1_2').val(),
                office: $('#office_sp1_2').val(),
                officeProvince: $('#province_sp1_2').val(),
                officeDistinct: $('#distint_sp1_2').val(),
                officeTumbon: $('#tumbon_sp1_2').val(),
                officeNumpost: $('#numpost_sp1_2').val(),
                officeTel: $('#tel_sp1_2').val(),
                officeFax: $('#fax_sp1_2').val(),

                income: $('#income5').val(),
                outcome: $('#expense5').val(),
                inDept: $('#indept').val(),
                exDept: $('#exdept').val(),

                bankName: $('#account').val(),
                bankBranch: $('#branch').val(),
                accountNum: $('#accountNum').val(),
                accountName: $('#accountName').val()
            };

            //---- กรณี นักเรียน / นักศึกษา ----
            if ($("#personal_sp1_2").val() == '7') {
                //replaceNull("person_type");

                //console.log('Student');
                personalTypeInfo.eduStatus = "001";
                if ($('#idParent').val() != "" && $('#idParent').val().length == 13 && ($('#idParent').val() != $('#IDNumber_info').val()) && $('#idParent').val() != "-" && /^\d+$/.test($('#idParent').val())) {

                    db.transaction(function (tx) {
                        tx.executeSql("SELECT PID FROM Person WHERE PID = '" + $('#idParent').val() + "'", [], function (tx, rs) {

                            //------ เพิ่มข้อมูลคนใหม่ ------
                            if (rs.rows.length == 0) {

                                console.log('เพิ่มผู้ปกครองคนใหม่')
                                var ParentInfo = {
                                    CID: $('#idParent').val(),
                                    Title: $('#titleParent').val(),
                                    FName: $('#nameParent').val(),
                                    LName: $('#surnameParent').val(),
                                    Phone: $('#telPr').val(),

                                    AddressID: guidGenerator(),
                                    AddressType: "9999",
                                    HouseNumber: $('#HouseNumberPr').val(),
                                    MooNumber: $('#MooNumberPr').val(),
                                    VillageName: $('#VillageNamePr').val(),
                                    Alley: $('#AlleyPr').val(),
                                    Street: $('#StreetPr').val(),
                                    Province: $('#provincePr').val(),
                                    Amphor: $('#distinctPr').val(),
                                    Tumbon: $('#tumbonPr').val(),
                                    Postcode: $('#numpostPr').val(),

                                    Relation: $('#relatedPr').val(),
                                    recordStaffCID: sessionStorage.getItem('CID')

                                };

                                var selectPersonQuery = "";

                                if (document.getElementById('addressCB').checked) {
                                    //alert("checked");

                                    if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'newFamily') {
                                        console.log('create new family exist address');
                                        selectPersonQuery = 'familyExistAddr';
                                    }
                                    else if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'newMate') {
                                        console.log('create new mate exist address');
                                        selectPersonQuery = 'mateExistAddr';
                                    }
                                    else {
                                        console.log('create new person exist address');
                                        selectPersonQuery = 'personExistAddr';
                                    }

                                    db.transaction(function (tx) { //หา AddressID ของ Person
                                        tx.executeSql('SELECT A.AddressID FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = "' + $('#IDNumber').val() + '" ', [],
                                            (function (tx, response) {

                                                if (document.getElementById('useInfoAddrCB').checked) {
                                                    ParentInfo.AddressID = personInfo.AddressID;
                                                    console.log(ParentInfo.AddressID + " " + personInfo.AddressID);
                                                    insertPerson(ParentInfo, 'parentExistAddr');
                                                }
                                                else {
                                                    insertPerson(ParentInfo, 'parent');
                                                }

                                                personInfo.AddressID = response.rows.item(0).AddressID;
                                                insertPerson(personInfo, selectPersonQuery);
                                                insertPersonalType(personalTypeInfo, 'insert');


                                            }), errorCB);
                                    });

                                }
                                else {
                                    //alert("You didn't check it! Let me check it for you.");
                                    if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'newFamily') {
                                        console.log('create new family new address');
                                        selectPersonQuery = 'family';
                                    }
                                    else if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'newMate') {
                                        console.log('create new mate new address');
                                        selectPersonQuery = 'mate';
                                    }
                                    else {
                                        console.log('create new person new address');
                                        selectPersonQuery = 'person';
                                    }

                                    if (document.getElementById('useInfoAddrCB').checked) {
                                        ParentInfo.AddressID = personInfo.AddressID;
                                        console.log(ParentInfo.AddressID + " " + personInfo.AddressID);
                                        insertPerson(ParentInfo, 'parentExistAddr');
                                    }
                                    else {
                                        insertPerson(ParentInfo, 'parent');
                                    }

                                    insertPerson(personInfo, selectPersonQuery);
                                    insertPersonalType(personalTypeInfo, 'insert');


                                }

                            }
                                //------ ใช้ข้อมูลผู้ปกครองคนเดิม ------
                            else {
                                var r = confirm("ผู้ปกครองดังกล่าวมีข้อมูลอยู่แล้ว กด 'ตกลง' ถ้าต้องการใช้ข้อมูล");
                                if (r == true) {
                                    if (document.getElementById('addressCB').checked) {

                                        if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'newFamily') {
                                            console.log('create new family exist address');
                                            selectPersonQuery = 'familyExistAddr';
                                        }
                                        else if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'newMate') {
                                            console.log('create new mate exist address');
                                            selectPersonQuery = 'mateExistAddr';
                                        }
                                        else {
                                            console.log('create new person exist address');
                                            selectPersonQuery = 'personExistAddr';
                                        }

                                        //alert("checked");
                                        db.transaction(function (tx) { //หา AddressID ของ Person
                                            tx.executeSql('SELECT A.AddressID FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = "' + $('#IDNumber').val() + '" ', [],
                                                (function (tx, response) {
                                                    personInfo.AddressID = response.rows.item(0).AddressID;
                                                    insertPerson(personInfo, selectPersonQuery);
                                                }), errorCB);
                                        });
                                    }
                                    else {
                                        //alert("You didn't check it! Let me check it for you.");

                                        if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'newFamily') {
                                            console.log('create new family new address');
                                            selectPersonQuery = 'family';
                                        }
                                        else if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'newMate') {
                                            console.log('create new mate new address');
                                            selectPersonQuery = 'mate';
                                        }
                                        else {
                                            console.log('create new person new address');
                                            selectPersonQuery = 'person';
                                        }

                                        insertPerson(personInfo, selectPersonQuery);
                                    }

                                    db.transaction(function (tx) {
                                        tx.executeSql('INSERT INTO PersonParent ( PID, ParentPID, ParentRole )'
                                            + ' VALUES ( "' + $('#IDNumber_info').val() + '", "' + $('#idParent').val() + '", "' + $('#relatedPr').val() + '" )');
                                    }, errorCB, function () {
                                    });

                                    insertPersonalType(personalTypeInfo, 'insert');
                                }
                                else {
                                    console.log('ยกเลิก');
                                }
                            }
                        }, errorCB);
                    }, errorCB, successCB);

                }
                else {
                    alert('เลขบัตรประชาชนผู้ปกครองไม่ถูกต้อง');
                }

            }
                //------ กรณีไม่่ใช่นักเรียน --------
            else if ($("#personal_sp1_2 :selected").val() == '0000') {
                //console.log('Personal = ' + txtPersonal);                
                alert("กรุณาเลือกอาชีพ");
            }
            else {
                //replaceNull("person_type");
                var selectPersonQuery = "";

                if (document.getElementById('addressCB').checked) {
                    //alert("checked");

                    if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'newFamily') {
                        console.log('create new family exist address');
                        selectPersonQuery = 'familyExistAddr';
                    }
                    else if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'newMate') {
                        console.log('create new mate exist address');
                        selectPersonQuery = 'mateExistAddr';
                    }
                    else {
                        console.log('create new person exist address');
                        selectPersonQuery = 'personExistAddr';
                    }

                    db.transaction(function (tx) { //หา AddressID ของ Person
                        tx.executeSql('SELECT A.AddressID FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = "' + $('#IDNumber').val() + '" ', [],
                            (function (tx, response) {
                                personInfo.AddressID = response.rows.item(0).AddressID;
                                insertPerson(personInfo, selectPersonQuery);
                            }), errorCB);
                    });
                } else {
                    //alert("You didn't check it! Let me check it for you.");

                    if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'newFamily') {
                        console.log('create new family new address');
                        selectPersonQuery = 'family';
                    }
                    else if (sessionStorage.getItem('PersonCID') !== null && sessionStorage.getItem('EditType') == 'newMate') {
                        console.log('create new mate new address');
                        selectPersonQuery = 'mate';
                    }
                    else {
                        console.log('create new person new address');
                        selectPersonQuery = 'person';
                    }


                    insertPerson(personInfo, selectPersonQuery);
                }
                //console.log('Personal = ' + txtPersonal);
                personalTypeInfo.eduStatus = "002";
                insertPersonalType(personalTypeInfo, 'insert');
            }
        }
    });

    $('#type_edit').click(function () {

        replaceNull("person_type");

        var editPID;
        if (sessionStorage.getItem('FamilyCID') !== null && sessionStorage.getItem('EditType') == 'famPersonalType') {
            editPID = sessionStorage.getItem('FamilyCID');
            console.log("fam : " + editPID);
        }
        else {
            editPID = sessionStorage.getItem('PersonCID');
            console.log("person : " + editPID);
        }

        if ($('#personal_sp1_2').val() != 0000) {
            var p = {
                CID: editPID
            };
            insertPersonalType(p, "insert");
            //var EduStat;
            //PID, OccupationID, Position, Office, Tumbon, Amphor, Province, Postcode, Phone, Fax
            updateOccupation = 'UPDATE PersonOccupation SET '
                + 'OccupationID = "' + $('#personal_sp1_2').val() + '", Position = "' + $('#position_sp1_2').val() + '", '
                + 'Office = "' + $('#office_sp1_2').val() + '", Tumbon = "' + $('#tumbon_sp1_2').val() + '", '
                + 'Amphor = "' + $('#distint_sp1_2').val() + '", Province = "' + $('#province_sp1_2').val() + '", '
                + 'Postcode = "' + $('#numpost_sp1_2').val() + '", Phone = "' + $('#tel_sp1_2').val() + '", '
                + 'Fax = "' + $('#fax_sp1_2').val() + '" '
                + 'WHERE PID = "' + editPID + '" ';



            updateFinace = 'UPDATE PersonFinance SET '
                + 'Income = ' + $('#income5').val() + ', Outgoing = ' + $('#expense5').val() + ', '
                + 'InDebt = ' + $('#indept').val() + ', ExDebt = ' + $('#exdept').val() + ' '
                + 'WHERE PID = "' + editPID + '" ';

            updateBank = 'UPDATE PersonBankAccount SET '
                + 'BankName = "' + $('#account').val() + '", BankBranch = "' + $('#branch').val() + '", '
                + 'AccountNo = "' + $('#accountNum').val() + '", AccountName = "' + $('#accountName').val() + '" '
                + 'WHERE PID = "' + editPID + '" ';

            if ($('#personal_sp1_2').val() != 7) {

                //PID, EduStatus, EduLevel, EduYear, Academy, Tumbon, Amphor, Province
                updateEducation = 'UPDATE PersonEducation SET '
                    + 'EduStatus = "002", '
                    + 'EduLevel = "' + $('#graduateLevel').val() + '", EduYear = "' + $('#endyear').val() + '", '
                    + 'Academy = "' + $('#academy').val() + '", Tumbon = "' + $('#tumbon_academy').val() + '", '
                    + 'Amphor = "' + $('#distint_academy').val() + '", Province = "' + $('#province_academy').val() + '" '
                    + 'WHERE PID = "' + editPID + '" ';

                db.transaction(function (tx) {
                    tx.executeSql(updateOccupation);
                    tx.executeSql(updateEducation);
                    tx.executeSql(updateFinace);
                    tx.executeSql(updateBank);
                }, errorCB, function () {

                    if (sessionStorage.getItem('FamilyCID') !== null && sessionStorage.getItem('EditType') == 'famPersonalType') {
                        window.location.href = 'home.html#profileFam';
                    }
                    else {
                        window.location.href = 'home.html#profile';
                    }
                })
            }
            else {

                //PID, EduStatus, EduLevel, EduYear, Academy, Tumbon, Amphor, Province
                updateEducation = 'UPDATE PersonEducation SET '
                    + 'EduStatus = "001", '
                    + 'EduLevel = "' + $('#graduateLevel').val() + '", EduYear = "' + $('#endyear').val() + '", '
                    + 'Academy = "' + $('#academy').val() + '", Tumbon = "' + $('#tumbon_academy').val() + '", '
                    + 'Amphor = "' + $('#distint_academy').val() + '", Province = "' + $('#province_academy').val() + '" '
                    + 'WHERE PID = "' + editPID + '" ';

                if (($('#idParent').val() != "" && $('#idParent').val() != "-") && $('#idParent').val().length == 13 && $('#idParent').val() != editPID && /^\d+$/.test($('#idParent').val())) {

                    var ParentInfo = {
                        CID: $('#idParent').val(),
                        Title: $('#titleParent').val(),
                        FName: $('#nameParent').val(),
                        LName: $('#surnameParent').val(),
                        Phone: $('#telPr').val(),

                        AddressID: guidGenerator(),
                        AddressType: "9999",
                        HouseNumber: $('#HouseNumberPr').val(),
                        MooNumber: $('#MooNumberPr').val(),
                        VillageName: $('#VillageNamePr').val(),
                        Alley: $('#AlleyPr').val(),
                        Street: $('#StreetPr').val(),
                        Province: $('#provincePr').val(),
                        Amphor: $('#distinctPr').val(),
                        Tumbon: $('#tumbonPr').val(),
                        Postcode: $('#numpostPr').val(),

                        Relation: $('#relatedPr').val(),
                        recordStaffCID: sessionStorage.getItem('CID')

                    };

                    var queryParent = 'INSERT INTO Person ( PID, Title, FirstName, LastName, Phone, RecordStaffPID, DateTimeUpdate, Passport, DOB, Nation, Race, Religion, MarriageStatus, AddressSatisfy, BirthProvince )'
                    + ' VALUES ( "' + ParentInfo.CID + '", "' + ParentInfo.Title + '", "' + ParentInfo.FName + '", "' + ParentInfo.LName + '", "' + ParentInfo.Phone + '", "' + ParentInfo.recordStaffCID + '", "-", "1950-01-01", "0", "0", "0", "0", "0", "0", "0" )';

                    var queryParentAddr = 'INSERT INTO Address ( AddressID, LiveHomeStatusID, HouseNumber, MooNumber, VillageName, Alley, StreetName, Tumbon, Amphor, Province, Postcode, HomeCode )'
                        + ' VALUES ( "' + ParentInfo.AddressID + '", "' + ParentInfo.AddressType + '", "' + ParentInfo.HouseNumber + '", "' + ParentInfo.MooNumber + '", "' + ParentInfo.VillageName + '",'
                        + ' "' + ParentInfo.Alley + '", "' + ParentInfo.Street + '", "' + ParentInfo.Tumbon + '", "' + ParentInfo.Amphor + '", "' + ParentInfo.Province + '", "' + ParentInfo.Postcode + '", "-" )';

                    db.transaction(function (tx) {

                        tx.executeSql("SELECT PID FROM Person WHERE PID = '" + $('#idParent').val() + "'", [], function (tx, rs) {
                            if (rs.rows.length == 0) {
                                var r = confirm("ต้องการเพิ่มข้อมูลของผู้ปกครองใหม่หรือไม่");
                                if (r == true) {
                                    console.log('เพิ่มผู้ปกครองคนใหม่')

                                    if (document.getElementById('useInfoAddrCB').checked) {
                                        tx.executeSql('SELECT A.AddressID FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = "' + editPID + '" ', [],
                                            (function (tx, response) {
                                                //var studentAddrID = response.rows.item(0).AddressID;
                                                tx.executeSql(queryParent, null, function () {
                                                    tx.executeSql('UPDATE PersonParent SET ParentPID = "' + $('#idParent').val() + '", ParentRole = "' + $('#relatedPr').val() + '" WHERE PID = "' + editPID + '" ', null, function () {
                                                        tx.executeSql('INSERT INTO PersonVSAddress VALUES ( "' + $('#idParent').val() + '", "' + response.rows.item(0).AddressID + '" )', null, function () {
                                                            if (sessionStorage.getItem('FamilyCID') !== null && sessionStorage.getItem('EditType') == 'famPersonalType') {
                                                                window.location.href = 'home.html#profileFam';
                                                            }
                                                            else {
                                                                window.location.href = 'home.html#profile';
                                                            }
                                                        }, errorCB);
                                                    }, errorCB);
                                                }, errorCB);
                                            }), errorCB);
                                    }
                                    else {
                                        tx.executeSql(queryParent, null, function () {
                                            tx.executeSql(queryParentAddr, null, function () {
                                                tx.executeSql('UPDATE PersonParent SET ParentPID = "' + $('#idParent').val() + '", ParentRole = "' + $('#relatedPr').val() + '" WHERE PID = "' + editPID + '" ', null, function () {
                                                    tx.executeSql('INSERT INTO PersonVSAddress VALUES ( "' + $('#idParent').val() + '", "' + ParentInfo.AddressID + '" )', null, function () {
                                                        if (sessionStorage.getItem('FamilyCID') !== null && sessionStorage.getItem('EditType') == 'famPersonalType') {
                                                            window.location.href = 'home.html#profileFam';
                                                        }
                                                        else {
                                                            window.location.href = 'home.html#profile';
                                                        }
                                                    }, errorCB);
                                                }, errorCB);
                                            }, errorCB);
                                        }, errorCB);
                                    }
                                }
                                else {
                                    console.log('ยกเลิก');
                                }

                            }
                            else {
                                var r = confirm("ผู้ปกครองดังกล่าวมีข้อมูลอยู่แล้ว กด 'ตกลง' ถ้าต้องการอัพเดทข้อมูล");
                                if (r == true) {

                                    queryParent = 'UPDATE Person SET Title = "' + $('#titleParent').val() + '", FirstName = "' + $('#nameParent').val() + '", LastName = "' + $('#surnameParent').val() + '", '
                                    + 'Phone = "' + $('#telPr').val() + '" WHERE PID = "' + $('#idParent').val() + '" ';

                                    queryParentAddr = 'UPDATE Address SET Tumbon = "' + $('#tumbonPr').val() + '", '
                                    + 'Amphor = "' + $('#distinctPr').val() + '", Province = "' + $('#provincePr').val() + '", Postcode = "' + $('#numpostPr').val() + '", '
                                    + 'HouseNumber = "' + $('#HouseNumberPr').val() + '", MooNumber = "' + $('#MooNumberPr').val() + '", VillageName = "' + $('#VillageNamePr').val() + '", '
                                    + 'Alley = "' + $('#AlleyPr').val() + '", StreetName = "' + $('#StreetPr').val() + '" '
                                    + 'WHERE AddressID = "' + $('#prAddrID').val() + '" ';

                                    if (document.getElementById('useInfoAddrCB').checked) {
                                        //alert("checked");

                                        tx.executeSql('SELECT A.AddressID FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = "' + editPID + '" ', [],
                                            (function (tx, response) {
                                                tx.executeSql(queryParent, null, function () {
                                                    tx.executeSql('UPDATE PersonParent SET ParentPID = "' + $('#idParent').val() + '", ParentRole = "' + $('#relatedPr').val() + '" WHERE PID = "' + editPID + '" ', null, function () {
                                                        tx.executeSql('UPDATE PersonVSAddress SET AddressID = "' + response.rows.item(0).AddressID + '" WHERE PID = "' + $('#idParent').val() + '" ', null, function () {
                                                            if (sessionStorage.getItem('FamilyCID') !== null && sessionStorage.getItem('EditType') == 'famPersonalType') {
                                                                window.location.href = 'home.html#profileFam';
                                                            }
                                                            else {
                                                                window.location.href = 'home.html#profile';
                                                            }
                                                        }, errorCB);
                                                    }, errorCB);
                                                }, errorCB);
                                            }), errorCB);
                                    }
                                    else {
                                        //alert("You didn't check it! Let me check it for you.");
                                        tx.executeSql(queryParent, null, function () {
                                            tx.executeSql(queryParentAddr, null, function () {
                                                tx.executeSql('UPDATE PersonParent SET ParentPID = "' + $('#idParent').val() + '", ParentRole = "' + $('#relatedPr').val() + '" WHERE PID = "' + editPID + '" ', null, function () {
                                                    if (sessionStorage.getItem('FamilyCID') !== null && sessionStorage.getItem('EditType') == 'famPersonalType') {
                                                        window.location.href = 'home.html#profileFam';
                                                    }
                                                    else {
                                                        window.location.href = 'home.html#profile';
                                                    }
                                                }, errorCB);
                                            }, errorCB);
                                        }, errorCB);
                                    }
                                }
                                else {
                                    console.log('Update cancel.');
                                }
                            }
                        }, errorCB);

                        tx.executeSql(updateOccupation);
                        tx.executeSql(updateEducation);

                    }, errorCB, function () {

                    });

                }
                else {
                    alert('เลขบัตรประชาชนผู้ปกครองไม่ถูกต้อง');
                }
            }

        }
        else {
            console.log('ไม่พบ');
        }
    });

    document.getElementById('personal_sp1_2').onchange = function () {
        var txtPersonal = $("#personal_sp1_2 :selected").val();

        if (txtPersonal == '7') {
            //console.log('Student');
            $('#jobDIV').hide(); $('#studentDIV').show(); $('#academyDIV').show();
            $('#academyHead').empty(); $('#academyHead').append("กรณีนักเรียน/นักศึกษา ที่กำลังศึกษา");
        }
        else if (txtPersonal == '0000') {
            //console.log('Personal = ' + txtPersonal);
            $('#jobDIV').hide(); $('#studentDIV').hide(); $('#academyDIV').hide();
            $('#academyHead').empty(); $('#academyHead').append("");
        }
        else {
            //console.log('Personal = ' + txtPersonal);
            $('#jobDIV').show(); $('#studentDIV').hide(); $('#academyDIV').show();
            $('#academyHead').empty(); $('#academyHead').append("7.จบการศึกษา");
        }
    };

    document.getElementById('province_sp1_2').onchange = function () {
        var txtProvinceID = $(this).val();
        $('#distint_sp1_2').empty();
        $('#distint_sp1_2').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#distint_sp1_2').selectmenu("refresh");
        $('#tumbon_sp1_2').empty();
        $('#tumbon_sp1_2').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#tumbon_sp1_2').selectmenu("refresh");
        insertCity(txtProvinceID, 'distint_sp1_2');
    };

    document.getElementById('distint_sp1_2').onchange = function () {
        var txtAmphorID = $(this).val();
        $('#tumbon_sp1_2').empty();
        $('#tumbon_sp1_2').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#tumbon_sp1_2').selectmenu("refresh");
        insertTumbon(txtAmphorID, 'tumbon_sp1_2');
    };

    document.getElementById('province_academy').onchange = function () {
        var txtProvinceID = $(this).val();
        $('#distint_academy').empty();
        $('#distint_academy').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#distint_academy').selectmenu("refresh");
        $('#tumbon_academy').empty();
        $('#tumbon_academy').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#tumbon_academy').selectmenu("refresh");
        insertCity(txtProvinceID, 'distint_academy');
    };

    document.getElementById('distint_academy').onchange = function () {
        var txtAmphorID = $(this).val();
        $('#tumbon_academy').empty();
        $('#tumbon_academy').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#tumbon_academy').selectmenu("refresh");
        insertTumbon(txtAmphorID, 'tumbon_academy');
    };

    document.getElementById('provincePr').onchange = function () {
        var txtProvinceID = $(this).val();
        $('#distinctPr').empty();
        $('#distinctPr').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#distinctPr').selectmenu("refresh");
        $('#tumbonPr').empty();
        $('#tumbonPr').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#tumbonPr').selectmenu("refresh");
        insertCity(txtProvinceID, 'distinctPr');
    };

    document.getElementById('distinctPr').onchange = function () {
        var txtAmphorID = $(this).val();
        $('#tumbonPr').empty();
        $('#tumbonPr').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#tumbonPr').selectmenu("refresh");
        insertTumbon(txtAmphorID, 'tumbonPr');
    };

    //-----------------------------------------------

});

//-----------------------------------------------------

//$('#relationAtn').on('focus', function () {  document.body.scrollTop = $(this).offset().top; }); //scroll to top

//----------- emergency_stand_in page --------------
/*
$(document).on("pagecreate", "#emergency_stand_in", function (event) {

    $('#emergency_next').click(function () {

        replaceNull("emergency_stand_in");

        var standIn = {
            CID: $('#idAttorney').val(),
            Title: $('#titleAttorney').val(),
            FName: $('#nameAttorney').val(),
            LName: $('#surnameAttorney').val(),
            Phone: $('#telAtn').val(),
            Relation: $('#relationAtn').val(),

            AddressID: guidGenerator(),
            Address: $('#addressAtn').val(),
            Address: "0000",
            Province: $('#provinceAtn').val(),
            Amphor: $('#distinctAtn').val(),
            Tumbon: $('#tumbonAtn').val(),
            Postcode: $('#numpostAtn').val(),

            recordStaffCID: sessionStorage.getItem('CID')
        };

        var personalTypeInfo = {
            CID: $('#idAttorney').val(),
            income: 0,
            outcome: 0,
            inDept: 0,
            exDept: 0
        };


        var queryStr = "SELECT PID FROM Person WHERE PID = '" + $('#idAttorney').val() + "' ";
        db.transaction(function (tx) {
            tx.executeSql(queryStr, [], (function (tx, response) {
                if (response.rows.length == 0 && $('#idAttorney').val() != "") {

                    //ใช้บ้านเลขที่เดียวกับผู้ให้ข้อมูล
                    if (document.getElementById('standInAddrCB').checked) {

                        db.transaction(function (tx) {
                            tx.executeSql('SELECT * FROM PersonVSAddress A WHERE PID = "' + $('#IDNumber').val() + '" ', [], (function (tx, response) {
                                standIn.AddressID = response.rows[0].AddressID;
                                //console.log(standIn.AddressID);
                                insertPerson(standIn, 'standInExistAddr'); insertPersonalType(personalTypeInfo, 'insert');
                                //window.location.href = 'home.html';                
                                $(":mobile-pagecontainer").pagecontainer("change", "#blank", { transition: "pop" });
                            }), errorCB, successCB);
                        });

                    } else {
                        console.log("ยังไม่มีข้อมูลคนนี้");
                        //console.log(standIn.AddressID);
                        insertPerson(standIn, 'standIn'); insertPersonalType(personalTypeInfo, 'insert');
                        //window.location.href = 'home.html';                
                        $(":mobile-pagecontainer").pagecontainer("change", "#blank", { transition: "slide" });

                    }


                } else if ($('#idAttorney').val() != "" && response.rows.length != 0) {
                    var r = confirm("พบเลขบัตรประชาชนนี้แล้ว \nต้องการใช้ข้อมูลบุคคลนี้ หรือไม่");
                    if (r == true) {
                        console.log("ใช่");
                        insertPerson(standIn, 'existStandIn');
                        $(":mobile-pagecontainer").pagecontainer("change", "#blank", { transition: "slide" });
                        //window.location.href = 'person_data.html#info';
                    } else {
                        console.log("ยกเลิก");
                    }
                } else {
                    alert("ยังไม่ได้กรอกเลขบัตรประชาชน");
                }
            }), errorCB, successCB);
        });

    });

    $("#standInAddrCB").change(function () {
        if (this.checked) {
            //$('#standInAddrDIV :input, :selected').attr('disabled', true);
            $('#standInAddrDIV').hide();
        } else {
            //$('#standInAddrDIV :input, :selected').attr('disabled', false);
            $('#standInAddrDIV').show();
        }
    });

    $("#standInCB").change(function () {

        if (this.checked) {
            $('#standInDIV, #standInAddrDIV').hide();
            $('#standInAddrCB').checkboxradio('disable');
            $("#standInAddrCB").attr("checked", false).checkboxradio("refresh");

            if ($('#IDNumber').val() == "" || $('#IDNumber').val() == "-") {
                $('#idAttorney').val($('#IDNumber_info').val());
            } else $('#idAttorney').val($('#IDNumber').val());

            document.body.scrollTop = $('#relationAtn').offset().top;

        } else {

            $('#standInDIV, #standInAddrDIV').show();
            $('#standInAddrCB').checkboxradio('enable');
            $("#standInAddrCB").attr("checked", false).checkboxradio("refresh");
            $('#idAttorney').val("");

        }

    });

    document.getElementById('provinceAtn').onchange = function () {
        var txtProvinceID = $(this).val();
        $('#distinctAtn').empty();
        $('#distinctAtn').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#distinctAtn').selectmenu("refresh");
        $('#tumbonAtn').empty();
        $('#tumbonAtn').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#tumbonAtn').selectmenu("refresh");
        insertCity(txtProvinceID, 'distinctAtn');
    };

    document.getElementById('distinctAtn').onchange = function () {
        var txtAmphorID = $(this).val();
        $('#tumbonAtn').empty();
        $('#tumbonAtn').append($("<option></option>").val("0000").html('ไม่ระบุ'));
        $('#tumbonAtn').selectmenu("refresh");
        insertTumbon(txtAmphorID, 'tumbonAtn');
    };

});
*/

//-----------------------------------------------------

function insertDropdown(page) {

    //$('#title').empty(); $('#title_info').empty(); $('#titleAttorney').empty();
    //$('.province_dropdown').each(function () {
    //    $('#' + $(this).attr('id')).empty();
    //});
    //$('#relation_info').empty(); $('#relationAtn').empty();
    //$('#nation_info').empty();
    //$('#race_info').empty();
    //$('#religion_info').empty();
    //$('#marriageStatus_info').empty();
    //$('#addressType_sp1_1').empty();
    //$('#personal_sp1_2').empty();
    //$('#graduateLevel').empty(); $('#educateLevel').empty();

    $('#' + page + ' select').empty();
    //$('#' + page + ' select').append($("<option></option>").val("0").html("เลือก"));

    var queryTitle = "SELECT * FROM Title";
    var queryProvince = "SELECT ProvinceID, ProvinceDescription FROM Province ORDER BY ProvinceDescription";
    var queryRelation = "SELECT RelationID, RelationDescription FROM Relation";
    var queryNation = "SELECT * FROM Nationality";
    var queryRace = "SELECT * FROM Race";
    var queryReligion = "SELECT * FROM Religion";
    var queryMarriageStatus = "SELECT * FROM MariageStatus";
    var queryLiveHomeStatus = "SELECT * FROM LiveHomeStatus";
    var queryOccupation = "SELECT * FROM Occupation";
    var queryEducationLevel = "SELECT * FROM EducationLevel";
    var queryBank = "SELECT * FROM Bank";

    db.transaction(function (tx) {

        tx.executeSql(queryTitle, [], (function (tx, response) {
            for (var i = 0; i < response.rows.length ; i++) {
                $('#title').append($("<option></option>").val(response.rows.item(i).TitleID).html(response.rows.item(i).TitleDESC));
                $('#title_info').append($("<option></option>").val(response.rows.item(i).TitleID).html(response.rows.item(i).TitleDESC));
                $('#titleAttorney').append($("<option></option>").val(response.rows.item(i).TitleID).html(response.rows.item(i).TitleDESC));
                $('#titleParent').append($("<option></option>").val(response.rows.item(i).TitleID).html(response.rows.item(i).TitleDESC));
            }
        }), errorCB);

        tx.executeSql(queryProvince, [], (function (tx, response) {
            $('.province_dropdown').each(function () {
                for (var i = 0; i < response.rows.length ; i++) {
                    $('#' + $(this).attr('id')).append($("<option></option>").val(response.rows.item(i).ProvinceID).html(response.rows.item(i).ProvinceDescription));
                }
            });
        }), errorCB);

        tx.executeSql(queryRelation, [], (function (tx, response) {
            for (var i = 0; i < response.rows.length ; i++) {
                $('#relation_info').append($("<option></option>").val(response.rows.item(i).RelationID).html(response.rows.item(i).RelationDescription));
                $('#relationAtn').append($("<option></option>").val(response.rows.item(i).RelationID).html(response.rows.item(i).RelationDescription));
                $('#relatedPr').append($("<option></option>").val(response.rows.item(i).RelationID).html(response.rows.item(i).RelationDescription));
                $('#relation').append($("<option></option>").val(response.rows.item(i).RelationID).html(response.rows.item(i).RelationDescription));

            }
        }), errorCB);

        tx.executeSql(queryNation, [], (function (tx, response) {
            for (var i = 0; i < response.rows.length ; i++) {
                $('#nation_info').append($("<option></option>").val(response.rows.item(i).NationalityID).html(response.rows.item(i).NationalityDescription));
            }
        }), errorCB);

        tx.executeSql(queryRace, [], (function (tx, response) {
            for (var i = 0; i < response.rows.length ; i++) {
                $('#race_info').append($("<option></option>").val(response.rows.item(i).RaceID).html(response.rows.item(i).RaceDescription));
            }
        }), errorCB);

        tx.executeSql(queryReligion, [], (function (tx, response) {
            for (var i = 0; i < response.rows.length ; i++) {
                $('#religion_info').append($("<option></option>").val(response.rows.item(i).Id).html(response.rows.item(i).Religion));
            }
        }), errorCB);

        tx.executeSql(queryMarriageStatus, [], (function (tx, response) {
            for (var i = 0; i < response.rows.length ; i++) {
                $('#marriageStatus_info').append($("<option></option>").val(response.rows.item(i).MariageStatusID).html(response.rows.item(i).MariageStatusDescription));
            }
        }), errorCB);

        tx.executeSql(queryLiveHomeStatus, [], (function (tx, response) {
            for (var i = 0; i < response.rows.length ; i++) {
                $('#addressType_sp1_1').append($("<option></option>").val(response.rows.item(i).LiveHomeStatusID).html(response.rows.item(i).LiveHomeStatusDescription));
            }
        }), errorCB);

        tx.executeSql(queryOccupation, [], (function (tx, response) {
            for (var i = 0; i < response.rows.length ; i++) {
                $('#personal_sp1_2').append($("<option></option>").val(response.rows.item(i).OccupationID).html(response.rows.item(i).OccupationDescription));
            }
        }), errorCB);

        tx.executeSql(queryEducationLevel, [], (function (tx, response) {
            for (var i = 0; i < response.rows.length ; i++) {
                $('#graduateLevel').append($("<option></option>").val(response.rows.item(i).EducationLevelID).html(response.rows.item(i).EducationLevelDescription));
                $('#educateLevel').append($("<option></option>").val(response.rows.item(i).EducationLevelID).html(response.rows.item(i).EducationLevelDescription));
            }
        }), errorCB);

        tx.executeSql(queryBank, [], (function (tx, response) {
            for (var i = 0; i < response.rows.length ; i++) {
                $('#account').append($("<option></option>").val(response.rows.item(i).BankID).html(response.rows.item(i).BankIDDesc));
                $('#bank_informant').append($("<option></option>").val(response.rows.item(i).BankID).html(response.rows.item(i).BankIDDesc));
            }
        }), errorCB);

        $('#satisfyStatus').append($("<option></option>").val("0000").html("ไม่ระบุ"));
        $('#satisfyStatus').append($("<option></option>").val("1").html("พอใจในที่อยู่อาศัยปัจจุบัน"));
        $('#satisfyStatus').append($("<option></option>").val("2").html("ต้องการย้ายที่อยู่อาศัยปัจจุบัน"));

    }, errorCB, function () {
        console.log('insert dropdown');
        //$('select').selectmenu().selectmenu('refresh', true);
        $('#' + page + ' select').selectmenu('refresh');
    });
}

function updateTable(queryStr) {
    db.transaction(function (tx) { tx.executeSql(queryStr); }, errorCB,
        (function () {
            return "OK";
        }));
}

function insertPerson(personData, selectQuery) {
    //console.log(personData);
    /*
    selectQuery :: informant, standIn, standInExistAddr, existStandIn, person, personExistAddr, family, familyExistAddr
    */

    var queryPerson; var queryAddress;
    var d = new Date();
    var currentDateTime = d.getFullYear() + '-' + d.getMonth() + '-' + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();

    if (selectQuery == 'informant' || selectQuery == 'standIn' || selectQuery == 'standInExistAddr' || selectQuery == 'parent' || selectQuery == 'parentExistAddr' || selectQuery == 'emergency' ||
        selectQuery == 'emergencyExistAddr') {
        queryPerson = 'INSERT INTO Person ( PID, Title, FirstName, LastName, Phone, RecordStaffPID, DateTimeUpdate, Passport, DOB, Nation, Race, Religion, MarriageStatus, AddressSatisfy, BirthProvince )'
            + ' VALUES ( "' + personData.CID + '", "' + personData.Title + '", "' + personData.FName + '", "' + personData.LName + '", "' + personData.Phone + '", "' + personData.recordStaffCID + '", "-", "0", "1950-01-01", "0", "0", "0", "0", "0", "0" )';

        queryAddress = 'INSERT INTO Address ( AddressID, LiveHomeStatusID, HouseNumber, MooNumber, VillageName, Alley, StreetName, Tumbon, Amphor, Province, Postcode, HomeCode, addrLat, addrLng )'
                        + ' VALUES ( "' + personData.AddressID + '", "' + personData.AddressType + '", "' + personData.HouseNumber + '", "' + personData.MooNumber + '", "' + personData.VillageName + '",'
                        + ' "' + personData.Alley + '", "' + personData.Street + '", "' + personData.Tumbon + '", "' + personData.Amphor + '", "' + personData.Province + '", "' + personData.Postcode + '", "-", 0.0, 0.0 )';

    }
    else if (selectQuery == 'person' || selectQuery == 'personExistAddr' || selectQuery == 'family' || selectQuery == 'familyExistAddr' ||
        selectQuery == 'mate' || selectQuery == 'mateExistAddr') {

        queryPerson = "INSERT INTO Person ( PID, Title, FirstName, LastName, Phone, Passport, DOB, Nation, Race, Religion, MarriageStatus, AddressSatisfy, BirthProvince, RecordStaffPID, DateTimeUpdate )"
        + ' VALUES ( "' + personData.CID + '", "' + personData.title + '", "' + personData.firstName + '", "' + personData.lastName + '", "' + personData.tel + '", "' + personData.passport + '", "' + personData.dob + '", '
        + ' "' + personData.nation + '", "' + personData.race + '", "' + personData.religion + '", "' + personData.marriageStatus + '", "' + personData.addressSatisfy + '", "' + personData.birthProvince + '", '
        + ' "' + personData.recordStaffCID + '", "-" )';

        queryAddress = 'INSERT INTO Address ( AddressID, LiveHomeStatusID, HouseNumber, MooNumber, VillageName, Alley, StreetName, Tumbon, Amphor, Province, Postcode, HomeCode, addrLat, addrLng )'
                        + ' VALUES ( "' + personData.AddressID + '", "' + personData.addressType + '", "' + personData.houseNumber + '", "' + personData.mooNumber + '", "' + personData.villageName + '",'
                        + ' "' + personData.alley + '", "' + personData.street + '", "' + personData.tumbon + '", "' + personData.distint + '", "' + personData.province + '", "' + personData.numpost + '", "' + personData.homenumber + '",'
                        + ' ' + addrLat + ', ' + addrLng + ')';

        if ($('#IDNumber').val() == "" || $('#IDNumber').val() == "-") {
            $('#IDNumber').val(personData.CID);
        }

    }
    else console.log('ไม่พบฟังก์ชั่น');


    console.log(queryPerson + "\n\n" + queryAddress);

    if (selectQuery == 'informant' || selectQuery == 'standIn' || selectQuery == 'person' || selectQuery == 'family' || selectQuery == 'parent' || selectQuery == 'emergency' || selectQuery == 'mate') {

        db.transaction(function (tx) {
            tx.executeSql("SELECT PID FROM Person WHERE PID = '" + personData.CID + "' ", [], function (tx, rs) {
                if (rs.rows.length == 0) {
                    tx.executeSql(queryPerson, null, function (tx) {
                        console.log('success person');
                        tx.executeSql(queryAddress, null, function (tx) {
                            console.log('success address');
                            tx.executeSql('INSERT INTO PersonVSAddress ( PID, AddressID ) VALUES ( "' + personData.CID + '", "' + personData.AddressID + '" )');
                        }, errorCB);
                    }, errorCB);
                }
                else {
                    console.log('Not insert because, this person ("' + personData.CID + '") is already exsist.');
                }
            }, errorCB);
        }, errorCB, (function () {
            //console.log('success');

            if (selectQuery == 'person') {
                // เพิ่มว่าใครเป็นคนให้ข้อมูล
                db.transaction(function (tx) {
                    tx.executeSql('INSERT INTO PersonRecordBy ( PID, RecordPID, Relation )'
                        + ' VALUES ( "' + personData.CID + '", ' + $('#IDNumber').val() + ', "' + personData.recorderRelation + '" )');
                    tx.executeSql("INSERT INTO PersonFamily VALUES ('" + guidGenerator() + "', '8888', " + personData.CID + ") ");
                }, errorCB, function () {
                    if (navigator.onLine) {
                        var uploadImg = $('#myImage').attr('src');
                        getBase64Img(uploadImg, 2);
                    } else { $(":mobile-pagecontainer").pagecontainer("change", "#blank", { transition: "slide" }); }
                });
            }
            else if (selectQuery == 'parent') {
                db.transaction(function (tx) {
                    tx.executeSql('INSERT INTO PersonParent ( PID, ParentPID, ParentRole )'
                        + ' VALUES ( "' + $('#IDNumber_info').val() + '", ' + personData.CID + ', "' + personData.Relation + '" )');
                }, errorCB, function () {
                });
            }
            else if (selectQuery == 'standIn') {
                db.transaction(function (tx) {
                    tx.executeSql('UPDATE PersonStandIn SET StandIn = ' + personData.CID + ', StandInRole = "' + personData.Relation + '"'
                        + ' WHERE PID = "' + sessionStorage.getItem('PersonCID') + '" ');
                }, errorCB, function () {
                    window.location.href = 'home.html#profile';
                });
            }
            else if (selectQuery == 'emergency') {
                db.transaction(function (tx) {
                    tx.executeSql('UPDATE PersonEmergencyContact SET EmergencyContactPID = ' + personData.CID + ', EmergencyContactRole = "' + personData.Relation + '"'
                        + ' WHERE PID = "' + sessionStorage.getItem('PersonCID') + '" ');
                }, errorCB, function () {
                    window.location.href = 'home.html#profile';
                });
            }
            else if (selectQuery == 'family') {

                db.transaction(function (tx) {
                    tx.executeSql("SELECT * FROM PersonFamily WHERE PID = '" + personData.CID + "' ", [],
                        (function (tx, results) {
                            if (results.rows.length == 0) {
                                db.transaction(function (tx) {
                                    tx.executeSql("INSERT INTO PersonFamily VALUES ( '" + sessionStorage.getItem('familyID') + "', '" + $("#relation_info").val() + "', '" + personData.CID + "' )");
                                }, errorCB);
                            }
                            else {
                                db.transaction(function (tx) {
                                    tx.executeSql("UPDATE PersonFamily SET FamilyID = '" + sessionStorage.getItem('familyID') + "', FamilyRole = '" + $("#relation_info").val() + "' WHERE PID = '" + personData.CID + "' ");
                                }, errorCB);
                            }
                        }), errorCB)
                }, errorCB, function () {
                    window.location.href = 'home.html#profile';
                });
            }
            else if (selectQuery == 'mate') {
                db.transaction(function (tx) {
                    tx.executeSql("SELECT * FROM PersonMate WHERE MatePID = '" + personData.CID + "' ", [],
                        //ชาย มี MatePID หลาย ID ได้ แต่ซ้ำไม่ได้ -- หญิงมี MatePID หลาย ID ไม่ได้ ดังนั้นซ้ำไม่ได้
                        //ดังนั้น MatePID มีซ้ำไม่ได้
                        (function (tx, results) {
                            if (results.rows.length == 0) {
                                db.transaction(function (tx) {
                                    tx.executeSql("INSERT INTO PersonMate VALUES ( '" + sessionStorage.getItem('PersonCID') + "', '" + personData.CID + "' )");
                                }, errorCB);
                            }
                            else {
                                db.transaction(function (tx) {
                                    tx.executeSql("UPDATE PersonMate SET PID = '" + sessionStorage.getItem('PersonCID') + "', MatePID = '" + personData.CID + "' WHERE PID = '" + sessionStorage.getItem('PersonCID') + "' AND MatePID = '" + personData.CID + "' ");
                                }, errorCB);
                            }
                        }), errorCB)
                }, errorCB, function () {
                    window.location.href = 'home.html#profile';
                });
            }
            else console.log('No query 2');

        }));

    } else if (selectQuery == 'personExistAddr' || selectQuery == 'standInExistAddr' || selectQuery == 'parentExistAddr' || selectQuery == 'emergencyExistAddr' || selectQuery == 'familyExistAddr' || selectQuery == 'mateExistAddr') {

        db.transaction(function (tx) {
            tx.executeSql("SELECT PID FROM Person WHERE PID = '" + personData.CID + "' ", [], function (tx, rs) {
                if (rs.rows.length == 0) {
                    tx.executeSql(queryPerson, null, function (tx) {
                        console.log('success person exist addr');
                        tx.executeSql('INSERT INTO PersonVSAddress ( PID, AddressID ) VALUES ( "' + personData.CID + '", "' + personData.AddressID + '" )');
                    }, errorCB);
                }
                else {
                    console.log('Not insert because, this person ("' + personData.CID + '") is already exsist.');
                }
            }, errorCB);
        }, errorCB, function () {

            if (selectQuery == 'personExistAddr') {
                // เพิ่มว่าใครเป็นคนให้ข้อมูล
                db.transaction(function (tx) {
                    tx.executeSql('INSERT INTO PersonRecordBy ( PID, RecordPID, Relation )'
                        + ' VALUES ( "' + personData.CID + '", "' + $('#IDNumber').val() + '", "' + personData.recorderRelation + '" )');
                    tx.executeSql("INSERT INTO PersonFamily VALUES ('" + guidGenerator() + "', '8888', " + personData.CID + ") ");
                }, errorCB, function () {
                    if (navigator.onLine) {
                        var uploadImg = $('#myImage').attr('src');
                        getBase64Img(uploadImg, 2);
                    } else { $(":mobile-pagecontainer").pagecontainer("change", "#blank", { transition: "slide" }); }
                });

            }
            else if (selectQuery == 'parentExistAddr') {
                db.transaction(function (tx) {
                    tx.executeSql('INSERT INTO PersonParent ( PID, ParentPID, ParentRole )'
                        + ' VALUES ( "' + $('#IDNumber_info').val() + '", "' + personData.CID + '", "' + personData.Relation + '" )');
                }, errorCB, function () {
                });
            }
            else if (selectQuery == 'standInExistAddr') {
                db.transaction(function (tx) {
                    tx.executeSql('UPDATE PersonStandIn SET StandIn = ' + personData.CID + ', StandInRole = "' + personData.Relation + '"'
                        + ' WHERE PID = "' + sessionStorage.getItem('PersonCID') + '" ');
                }, errorCB, function () {
                    window.location.href = 'home.html#profile';
                });
            }
            else if (selectQuery == 'emergencyExistAddr') {
                db.transaction(function (tx) {
                    tx.executeSql('UPDATE PersonEmergencyContact SET EmergencyContactPID = ' + personData.CID + ', EmergencyContactRole = "' + personData.Relation + '"'
                        + ' WHERE PID = "' + sessionStorage.getItem('PersonCID') + '" ');
                }, errorCB, function () {
                    window.location.href = 'home.html#profile';
                });
            }
            else if (selectQuery == 'familyExistAddr') {

                db.transaction(function (tx) {
                    tx.executeSql("SELECT * FROM PersonFamily WHERE PID = '" + personData.CID + "' ", [],
                        (function (tx, results) {
                            if (results.rows.length == 0) {
                                db.transaction(function (tx) {
                                    tx.executeSql("INSERT INTO PersonFamily VALUES ( '" + sessionStorage.getItem('familyID') + "', '" + $("#relation_info").val() + "', '" + personData.CID + "' )");
                                }, errorCB);
                            }
                            else {
                                db.transaction(function (tx) {
                                    tx.executeSql("UPDATE PersonFamily SET FamilyID = '" + sessionStorage.getItem('familyID') + "', FamilyRole = '" + $("#relation_info").val() + "' WHERE PID = '" + personData.CID + "' ");
                                }, errorCB);
                            }
                            //insertPersonalType(personData, "insert");
                        }), errorCB)
                }, errorCB, function () {
                    window.location.href = 'home.html#profile';
                });
            }
            else if (selectQuery == 'mateExistAddr') {
                db.transaction(function (tx) {
                    tx.executeSql("SELECT * FROM PersonMate WHERE MatePID = '" + personData.CID + "' ", [],
                        //ชาย มี MatePID หลาย ID ได้ แต่ซ้ำไม่ได้ -- หญิงมี MatePID หลาย ID ไม่ได้ ดังนั้นซ้ำไม่ได้
                        //ดังนั้น MatePID มีซ้ำไม่ได้
                        (function (tx, results) {
                            if (results.rows.length == 0) {
                                db.transaction(function (tx) {
                                    tx.executeSql("INSERT INTO PersonMate VALUES ( '" + sessionStorage.getItem('PersonCID') + "', '" + personData.CID + "' )");
                                }, errorCB);
                            }
                            else {
                                db.transaction(function (tx) {
                                    tx.executeSql("UPDATE PersonMate SET PID = '" + sessionStorage.getItem('PersonCID') + "', MatePID = '" + personData.CID + "' WHERE PID = '" + sessionStorage.getItem('PersonCID') + "' AND MatePID = '" + personData.CID + "' ");
                                }, errorCB);
                            }

                        }), errorCB)
                }, errorCB, function () {
                    window.location.href = 'home.html#profile';
                });
            }
            else console.log('No query 2');

        });

    } else console.log('No query');

}

function insertPersonalType(personalTypeData, selectQuery) {
    var queryJob, queryEducation, queryFinance, queryBank;

    if (selectQuery == 'insert') {
        queryJob = 'INSERT INTO PersonOccupation ( PID, OccupationID, Position, Office, Tumbon, Amphor, Province, Postcode, Phone, Fax )'
        + ' VALUES ( "' + personalTypeData.CID + '", "' + personalTypeData.personType + '", "' + personalTypeData.position + '", "' + personalTypeData.office + '",'
        + ' "' + personalTypeData.officeTumbon + '", "' + personalTypeData.officeDistinct + '", "' + personalTypeData.officeProvince + '", "' + personalTypeData.officeNumpost + '",'
        + ' "' + personalTypeData.officeTel + '", "' + personalTypeData.officeFax + '"'
        + ' )';

        queryEducation = 'INSERT INTO PersonEducation ( PID, EduStatus, EduLevel, EduYear, Academy, Tumbon, Amphor, Province )'
        + ' VALUES ( "' + personalTypeData.CID + '", "' + personalTypeData.eduStatus + '", "' + personalTypeData.eduLevel + '", "' + personalTypeData.eduYear + '",'
        + ' "' + personalTypeData.academy + '", "' + personalTypeData.academyTumbon + '", "' + personalTypeData.academyDistinct + '", "' + personalTypeData.academyProvince + '"'
        + ' )';

        queryFinance = 'INSERT INTO PersonFinance ( PID, Income, Outgoing, InDebt, ExDebt )'
        + ' VALUES ( "' + personalTypeData.CID + '", ' + personalTypeData.income + ', ' + personalTypeData.outcome + ', ' + personalTypeData.inDept + ', ' + personalTypeData.exDept + ' )';

        queryBank = 'INSERT INTO PersonBankAccount ( PID, BankName, BankBranch, AccountNo, AccountName )'
        + ' VALUES ( "' + personalTypeData.CID + '", "' + personalTypeData.bankName + '", "' + personalTypeData.bankBranch + '", "' + personalTypeData.accountNum + '", "' + personalTypeData.accountName + '" )';

    }

    console.log(queryJob + "\n\n" + queryEducation + "\n\n" + queryFinance + "\n\n" + queryBank);

    //เพิ่ม Job Detail

    db.transaction(function (tx) {
        tx.executeSql("SELECT PID FROM PersonOccupation WHERE PID = '" + personalTypeData.CID + "' ", [], function (tx, rs) {
            if (rs.rows.length == 0) {
                tx.executeSql(queryJob);
            }
        }, errorCB);

        tx.executeSql("SELECT PID FROM PersonEducation WHERE PID = '" + personalTypeData.CID + "' ", [], function (tx, rs) {
            if (rs.rows.length == 0) {
                tx.executeSql(queryEducation);
            }
        }, errorCB);

        tx.executeSql("SELECT PID FROM PersonFinance WHERE PID = '" + personalTypeData.CID + "' ", [], function (tx, rs) {
            if (rs.rows.length == 0) {
                if (personalTypeData.income != null || personalTypeData.outcome != null || personalTypeData.inDept != null || personalTypeData.exDept != null) {
                    tx.executeSql(queryFinance);
                } else {
                    tx.executeSql('INSERT INTO PersonFinance ( PID, Income, Outgoing, InDebt, ExDebt )'
                    + ' VALUES ( "' + personalTypeData.CID + '", 0, 0, 0, 0 )');
                }
            }
        }, errorCB);

        tx.executeSql("SELECT PID FROM PersonBankAccount WHERE PID = '" + personalTypeData.CID + "' ", [], function (tx, rs) {
            if (rs.rows.length == 0) {
                tx.executeSql(queryBank);
            }
        }, errorCB);

    }, errorCB, successCB);

}


function loadInformantData(personCID) {

    var queryGetInformant = "SELECT P.*, A.*, PBA.BankName, PBA.BankBranch, PBA.AccountNo, PBA.AccountName FROM Person P"
            + " LEFT JOIN PersonVSAddress PVA ON P.PID = PVA.PID"
            + " LEFT JOIN Address A ON PVA.AddressID = A.AddressID"
            + " LEFT JOIN PersonBankAccount PBA ON PBA.PID = P.PID"
            + " WHERE P.PID = '" + personCID + "' ";
    console.log(queryGetInformant);
    db.transaction(function (tx) {
        tx.executeSql(queryGetInformant, [], (function (tx, results) {
            if (results.rows.length != 0) {
                $('#IDNumber').val(results.rows[0].PID);
                $('#title').val(results.rows[0].Title);
                $('#firstName').val(results.rows[0].FirstName);
                $('#lastName').val(results.rows[0].LastName);
                $('#tel').val(results.rows[0].Phone);

                $('#HouseNumber').val(results.rows[0].HouseNumber);
                $('#MooNumber').val(results.rows[0].MooNumber);
                $('#VillageName').val(results.rows[0].VillageName);
                $('#Alley').val(results.rows[0].Alley);
                $('#Street').val(results.rows[0].StreetName);
                $('#province').val(results.rows[0].Province);
                autoLoadCityAndTumbon('informant', 'tumbon', 'amphor', results.rows[0].Tumbon, results.rows[0].Amphor, results.rows[0].Province);
                $('#postcode').val(results.rows[0].Postcode);

                $('#bank_informant').val(results.rows[0].BankName);
                $('#branch_informant').val(results.rows[0].BankBranch);
                $('#accountNum_informant').val(results.rows[0].AccountNo);
                $('#accountName_informant').val(results.rows[0].AccountName);

                $('#addrID').val(results.rows[0].AddressID);
            }
            $('#informant select').selectmenu('refresh');
        }), errorCB);
    }, errorCB);

}

function loadPersonData(personCID) {
    //var staffCID = sessionStorage.getItem('CID');
    //"PID, Title, FirstName, LastName, Phone, Passport, DOB, Nation, Race, Religion, MarriageStatus, AddressSatisfy, BirthProvince, PersonalType, RecordStaffPID";
    var queryStr = "SELECT * FROM Person WHERE PID = '" + personCID + "' ";
    db.transaction(function (tx) {
        tx.executeSql(queryStr, [], (function (tx, response) {

            if (response.rows.length != 0) {
                //$('#staIDNumber').val(response.rows.item(0).StaffID);
                $('#IDNumber_info').val(personCID);
                $('#title_info').val(response.rows.item(0).Title);
                $('#firstName_info').val(response.rows.item(0).FirstName);
                $('#lastName_info').val(response.rows.item(0).LastName);
                $('#tel_info').val(response.rows.item(0).Phone);

                $('#passport_info').val(response.rows.item(0).Passport);
                $('#dob_info').val(response.rows.item(0).DOB);
                $('#nation_info').val(response.rows.item(0).Nation);
                $('#race_info').val(response.rows.item(0).Race);
                $('#religion_info').val(response.rows.item(0).Religion);

                $('#marriageStatus_info').val(response.rows.item(0).MarriageStatus);
                $('#satisfyStatus').val(response.rows.item(0).AddressSatisfy);
                $('#birthProvince_info').val(response.rows.item(0).BirthProvince);

                var PicPath = "";
                if (navigator.onLine) { PicPath = "http://seniorservice.azurewebsites.net/img/" + response.rows[0].PID + ".jpg"; }
                else { PicPath = "img/avatar-placeholder.png"; }
                $('#myImage').attr('src', PicPath);
                //$('#myImage').attr("src", response.rows.item(0).PicPath);
                /*
                $('#myImage').on('load', function () {
                    console.log("image loaded correctly");
                }).on('error', function () {
                    console.log("error loading image");
                    $('#myImage').attr("src", 'img/avatar-placeholder.png');
                });//.attr("src", 'img/avatar-placeholder.png');
                */

                $('#info select').selectmenu('refresh');
            }

        }), errorCB, successCB);
    });
}

function loadPersonAddress(personCID) {

    //"AddressID, LiveHomeStatusID, HouseNumber, MooNumber, VillageName, Alley, StreetName, Tumbon, Amphor, Province, Postcode"
    db.transaction(function (tx) { //หา AddressID ของ Person
        tx.executeSql('SELECT A.* FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = "' + personCID + '" ', [],
            (function (tx, response) {
                if (response.rows.length != 0) {
                    $('#homeNumber_sp1_1').val(response.rows[0].HomeCode);
                    $('#addressType_sp1_1').val(response.rows[0].LiveHomeStatusID);
                    $('#HouseNumber_sp1_1').val(response.rows[0].HouseNumber);
                    $('#MooNumber_sp1_1').val(response.rows[0].MooNumber);
                    $('#VillageName_sp1_1').val(response.rows[0].VillageName);

                    $('#Alley_sp1_1').val(response.rows[0].Alley);
                    $('#Street_sp1_1').val(response.rows[0].StreetName);
                    $('#province_sp1_1').val(response.rows[0].Province);
                    autoLoadCityAndTumbon('address_info', 'tumbon_sp1_1', 'distint_sp1_1', response.rows[0].Tumbon, response.rows[0].Amphor, response.rows[0].Province);
                    $('#numpost_sp1_1').val(response.rows[0].Postcode);

                    if (navigator.onLine && sessionStorage.getItem('EditType') === null) { initMapInfo(response.rows[0].addrLat, response.rows[0].addrLng); } else { }

                }
                //$('#address_info select').selectmenu('refresh');

            }), errorCB);
    });
}

function loadPersonType(personCID, type) {
    var qrStr = "";
    if (type == 7) {
        qrStr = 'SELECT Job.*, ' +
                'Edu.EduLevel, Edu.EduYear, Edu.Academy, Edu.Tumbon AS EduT, Edu.Amphor AS EduC, Edu.Province AS EduP, ' +
                'Per.PID AS PerPID, Per.Title, Per.FirstName, Per.LastName, Per.Phone AS PerPhone, PP.ParentRole, ' +
                'A.AddressID, A.HouseNumber, A.MooNumber, A.VillageName, A.Alley, A.StreetName, A.PostCode AS PerPostCode, A.Province AS AddrP, A.Tumbon AS AddrT, A.Amphor AS AddrC ' +
                'FROM PersonOccupation Job ' +
                'LEFT JOIN PersonEducation Edu ON Edu.PID = Job.PID ' +
                'LEFT JOIN PersonParent PP ON PP.PID = Job.PID ' +
                'LEFT JOIN Person Per ON Per.PID = PP.ParentPID ' +
                'LEFT JOIN PersonVSAddress PVA ON PVA.PID = Per.PID ' +
                'LEFT JOIN Address A ON A.AddressID = PVA.AddressID ' +
                'WHERE Job.PID = "' + personCID + '" ';
    }
    else {
        qrStr = 'SELECT Job.*, Fina.*, Bank.*, ' +
                'Edu.EduLevel, Edu.EduYear, Edu.Academy, Edu.Tumbon AS EduT, Edu.Amphor AS EduC, Edu.Province  AS EduP ' +
                'FROM PersonOccupation Job ' +
                'LEFT JOIN PersonEducation Edu ON Job.PID = Edu.PID ' +
                'LEFT JOIN PersonFinance Fina ON Job.PID = Fina.PID ' +
                'LEFT JOIN PersonBankAccount Bank ON Job.PID = Bank.PID ' +
                'WHERE Job.PID = "' + personCID + '" ';
    }
    db.transaction(function (tx) {
        tx.executeSql(qrStr, [], (function (tx, results) {
            if (results.rows.length != 0 && type != 7) {
                //PID, EduStatus, EduLevel, EduYear, Academy, Tumbon, Amphor, Province
                $('#graduateLevel').val(results.rows[0].EduLevel);
                $('#academy').val(results.rows[0].Academy);
                $('#endyear').val(results.rows[0].EduYear);
                $("#province_academy").val(results.rows[0].EduP);
                autoLoadCityAndTumbon('person_type', 'tumbon_academy', 'distint_academy', results.rows[0].EduT, results.rows[0].EduC, results.rows[0].EduP);

                //PID, OccupationID, Position, Office, Tumbon, Amphor, Province, Postcode, Phone, Fax
                $("#personal_sp1_2").val(results.rows[0].OccupationID);
                $('#position_sp1_2').val(results.rows[0].Position);
                $('#office_sp1_2').val(results.rows[0].Office);
                $("#province_sp1_2").val(results.rows[0].Province);
                autoLoadCityAndTumbon('person_type', 'tumbon_sp1_2', 'distint_sp1_2', results.rows[0].Tumbon, results.rows[0].Amphor, results.rows[0].Province);
                $("#numpost_sp1_2").val(results.rows[0].Postcode);
                $('#tel_sp1_2').val(results.rows[0].Phone);
                $('#fax_sp1_2').val(results.rows[0].Fax);

                $('#income5').val(results.rows[0].Income);
                $('#expense5').val(results.rows[0].Outgoing);
                $('#indept').val(results.rows[0].InDebt);
                $('#exdept').val(results.rows[0].ExDebt);

                $('#account').val(results.rows[0].BankName);
                $('#branch').val(results.rows[0].BankBranch);
                $('#accountNum').val(results.rows[0].AccountNo);
                $('#accountName').val(results.rows[0].AccountName);

            } else if (results.rows.length != 0 && type == 7) {
                console.log('student');
                $('#graduateLevel').val(results.rows[0].EduLevel);
                $('#academy').val(results.rows[0].Academy);
                $('#endyear').val(results.rows[0].EduYear);
                $("#province_academy").val(results.rows[0].EduP);
                autoLoadCityAndTumbon('person_type', 'tumbon_academy', 'distint_academy', results.rows[0].EduT, results.rows[0].EduC, results.rows[0].EduP);

                $('#idParent').val(results.rows[0].PerPID);
                $("#relatedPr").val(results.rows[0].ParentRole);
                $('#titleParent').val(results.rows[0].Title);
                $('#nameParent').val(results.rows[0].FirstName);
                $("#surnameParent").val(results.rows[0].LastName);
                $("#telPr").val(results.rows[0].PerPhone);

                $('#HouseNumberPr').val(results.rows[0].HouseNumber);
                $('#MooNumberPr').val(results.rows[0].MooNumber);
                $('#VillageNamePr').val(results.rows[0].VillageName);
                $("#AlleyPr").val(results.rows[0].Alley);
                $("#StreetPr").val(results.rows[0].StreetName);
                $("#provincePr").val(results.rows[0].AddrP);
                autoLoadCityAndTumbon('person_type', 'tumbonPr', 'distinctPr', results.rows[0].AddrT, results.rows[0].AddrC, results.rows[0].AddrP);
                $('#numpostPr').val(results.rows[0].PerPostCode);

                $('#prAddrID').val(results.rows[0].AddressID);

            }

        }), errorCB);
    }, errorCB, function () {
        stopAjaxLoader();
    });

}



function insertCity(provinceID, dropdownID) {

    var queryGetCityByProvince = "SELECT C.CityID, C.CityDescription "
        + "FROM City C INNER JOIN Province P ON C.ProvinceID = P.ProvinceID "
        + "WHERE P.ProvinceID = '" + provinceID + "' ";

    db.transaction(function (tx) {
        tx.executeSql(queryGetCityByProvince, [], (function (tx, response) {

            if (response.rows.length != 0) {
                for (var i = 0; i < response.rows.length ; i++) {
                    $('#' + dropdownID + '').append($("<option></option>").val(response.rows[i].CityID).html(response.rows[i].CityDescription));
                }
            }

        }), errorCB);
    }, errorCB, function () {
        $('#' + dropdownID + '').selectmenu("refresh");
    });
}

function insertTumbon(cityID, dropdownID) {

    var queryGetCityByProvince = "SELECT T.TumbonID, T.TumbonDescription "
        + "FROM Tumbon T INNER JOIN City C ON T.CityID = C.CityID "
        + "WHERE C.CityID = '" + cityID + "' ";

    db.transaction(function (tx) {
        tx.executeSql(queryGetCityByProvince, [], (function (tx, response) {

            if (response.rows.length != 0) {
                for (var i = 0; i < response.rows.length ; i++) {

                    $('#' + dropdownID + '').append($("<option></option>").val(response.rows[i].TumbonID).html(response.rows[i].TumbonDescription));
                }
            }

        }), errorCB);
    }, errorCB, function () {
        $('#' + dropdownID + '').selectmenu("refresh");
    });

}

function autoLoadCityAndTumbon(page, tumbonTB, cityTB, tumbonVal, cityVal, provinceVal) {
    //console.log(page);
    db.transaction(function (tx) {
        tx.executeSql("SELECT CityID, CityDescription FROM City WHERE ProvinceID = '" + provinceVal + "' ", [],
            function (tx, result) {
                if (result.rows.length != 0) {
                    for (var i = 0; i < result.rows.length ; i++) {
                        $('#' + cityTB).append($("<option></option>").val(result.rows[i].CityID).html(result.rows[i].CityDescription));
                    }
                }
            }, errorCB);
    }, errorCB, function () {
        $('#' + cityTB).val(cityVal);

        db.transaction(function (tx) {
            tx.executeSql("SELECT TumbonID, TumbonDescription FROM Tumbon WHERE CityID = '" + cityVal + "' ", [],
                function (tx, result) {
                    if (result.rows.length != 0) {
                        for (var i = 0; i < result.rows.length ; i++) {
                            $('#' + tumbonTB).append($("<option></option>").val(result.rows[i].TumbonID).html(result.rows[i].TumbonDescription));
                        }
                    }
                }, errorCB);
        }, errorCB, function () {
            //console.log(page);
            $('#' + tumbonTB).val(tumbonVal);
            //$('#' + page + ' select').selectmenu('refresh');

            if (page == "address_info") {
                //$('#' + page + ' select').selectmenu('refresh'); // edit some error on load data parent
                $('#' + page + ' select').selectmenu().selectmenu('refresh', true);
            }
            else { $('#' + page + ' select').selectmenu('refresh'); }

        });
    });
}

function replaceNull(pageID) {
    $('#' + pageID + ' input[type=text]').each(function () {
        if ($(this).val() == "" || $(this).val() == "undefined") {
            $(this).val("-");
        }
        else {
            //inputNotNull = true;            
        }
    });

    $('#' + pageID + ' input[type=tel], input[type=number]').each(function () {
        if ($(this).val() == "" || $(this).val() == "undefined") {
            $(this).val("0");
        }
        else {
            //inputNotNull = true;            
        }
    });


    $('#' + pageID + ' select').each(function () {

        if ($(this).val() == "" || $(this).val() == null || $(this).val() == "null") {
            $(this).empty();
            $(this).append($("<option></option>").val("0000").html('ไม่ระบุ')).selectmenu('refresh');
            //console.log($(this).val());
        }
        else {
            //inputNotNull = true;            
        }
    });
}

function imgError(image) {
    console.log("err");
    image.onerror = "";
    image.src = "img/avatar-placeholder.png";
    return true;
}

function startAjaxLoader() {
    if ($('#ajaxLoadScreen') != null) {
        var loader = '<div id="ajaxLoadScreen"><img id="loadingImg" src="img/ajax-loader-bar.gif"></div>';
        $(loader).appendTo('body');
    }
}

function stopAjaxLoader() {
    $('#ajaxLoadScreen').remove();
}

function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}

function fillZero(num, type) {
    var response = "";
    if (num >= 0 && num < 10 && type == 1) {
        response = "0" + num;
    }
    else if (num >= 0 && num < 10 && type == 2) {
        response = "00" + num;
    }
    else if (num >= 10 && num < 100 && type == 2) {
        response = "0" + num;
    }
    else {
        response = num;
    }
    return response;
}

function getCurentDateTime() {
    var d = new Date();
    var currentDateTime = d.getFullYear() + '-' + fillZero(d.getMonth(), 1) + '-' + fillZero(d.getDate(), 1) + " " + fillZero(d.getHours(), 1) + ":" + fillZero(d.getMinutes(), 1) + ":" + fillZero(d.getSeconds(), 1) + "." + fillZero(d.getMilliseconds(), 2);
    return currentDateTime;
}

function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

function maxLengthPostCode(object) {
    if (object.value.length > 5)
        object.value = object.value.slice(0, 5)
}

function lvCreate(page) {

    var selectID = [];

    //$('#' + page + ' .add-filter').each(function () {
    $('.add-filter').each(function () {
        //console.log($(this).attr('id'));
        selectID.push($(this).attr('id'));
    });

    for (var i = 0; i < selectID.length; i++) {
        addFilterToSelect(selectID[i]);
    }

}

function addFilterToSelect(selectID) {
    $.mobile.document
        .on("listviewcreate", "#" + selectID + "-menu", function (e) {
            var input,
                listbox = $("#" + selectID + "-listbox"),
                form = listbox.jqmData("filter-form"),
                listview = $(e.target);
            if (!form) {
                input = $("<input data-type='search'></input>");
                form = $("<form></form>").append(input);
                input.textinput();
                $("#" + selectID + "-listbox")
                    .prepend(form)
                    .jqmData("filter-form", form);
            }
            listview.filterable({ input: input });
        })
        .on("pagebeforeshow pagehide", "#" + selectID + "-dialog", function (e) {
            var form = $("#" + selectID + "-listbox").jqmData("filter-form"),
                placeInDialog = (e.type === "pagebeforeshow"),
                destination = placeInDialog ? $(e.target).find(".ui-content") : $("#" + selectID + "-listbox");
            form
                .find("input")
                .textinput("option", "inset", !placeInDialog)
                .end()
                .prependTo(destination);
        });
}

