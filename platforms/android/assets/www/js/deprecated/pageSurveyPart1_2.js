﻿var db = window.openDatabase("Database", "1.0", "Survey", 200000);

function errorCB(err) {
    alert("Error processing SQL: " + err.code);
}

function successCB() {
    //alert("success!");
}

$('#jobDIV').hide();
$('#studentDIV').hide();

var provinceDropdown = ["provinceStudent", "provincePr", "province_sp1_2"];
for (i = 0; i < provinceDropdown.length; i++) {
    provinceService(provinceDropdown[i]);
}

function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

function personalDD() {
    var txtPersonal = $("#personal_sp1_2 :selected").val();

    if (txtPersonal == '001')
    {
        //console.log('Student');
        $('#jobDIV').hide();
        $('#studentDIV').show();

    }
    else if (txtPersonal == '000')
    {
        //console.log('Personal = ' + txtPersonal);
        $('#jobDIV').hide();
        $('#studentDIV').hide();
    }
    else
    {
        //console.log('Personal = ' + txtPersonal);
        $('#jobDIV').show();
        $('#studentDIV').hide();
    }
    
}

function submitBtnSP1_2() {
    var txtPersonal = $("#personal_sp1_2 :selected").val();
    txtIDNumber = $('#IDNumber_sp1_1').val();

    if (txtPersonal == '001') {

        //var academyID = guidGenerator();

        var Student = {
            eduStatus: "001",
            eduLevel: $('#educateLevel').val(),
            academy: $('#academyStudent').val(),
            //tumbonStudent: $('#tumbonStudent').val(),
            //distintStudent: $('#distintStudent').val(),
            //provinceStudent: $('#provinceStudent').val(),
            eduYear: $('#termStudent').val()
        };

        var Parent = {
            idParent: $('#idParent').val(),
            nameParent: $('#nameParent').val(),
            surnameParent: $('#surnameParent').val(),

            addressParent: $('#addressParent').val(),
            provincePr: $('#provincePr').val(),
            distinctPr: $('#distinctPr').val(),
            tumbonPr: $('#tumbonPr').val(),
            numpostPr: $('#numpostPr').val(),
            telPr: $('#telPr').val(),

            relatedPr: $('#relatedPr').val()
        };                

        //--------- อัพเดทตาราง Person ว่าเป็น นักเรียน --------------
        db.transaction(function (tx) {
            tx.executeSql('UPDATE Person SET PersonalType = "' + txtPersonal + '" WHERE PID = ' + txtIDNumber + ' ');
        }, errorCB, successCB);
        //-----------------------------------------     

        // เพิ่มข้อมูลที่อยู่โรงเรียน
        //db.transaction(function (tx) {
        //    tx.executeSql('INSERT INTO Address ( AddressID, AddressType, AddressData, Tumbon, Amphor, Province )'
        //        + ' VALUES ( "' + academyID + '", "006", "' + Student.academyStudent + '", "' + Student.tumbonStudent + '", "' + Student.distintStudent + '", "' + Student.provinceStudent + '" )');
        //}, errorCB, successCB);

        // INSERT ข้อมูลลงใน PersonEducation
        db.transaction(function (tx) {
            tx.executeSql('INSERT INTO PersonEducation ( PID, EduStatus, EduLevel, EduYear, Academy )'
                + ' VALUES ( ' + txtIDNumber + ', "' + Student.eduStatus + '", "' + Student.eduLevel + '", "' + Student.eduYear + '", "' + Student.academy + '"  )');
        }, errorCB, successCB);

        /*
        var chk = document.getElementById("informantCB_sp1_2").checked;
        if (chk == false) {

            // เพิ่มข้อมูลผู้ปกครอง
            db.transaction(function (tx) {
                tx.executeSql('INSERT INTO Person ( PID, FirstName, LastName, RecordStaffPID )'
                    + ' VALUES ( ' + Parent.idParent + ', "' + Parent.nameParent + '", "' + Parent.surnameParent + '", ' + $("#staIDNumber").val() + ' )');
            }, errorCB, successCB);

            var ParentAddressID = guidGenerator();

            //เพิ่ม address ของผู้ปกครอง
            db.transaction(function (tx) {
                tx.executeSql('INSERT INTO Address ( AddressID, AddressData, Tumbon, Amphor, Province, Postcode, Phone )'
                    + ' VALUES ( "' + ParentAddressID + '", "' + Parent.addressParent + '", "' + Parent.tumbonPr + '", "' + Parent.distinctPr + '", "' + Parent.provincePr + '", "' + Parent.numpostPr + '", "' + Parent.telPr + '" )');
            }, errorCB, successCB);

            // สร้างความเกี่ยวข้องระหว่าง ผู้ปกครอง กับ ที่อยู่
            db.transaction(function (tx) {
                tx.executeSql('INSERT INTO PersonVSAddress ( PID, AddressID )'
                    + ' VALUES ( ' + Parent.idParent + ', "' + ParentAddressID + '" )');
            }, errorCB, successCB);

        } else {
            console.log("ใช้ผู้ให้ข้อมูลเป็นผู้ปกครอง");
        }
        */

    }
    else if (txtPersonal == '000') {
        console.log("กรุณาเลือกประเถทบุคคล");
    }
    else {

        var Job = {
            occupation: $('#occupation_sp1_2').val(),
            position: $('#position_sp1_2').val(),
            office: $('#office_sp1_2').val(),
            //tumbon: $('#tumbon_sp1_2').val(),
            //distint: $('#distint_sp1_2').val(),
            //province: $('#province_sp1_2').val(),
            //numpost: $('#numpost_sp1_2').val(),
            //tel: $('#tel_sp1_2').val()
        }; // insert to PersonJobDetail    

        var Finance = {
            income: $('#income5').val(),
            outcome: $('#expense5').val(),
            inDept: $('#indept').val(),
            exDept: $('#exdept').val()
        }; // insert to PersonFinamce

        var Edu = {
            eduStatus: "002",
            eduLevel: $('#graduateLevel').val(),
            academy: $('#academy').val(),
            eduYear: $('#endyear').val()
            //qualification: $('#qualification').val(),
        }; // insert to PersonEducation

        var Bank = {
            name: $('#account').val(),
            branch: $('#branch').val(),
            accountNum: $('#accountNum').val(),
            accountName: $('#accountName').val()
        }; // insert to PersonBankAccount

        //--------- อัพเดท PersonType ตาราง Person --------------
        db.transaction(function (tx) {
            tx.executeSql('UPDATE Person SET PersonalType = "' + txtPersonal + '" WHERE PID = ' + txtIDNumber + ' ');
        }, errorCB, successCB);

        //เพิ่ม office_address
        //var OfficeID = guidGenerator();

        //db.transaction(function (tx) {
        //    tx.executeSql('INSERT INTO Address ( AddressID, AddressType, AddressData, Tumbon, Amphor, Province, Postcode, Phone )'
        //        + ' VALUES ( "' + OfficeID + '", "005", "' + Job.office + '", "' + Job.tumbon + '", "' + Job.distint + '", "' + Job.province + '", ' + Job.numpost + ', ' + Job.tel + ' )');
        //}, errorCB, successCB);        

        //เพิ่ม Job Detail
        db.transaction(function (tx) {
            tx.executeSql('INSERT INTO PersonJobDetail ( PID, Position, Office )'
                + ' VALUES ( ' + txtIDNumber + ', "' + Job.position + '", "' + Job.office + '" )');
        }, errorCB, successCB);

        //เพิ่ม Person Education
        db.transaction(function (tx) {
            tx.executeSql('INSERT INTO PersonEducation ( PID, EduStatus, EduLevel, EduYear, Academy )'
                + ' VALUES ( ' + txtIDNumber + ', "' + Edu.eduStatus + '", "' + Edu.eduLevel + '", "' + Edu.eduYear + '", "' + Edu.academy + '" )');
        }, errorCB, successCB);       

        //เพิ่ม การเงิน
        db.transaction(function (tx) {
            tx.executeSql('INSERT INTO PersonFinance ( PID, Income, Outgoing, InDebt, ExDebt )'
                + ' VALUES ( ' + txtIDNumber + ', ' + Finance.income + ', ' + Finance.outcome + ', ' + Finance.inDept + ', ' + Finance.exDept + ' )');
        }, errorCB, successCB);

        //เพิ่ม บัญชีธนาคาร
        db.transaction(function (tx) {
            tx.executeSql('INSERT INTO PersonBankAccount ( PID, BankName, BankBranch, AccountNo, AccountName )'
                + ' VALUES ( ' + txtIDNumber + ', "' + Bank.name + '", "' + Bank.branch + '", "' + Bank.accountNum + '", "' + Bank.accountName + '" )');
        }, errorCB, successCB);

    }
}

$("#informantCB_sp1_2").change(function () {    
    var chk = document.getElementById("informantCB_sp1_2").checked;
    if (chk == true) {
        if ($('#informantDIV_sp1_2').is(':visible') ) {
            $('#informantDIV_sp1_2').hide();
            $('#idParent').val($('#IDNumber').val());
        }

    } else {

        if ($('#informantDIV_sp1_2').is(':hidden') ) {
            $('#informantDIV_sp1_2').show();
            $('#idParent').val("");
        }
    }
});


/*
function addProvince(dropdownID, province) {
    $('#' + dropdownID + '').empty();
    $('#' + dropdownID + '').append($("<option></option>").val(000).html('ไม่ระบุ'));
    for (var i = 0; i < province.length ; i++) {
        $('#' + dropdownID + '').append($("<option></option>").val(province[i]['provinceID']).html(province[i]['provinceDes']));
    }
}

function addCity(dropdownID, city) {
    $('#' + dropdownID + '').empty();
    $('#' + dropdownID + '').append($("<option></option>").val(000).html('ไม่ระบุ'));
    for (var i = 0; i < city.length ; i++) {
        $('#' + dropdownID + '').append($("<option></option>").val(city[i]['cityID']).html(city[i]['cityDes']));
    }
    $('#' + dropdownID + '').selectmenu("refresh");
}

function addTumbon(dropdownID, tumbon) {
    $('#' + dropdownID + '').empty();
    $('#' + dropdownID + '').append($("<option></option>").val(000).html('ไม่ระบุ'));
    for (var i = 0; i < tumbon.length ; i++) {
        $('#' + dropdownID + '').append($("<option></option>").val(tumbon[i]['tumbonID']).html(tumbon[i]['tumbonDes']));
    }
    $('#' + dropdownID + '').selectmenu("refresh");
}

function provinceService(dropdownID) {
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/getProvinceList",
        dataType: 'jsonp',
        success: function (msg) {
            var a = JSON.parse(msg);
            //console.log(a);
            addProvince(dropdownID, a);
        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
}

function cityByProvince(provinceID, dropdownID) {
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/getCityListFromProvince?provinceID="+provinceID+"",
        dataType: 'jsonp',
        success: function (msg) {
            var a = JSON.parse(msg);
            //console.log(a);
            addCity(dropdownID, a);
        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
}

function tumbonByCity(cityID, dropdownID) {
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/getTumbonListFromCity?cityID=" + cityID + "",
        dataType: 'jsonp',
        success: function (msg) {
            var a = JSON.parse(msg);
            //console.log(a);
            addTumbon(dropdownID, a);
        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
}

//onchange ของ input จังหวัด -> ทำให้ได้ข้อมูล อำเถอ
function addCityStudent_sp1_2() {
    var txtProvince = $("#provinceStudent :selected").val();    
    cityByProvince(txtProvince, 'distintStudent');
}
function addCityPr_sp1_2() {
    var txtProvince = $("#provincePr :selected").val();
    cityByProvince(txtProvince, 'distinctPr');
}
function addCityJob_sp1_2() {
    var txtProvince = $("#province_sp1_2 :selected").val();
    cityByProvince(txtProvince, 'distint_sp1_2');
}
//---------------------

//onchange ของ input อำเภอ -> ทำให้ได้ข้อมูล ตำบล
function addTumbonStudent_sp1_2() {
    var txtCity = $("#distintStudent :selected").val();
    tumbonByCity(txtCity, 'tumbonStudent');
}
function addTumbonPr_sp1_2() {
    var txtCity = $("#distinctPr :selected").val();
    tumbonByCity(txtCity, 'tumbonPr');
}
function addTumbonJob_sp1_2() {
    var txtCity = $("#distint_sp1_2 :selected").val();
    tumbonByCity(txtCity, 'tumbon_sp1_2');
}
//---------------------
*/