﻿var db = window.openDatabase("Database", "1.0", "Survey", 200000);

function errorCB(err) {
    alert("Error processing SQL: " + err.code);
}

function successCB() {
    //alert("success!");
}

function submitBtnSP1_3() {

    var Standin = {
        CID: $('#idAttorney').val(),
        firstName: $('#nameAttorney').val(),
        lastName: $('#surnameAttorney').val(),
        relation: $('#radio_relation').val(),

        address: $('#addressAtn').val(),
        province: $('#provinceAtn').val(),
        city: $('#distinctAtn').val(),
        tumbon: $('#tumbonAtn').val(),
        numPost: $('#numpostAtn').val(),
        tel: $('#telAtn').val()
    }; // insert to Person & Address (Standin)

    var Emergency = {
        CID: $('#idEmg').val(),
        firstName: $('#nameEmg').val(),
        lastName: $('#surnameEmg').val(),
        relation: $('#relatedEmg').val(),

        address: $('#addressEmg').val(),
        province: $('#provinceEmg').val(),
        city: $('#distinctEmg').val(),
        tumbon: $('#tumbonEmg').val(),
        numPost: $('#numpostEmg').val(),
        tel: $('#telEmg').val()
    }; // insert to Person & Address (Emergency Contact)

    var txtIDNumber = $('#IDNumber_sp1_1').val();
    var informantStandinChk = document.getElementById("informantStandIn_sp1_3").checked;
    var informantEmgChk = document.getElementById("informantEmerg_sp1_3").checked;
    
    if (Standin.CID != "") {

        var queryStr = "SELECT PID FROM Person WHERE PID = '" + Standin.CID + "' ";
        db.transaction(function (tx) {
            tx.executeSql(queryStr, [],
                (function (tx, response) {

                    if (response.rows.length == 0) {
                        // เพิ่มข้อมูลผู้รับอำนาจแทน
                        db.transaction(function (tx) {
                            tx.executeSql('INSERT INTO Person ( PID, FirstName, LastName, RecordStaffPID )'
                                + ' VALUES ( ' + Standin.CID + ', "' + Standin.firstName + '", "' + Standin.lastName + '", ' + $("#staIDNumber").val() + ' )');
                        }, errorCB, successCB);

                        var standInAddressID = guidGenerator();

                        //เพิ่ม address ของผู้รับอำนาจแทน
                        db.transaction(function (tx) {
                            tx.executeSql('INSERT INTO Address ( AddressID, AddressData, Tumbon, Amphor, Province, Postcode, Phone )'
                                + ' VALUES ( "' + standInAddressID + '", "' + Standin.address + '", "' + Standin.tumbon + '", "' + Standin.city + '", "' + Standin.province + '", "' + Standin.numPost + '", "' + Standin.tel + '" )');
                        }, errorCB, successCB);

                        // สร้างความเกี่ยวข้องระหว่าง ผู้รับอำนาจแทน กับ ที่อยู่
                        db.transaction(function (tx) {
                            tx.executeSql('INSERT INTO PersonVSAddress ( PID, AddressID )'
                                + ' VALUES ( ' + Standin.CID + ', "' + standInAddressID + '" )');
                        }, errorCB, successCB);
                    }
                }), errorCB, successCB);
        });

        // สร้างความเกี่ยวข้องของ ผู้รับอำนาจแทน กับ Person
        db.transaction(function (tx) {
            tx.executeSql('INSERT INTO PersonStandIn ( PID, StandIn, StandInRole )'
                + ' VALUES ( ' + txtIDNumber + ', ' + Standin.CID + ', "' + Standin.relation + '" )');
        }, errorCB, successCB);        

    } else if (Standin.CID == "" && informantStandinChk == true) {
        Standin.CID = $('#IDNumber').val();
        Standin.relation = $('#relation').val();

        // สร้างความเกี่ยวข้องของ ผู้รับอำนาจแทน กับ Person
        db.transaction(function (tx) {
            tx.executeSql('INSERT INTO PersonStandIn ( PID, StandIn, StandInRole )'
                + ' VALUES ( ' + txtIDNumber + ', ' + Standin.CID + ', "' + Standin.relation + '" )');
        }, errorCB, successCB);
    } else {
        console.log('ไม่ใส่ผู้รับอำนาจแทน');
    }

    //-----------------------------------------------

    if (Emergency.CID != "") {

        var queryStr = "SELECT PID FROM Person WHERE PID = '" + Emergency.CID + "' ";
        db.transaction(function (tx) {
            tx.executeSql(queryStr, [],
                (function (tx, response) {
                    if (response.rows.length == 0) {

                        // เพิ่มข้อมูลผู้ติดต่อฉุกเฉิน
                        db.transaction(function (tx) {
                            tx.executeSql('INSERT INTO Person ( PID, FirstName, LastName, RecordStaffPID )'
                                + ' VALUES ( ' + Emergency.CID + ', "' + Emergency.firstName + '", "' + Emergency.lastName + '", ' + $("#staIDNumber").val() + ' )');
                        }, errorCB, successCB);

                        var emergencyAddressID = guidGenerator();

                        //เพิ่ม address ของผู้ติดต่อฉุกเฉิน
                        db.transaction(function (tx) {
                            tx.executeSql('INSERT INTO Address ( AddressID, AddressData, Tumbon, Amphor, Province, Postcode, Phone )'
                                + ' VALUES ( "' + emergencyAddressID + '", "' + Emergency.address + '", "' + Emergency.tumbon + '", "' + Emergency.city + '", "' + Emergency.province + '", "' + Emergency.numPost + '", "' + Emergency.tel + '" )');
                        }, errorCB, successCB);

                        // สร้างความเกี่ยวข้องระหว่าง ผู้ติดต่อฉุกเฉิน กับ ที่อยู่
                        db.transaction(function (tx) {
                            tx.executeSql('INSERT INTO PersonVSAddress ( PID, AddressID )'
                                + ' VALUES ( ' + Emergency.CID + ', "' + emergencyAddressID + '" )');
                        }, errorCB, successCB);

                    }

                }), errorCB, successCB);
        });

        // สร้างความเกี่ยวข้องของ ผู้ติดต่อฉุกเฉิน กับ Person
        db.transaction(function (tx) {
            tx.executeSql('INSERT INTO PersonEmergencyContact ( PID, EmergencyContactPID, EmergencyContactRole )'
                + ' VALUES ( ' + txtIDNumber + ', ' + Emergency.CID + ', "' + Emergency.relation + '" )');
        }, errorCB, successCB);

    } else if (Emergency.CID == "" && informantEmgChk == true) {
        Emergency.CID = $('#IDNumber').val();
        Emergency.relation = $('#relation').val();

        // สร้างความเกี่ยวข้องของ ผู้ติดต่อฉุกเฉิน กับ Person
        db.transaction(function (tx) {
            tx.executeSql('INSERT INTO PersonEmergencyContact ( PID, EmergencyContactPID, EmergencyContactRole )'
                + ' VALUES ( ' + txtIDNumber + ', ' + Emergency.CID + ', "' + Emergency.relation + '" )');
        }, errorCB, successCB);

    } else {
        console.log('ไม่ใส่ผู้ติดต่อฉุกเฉิน');
    }

}

$("#informantStandIn_sp1_3").change(function () {
    var chk = document.getElementById("informantStandIn_sp1_3").checked;
    if (chk == true) {
        if ($('#standInDIV').is(':visible')) {
            $('#standInDIV').hide();
            $('#idAttorney').val($('#IDNumber').val());
        }

    } else {

        if ($('#standInDIV').is(':hidden')) {
            $('#standInDIV').show();
            $('#idAttorney').val("");
        }
    }
});

$("#informantEmerg_sp1_3").change(function () {
    var chk = document.getElementById("informantEmerg_sp1_3").checked;
    if (chk == true) {
        if ($('#EmergencyDIV').is(':visible')) {
            $('#EmergencyDIV').hide();
            $('#idEmg').val($('#IDNumber').val());
        }

    } else {

        if ($('#EmergencyDIV').is(':hidden')) {
            $('#EmergencyDIV').show();
            $('#idEmg').val("");
        }
    }
});

function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}
