﻿var db;

function init() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
}

function onDeviceReady() {
    db = window.openDatabase("Database", "1.0", "Survey", 200000);
    db.transaction(populateDB, errorCB, successCB);
}

function populateDB(tx) {
    /*
    tx.executeSql("DROP TABLE IF EXISTS Person");
    tx.executeSql("DROP TABLE IF EXISTS Staff");
    tx.executeSql("DROP TABLE IF EXISTS Address");
    tx.executeSql("DROP TABLE IF EXISTS PersonVSAddress");    
    tx.executeSql("DROP TABLE IF EXISTS PersonJobDetail");

    tx.executeSql("DROP TABLE IF EXISTS PersonFinance");
    tx.executeSql("DROP TABLE IF EXISTS PersonBankAccount");
    tx.executeSql("DROP TABLE IF EXISTS PersonEducation");
    tx.executeSql("DROP TABLE IF EXISTS PersonRecordBy");
    tx.executeSql("DROP TABLE IF EXISTS PersonStandIn");    

    //<--------------- Dropdown Table --------------->
    tx.executeSql("DROP TABLE IF EXISTS Title");
    tx.executeSql("DROP TABLE IF EXISTS Province");
    tx.executeSql("DROP TABLE IF EXISTS MariageStatus");
    tx.executeSql("DROP TABLE IF EXISTS Nationality");
    tx.executeSql("DROP TABLE IF EXISTS Race");

    tx.executeSql("DROP TABLE IF EXISTS Religion");
    tx.executeSql("DROP TABLE IF EXISTS Relation");
    tx.executeSql("DROP TABLE IF EXISTS EducationLevel");
    tx.executeSql("DROP TABLE IF EXISTS Occupation");
    tx.executeSql("DROP TABLE IF EXISTS LiveHomeStatus");    
    //<---------------------------------------------->
    */


    /*
    tx.executeSql("DROP TABLE IF EXISTS PersonEmergencyContact");
    tx.executeSql("DROP TABLE IF EXISTS PersonMate");
    tx.executeSql("DROP TABLE IF EXISTS PersonFamily");
    */

    tx.executeSql("CREATE TABLE IF NOT EXISTS Person ("
        + " PID INT PRIMARY KEY, Title VARCHAR(4), FirstName VARCHAR(50), LastName VARCHAR(50), Phone VARCHAR(20), Passport VARCHAR(50),"
        + " DOB DATE, Nation VARCHAR(50), Race VARCHAR(50), Religion VARCHAR(10), MarriageStatus VARCHAR(10),"
        + " AddressSatisfy VARCHAR(3), BirthProvince VARCHAR(3), PersonalType VARCHAR(3), RecordStaffPID INT"
        + " )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS Staff ("
        + " StaffID INT PRIMARY KEY, Password VARCHAR(50),Title VARCHAR(4), FirstName VARCHAR(50), LastName VARCHAR(50),"
        + " Phone VARCHAR(20) ,Job VARCHAR(4), Position VARCHAR(50)"
        + " )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS Address ("
        + " AddressID VARCHAR(50) PRIMARY KEY, AddressType VARCHAR(4), AddressData VARCHAR(255), Tumbon VARCHAR(50),"
        + " Amphor VARCHAR(50), Province VARCHAR(3), Postcode INT, HomeCode INT"
        + " )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS Title ("
        + " TitleID VARCHAR(4) PRIMARY KEY, TitleDESC VARCHAR(50)"
        + " )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonVSAddress ( PID INT PRIMARY KEY, AddressID VARCHAR(50) )");    
    
    tx.executeSql("CREATE TABLE IF NOT EXISTS Province ("
        + " ProvinceID VARCHAR(3) PRIMARY KEY, ProvinceDescription VARCHAR(150), Latitude float(53), Longtitude float(53)"
        + " )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS MariageStatus ( MariageStatusID VARCHAR(10) PRIMARY KEY, MariageStatusDescription VARCHAR(255) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS Nationality ( NationalityID VARCHAR(50) PRIMARY KEY, NationalityDescription VARCHAR(255), Zone NVARCHAR(50) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS Race ( RaceID VARCHAR(50) PRIMARY KEY, RaceDescription NVARCHAR(255), Zone NVARCHAR(50) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS Religion ( Id VARCHAR(10) PRIMARY KEY, Religion NVARCHAR(50) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS Relation ( RelationID VARCHAR(4) PRIMARY KEY, RelationDescription VARCHAR(50) )");    

    tx.executeSql("CREATE TABLE IF NOT EXISTS Occupation ( OccupationID VARCHAR(4) PRIMARY KEY, OccupationDescription VARCHAR(255) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS LiveHomeStatus ( LiveHomeStatusID VARCHAR(4) PRIMARY KEY, LiveHomeStatusDescription VARCHAR(50) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS EducationLevel ( EducationLevelID VARCHAR(10) PRIMARY KEY, EducationLevelDescription VARCHAR(255) )");

    //tx.executeSql("CREATE TABLE IF NOT EXISTS PersonAsStudent ( PID INT PRIMARY KEY, CurrentEduLevel varchar(50), AcademyID varchar(50), SchoolYear INT, ParentID INT, ParentRelation varchar(50) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonJobDetail ( PID INT PRIMARY KEY, Position varchar(50), Office varchar(50) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonFinance ( PID INT PRIMARY KEY, Income INT, Outgoing INT, InDebt int, ExDebt int )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonBankAccount ( PID INT PRIMARY KEY, BankName varchar(50), BankBranch varchar(50), AccountNo varchar(50), AccountName varchar(50) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonEducation ( PID INT PRIMARY KEY, EduStatus varchar(50), EduLevel varchar(10), EduYear varchar(50), Academy varchar(50) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonRecordBy ( PID INT PRIMARY KEY, RecordPID INT, Relation varchar(4) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonStandIn ( PID INT PRIMARY KEY, StandIn INT, StandInRole varchar(50) )");

    //tx.executeSql("CREATE TABLE IF NOT EXISTS PersonEmergencyContact ( PID INT PRIMARY KEY, EmergencyContactPID INT, EmergencyContactRole varchar(50) )");

    //tx.executeSql("CREATE TABLE IF NOT EXISTS PersonFamily ( FamilyID INT, FamilyRole varchar(50), PID int )");

    insertTable(tx);
    //addListView();
}

function insertTable(tx) {

    db.transaction(function (tx) {
        tx.executeSql('SELECT * FROM Province', [],
            (function (tx, response) {
                if (response.rows.length == 0) {
                    console.log('insert');

                    titleService();
                    provinceService();
                    mariageService();
                    nationService();
                    raceService();
                    religionService();
                    relationService();
                    educationLevelService();
                    occupationService();
                    liveHomeStatusService();

                } else {
                    console.log('not insert');
                }
            }));
    });    

}

function addListView() {

    var queryStrID = "SELECT PID, FirstName FROM Person";
    $('#lvIDNumber').children().remove('li');
    db.transaction(function (tx) {
        tx.executeSql(queryStrID, [],
            (function (tx, response) {
                for (var i = 0; i < response.rows.length ; i++) {
                    //$('#IDNumber_datalist').append($("<option></option>").val(response.rows.item(i).PID).html(response.rows.item(i).FirstName));
                    $("#lvIDNumber").append($("<li>" + response.rows.item(i).PID + "</li>"));
                    $('#lvIDNumber').listview("refresh");

                    $("#lvIDNumber_sp1_1").append($("<li>" + response.rows.item(i).PID + "</li>"));
                    //$('#lvIDNumber_sp1_1').listview("refresh");
                }
            }), errorCB, successCB);
    });    

    var queryStaID = "SELECT StaffID, FirstName FROM Staff";
    $('#lvStaIDNumber').children().remove('li');
    db.transaction(function (tx) {
        tx.executeSql(queryStaID, [],
            (function (tx, response) {
                for (var i = 0; i < response.rows.length ; i++) {
                    $("#lvStaIDNumber").append($("<li>" + response.rows.item(i).StaffID + "</li>"));
                    
                    $("#lvStaIDNumber").listview("refresh");
                }
            }), errorCB, successCB);
    });    

}

function errorCB(err) {
    alert("Error processing SQL: " + err.code);
}

function successCB() {
    //alert("success!");
}

$(document).on("pagecreate", "#login", function (event) {
    insertTable();

});

/*
 * Save data from Survey Page to database (table: Person, Staff)
 */
function submitBtn() {

    // ผู้ให้ข้อมูล
    var txtTitle = $("#title").val();
    var txtFirstName = $("#firstName").val();
    var txtLastName = $("#lastName").val();
    var txtIDNumber = $("#IDNumber").val();
    //var txtIDNumber = document.getElementById("IDNumber").options[0].value;
    var txtAddress = $("#address").val();

    var txtTumbon = $("#tumbon").val();
    var txtAmphor = $("#amphor").val();
    var txtProvince = $("#province").val();
    var txtPostcode = $("#postcode").val();
    var txtTel = $("#tel").val();    
    //----------------------

    // เจ้าหน้าที่
    var staTitle = $("#staTitle").val();
    var staFirstName = $("#staFirstName").val();
    var staLastName = $("#staLastName").val();
    var staIDNumber = $("#staIDNumber").val();
    var staJob = $("#staJob").val();

    var staPosition = $("#staPosition").val();
    var staAddress = $("#staAddress").val();
    var staTumbon = $("#staTumbon").val();
    var staAmphor = $("#staAmphor").val();
    var staProvince = $("#staProvince").val();

    var staPhone = $("#staPhone").val();
    //---------------------

    //console.log(txtIDNumber);
    /*
    alert(
        //txtTitle + "\n" +
        //txtFirstName + "\n" +
        //txtLastName + "\n" +
        //txtIDNumber + "\n" +
        //txtAddress + "\n" +

        //txtTumbon + "\n" +
        //txtAmphor + "\n" +
        //txtProvince + "\n" +
        //txtPostcode + "\n" +
        //txtTel + "\n" +        

        //$("#staTitle").val() + "\n" +
        //$("#staFirstName").val() + "\n" +
        //$("#staLastName").val() + "\n" +
        //$("#staIDNumber").val() + "\n" +
        //$("#staJob").val() + "\n" +

        //$("#staPosition").val() + "\n" +
        //$("#staAddress").val() + "\n" +
        //$("#staTumbon").val() + "\n" +
        //$("#staAmphor").val() + "\n" +
        //$("#staProvince").val() + "\n" +

        //$("#staPhone").val()

        );
        */

    var queryStr = "SELECT PID FROM Person WHERE PID = '" + txtIDNumber + "' ";
    db.transaction(function (tx) {
        tx.executeSql(queryStr, [],
            (function (tx, response) {
                if (response.rows.length == 0 && txtIDNumber != "") {
                    console.log("ยังไม่มีข้อมูลคนนี้");

                    //กรอกข้อมูลผู้ให้ข้อมูล
                    db.transaction(function (tx) {
                        tx.executeSql('INSERT INTO Person ( PID, Title, FirstName, LastName, RecordStaffPID )'
                            + ' VALUES ( ' + txtIDNumber + ', "' + txtTitle + '", "' + txtFirstName + '", "' + txtLastName + '", ' + staIDNumber + ' )');
                    }, errorCB, successCB);

                    var AddressID = guidGenerator();

                    //หา AddressID อันสุดท้าย เพื่อที่จะเอามา +1
                    //var AddressID;
                    //db.transaction(function (tx) {
                    //    tx.executeSql('SELECT AddressID FROM Address', [],
                    //        (function (tx, response) {
                    //            if (response.rows.length == 0) {
                    //                var i = response.rows.length;
                    //                i++;
                    //                AddressID = i;
                    //            } else {
                    //                var i = response.rows.length - 1;
                    //                AddressID = response.rows.item(i).AddressID + 1;
                    //            }
                    //        }), errorCB, successCB);
                    //});

                    // สร้างความเกี่ยวข้องระหว่าง บุคคล กับ ที่อยู่
                    db.transaction(function (tx) {
                        tx.executeSql('INSERT INTO PersonVSAddress ( PID, AddressID )'
                            + ' VALUES ( ' + txtIDNumber + ', "' + AddressID + '" )');
                    }, errorCB, successCB);
                    
                    //เพิ่ม address ของผู้ให้ข้อมูล
                    db.transaction(function (tx) {
                        tx.executeSql('INSERT INTO Address ( AddressID, AddressType, AddressData, Tumbon, Amphor, Province, Postcode, Phone )'
                            + ' VALUES ( "' + AddressID + '", "000", "' + txtAddress + '", "' + txtTumbon + '", "' + txtAmphor + '", "' + txtProvince + '", ' + txtPostcode + ', ' + txtTel + ' )');
                    }, errorCB, successCB);


                } else if (txtIDNumber != "" && response.rows.length != 0) {

                    var r = confirm("พบเลขบัตรประชาชนนี้แล้ว \nต้องการอัพเดทข้อมูล หรือไม่");
                    if (r == true) {

                        //--------- อัพเดทตาราง Person --------------
                        db.transaction(function (tx) {
                            tx.executeSql('UPDATE Person SET Title = "' + txtTitle + '", FirstName = "' + txtFirstName + '", LastName = "' + txtLastName + '", RecordStaffPID = ' + staIDNumber + ' WHERE PID = ' + txtIDNumber + ' ');
                        }, errorCB, successCB);
                        //-----------------------------------------

                        //--------- อัพเดทตาราง Address --------------
                        var AddressID;
                        db.transaction(function (tx) { //หา AddressID ของ Person
                            tx.executeSql('SELECT A.AddressID FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = ' + txtIDNumber + ' ', [],
                                (function (tx, response) {
                                    AddressID = response.rows.item(0).AddressID;
                                }), errorCB);
                        });

                        db.transaction(function (tx) {
                            tx.executeSql('UPDATE Address SET AddressData = "' + txtAddress + '", Tumbon = "' + txtTumbon + '", Amphor = "' + txtAmphor + '", Province = "' + txtProvince + '", Postcode = ' + txtPostcode + ', Phone = ' + txtTel + ' WHERE AddressID = "' + AddressID + '" ');
                        }, errorCB, successCB);
                        //-----------------------------------------

                        console.log("อัพเดท");
                    } else {
                        console.log("ยกเลิก");
                    }

                } else {
                    console.log("ยังไม่ได้กรอกเลขบัตรประชาชน");
                }
            }), errorCB, successCB);
    });

    //-------------------------------------------

    var queryStrSta = "SELECT StaffID FROM Staff WHERE StaffID = '" + staIDNumber + "' ";
    db.transaction(function (tx) {
        tx.executeSql(queryStrSta, [],
            (function (tx, response) {
                if (response.rows.length == 0 && staIDNumber != "") {
                    console.log("ยังไม่มีข้อมูลเจ้าหน้าที่คนนี้");

                    //กรอกข้อมูลเจ้าหน้าที่
                    db.transaction(function (tx) {
                        tx.executeSql('INSERT INTO Staff ( StaffID, Title, FirstName, LastName, Job, Position )'
                            + ' VALUES ( ' + $("#staIDNumber").val() + ', "' + $("#staTitle").val() + '", "' + $("#staFirstName").val() + '", "' + $("#staLastName").val() + '", "' + $("#staJob").val() + '", "' + $("#staPosition").val() + '" )');
                    }, errorCB, successCB);

                    var AddressID = guidGenerator();

                    //หา AddressID อันสุดท้าย เพื่อที่จะเอามา +1
                    //var AddressID;
                    //db.transaction(function (tx) {
                    //    tx.executeSql('SELECT AddressID FROM Address', [],
                    //        (function (tx, response) {
                    //            if (response.rows.length == 0) {
                    //                var i = response.rows.length;
                    //                i++;
                    //                AddressID = i;
                    //            } else {
                    //                var i = response.rows.length - 1;
                    //                AddressID = response.rows.item(i).AddressID + 1;
                    //            }
                    //        }), errorCB, successCB);
                    //});

                    // สร้างความเกี่ยวข้องระหว่าง เจ้าหน้าที่ กับ ที่ทำงาน
                    db.transaction(function (tx) {
                        tx.executeSql('INSERT INTO PersonVSAddress ( PID, AddressID )'
                            + ' VALUES ( ' + staIDNumber + ', "' + AddressID + '" )');
                    }, errorCB, successCB);

                    //เพิ่ม office ของเจ้าหน้าที่
                    db.transaction(function (tx) {
                        tx.executeSql('INSERT INTO Address ( AddressID, AddressType, AddressData, Tumbon, Amphor, Province, Postcode, Phone )'
                            + ' VALUES ( "' + AddressID + '", "005", "' + staAddress + '", "' + staTumbon + '", "' + staAddress + '", "' + staProvince + '", ' + 000000 + ', ' + staPhone + ' )');
                    }, errorCB, successCB);

                } else if (response.rows.length != 0 && staIDNumber != "") {

                    var r = confirm("พบเลขบัตรประชาชนนี้แล้ว \nต้องการอัพเดทข้อมูล หรือไม่");
                    if (r == true) {

                        //--------- อัพเดทตาราง Staff ---------------
                        db.transaction(function (tx) {
                            tx.executeSql('UPDATE Staff SET Title = "' + staTitle + '", FirstName = "' + staFirstName + '", LastName = "' + staLastName + '", Job = "' + staJob + '", Position = "' + staPosition + '" WHERE StaffID = ' + staIDNumber + ' ');
                        }, errorCB, successCB);
                        //-----------------------------------------

                        //--------- อัพเดทตาราง Address --------------
                        var AddressID;
                        db.transaction(function (tx) { //หา AddressID ของ Person
                            tx.executeSql('SELECT A.AddressID FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = ' + staIDNumber + ' ', [],
                                (function (tx, response) {
                                    AddressID = response.rows.item(0).AddressID;
                                }), errorCB);
                        });                       

                        db.transaction(function (tx) {
                            tx.executeSql('UPDATE Address SET AddressData = "' + staAddress + '", Tumbon = "' + staTumbon + '", Amphor = "' + staAmphor + '", Province = "' + staPhone + '", Postcode = ' + 0 + ', Phone = ' + staPhone + ' WHERE AddressID = "' + AddressID + '" ');
                        }, errorCB, successCB);
                        //-----------------------------------------

                        console.log("อัพเดท");
                    } else {
                        console.log("ยกเลิก");
                    }

                } else {
                    console.log("ยังไม่ได้กรอกเลขบัตรประชาชน");
                }
            }), errorCB, successCB);
    });

}

/*
$("#IDNumber").on('input', function () { //ใส่ข้อมูลอัติโนมัติ หลังจากที่เลือก IDNumber_datalist
    clearData();
    var val = this.value;
    if ($('#IDNumber_datalist option').filter(function () { return this.value === val;}).length) {        
        console.log(this.value);
    }
});
*/

function clearData() {
    if ($('#lvIDNumber').is(':hidden')) {
        $('#lvIDNumber').show();
    }
    var checkNull;
    checkNull = $('#IDNumber').val();
    if (checkNull == "") {
        $('#title').val("");
        $('#firstName').val("");
        $('#lastName').val("");       

        $('#address').val("");
        $('#tumbon').val("");
        $('#amphor').val("");
        $('#province').val("");
        $('#postcode').val("");
        $('#tel').val("");
    }
}

function clearStaData() {
    //$('#lvStaIDNumber').show();
    if ($('#lvStaIDNumber').is(':hidden')) {
        $('#lvStaIDNumber').show();
    }
    var checkNull;
    checkNull = $('#staIDNumber').val();
    if (checkNull == "") {
        $('#staTitle').val("");
        $('#staFirstName').val("");
        $('#staLastName').val("");
        $('#staJob').val("");
        $('#staPosition').val("");

        $('#staAddress').val("");
        $('#staTumbon').val("");
        $('#staAmphor').val("");
        $('#staProvince').val("");
        $('#staPhone').val("");
    }
}

$('#lvIDNumber').on('click', 'li', function () {
    //alert("Works"); // id of clicked li by directly accessing DOMElement property
    //console.log($(this).text());
    var val = $(this).text();
    $('#IDNumber').val(val);
    $('#lvIDNumber').toggle();

    var txtIDNumber = $(this).text();
    //console.log(txtIDNumber);

    var queryStr = "SELECT Title, FirstName, LastName FROM Person WHERE PID = '" + txtIDNumber + "' ";
    db.transaction(function (tx) {
        tx.executeSql(queryStr, [],
            (function (tx, response) {
                //console.log(response.rows.item(0).Title);
                $('#title').val(response.rows.item(0).Title); $('#title').selectmenu("refresh");
                $('#firstName').val(response.rows.item(0).FirstName);
                $('#lastName').val(response.rows.item(0).LastName);
            }), errorCB, successCB);
    });

    var queryAddr = "SELECT AddressData, Tumbon, Amphor, Province, Postcode, Phone FROM Address INNER JOIN PersonVSAddress ON Address.AddressID = PersonVSAddress.AddressID WHERE PersonVSAddress.PID = '" + txtIDNumber + "' ";
    db.transaction(function (tx) {
        tx.executeSql(queryAddr, [],
            (function (tx, response) {
                //console.log(response.rows.item(0).AddressID);
                $('#address').val(response.rows.item(0).AddressData);
                $('#tumbon').val(response.rows.item(0).Tumbon);
                $('#amphor').val(response.rows.item(0).Amphor);
                $('#province').val(response.rows.item(0).Province); $('#province').selectmenu("refresh");
                $('#postcode').val(response.rows.item(0).Postcode);
                $('#tel').val(response.rows.item(0).Phone);
            }), errorCB, successCB);
    });

});

$('#lvStaIDNumber').on('click', 'li', function () {
    //alert("Works"); // id of clicked li by directly accessing DOMElement property
    //console.log($(this).text());
    var val = $(this).text();
    $('#staIDNumber').val(val);
    $('#lvStaIDNumber').toggle();

    var txtStaIDNumber = $(this).text();
    //console.log(txtStaIDNumber);

    var queryStr = "SELECT Title, FirstName, LastName, Job, Position FROM Staff WHERE StaffID = '" + txtStaIDNumber + "' ";
    db.transaction(function (tx) {
        tx.executeSql(queryStr, [],
            (function (tx, response) {
                //console.log(response.rows.item(0).Title);
                $('#staTitle').val(response.rows.item(0).Title); $('#staTitle').selectmenu("refresh");
                $('#staFirstName').val(response.rows.item(0).FirstName);
                $('#staLastName').val(response.rows.item(0).LastName);
                $('#staJob').val(response.rows.item(0).Job);
                $('#staPosition').val(response.rows.item(0).Position);
            }), errorCB, successCB);
    });

    var queryAddr = "SELECT AddressData, Tumbon, Amphor, Province, Phone FROM Address INNER JOIN PersonVSAddress ON Address.AddressID = PersonVSAddress.AddressID WHERE PersonVSAddress.PID = '" + txtStaIDNumber + "' ";
    db.transaction(function (tx) {
        tx.executeSql(queryAddr, [],
            (function (tx, response) {
                //console.log(response.rows.item(0).AddressID);
                $('#staAddress').val(response.rows.item(0).AddressData);
                $('#staTumbon').val(response.rows.item(0).Tumbon);
                $('#staAmphor').val(response.rows.item(0).Amphor); 
                $('#staProvince').val(response.rows.item(0).Province); $('#staProvince').selectmenu("refresh");
                $('#staPhone').val(response.rows.item(0).Phone);
            }), errorCB, successCB);
    });

});

function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

//clear all input type=text all page
function clearAllInputText() {
    var elements = document.getElementsByTagName("input");
    for (var ii = 0; ii < elements.length; ii++) {
        if (elements[ii].type == "text") {
            elements[ii].value = "";
        }
    }
}


function titleService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=TitleID&desc=TitleDescription&table=Title&condition",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);     

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO Title (TitleID, TitleDESC) VALUES ( "
                    + " '" + a[i]['TitleID'] + "', "
                    + " '" + a[i]['TitleDescription'] + "' "
                    + " ) ");
                }
            });

        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function provinceService() {
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/getProvinceList",
        dataType: 'jsonp',
        success: function (msg) {
            var a = JSON.parse(msg);

            //insertProvinceTable(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO Province (ProvinceID, ProvinceDescription) VALUES ( "
                    + " '" + a[i]['provinceID'] + "', "
                    + " '" + a[i]['provinceDes'] + "' "
                    + " ) ");
                }
            });

        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function mariageService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=MariageStatusID&desc=MariageStatusDescription&table=MariageStatus&condition=",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO MariageStatus ( MariageStatusID, MariageStatusDescription ) VALUES ( "
                    + " '" + a[i]["MariageStatusID"] + "', "
                    + " '" + a[i]["MariageStatusDescription"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function nationService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=NationalityID&desc=NationalityDescription&table=Nationality&condition=",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO Nationality ( NationalityID, NationalityDescription ) VALUES ( "
                    + " '" + a[i]["NationalityID"] + "', "
                    + " '" + a[i]["NationalityDescription"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function raceService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=RaceID&desc=NationalityDescription&table=Race&condition=",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO Race ( RaceID, RaceDescription ) VALUES ( "
                    + " '" + a[i]["RaceID"] + "', "
                    + " '" + a[i]["NationalityDescription"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function religionService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=Id&desc=Religion&table=Religion&condition=",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO Religion ( Id, Religion ) VALUES ( "
                    + " '" + a[i]["Id"] + "', "
                    + " '" + a[i]["Religion"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function relationService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=RelationID&desc=RelationThaiDescription&table=Relation&condition=",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO Relation ( RelationID, RelationDescription ) VALUES ( "
                    + " '" + a[i]["RelationID"] + "', "
                    + " '" + a[i]["RelationThaiDescription"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function educationLevelService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=EducationLevelID&desc=EducationLevelDescription&table=EducationLevel&condition=",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO EducationLevel ( EducationLevelID, EducationLevelDescription ) VALUES ( "
                    + " '" + a[i]["EducationLevelID"] + "', "
                    + " '" + a[i]["EducationLevelDescription"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function occupationService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=OccupationID&desc=OccupationDescription&table=Occupation&condition",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);            

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO Occupation ( OccupationID, OccupationDescription ) VALUES ( "
                    + " '" + a[i]["OccupationID"] + "', "
                    + " '" + a[i]["OccupationDescription"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function liveHomeStatusService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=LiveHomeStatusID&desc=LiveHomeStatusDescription&table=LiveHomeStatus&condition",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO LiveHomeStatus ( LiveHomeStatusID, LiveHomeStatusDescription ) VALUES ( "
                    + " '" + a[i]["LiveHomeStatusID"] + "', "
                    + " '" + a[i]["LiveHomeStatusDescription"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

