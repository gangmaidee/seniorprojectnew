﻿var db = window.openDatabase("Database", "1.0", "Survey", 200000);

function errorCB(err) {
    alert("Error processing SQL: " + err.code);
}

function successCB() {
    //alert("success!");
}

function submitNewFamily() {    

    var queryStr = "SELECT PID FROM PersonFamily WHERE PID = '" + $('#IDNumber_sp1_1') + "' ";
    db.transaction(function (tx) {
        tx.executeSql(queryStr, [],
            (function (tx, response) {
                if (response.rows.length == 0) {
                    var familyID = guidGenerator();

                    db.transaction(function (tx) {
                        tx.executeSql('INSERT INTO PersonFamily ( FamilyID, FamilyRole, PID )'
                            + ' VALUES ( "' + familyID + '", "000", ' + $('#IDNumber_sp1_1') + ' )');
                    }, errorCB, successCB);

                }
                else {

                    console.log(response.rows.item(0).FamilyID);

                }
            }), errorCB, successCB);
    });

    var newFamily = {
        CID: $('#IDNumber_new').val(),
        title: $('#title_new').val(),
        firstName: $('#firstName_new').val(),
        lastName: $('#lastName_new').val(),
        DOB: $('#dob_new').val(),
        relation: $('#relation_new').val(),
        tel: $('#tel_new').val(),
        nation: $('#nation_new').val(),
        race: $('#race_new').val(),
        religion: $('#religion_new').val()
    };

    var newFamilyEdu = {
        eduStatus: $('#eduStatus_new').val(),
        eduLevel: $('#graduateLevel_new').val(),
        academy: $('#academy_new').val(),
        //eduYear: $('#endyear').val()
    };

    var newFamilyJob = {
        occupation: $('#occupation_new').val(),
        //position: $('#position_sp1_2').val(),
        office: $('#office_new').val(),
    };

    var Finance = {
        income: $('#income_new').val(),
        //outcome: $('#expense5').val(),
        //inDept: $('#indept').val(),
        //exDept: $('#exdept').val()
    }; // insert to PersonFinamce

}

/*
 insert list
 - Person
 - PersonFamily
 - PersonEdu
 - PersonJobDetail
 - PersonFinance
*/

function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}