﻿function getAnswer(Name) {
    var response;

    if (Name == "AccidType") {
        response =
             [
                {
                    "AnswerID": "001",
                    "AnswerDesc": "วางเพลิง",
                },
                {
                    "AnswerID": "002",
                    "AnswerDesc": "วางระเบิด",
                },
                {
                    "AnswerID": "003",
                    "AnswerDesc": "ลอบยิง",
                },
                {
                    "AnswerID": "004",
                    "AnswerDesc": "ทำร้ายร่างกาย",
                },
                {
                    "AnswerID": "005",
                    "AnswerDesc": "โจมตีสถานทีราชการ",
                },
                {
                    "AnswerID": "999",
                    "AnswerDesc": "อื่นๆ",
                }
             ];
    }
    else {
        response = "";
    }

    return response;
}

function getJsonData(Type) {
    var returnJson;

    if (Type == "Question") {
        returnJson = {
            "RECORDS": [
            {
                "QID": "00001",
                "QDesc": "วันที่ประสบเหตุ",
                "QTID": "00001",
                "UIControl": "10020"
            },
            {
                "QID": "00002",
                "QDesc": "เหตุการณ์",
                "QTID": "00001",
                "UIControl": "10010"
            },
            {
                "QID": "00003",
                "QDesc": "ผู้ระบุเหตุการณ์",
                "QTID": "00001",
                "UIControl": "10010"
            },
            {
                "QID": "00004",
                "QDesc": "ประเภทเหตุกาณ์",
                "QTID": "00001",
                "UIControl": "20020"
            },
            {
                "QID": "00001",
                "QDesc": "ที่อยู่ที่เกิดเหตุ",
                "QTID": "00002",
                "UIControl": "10010"
            },
            {
                "QID": "00002",
                "QDesc": "หมู่บ้านที่เกิดเหตุ",
                "QTID": "00002",
                "UIControl": "10010"
            },
            {
                "QID": "00003",
                "QDesc": "ตำบลที่เกิดเหตุ",
                "QTID": "00002",
                "UIControl": "30011"
            },
            {
                "QID": "00004",
                "QDesc": "อำเภอที่เกิดเหตุ",
                "QTID": "00002",
                "UIControl": "30012"
            },
            {
                "QID": "00005",
                "QDesc": "จังหวัดที่เกิดเหตุ",
                "QTID": "00002",
                "UIControl": "30013"
            },
            {
                "QID": "00006",
                "QDesc": "รหัสไปรษณีย์ที่เกิดเหตุ",
                "QTID": "00002",
                "UIControl": "10010"
            },
            {
                "QID": "00007",
                "QDesc": "สถานีตำรวจที่บันทึกประจำวัน",
                "QTID": "00002",
                "UIControl": "10010"
            }
            ]
        };
    }
    else if (Type == "QuestionType") {
        returnJson = {
            "RECORDS": [
            {
                "QTID": "00001",
                "QTDesc": "ข้อมูลเหตุการณ์"
            },
            {
                "QTID": "00002",
                "QTDesc": "สถานที่เกิดเหตุการณ์ และสถานีตำรวจที่บันทึกประจำวัน"
            }
            ]
        };
    }
    else if (Type == "QuestionVSAnswer") {
        returnJson = {
            "RECORDS": [
            {
                "QID": "00001",
                "AID": "00001",
                "QTID": "00001"
            },
            {
                "QID": "00002",
                "AID": "00002",
                "QTID": "00001"
            },
            {
                "QID": "00003",
                "AID": "00003",
                "QTID": "00001"
            },
            {
                "QID": "00004",
                "AID": "00004",
                "QTID": "00001"
            },
            {
                "QID": "00001",
                "AID": "00005",
                "QTID": "00002"
            },
            {
                "QID": "00002",
                "AID": "00006",
                "QTID": "00002"
            },
            {
                "QID": "00003",
                "AID": "00007",
                "QTID": "00002"
            },
            {
                "QID": "00004",
                "AID": "00008",
                "QTID": "00002"
            },
            {
                "QID": "00005",
                "AID": "00009",
                "QTID": "00002"
            },
            {
                "QID": "00006",
                "AID": "00010",
                "QTID": "00002"
            },
            {
                "QID": "00007",
                "AID": "00011",
                "QTID": "00002"
            }
            ]
        };
    }
    else if (Type == "Answer") {
        returnJson = {
            "RECORDS": [
            {
                "AID": "00001",
                "ADesc": "วันที่ประสบเหตุ",
                "QID": "00001",
                "QTID": "00001"
            },
            {
                "AID": "00002",
                "ADesc": "เหตุการณ์",
                "QID": "00002",
                "QTID": "00001"
            },
            {
                "AID": "00003",
                "ADesc": "ผู้ระบุเหตุการณ์",
                "QID": "00003",
                "QTID": "00001"
            },
            {
                "AID": "00004",
                "ADesc": "วางเพลิง",
                "QID": "00004",
                "QTID": "00001"
            },
            {
                "AID": "00005",
                "ADesc": "วางระเบิด",
                "QID": "00004",
                "QTID": "00001"
            },
            {
                "AID": "00006",
                "ADesc": "ลอบยิง",
                "QID": "00004",
                "QTID": "00001"
            },
            {
                "AID": "00007",
                "ADesc": "ทำร้ายร่างกาย",
                "QID": "00004",
                "QTID": "00001"
            },
            {
                "AID": "00008",
                "ADesc": "โจมตีสถานที่ราชการ",
                "QID": "00004",
                "QTID": "00001"
            },
            {
                "AID": "00009",
                "ADesc": "อื่น ๆ",
                "QID": "00004",
                "QTID": "00001"
            },
            {
                "AID": "00010",
                "ADesc": "ที่อยู่ที่เกิดเหตุ",
                "QID": "00001",
                "QTID": "00002"
            },
            {
                "AID": "00011",
                "ADesc": "หมู่บ้านที่เกิดเหตุ",
                "QID": "00002",
                "QTID": "00002"
            },
            {
                "AID": "00012",
                "ADesc": "ตำบลที่เกิดเหตุ",
                "QID": "00003",
                "QTID": "00002"
            },
            {
                "AID": "00013",
                "ADesc": "อำเภอที่เกิดเหตุ",
                "QID": "00004",
                "QTID": "00002"
            },
            {
                "AID": "00014",
                "ADesc": "จังหวัดที่เกิดเหตุ",
                "QID": "00005",
                "QTID": "00002"
            },
            {
                "AID": "00015",
                "ADesc": "รหัสไปรษณีย์ที่เกิดเหตุ",
                "QID": "00006",
                "QTID": "00002"
            },
            {
                "AID": "00016",
                "ADesc": "สถานีตำรวจที่บันทึกประจำวัน",
                "QID": "00007",
                "QTID": "00002"
            }
            ]
        };
    }
    else if (Type == "AnswerGroup") {
        returnJson = {
            "RECORDS": [
            {
                "AnswerGroupID": "00001",
                "AnswerGroupDesc": "เหตุการณ์"
            },
            {
                "AnswerGroupID": "00002",
                "AnswerGroupDesc": "สถานที่เกิดเหตุการณ์ และสถานีตำรวจที่บันทึกประจำวัน"
            }
            ]
        };
    }
    else if (Type == "UIControl") {
        returnJson = {
            "RECORDS": [
            {
                "UIControlID": "00000",
                "UIControlDesc": "ไม่ระบุ"
            },
            {
                "UIControlID": "10010",
                "UIControlDesc": "input textbox"
            },
            {
                "UIControlID": "10020",
                "UIControlDesc": "input date"
            },
            {
                "UIControlID": "20010",
                "UIControlDesc": "controlgroup"
            },
            {
                "UIControlID": "20010",
                "UIControlDesc": "controlgroup ที่มีตัวเลือก อื่นๆ เป็น input textbox"
            },
            {
                "UIControlID": "99999",
                "UIControlDesc": "อื่นๆ"
            }
            ]
        };
    }
    else {
        console.log('Not found Type');
    }

    return returnJson;
}