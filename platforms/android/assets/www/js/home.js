﻿var db;

var cid = sessionStorage.getItem('PersonCID');

var famCid;

function errorCB(err) {
    alert("Error processing SQL: " + err.code);
}

function successCB() {
    //alert("success!");
}

function clearSession() {
    //sessionStorage.setItem('CID', null);
    //sessionStorage.removeItem('CID');
}

document.addEventListener("backbutton", function (e) {
    if ($.mobile.activePage.is('#home')) {
        e.preventDefault();
        var r = confirm("ต้องการออกจากแอพฯ ?");
        if (r) { navigator.app.exitApp(); }
        //navigator.app.exitApp();
    }
    else {
        navigator.app.backHistory()
    }
}, false);

$('#logoutBtn').click(function (e) {
    var r = confirm("ต้องการออกจากระบบ หรือไม่\n*คำเตือน: ข้อมูลที่ยังไม่ได้อัพโหลดจะหายไป");
    if (r == true) {

        //clearSession();
        sessionStorage.clear();
        localStorage.clear();
        //window.location.replace('index.html');
        //$(":mobile-pagecontainer").pagecontainer("change", "index.html", { transition: "slidedown", changeHash: false, reload: true });
        //history.go(-(history.length - 1));    

        startAjaxLoader();
        db.transaction(function (tx) {
            //Drop Table
            tx.executeSql('DELETE FROM AccidentDamageAndAssist');
            tx.executeSql('DELETE FROM Address');
            tx.executeSql('DELETE FROM Person');
            tx.executeSql('DELETE FROM PersonAccident');
            tx.executeSql('DELETE FROM PersonBankAccount');

            tx.executeSql('DELETE FROM PersonEducation');
            tx.executeSql('DELETE FROM PersonEmergencyContact');
            tx.executeSql('DELETE FROM PersonFamily');
            tx.executeSql('DELETE FROM PersonFinance');
            tx.executeSql('DELETE FROM PersonMate');

            tx.executeSql('DELETE FROM PersonOccupation');
            tx.executeSql('DELETE FROM PersonParent');
            tx.executeSql('DELETE FROM PersonRecordBy');
            tx.executeSql('DELETE FROM PersonStandIn');
            tx.executeSql('DELETE FROM PersonVSAddress');

            tx.executeSql('DELETE FROM Staff');
        }, errorCB, function () {
            //$.mobile.changePage("#login");
            isAccess = false;
            stopAjaxLoader();
            window.location.href = 'index.html';
        });

    } else { }

});

function startAjaxLoader() {
    if ($('#ajaxLoadScreen') != null) {
        var loader = '<div id="ajaxLoadScreen"><img id="loadingImg" src="img/ajax-loader-bar.gif"></div>';
        $(loader).appendTo('body');
    }
}

function stopAjaxLoader() {
    $('#ajaxLoadScreen').remove();
}

$('#addNewPerson').click(function (e) {
    sessionStorage.removeItem('PersonCID');
    //window.location.href = 'person_data.html#informant';
    //$(":mobile-pagecontainer").pagecontainer("change", "person_data.html#informant", { transition: "flip" });
});

$('#editStaffInfo').click(function (e) {
    //window.location.href = 'index.html#register';
});

$(document).on('pagebeforeshow', '#home', function () {
    if (sessionStorage.getItem('CID') === null) {
        alert("ไม่มีการ Login");
        window.location.href = 'index.html';
    }

});

$(document).on("pageshow", "#home", function () {
    sessionStorage.removeItem('PersonCID'); sessionStorage.removeItem('EditType'); sessionStorage.removeItem('familyID'); sessionStorage.removeItem('FamilyCID');
    sessionStorage.removeItem("MenuID"); sessionStorage.removeItem("MenuID_2"); sessionStorage.removeItem("AccidentID");

    if (sessionStorage.getItem("isFirstLogin") == "1") {
        //console.log(sessionStorage.getItem("isFirstLogin"));

        //startAjaxLoader();
        loadPerson(sessionStorage.getItem('CID'));

        sessionStorage.removeItem("isFirstLogin");
        //console.log(sessionStorage.getItem("isFirstLogin"));
    }
    else { }

});

$(document).on("pagecreate", "#home", function (event) {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);

    var username = sessionStorage.getItem('CID');
    //alert("Welcome : " + username);

    var queryStr = "SELECT * FROM Staff WHERE StaffID = '" + username + "' ";
    db.transaction(function (tx) {
        tx.executeSql(queryStr, [], (function (tx, response) {
            if (response.rows.length != 0) {
                //alert("�����");
                var Fullname = response.rows[0].FirstName + " " + response.rows[0].LastName;
                document.getElementById("staffFullName").innerHTML = Fullname;
            } else {
                //alert("username ���� password ���١��ͧ");
                console.log('ไม่พบ CID');
            }
        }), errorCB, successCB);
    });

    db.transaction(queryShownPerson, errorCB);

    $(document).on("swiperight", "#home", function (e) {

        if ($(".ui-page-active").jqmData("panel") !== "open") {
            if (e.type === "swiperight") {
                $("#menuOverlayPanel").panel("open");
            }
        }
    });

    $('#searchData').on('keyup', function (e) {
        var theEvent = e || window.event;
        var keyPressed = theEvent.keyCode || theEvent.which;

        //var query = "SELECT * FROM Person WHERE (PID LIKE '%" + $('#searchData').val() + "%' OR FirstName LIKE '%" + $('#searchData').val() + "%' OR LastName LIKE '%" + $('#searchData').val() + "%') LIMIT 10";

        //if (keyPressed == 13) {        
        //}
        fetchPersonList();

    });

    $('#dataTableSearch').on('click', 'li', function () {
        cid = $(this).attr('id');
        sessionStorage.setItem('PersonCID', cid);
        sessionStorage.setItem('firstClickPersonShowPanel', "1");
        //$.mobile.changePage("#profile", { transition: "slide" });
        $(":mobile-pagecontainer").pagecontainer("change", "#profile", { transition: "slide" });
    });

    $('#helpBtn').click(function () {

        if (navigator.onLine) {
            var r = confirm("ต้องการโหลดข้อมูลผู้รับบริการใหม่ หรือไม่\n*คำเตือน: ข้อมูลที่ยังไม่ได้อัพโหลดจะหายไป");
            if (r == true) {
                loadPerson(sessionStorage.getItem('CID'));

                /*
                db.transaction(function (tx) {
                    //Drop Table
                    tx.executeSql('DELETE FROM AccidentDamageAndAssist');
                    tx.executeSql('DELETE FROM Address');
                    tx.executeSql('DELETE FROM Person');
                    tx.executeSql('DELETE FROM PersonAccident');
                    tx.executeSql('DELETE FROM PersonBankAccount');

                    tx.executeSql('DELETE FROM PersonEducation');
                    tx.executeSql('DELETE FROM PersonEmergencyContact');
                    tx.executeSql('DELETE FROM PersonFamily');
                    tx.executeSql('DELETE FROM PersonFinance');
                    tx.executeSql('DELETE FROM PersonMate');

                    tx.executeSql('DELETE FROM PersonOccupation');
                    tx.executeSql('DELETE FROM PersonParent');
                    tx.executeSql('DELETE FROM PersonRecordBy');
                    tx.executeSql('DELETE FROM PersonStandIn');
                    tx.executeSql('DELETE FROM PersonVSAddress');

                }, errorCB, function () {
                    loadPerson(sessionStorage.getItem('CID'));
                });
                */


            }
            else {
                console.log('cancel');
            }
        }
        else {
            alert("กรุณาเชื่อมต่อ Internet");
        }

    });

    $('#myImage').click(function () {
        alert(sessionStorage.getItem("pathIMG"));
        var myImage = document.getElementById('myImage');
        myImage.style.display = 'block';
        //myImage.src = 'dataimage/jpeg;base64,' + imageData;
        var ImageData = sessionStorage.getItem("pathIMG");
        alert(ImageData);
        myImage.src = ImageData;
    });

    $("#homeHeader").on("taphold", function () {
        window.location.reload();
    });

});

var recordReponse;
function loadPerson(StaffPID) {
    console.log(StaffPID);
    startAjaxLoader();
    $.ajax({
        type: "GET",
        url: "http://seniorservice.azurewebsites.net/Service1.svc/GetPerson?staffID=" + StaffPID,
        //url: "http://localhost:1722/Service1.svc/GetPerson?staffID=" + StaffPID,
        success: function (response) {
            isAccess = false;
            //var a = JSON.parse(response);            
            recordReponse = JSON.parse(response);

            db.transaction(function (tx) {
                //Delete Table Before Update
                tx.executeSql('DELETE FROM AccidentDamageAndAssist');
                tx.executeSql('DELETE FROM Address');
                tx.executeSql('DELETE FROM Person');
                tx.executeSql('DELETE FROM PersonAccident');
                tx.executeSql('DELETE FROM PersonBankAccount');

                tx.executeSql('DELETE FROM PersonEducation');
                tx.executeSql('DELETE FROM PersonEmergencyContact');
                tx.executeSql('DELETE FROM PersonFamily');
                tx.executeSql('DELETE FROM PersonFinance');
                tx.executeSql('DELETE FROM PersonMate');

                tx.executeSql('DELETE FROM PersonOccupation');
                tx.executeSql('DELETE FROM PersonParent');
                tx.executeSql('DELETE FROM PersonRecordBy');
                tx.executeSql('DELETE FROM PersonStandIn');
                tx.executeSql('DELETE FROM PersonVSAddress');

            }, errorCB, function () {
                console.log('all relate person table is delete');
                db.transaction(insertRecordData, errorCB, function () {
                    fetchPersonList();
                });
            });


        },
        error: function (request, status, error) {
            stopAjaxLoader();
            alert("Error: " + error);
        }
    });
}

function insertRecordData(tx) {
    //console.log(recordReponse);
    var tableName;
    var tableData;
    var tableColumnName = [];
    var tableValueArray = [];
    var queryRecordData = "";

    for (var name in recordReponse) {
        if (recordReponse.hasOwnProperty(name)) {
            //console.log("name = " + name);

            switch (name) {
                case "PBank":
                    tableName = "PersonBankAccount";
                    break;
                case "PEdu":
                    tableName = "PersonEducation";
                    break;
                case "PData":
                    tableName = "Person";
                    break;
                case "PEmer":
                    tableName = "PersonEmergencyContact";
                    break;
                case "POcc":
                    tableName = "PersonOccupation";
                    break;

                case "PFin":
                    tableName = "PersonFinance";
                    break;
                case "PPar":
                    tableName = "PersonParent";
                    break;
                case "PRec":
                    tableName = "PersonRecordBy";
                    break;
                case "PStan":
                    tableName = "PersonStandIn";
                    break;
                case "PMate":
                    tableName = "PersonMate";
                    break;

                case "PFam":
                    tableName = "PersonFamily";
                    break;
                case "PVA":
                    tableName = "PersonVSAddress";
                    break;
                case "Addr":
                    tableName = "Address";
                    break;
                case "Accid":
                    tableName = "PersonAccident";
                    break;
                case "ADA":
                    tableName = "AccidentDamageAndAssist";
                    break;
            }
            if (recordReponse[name].length != 0) {
                //console.log(tableName);
                //console.log(recordReponse[name]);
                //console.log(recordReponse[name].length);                
                if (recordReponse[name].length != 0) {
                    //queryRecordData = "INSERT INTO " + tableName + " VALUES (" + + ")";
                    for (var i = 0; i < recordReponse[name].length; i++) {
                        //console.log(recordReponse[name][i]);
                        tableColumnName = [];
                        tableValueArray = [];
                        for (var name2 in recordReponse[name][i]) {
                            if (recordReponse[name][i].hasOwnProperty(name2)) {
                                //console.log("name = " + name2 + " //value = " + recordReponse[name][i][name2]);
                                tableColumnName.push(name2);
                                tableValueArray.push(recordReponse[name][i][name2]);
                            }
                        }
                        //console.log(tableColumnName);
                        //console.log(tableValueArray);
                        queryRecordData = "INSERT INTO " + tableName + " (" + tableColumnName[0];
                        for (var j = 1; j < tableColumnName.length - 1; j++) {
                            queryRecordData += ", " + tableColumnName[j];
                        }
                        queryRecordData += ", " + tableColumnName[tableColumnName.length - 1] + ") VALUES ('" + tableValueArray[0] + "'";
                        for (var j = 1; j < tableValueArray.length - 1; j++) {
                            queryRecordData += ", '" + tableValueArray[j] + "'";
                        }
                        queryRecordData += ", '" + tableValueArray[tableValueArray.length - 1] + "')";
                        console.log(queryRecordData);

                        tx.executeSql(queryRecordData, null, function () {
                            stopAjaxLoader();
                            //fetchPersonList();
                        }, function (err) {
                            console.log("*Error: " + queryRecordData);
                            console.log("error on Table: " + tableName + " code:" + err.code);
                        });

                    }


                }

            }
            else { /*console.log("recordReponse[name].length == 0");*/ }
        }
        else {
            console.log('!recordReponse.hasOwnProperty(name)');
        }
    }
    stopAjaxLoader();
}

function fetchPersonList() {
    db.transaction(function (tx) {
        $("#dataTableSearch").empty();
        //$("mypmenu").append();
        tx.executeSql("SELECT P.* FROM Person P INNER JOIN PersonRecordBy PRB ON P.PID=PRB.PID WHERE P.RecordStaffPID = '" + sessionStorage.getItem('CID') + "' AND (P.PID LIKE '%" + $('#searchData').val() + "%' OR P.FirstName LIKE '%" + $('#searchData').val() + "%' OR P.LastName LIKE '%" + $('#searchData').val() + "%') LIMIT 10", [],
            (function (tx, results) {
                //alert(results);
                for (var i = 0; i < results.rows.length; i++) {
                    var PicPath = "";
                    if (navigator.onLine) { PicPath = "http://seniorservice.azurewebsites.net/img/" + results.rows[i].PID + ".jpg"; }
                    else { PicPath = "img/avatar-placeholder.png"; }
                    $("#dataTableSearch").append("<li class='ui-li-has-thumb' id='" + results.rows[i].PID + "'><a class='ui-btn waves-effect waves-button waves-effect waves-button' data-transition='slide'><img id='" + results.rows[i].PID + "-pic' style='width: 100px; height: 100px;' class='ui-thumbnail ui-thumbnail-circular' src='" + PicPath + "' onerror='imgError(this)'><h2>"
                    + results.rows[i].FirstName + " " + results.rows[i].LastName + "</h2><p>" + results.rows[i].PID + "</p></a></li>");

                    //imgID = "#" + results.rows[i].PID + "-pic"; loadDefaultPic(imgID);

                }
                $("#dataTableSearch").listview('refresh');
            }), errorCB);
    }, errorCB);
}

//----------- profile --------------
$(document).on('pagebeforeshow', '#profile', function (e) {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);

    //$('#multiPersonDIV').hide(); $('#dataTablePerson').empty();    
    sessionStorage.removeItem('EditType'); sessionStorage.removeItem('familyID'); sessionStorage.removeItem('FamilyCID');
    sessionStorage.removeItem('AccidentID'); sessionStorage.removeItem("MenuID_2"); sessionStorage.removeItem("SubMenuID");

    if ($("#headerProfile").val() == "") {
        db.transaction(function (tx) {
            tx.executeSql("SELECT FirstName, LastName FROM Person WHERE PID = '" + cid + "' ", [], function (tx, rs) {
                if (rs.rows.length != 0) {
                    $("#headerProfile").empty();
                    $("#headerProfile").append(rs.rows[0].FirstName + " " + rs.rows[0].LastName);
                }
            }, errorCB);
        }, errorCB);
    }

    var countN = 0;
    $('#menuUl li').each(function () {
        //console.log($(this).attr("id") + "//" + sessionStorage.getItem("MenuID") + " N = " + countN);
        if (sessionStorage.getItem("MenuID") != null && $(this).attr("id") == sessionStorage.getItem("MenuID")) {
            $('#menuUl #' + $(this).attr("id")).click();
            return false;
        }
        else if (countN == $('#menuUl li').length - 1) {
            $('#menuUl #personProfile').click();
        }
        else {
            countN++;
            //console.log("ไม่พบ Menu");
        }
    });

});

$(document).on("pageshow", "#profile", function () {
    if (sessionStorage.getItem("firstClickPersonShowPanel") == "1") {
        $('#openPanelA').click();
        sessionStorage.removeItem("firstClickPersonShowPanel");
    }
    else { }

});

$(document).on("pagecreate", "#profile", function () {

    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);
    //alert('create');

    //$('#dataTable thead').empty(); $('#dataTable tbody').empty();
    //$("#headerProfile").empty();
    //$('#multiPersonDIV').hide(); $('#dataTablePerson').empty(); $('#editBtn').closest('.ui-btn').show();
    //db.transaction(queryShownPersonProfile, errorCB);


    var menuName;
    //$('#addBtn').closest('.ui-btn').hide();

    $(document).on("swiperight", "#profile", function (e) {
        // We check if there is no open panel on the page because otherwise
        // a swipe to close the left panel would also open the right panel (and v.v.).
        // We do this by checking the data that the framework stores on the page element (panel: open).
        if ($(".ui-page-active").jqmData("panel") !== "open") {
            if (e.type === "swiperight") {
                $("#left-panel").panel("open");
            }
        }
    });

    $('#menuUl li').click(function () {

        $('#menuUl li').css({ background: '#333333' });
        $("#menuUl li:eq(0)").css({ background: 'white' });
        $("#menuUl li:eq(7)").css({ background: 'white' });
        $("#menuUl li:eq(10)").css({ background: 'white' });
        $(this).css({ background: 'orange' });
        var id = this.id; // get id of clicked li
        sessionStorage.setItem("MenuID", id);
        menuName = id;
        showMenuUI(id);

    });

    $('#menuPerson li').click(function () {
        var id = this.id; // get id of clicked li
        //sessionStorage.setItem("MenuID", id);
        //menuName = id;    
        sessionStorage.setItem("SubMenuID", id);

        if (id == 'personProfile2') {
            showMenuUI("personProfile");
            menuName = "personProfile";
        }
        else if (id == 'personAddress') {
            showMenuUI("personAddress");
            menuName = "personAddress";
        }
        else if (id == 'personalType') {
            showMenuUI("personalType");
            menuName = "personalType";
        }
        else { console.log('ไม่พบ menuUl id'); }

        //console.log(menuName);

    });

    $('#editBtn').click(function () {
        if (menuName == "personProfile") {
            window.location.href = 'person_data.html#info';

        }
        else if (menuName == "personAddress") {
            window.location.href = 'person_data.html#address_info';

        }
        else if (menuName == "personalType") {
            window.location.href = 'person_data.html#person_type';

        }
        else if (menuName == "personStandIn") {
            sessionStorage.setItem('EditType', 'standIn');

            db.transaction(function (tx) {
                tx.executeSql("SELECT * FROM PersonStandIn WHERE PID = '" + cid + "' ", [], (function (tx, results) {
                    if (results.rows.length == 0) {
                        console.log('no');
                        db.transaction(function (tx) {
                            tx.executeSql("INSERT INTO PersonStandIn VALUES (" + cid + ", '', '0000') ");
                        }, errorCB, (function () {
                            window.location.href = 'person_data.html#informant';
                        }));

                    }
                    else {
                        console.log('have');
                        window.location.href = 'person_data.html#informant';
                    }
                }), errorCB);
            }, errorCB, (function () {

            }));


        }
        else if (menuName == "personEmergency") {
            sessionStorage.setItem('EditType', 'emergency');

            db.transaction(function (tx) {
                tx.executeSql("SELECT * FROM PersonEmergencyContact WHERE PID = '" + cid + "' ", [], (function (tx, results) {
                    if (results.rows.length == 0) {
                        console.log('no');
                        db.transaction(function (tx) {
                            tx.executeSql("INSERT INTO PersonEmergencyContact VALUES (" + cid + ", '', '0000') ");
                        }, errorCB, (function () {
                            window.location.href = 'person_data.html#informant';
                        }));

                    }
                    else {
                        console.log('have');
                        window.location.href = 'person_data.html#informant';
                    }
                }), errorCB);
            }, errorCB, (function () {

            }));


        }
        else {
            console.log('ไม่พบ menuName');
        }
    });

    $('#addBtn').click(function () {
        if (menuName == "personFamily") {
            sessionStorage.setItem('EditType', 'newFamily');

            db.transaction(function (tx) {
                tx.executeSql("SELECT * FROM PersonFamily WHERE PID = '" + cid + "' ", [],
                    (function (tx, results) {
                        if (results.rows.length == 0) {
                            console.log('no');
                            var famID = guidGenerator();
                            //role 8888 = owner
                            db.transaction(function (tx) {
                                tx.executeSql("INSERT INTO PersonFamily VALUES ('" + famID + "', '8888', " + cid + ") ");
                            }, errorCB, (function () {
                                sessionStorage.setItem('familyID', famID);
                                window.location.href = 'person_data.html#info';
                            }));

                        }
                        else {
                            console.log('have');
                            sessionStorage.setItem('familyID', results.rows[0].FamilyID);
                            window.location.href = 'person_data.html#info';
                        }
                    }), errorCB);
            }, errorCB, (function () {
                //window.location.href = 'person_data.html#info';
            }));

        }
        else if (menuName == "personMate") {
            sessionStorage.setItem('EditType', 'newMate');
            window.location.href = 'person_data.html#info';
        }
        else if (menuName == "accidentData") {
            window.location.href = 'accident_data.html#accident';
        }
        else {
            console.log('ไม่พบ menuName');
        }
    });

    $('#dataTablePerson').on('click', 'li', function () {
        //cid = $(this).attr('id');
        //console.log($(this).attr('id'));
        sessionStorage.setItem('FamilyCID', $(this).attr('id'));
        ////$.mobile.changePage("#profile", { transition: "slide" });
        $(":mobile-pagecontainer").pagecontainer("change", "#profileFam", { transition: "slide" });
    });

    $('#dataTableAccident').on('click', 'li', function () {
        sessionStorage.setItem('AccidentID', $(this).attr('id'));
        if (menuName == "accidentData") {
            $(":mobile-pagecontainer").pagecontainer("change", "#accidentProfile", { transition: "slide" });
        }
        else if (menuName == "accidAssistance") {
            $(":mobile-pagecontainer").pagecontainer("change", "#assistance", { transition: "slide" });
        }
        else { }
    });

    $('#accidSelect').on('change', function (e) {
        //var optionSelected = $("option:selected", this);
        //console.log(optionSelected);

        var valueSelected = this.value;
        console.log(valueSelected);
        if (sessionStorage.getItem('SubMenuID') === null) {
            showReport('propReport');
        }
        else {
            $('#menuReport li').each(function (index) {
                if (sessionStorage.getItem('SubMenuID') == this.id) {
                    showReport(sessionStorage.getItem('SubMenuID'));
                    return false;
                }
                else {
                    if (index == $('#menuReport li').length - 1) {
                        showReport('propReport');
                    }
                }
            });
        }
    });

    $('#menuReport li').click(function () {
        var id = this.id;
        sessionStorage.setItem("SubMenuID", id);
        showReport(id);
    });

});

function showMenuUI(menuID) {
    if (menuID != "") {
        $("#dataTableDIV ul, #subDataHeader, #dataTablePerson, #dataTable").empty();
        $('#menuPerson, .reportNav').hide();
        $('#divMapAbove, #divMapBelow, #subDataHeader, #imgDiv').css("display", "none");
    }
    //console.log(id);
    if (menuID == 'personProfile') {
        menuName = "person";
        $('#menuPerson').show();
        $('#multiPersonDIV').hide(); $('#editBtn').closest('.ui-btn').show();
        $('#dataHeader').empty(); $('#dataHeader').append('ข้อมูลส่วนบุคคล');
        $('#dataTable thead').empty(); $('#dataTable tbody').empty();
        document.getElementById("imgDiv").style.display = "inline";

        $('#menuPerson li a').removeClass('ui-btn-active');
        $('#menuPerson #personProfile2 a').addClass('ui-btn-active');

        db.transaction(queryShownPersonProfile, errorCB);

        //db.transaction(queryShownPersonAddress, errorCB);
        //db.transaction(queryShownPersonType, errorCB);

    }
    else if (menuID == 'personAddress') {
        menuName = "address";
        $('#menuPerson').show();
        $('#multiPersonDIV').hide(); $('#editBtn').closest('.ui-btn').show();
        $('#dataHeader').empty(); $('#dataHeader').append('ที่อยู่');
        $('#dataTable thead').empty(); $('#dataTable tbody').empty();

        db.transaction(queryShownPersonAddress, errorCB);

        //if (navigator.onLine) {
        //    loadGoogleMaps("mapAboveTable");
        //} else { }

    }
    else if (menuID == 'personalType') {
        menuName = "personal";
        $('#menuPerson').show();
        $('#multiPersonDIV').hide(); $('#editBtn').closest('.ui-btn').show();
        $('#dataHeader').empty(); $('#dataHeader').append('อาชีพ การศึกษา และอื่นๆ');
        $('#dataTable thead').empty(); $('#dataTable tbody').empty();

        db.transaction(queryShownPersonType, errorCB);

    }
    else if (menuID == 'personRecord') {
        menuName = "personRecord";

        $('#multiPersonDIV').hide(); $('#editBtn').closest('.ui-btn').hide();
        $('#dataHeader').empty(); $('#dataHeader').append('ผู้ให้ข้อมูล');
        $('#dataTable thead').empty(); $('#dataTable tbody').empty();
        //document.getElementById("imgDiv").style.display = "inline";

        getPerson(menuName);

        //if (navigator.onLine) {
        //    loadGoogleMaps("mapBelowTable");
        //} else { }

    }
    else if (menuID == 'personStandIn') {
        menuName = 'standIn';

        $('#multiPersonDIV').hide(); $('#editBtn').closest('.ui-btn').show();
        $('#dataHeader').empty(); $('#dataHeader').append('ผู้ดำเนินการแทนกรณีเกิดภัยพิบัติ');
        $('#dataTable thead').empty(); $('#dataTable tbody').empty();
        //document.getElementById("imgDiv").style.display = "inline";

        getPerson(menuName);

        //if (navigator.onLine) {
        //    loadGoogleMaps("mapBelowTable");
        //} else { }

    }
    else if (menuID == 'personEmergency') {
        menuName = 'emergency';

        $('#multiPersonDIV').hide(); $('#editBtn').closest('.ui-btn').show();
        $('#dataHeader').empty(); $('#dataHeader').append('ผู้ติดต่อฉุกเฉิน');
        $('#dataTable thead').empty(); $('#dataTable tbody').empty();
        //document.getElementById("imgDiv").style.display = "inline";

        getPerson(menuName);

        //if (navigator.onLine) {
        //    loadGoogleMaps("mapBelowTable");
        //} else { }

    }
    else if (menuID == 'personFamily') {
        menuName = "family";

        $('#multiPersonDIV').show(); $('#editBtn').closest('.ui-btn').hide();
        $('#dataHeader').empty(); $('#dataHeader').append('สมาชิกครอบครัว');
        $('#dataTable thead').empty(); $('#dataTable tbody').empty();
        $('#maxPerson').empty();

        db.transaction(showFamList, errorCB);

    }
    else if (menuID == 'personMate') {
        menuName = "mate";

        $('#multiPersonDIV').show(); $('#editBtn').closest('.ui-btn').hide();
        $('#dataHeader').empty().append('คู่สมรส');
        $('#dataTable thead').empty(); $('#dataTable tbody').empty();
        $('#maxPerson').empty();

        db.transaction(showMateList, errorCB);

    }
    else if (menuID == 'accidentData') {
        menuName = "accidentData";

        $('#multiPersonDIV').show(); $('#editBtn').closest('.ui-btn').hide();
        $('#dataHeader').empty(); $('#dataHeader').append('เหตุการณ์');
        $('#dataTable thead').empty(); $('#dataTable tbody').empty();
        $('#maxPerson').empty();

        db.transaction(showAccidList, errorCB);

    }
    else if (menuID == 'accidAssistance') {
        $('#multiPersonDIV').hide();
        //$('#addBtn').closest('.ui-btn').hide();
        $('#editBtn').closest('.ui-btn').hide();
        $('#dataHeader').empty(); $('#dataHeader').append('ความช่วยเหลือ');
        $('#dataTable thead').empty(); $('#dataTable tbody').empty();
        $('#maxPerson').empty();
        $('#subDataHeader').append("เลือกเหตุการณ์"); document.getElementById("subDataHeader").style.display = "inline";

        db.transaction(showAccidList, errorCB);
    }
    else if (menuID == 'report') {
        $('.reportNav').show(); $('.reportNav[data-role="collapsible"]').collapsible("expand");
        $('#multiPersonDIV').hide();
        $('#editBtn').closest('.ui-btn').hide();
        $('#dataHeader').empty(); $('#dataHeader').append('สรุปความช่วยเหลือ');
        $('#dataTable thead').empty(); $('#dataTable tbody').empty();
        $('#maxPerson').empty();

        db.transaction(addAccidList, errorCB);
    }
    else {
        console.log('ไม่พบ menuUl id');
    }

}

function queryShownPerson(tx) {
    var query = "SELECT P.* FROM Person P INNER JOIN PersonRecordBy PRB ON P.PID=PRB.PID WHERE P.RecordStaffPID = '" + sessionStorage.getItem('CID') + "' LIMIT 10 ";
    //console.log(query);
    //var query = "SELECT * FROM Person LIMIT 10";   
    tx.executeSql(query, [], function (tx, results) {
        var len = results.rows.length;
        for (var i = 0 ; i < len; i++) {
            var PicPath = "";
            if (navigator.onLine) { PicPath = "http://seniorservice.azurewebsites.net/img/" + results.rows[i].PID + ".jpg"; }
            else { PicPath = "img/avatar-placeholder.png"; }
            $("#dataTableSearch").append("<li class='ui-li-has-thumb' id='" + results.rows[i].PID + "'><a class='ui-btn waves-effect waves-button waves-effect waves-button' data-transition='slide'><img id='" + results.rows[i].PID + "-pic' style='width: 100px; height: 100px;' class='ui-thumbnail ui-thumbnail-circular' src='" + PicPath + "' onerror='imgError(this)'><h2>"
                    + results.rows[i].FirstName + " " + results.rows[i].LastName + "</h2><p>" + results.rows[i].PID + "</p></a></li>");

            //imgID = "#" + results.rows[i].PID + "-pic"; loadDefaultPic(imgID);
        }
        $("#dataTableSearch").listview('refresh');

    }, errorCB);
}
function queryShownPersonProfile(tx) {
    var queryPerson = "SELECT P.*, T.TitleDESC, N.NationalityDescription, R.RaceDescription, Re.Religion, M.MariageStatusDescription, Prov.ProvinceDescription"
    + " FROM Person P"
    + " LEFT JOIN Title T ON P.Title = T.TitleID"
    + " LEFT JOIN PersonVSAddress PVA ON P.PID = PVA.PID"
    + " LEFT JOIN Address A ON A.AddressID = PVA.AddressID"
    + " LEFT JOIN Nationality N ON N.NationalityID = P.Nation"
    + " LEFT JOIN Race R ON R.RaceID = P.Race"
    + " LEFT JOIN Religion Re ON Re.Id = P.Religion"
    + " LEFT JOIN MariageStatus M ON M.MariageStatusID = P.MarriageStatus"
    + " LEFT JOIN Province Prov ON Prov.ProvinceID = P.BirthProvince"
    + " WHERE P.PID = '" + cid + "' ";
    //console.log(queryPerson);

    //tx.executeSql('SELECT * FROM Person WHERE PID = "' + cid + '" ', [], function (tx, results) {
    tx.executeSql(queryPerson, [], function (tx, results) {

        $("#headerProfile").empty();
        $('#dataHeader').empty(); $('#dataHeader').append('ข้อมูลส่วนบุคคล');
        if (results.rows.length != 0) {
            //console.log(results.rows[0]);
            $("#dataTable").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
                + "<tbody>"
                + "<tr><td><b>เลขบัตรประชาชน</b></td><td>" + results.rows[0].PID + "</td></tr>"
                + "<tr><td><b>ชื่อ-นามสกุล</b></td><td>" + results.rows[0].TitleDESC + " " + results.rows[0].FirstName + " " + results.rows[0].LastName + "</td></tr>"
                + "<tr><td><b>เบอร์โทร</b></td><td>" + results.rows[0].Phone + "</td></tr>"
                + "<tr><td><b>หมายเลขหนังสือเดินทาง</b></td><td>" + results.rows[0].Passport + "</td></tr>"
                + "<tr><td><b>วันเกิด</b></td><td>" + results.rows[0].DOB + "</td></tr>"

                + "<tr><td><b>สัญชาติ</b></td><td>" + results.rows[0].NationalityDescription + "</td></tr>"
                + "<tr><td><b>เชื้อชาติ</b></td><td>" + results.rows[0].RaceDescription + "</td></tr>"
                + "<tr><td><b>ศาสนา</b></td><td>" + results.rows[0].Religion + "</td></tr>"
                + "<tr><td><b>สถานภาพทางการสมรส</b></td><td>" + results.rows[0].MariageStatusDescription + "</td></tr>"
                + "<tr><td><b>ความพอใจในที่อยู่</b></td><td>" + getSatisfyStatus(results.rows[0].AddressSatisfy) + "</td></tr>"

                + "<tr><td><b>ภูมิลำเนา</b></td><td>" + results.rows[0].ProvinceDescription + "</td></tr>"
                + "</tbody>").closest("table#dataTable").table("refresh").trigger("create");


            $("#headerProfile").append(results.rows[0].FirstName + " " + results.rows[0].LastName);
            $("#dataTable").table

            var PicPath = "";
            if (navigator.onLine) {
                PicPath = "http://seniorservice.azurewebsites.net/img/" + results.rows[0].PID + ".jpg";
            }
            else { PicPath = "img/avatar-placeholder.png"; }
            $('#personImg').error(function () {                
                //imgError(this);
                $(this).attr("src", "img/avatar-placeholder.png").off("error");
                console.log("err");
            }).attr('src', PicPath);

        } else {
            $("#dataTable").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
                + "<tbody>"
                + "<tr><td><b>ไม่พบข้อมูล</b></td></tr>"
                + "</tbody>").closest("table#dataTable").table("refresh").trigger("create");
        }

    }, errorCB);
}
function queryShownPersonAddress(tx) {
    var queryAddr = "SELECT A.*, P.ProvinceDescription, T.TumbonDescription, C.CityDescription, L.LiveHomeStatusDescription "
    + " FROM Address A"
    + " LEFT JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID"
    + " LEFT JOIN Province P ON P.ProvinceID = A.Province"
    + " LEFT JOIN Tumbon T ON T.TumbonID = A.Tumbon"
    + " LEFT JOIN City C ON C.CityID = A.Amphor"
    + " LEFT JOIN LiveHomeStatus L ON L.LiveHomeStatusID = A.LiveHomeStatusID"
    + " WHERE PVA.PID = '" + cid + "' ";
    //console.log(queryAddr);

    //tx.executeSql('SELECT A.* FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = "' + cid + '" ', [], function (tx, results) {
    tx.executeSql(queryAddr, [], function (tx, results) {
        //console.log(results.rows[0]);
        if (results.rows.length != 0) {
            $("#dataTable").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
            + "<tbody>"
            + "<tr><td><b>รหัสบ้าน</b></td><td>" + results.rows[0].HomeCode + "</td></tr>"
            + "<tr><td><b>ประเภทบ้าน</b></td><td>" + results.rows[0].LiveHomeStatusDescription + "</td></tr>"
            + "<tr><td><b>บ้านเลขที่</b></td><td>" + results.rows[0].HouseNumber + "</td></tr>"
            + "<tr><td><b>หมู่ที่</b></td><td>" + results.rows[0].MooNumber + "</td></tr>"
            + "<tr><td><b>หมู่บ้าน</b></td><td>" + results.rows[0].VillageName + "</td></tr>"
            + "<tr><td><b>ซอย</b></td><td>" + results.rows[0].Alley + "</td></tr>"
            + "<tr><td><b>ถนน</b></td><td>" + results.rows[0].StreetName + "</td></tr>"
            + "<tr><td><b>ตำบล</b></td><td>" + results.rows[0].TumbonDescription + "</td></tr>"
            + "<tr><td><b>อำเภอ</b></td><td>" + results.rows[0].CityDescription + "</td></tr>"
            + "<tr><td><b>จังหวัด</b></td><td>" + results.rows[0].ProvinceDescription + "</td></tr>"
            + "<tr><td><b>รหัสไปรษณีย์</b></td><td>" + results.rows[0].Postcode + "</td></tr>"
            + "</tbody>").closest("table#dataTable").table("refresh").trigger("create");

            if (navigator.onLine) { loadGoogleMaps("mapAboveTable", results.rows[0].addrLat, results.rows[0].addrLng); } else { }

        } else {
            $("#dataTable").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
                + "<tbody>"
                + "<tr><td><b>ไม่พบข้อมูล</b></td></tr>"
                + "</tbody>").closest("table#dataTable").table("refresh").trigger("create");
        }
    }, errorCB);
}
function queryShownPersonType(tx) {
    var qrStr = 'SELECT PO.*, Edu.*, Fina.*, Bank.*, POO.OccupationDescription, POP.ProvinceDescription AS POP, POC.CityDescription AS POC, POT.TumbonDescription AS POT, ' +
                'EduL.EducationLevelDescription AS EduL, EduP.ProvinceDescription AS EduP, EduC.CityDescription AS EduC, EduT.TumbonDescription AS EduT, BankB.BankIDDesc, ' +
                //---- for student parent -----
                'Per.PID, PerT.TitleDESC, Per.FirstName, Per.LastName, Per.Phone AS PerPhone, PPR.RelationDescription, ' +
                'A.HouseNumber, A.MooNumber, A.VillageName, A.Alley, A.StreetName, A.PostCode AS PerPostCode, AddrP.ProvinceDescription AS AddrP, AddrT.TumbonDescription AS AddrT, AddrC.CityDescription AS AddrC ' +
                //------------------------
                //'SELECT Per.PID, PerT.TitleDESC, Per.FirstName, Per.LastName, PPR.RelationDescription ' +

                'FROM PersonOccupation PO ' +
                'LEFT JOIN Occupation POO ON POO.OccupationID = PO.OccupationID ' +
                'LEFT JOIN Province POP ON POP.ProvinceID = PO.Province ' +
                'LEFT JOIN City POC ON POC.CityID = PO.Amphor ' +
                'LEFT JOIN Tumbon POT ON POT.TumbonID = PO.Tumbon ' +

                //---- for student parent -----
                'LEFT JOIN PersonParent PP ON PP.PID = PO.PID ' +
                'LEFT JOIN Person Per ON Per.PID = PP.ParentPID ' +
                'LEFT JOIN Title PerT ON PerT.TitleID = Per.Title ' +
                'LEFT JOIN Relation PPR ON PPR.RelationID = PP.ParentRole ' +

                'LEFT JOIN PersonVSAddress PVA ON PVA.PID = Per.PID ' +
                'LEFT JOIN Address A ON A.AddressID = PVA.AddressID ' +
                'LEFT JOIN Province AddrP ON AddrP.ProvinceID = A.Province ' +
                'LEFT JOIN Tumbon AddrT ON AddrT.TumbonID = A.Tumbon ' +
                'LEFT JOIN City AddrC ON AddrC.CityID = A.Amphor ' +
                //----------------------------

                'LEFT JOIN PersonEducation Edu ON PO.PID = Edu.PID ' +
                'LEFT JOIN EducationLevel EduL ON EduL.EducationLevelID = Edu.EduLevel ' +
                'LEFT JOIN Province EduP ON EduP.ProvinceID = Edu.Province ' +
                'LEFT JOIN City EduC ON EduC.CityID = Edu.Amphor ' +
                'LEFT JOIN Tumbon EduT ON EduT.TumbonID = Edu.Tumbon ' +


                'LEFT JOIN PersonFinance Fina ON PO.PID = Fina.PID ' +

                'LEFT JOIN PersonBankAccount Bank ON PO.PID = Bank.PID ' +
                'LEFT JOIN Bank BankB ON BankB.BankID = Bank.BankName ' +

                'WHERE PO.PID = "' + cid + '" ';
    //console.log(qrStr);

    tx.executeSql(qrStr, [], function (tx, results) {
        //console.log(results.rows.length);

        if (results.rows.length != 0) {
            //console.log(results.rows[0]);
            if (results.rows[0].OccupationID != 7) {

                $("#dataTable").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
                + "<tbody>"
                + "<tr><td><b>อาชีพ</b></td><td>" + results.rows[0].OccupationDescription + "</td></tr>"
                + "<tr><td><b>ตำแหน่ง</b></td><td>" + results.rows[0].Position + "</td></tr>"
                + "<tr><td><b>ชื่อสถานที่ทำงาน</b></td><td>" + results.rows[0].Office + "</td></tr>"
                + "<tr><td><b>ตำบล</b></td><td>" + results.rows[0].POT + "</td></tr>"
                + "<tr><td><b>อำเภอ</b></td><td>" + results.rows[0].POC + "</td></tr>"
                + "<tr><td><b>จังหวัด</b></td><td>" + results.rows[0].POP + "</td></tr>"
                + "<tr><td><b>รหัสไปรษณีย์</b></td><td>" + results.rows[0].Postcode + "</td></tr>"
                + "<tr><td><b>เบอร์โทรศัพท์</b></td><td>" + results.rows[0].Phone + "</td></tr>"
                + "<tr><td><b>โทรสาร</b></td><td>" + results.rows[0].Fax + "</td></tr>"

                + "<tr><td><b> </b></td><td> </td></tr>"
                + "<tr><td><b> </b></td><td> </td></tr>"
                + "<tr><td><b>สถานภาพทางการศึกษา</b></td><td>" + getEduStatus(results.rows[0].EduStatus) + "</td></tr>"
                + "<tr><td><b>ระดับชั้น</b></td><td>" + results.rows[0].EduL + "</td></tr>"
                + "<tr><td><b>ปีการศึกษา</b></td><td>" + results.rows[0].EduYear + "</td></tr>"
                + "<tr><td><b>ชื่อสถานบันการศึกษา</b></td><td>" + results.rows[0].Academy + "</td></tr>"
                + "<tr><td><b>ตำบล</b></td><td>" + results.rows[0].EduT + "</td></tr>"
                + "<tr><td><b>อำเภอ</b></td><td>" + results.rows[0].EduC + "</td></tr>"
                + "<tr><td><b>จังหวัด</b></td><td>" + results.rows[0].EduP + "</td></tr>"

                + "<tr><td><b> </b></td><td> </td></tr>"
                + "<tr><td><b> </b></td><td> </td></tr>"
                + "<tr><td><b>รายได้</b></td><td>" + results.rows[0].Income + "</td></tr>"
                + "<tr><td><b>รายจ่าย</b></td><td>" + results.rows[0].Outgoing + "</td></tr>"
                + "<tr><td><b>หนี้ในระบบ</b></td><td>" + results.rows[0].InDebt + "</td></tr>"
                + "<tr><td><b>หนี้นอกระบบ</b></td><td>" + results.rows[0].ExDebt + "</td></tr>"

                + "<tr><td><b> </b></td><td> </td></tr>"
                + "<tr><td><b> </b></td><td> </td></tr>"
                + "<tr><td><b>ชื่อธนาคาร</b></td><td>" + results.rows[0].BankIDDesc + "</td></tr>"
                + "<tr><td><b>สาขา</b></td><td>" + results.rows[0].BankBranch + "</td></tr>"
                + "<tr><td><b>เลขบัญชี</b></td><td>" + results.rows[0].AccountNo + "</td></tr>"
                + "<tr><td><b>ชื่อบัญชี</b></td><td>" + results.rows[0].AccountName + "</td></tr>"

                + "</tbody>").closest("table#dataTable").table("refresh").trigger("create");
            }
            else {
                $("#dataTable").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
                + "<tbody>"

                + "<tr><td><b>อาชีพ</b></td><td>" + results.rows[0].OccupationDescription + "</td></tr>"
                + "<tr><td><b>สถานภาพทางการศึกษา</b></td><td>" + getEduStatus(results.rows[0].EduStatus) + "</td></tr>"
                + "<tr><td><b>ระดับชั้น</b></td><td>" + results.rows[0].EduL + "</td></tr>"
                + "<tr><td><b>ปีการศึกษา</b></td><td>" + results.rows[0].EduYear + "</td></tr>"
                + "<tr><td><b>ชื่อสถานบันการศึกษา</b></td><td>" + results.rows[0].Academy + "</td></tr>"
                + "<tr><td><b>ตำบล</b></td><td>" + results.rows[0].EduT + "</td></tr>"
                + "<tr><td><b>อำเภอ</b></td><td>" + results.rows[0].EduC + "</td></tr>"
                + "<tr><td><b>จังหวัด</b></td><td>" + results.rows[0].EduP + "</td></tr>"

                + "<tr><td><b> </b></td><td> </td></tr>"
                + "<tr><td><b> </b></td><td> </td></tr>"
                + "<tr><td><b>เลขบัตรประชาชน</b></td><td>" + results.rows[0].PID + "</td></tr>"
                + "<tr><td><b>ผู้ปกครอง</b></td><td>" + results.rows[0].TitleDESC + " " + results.rows[0].FirstName + " " + results.rows[0].LastName + "</td></tr>"
                + "<tr><td><b>เบอร์โทร</b></td><td>" + results.rows[0].PerPhone + "</td></tr>"
                + "<tr><td><b>เกี่ยวข้องเป็น</b></td><td>" + results.rows[0].RelationDescription + "</td></tr>"

                + "<tr><td><b>บ้านเลขที่</b></td><td>" + results.rows[0].HouseNumber + "</td></tr>"
                + "<tr><td><b>หมู่ที่</b></td><td>" + results.rows[0].MooNumber + "</td></tr>"
                + "<tr><td><b>หมู่บ้าน</b></td><td>" + results.rows[0].VillageName + "</td></tr>"
                + "<tr><td><b>ซอย</b></td><td>" + results.rows[0].Alley + "</td></tr>"
                + "<tr><td><b>ถนน</b></td><td>" + results.rows[0].StreetName + "</td></tr>"
                + "<tr><td><b>ตำบล</b></td><td>" + results.rows[0].AddrT + "</td></tr>"
                + "<tr><td><b>อำเภอ</b></td><td>" + results.rows[0].AddrC + "</td></tr>"
                + "<tr><td><b>จังหวัด</b></td><td>" + results.rows[0].AddrP + "</td></tr>"
                + "<tr><td><b>รหัสไปรษณีย์</b></td><td>" + results.rows[0].PerPostCode + "</td></tr>"

                + "</tbody>").closest("table#dataTable").table("refresh").trigger("create");
            }


        } else {
            $("#dataTable").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
                + "<tbody>"
                + "<tr><td><b>ไม่พบข้อมูล</b></td></tr>"
                + "</tbody>").closest("table#dataTable").table("refresh").trigger("create");
        }
    }, errorCB);
}

function getPerson(type) {
    var joinTable = "";
    if (type == 'personRecord') {
        joinTable = "FROM PersonRecordBy Pers "
        + "LEFT JOIN Relation R ON R.RelationID = Pers.Relation "
        + "LEFT JOIN Person P ON P.PID = Pers.RecordPID ";
    }
    else if (type == 'standIn') {
        joinTable = ", B.BankIDDesc, PerB.BankBranch, PerB.AccountNo, PerB.AccountName FROM PersonStandIn Pers "
        + "LEFT JOIN Relation R ON R.RelationID = Pers.StandInRole "
        + "LEFT JOIN Person P ON P.PID = Pers.StandIn "
        + "LEFT JOIN PersonBankAccount PerB ON PerB.PID = P.PID "
        + "LEFT JOIN Bank B ON B.BankID = PerB.BankName ";
    }
    else if (type == 'emergency') {
        joinTable = "FROM PersonEmergencyContact Pers "
        + "LEFT JOIN Relation R ON R.RelationID = Pers.EmergencyContactRole "
        + "LEFT JOIN Person P ON P.PID = Pers.EmergencyContactPID ";
    }
    else { }

    var queryPerson = "SELECT P.PID, T.TitleDESC, P.FirstName, P.LastName, P.Phone, R.RelationDescription, "
        + "A.HouseNumber, A.MooNumber, A.VillageName, A.Alley, A.StreetName, A.PostCode, T.TumbonDescription, C.CityDescription, Pro.ProvinceDescription "
        + joinTable
        + "LEFT JOIN Title T ON T.TitleID = P.Title "
        + "LEFT JOIN PersonVSAddress PVA ON PVA.PID = P.PID "
        + "LEFT JOIN Address A ON A.AddressID = PVA.AddressID "
        + "LEFT JOIN Province Pro ON Pro.ProvinceID = A.Province "
        + "LEFT JOIN Tumbon T ON T.TumbonID = A.Tumbon "
        + "LEFT JOIN City C ON C.CityID = A.Amphor "

        + "WHERE Pers.PID = '" + cid + "' ";

    db.transaction(function (tx) {
        tx.executeSql(queryPerson, [], function (tx, results) {
            //$('#dataHeader').empty(); $('#dataHeader').append('ข้อมูลส่วนบุคคล');
            if (results.rows.length != 0 && results.rows[0].PID != null) {
                //console.log(results.rows[0]);                                

                $("#dataTable").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
                    + "<tbody>"

                    + "<tr><td><b>เลขบัตรประชาชน</b></td><td>" + results.rows[0].PID + "</td></tr>"
                    + "<tr><td><b>ชื่อ-นามสกุล</b></td><td>" + results.rows[0].TitleDESC + " " + results.rows[0].FirstName + " " + results.rows[0].LastName + "</td></tr>"
                    + "<tr><td><b>เบอร์โทร</b></td><td>" + results.rows[0].Phone + "</td></tr>"
                    + "<tr><td><b>เกี่ยวข้องเป็น</b></td><td>" + results.rows[0].RelationDescription + "</td></tr>"

                    + "<tr><td><b>บ้านเลขที่</b></td><td>" + results.rows[0].HouseNumber + "</td></tr>"
                    + "<tr><td><b>หมู่ที่</b></td><td>" + results.rows[0].MooNumber + "</td></tr>"
                    + "<tr><td><b>หมู่บ้าน</b></td><td>" + results.rows[0].VillageName + "</td></tr>"
                    + "<tr><td><b>ซอย</b></td><td>" + results.rows[0].Alley + "</td></tr>"
                    + "<tr><td><b>ถนน</b></td><td>" + results.rows[0].StreetName + "</td></tr>"
                    + "<tr><td><b>ตำบล</b></td><td>" + results.rows[0].TumbonDescription + "</td></tr>"
                    + "<tr><td><b>อำเภอ</b></td><td>" + results.rows[0].CityDescription + "</td></tr>"
                    + "<tr><td><b>จังหวัด</b></td><td>" + results.rows[0].ProvinceDescription + "</td></tr>"
                    + "<tr><td><b>รหัสไปรษณีย์</b></td><td>" + results.rows[0].Postcode + "</td></tr>"

                    + "</tbody>").closest("table#dataTable").table("refresh").trigger("create");

                if (type == 'standIn') {
                    $("#dataTable tbody").append("<tr><td><b>ชื่อธนาคาร</b></td><td>" + results.rows[0].BankIDDesc + "</td></tr>"
                    + "<tr><td><b>สาขา</b></td><td>" + results.rows[0].BankBranch + "</td></tr>"
                    + "<tr><td><b>เลขบัญชี</b></td><td>" + results.rows[0].AccountNo + "</td></tr>"
                    + "<tr><td><b>ชื่อบัญชี</b></td><td>" + results.rows[0].AccountName + "</td></tr>").closest("table#dataTable").table("refresh").trigger("create");
                }


            } else {
                $("#dataTable").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
                    + "<tbody>"
                    + "<tr><td><b>ไม่พบข้อมูล</b></td></tr>"
                    + "</tbody>").closest("table#dataTable").table("refresh").trigger("create");
            }

        }, errorCB)
    }, errorCB);

}

function addAccidList(tx) {
    $('#accidSelect').empty();
    var queryStrAccid = "SELECT AccidID, AccidDate, AccidType FROM PersonAccident WHERE PID = '" + cid + "' ";
    tx.executeSql(queryStrAccid, [], function (tx, response) {
        if ($('#accidSelect').children('option').length == 0) {
            //console.log($('#accidSelect').children('option').length + " | 0");
            for (var i = 0; i < response.rows.length ; i++) {
                $('#accidSelect').append($("<option></option>").val(response.rows[i].AccidID).html(getQuestionJSDesc("AccidType", response.rows[i].AccidType)));
            }
            $('#accidSelect').selectmenu('refresh');
            if (sessionStorage.getItem('SubMenuID') === null) {
                showReport('propReport');
            }
            else {
                $('#menuReport li').each(function (index) {
                    if (sessionStorage.getItem('SubMenuID') == this.id) {
                        showReport(sessionStorage.getItem('SubMenuID'));
                        return false;
                    }
                    else {
                        if (index == $('#menuReport li').length - 1) {
                            showReport('propReport');
                        }
                    }
                });
            }

        }
        else { console.log("Already have accid list"); }

    }, errorCB);

}
function showReport(type) {
    var bodyHelp = [], bodyDmg = "", CID = sessionStorage.getItem("PersonCID");
    $('.reportNav ul').empty(); //console.log('empty');

    $('#menuReport li a').removeClass('ui-btn-active');
    $('#menuReport #' + type + ' a').addClass('ui-btn-active');

    $('#spanReportDmg').empty().append(0);
    $('#spanReportAssist').empty().append(0);
    $('#spanGuide').empty().append(0);

    var queryDataAndChoice = "SELECT A.*, C.ChoiceDesc, CG.GroupTypeDesc FROM AccidentDamageAndAssist A "
    + "LEFT JOIN Choice C ON C.ChoiceID = A.Value "
    + "LEFT JOIN ChoiceGroupType CG ON CG.GroupTypeID = A.GroupType "
    + "WHERE A.AccidID = '" + $('#accidSelect').val() + "' AND A.GroupType = C.GroupTypeID "
    + "OR ((A.GroupType = '001' OR A.GroupType LIKE '01%' OR A.GroupType = '025' OR A.GroupType = '028') AND A.AccidID = '" + $('#accidSelect').val() + "') "
    + "ORDER BY A.GroupType ";

    db.transaction(function (tx) {
        tx.executeSql(queryDataAndChoice, [], function (tx, rs) {
            if (rs.rows.length != 0) {
                var counter_1 = 0, counter_2 = 0, otherGuide = [];

                for (var i = 0; i < rs.rows.length ; i++) {
                    if (type == "propReport") {
                        if (rs.rows[i].GroupType == '001') {
                            counter_1++;
                            $('#reportDmg').append('<li>' + counter_1 + '. ' + rs.rows[i].Note + ': ' + rs.rows[i].Value + ' บาท</li>').listview('refresh');
                        }
                        else if (rs.rows[i].GroupType.match(/01.*/)) {
                            counter_2++;
                            var txt = rs.rows[i].GroupTypeDesc.split(" ");
                            //$('#reportAssist').append('<li><h3>' + counter_2 + '. ' + txt[1] + '</h3><p>' + rs.rows[i].Note + ': ' + rs.rows[i].Value + ' บาท</p></li>').listview('refresh');
                            $('#reportAssist').append('<li><h3>' + counter_2 + '. ' + rs.rows[i].Note + ': ' + rs.rows[i].Value + ' บาท</h3><p>' + txt[1] + '</p></li>').listview('refresh');
                        }
                        else { }
                    }
                    else if (type == "bodyReport") {
                        if (rs.rows[i].GroupType == '002') {
                            counter_1++;
                            bodyDmg = rs.rows[i].Value;
                            $('#reportDmg').append('<li>' + counter_1 + '. ' + rs.rows[i].ChoiceDesc + '</li>').listview('refresh');
                        }
                        else if (rs.rows[i].GroupType.match(/02.*/)) {
                            counter_2++;
                            if (rs.rows[i].GroupType == '025' || rs.rows[i].GroupType == '028') {
                                if (rs.rows[i].GroupType == '025') {
                                    var txt = rs.rows[i].GroupTypeDesc.split(" ");
                                    //$('#reportAssist').append('<li><h3 class="txtWrap">' + counter_2 + '. ' + txt[1] + '</h3><p class="txtWrap">' + rs.rows[i].Note + ': ' + rs.rows[i].Value + ' บาท</p></li>').listview('refresh');
                                    $('#reportAssist').append('<li><h3 class="txtWrap">' + counter_2 + '. ' + rs.rows[i].Note + ': ' + rs.rows[i].Value + ' บาท</h3><p class="txtWrap">' + txt[1] + '</p></li>').listview('refresh');
                                }
                                else {
                                    var txt = rs.rows[i].GroupTypeDesc.split(" ");
                                    //$('#reportAssist').append('<li><h3 class="txtWrap">' + counter_2 + '. (นักเรียน/นักศึกษา) ' + txt[1] + '</h3><p class="txtWrap">' + rs.rows[i].Note + ': ' + rs.rows[i].Value + ' บาท</p></li>').listview('refresh');
                                    $('#reportAssist').append('<li><h3 class="txtWrap">' + counter_2 + '. ' + rs.rows[i].Note + ': ' + rs.rows[i].Value + ' บาท</h3><p class="txtWrap">' + txt[1] + ' (นักเรียน/นักศึกษา)</p></li>').listview('refresh');
                                }

                            }
                            else {
                                if (rs.rows[i].GroupType == '026' || rs.rows[i].GroupType == '027') {
                                    var txt = rs.rows[i].GroupTypeDesc.split(" ");
                                    //$('#reportAssist').append('<li><h3 class="txtWrap">' + counter_2 + '. (นักเรียน/นักศึกษา) ' + txt[1] + '</h3><p class="txtWrap">' + rs.rows[i].ChoiceDesc + '</p></li>').listview('refresh');
                                    $('#reportAssist').append('<li><h3 class="txtWrap">' + counter_2 + '. ' + rs.rows[i].ChoiceDesc + '</h3><p class="txtWrap">' + txt[1] + ' (นักเรียน/นักศึกษา)</p></li>').listview('refresh');
                                }
                                else {
                                    var txt = rs.rows[i].GroupTypeDesc.split(" ");
                                    //$('#reportAssist').append('<li><h3 class="txtWrap">' + counter_2 + '. ' + txt[1] + '</h3><p class="txtWrap">' + rs.rows[i].ChoiceDesc + '</p></li>').listview('refresh');
                                    $('#reportAssist').append('<li><h3 class="txtWrap">' + counter_2 + '. ' + rs.rows[i].ChoiceDesc + '</h3><p class="txtWrap">' + txt[1] + '</p></li>').listview('refresh');
                                }
                                bodyHelp.push(rs.rows[i].Value + "/" + rs.rows[i].GroupType);
                            }

                        }
                        else { }
                    }
                    else if (type == "otherReport") {
                        if (rs.rows[i].GroupType == '003') {
                            counter_1++;
                            $('#reportDmg').append('<li>' + counter_1 + '. ' + rs.rows[i].Note + '</li>').listview('refresh');
                            otherGuide.push(rs.rows[i].Note);
                        }
                        else if (rs.rows[i].GroupType == '004') {
                            counter_1++;
                            if (rs.rows[i].Value != '999') {
                                $('#reportDmg').append('<li>' + counter_1 + '. ' + rs.rows[i].ChoiceDesc + '</li>').listview('refresh');
                                otherGuide.push(rs.rows[i].ChoiceDesc);
                            }
                            else {
                                $('#reportDmg').append('<li>' + counter_1 + '. ' + rs.rows[i].Note + '</li>').listview('refresh');
                                otherGuide.push(rs.rows[i].Note);
                            }

                        }
                        else if (rs.rows[i].GroupType == '031') {
                            counter_2++;
                            if (rs.rows[i].Value != '999') {
                                $('#reportAssist').append('<li>' + counter_2 + '. ' + rs.rows[i].ChoiceDesc + '</li>').listview('refresh');
                                for (var j = otherGuide.length - 1; j >= 0; j--) {
                                    if (otherGuide[j] === rs.rows[i].ChoiceDesc) {
                                        otherGuide.splice(j, 1);
                                        // break;
                                    }
                                }
                            }
                            else {
                                $('#reportAssist').append('<li>' + counter_2 + '. ' + rs.rows[i].Note + '</li>').listview('refresh');
                                for (var j = otherGuide.length - 1; j >= 0; j--) {
                                    if (otherGuide[j] === rs.rows[i].Note) {
                                        otherGuide.splice(j, 1);
                                        // break;
                                    }
                                }
                            }
                        }
                        else { }
                    }
                    else { }

                }

                if (type == "propReport") {
                    $('#reportGuide').append('<li><h3 class="txtWrap">การช่วยเหลือด้านทรัพย์สิน</h3> '
                    + '<p class="txtWrap">เจ้าหน้าที่จะใช้เวลาตรวจสอบข้อเท็จจริงต่างๆ และประเมินราคาทรัพย์สินที่เสียหายทั้งหมด</p> '
                    + '<p class="txtWrap">• หากตรวสอบแล้วว่าเป็นเหตุการณ์ที่เกิดจากสถานการณ์ความไม่สงบฯ ศูนย์ช่วยเหลือเยียวยาฯ ระดับจังหวัดจะจ่าย 100 % จากวงเงินที่ประเมิน  ภายใน 2 สัปดาห์</p> '
                    + '<p class="txtWrap">• หากไม่แล้วเสร็จภายใน 2 สัปดาห์ ศูนย์ช่วยเหลือเยียวยาฯ ระดับจังหวัดจะจ่ายเงินไปก่อน 25% จากวงเงินที่ประเมินไว้ในเบื้องต้น และเมื่อประเมินเสร็จเรียบร้อยจะจ่ายเพิ่มอีก 75% ของวงเงินที่เหลือ โดยจะใช้เวลาประมาณ  4 สัปดาห์</p>'
                    + '</li>').listview('refresh');
                }
                else if (type == "bodyReport") {
                    console.log(bodyHelp); //console.log(bodyDmg);

                    var helpGuide = [];

                    var queryForGuide = "SELECT PO.PID, PO.OccupationID, PE.EduLevel, PF2.FamilyRole, PF2.PID AS PFamCID, PE2.EduLevel AS PFamEduLv "
                    + "FROM PersonOccupation PO "
                    + "LEFT JOIN PersonEducation PE ON PE.PID = PO.PID "
                    + "LEFT JOIN PersonFamily PF ON PF.PID = PO.PID "
                    + "LEFT JOIN PersonFamily PF2 ON PF2.FamilyID = PF.FamilyID "
                    + "LEFT JOIN PersonOccupation PO2 ON PO2.PID = PF2.PID "
                    + "LEFT JOIN PersonEducation PE2 ON PE2.PID = PF2.PID "
                    + "WHERE (PO.PID = '" + CID + "' AND PF2.PID = '" + CID + "') "
                    + "OR (PO.PID = '" + CID + "' AND PE2.EduStatus = '001') ";

                    db.transaction(function (tx) {
                        tx.executeSql(queryForGuide, [], function (tx, rs) {
                            if (rs.rows.length != 0) {
                                for (var i = 0; i < rs.rows.length; i++) {
                                    if (rs.rows[i].PFamCID == CID) {
                                        console.log(rs.rows[i]);
                                        var res = checkBody(bodyDmg, rs.rows[i], '1');
                                        console.log(res);
                                        console.log(helpGuide);
                                        if (helpGuide.length == 0) {
                                            helpGuide = res;
                                        }
                                        else {
                                            for (var j = 0; j < res.length ; j++) {
                                                var isDup = true;
                                                for (var k = 0; k < helpGuide.length; k++) {
                                                    if (res[j] != helpGuide[k]) {
                                                        isDup = false;
                                                    }
                                                    else {
                                                        isDup = true;
                                                        k = helpGuide.length - 1; //return false;
                                                    }
                                                }
                                                if (isDup == false) {
                                                    helpGuide.push(res[j]);
                                                } else { }
                                            }
                                        }
                                        console.log(helpGuide);
                                    }
                                    else if (rs.rows[i].PFamCID != CID) {
                                        console.log(rs.rows[i]);
                                        var res = checkBody(bodyDmg, rs.rows[i], '2');
                                        console.log(res);
                                        console.log(helpGuide);

                                        if (helpGuide.length == 0) {
                                            helpGuide = res;
                                        }
                                        else {
                                            for (var j = 0; j < res.length ; j++) {
                                                var isDup = true;
                                                for (var k = 0; k < helpGuide.length; k++) {
                                                    if (res[j] != helpGuide[k]) {
                                                        isDup = false;
                                                    }
                                                    else {
                                                        isDup = true;
                                                        k = helpGuide.length - 1; //return false;
                                                    }
                                                }
                                                if (isDup == false) {
                                                    helpGuide.push(res[j]);
                                                } else { }
                                            }
                                        }
                                        console.log(helpGuide);

                                    } else { }
                                }
                            } else { }
                            console.log(helpGuide);
                            for (var k = bodyHelp.length - 1; k >= 0; k--) {
                                for (var l = helpGuide.length - 1; l >= 0; l--) {
                                    if (bodyHelp[k] === helpGuide[l]) {
                                        helpGuide.splice(l, 1);
                                        // break;
                                    }
                                }
                            } console.log(helpGuide);
                            convertGuideToTxt(helpGuide);
                        }, errorCB);
                    }, errorCB);

                }
                else if (type == "otherReport") {
                    counter_1 = 0;
                    for (var i = 0 ; i < otherGuide.length; i++) {
                        counter_1++;
                        $('#reportGuide').append('<li>' + counter_1 + '. ' + otherGuide[i] + '</li>').listview('refresh');
                    }
                }

                //console.log(otherGuide);
                $('#spanReportDmg').empty().append($('#reportDmg li').length);
                $('#spanReportAssist').empty().append($('#reportAssist li').length);
                $('#spanGuide').empty().append($('#reportGuide li').length);
            }
            else { }
        }, errorCB);
    }, errorCB);


}

function convertGuideToTxt(helpGuideStr) {
    //console.log(helpGuideStr);
    var bodyGuideStr = [];

    db.transaction(function (tx) {
        tx.executeSql("SELECT C.*, CG.GroupTypeDesc FROM Choice C "
            + "LEFT JOIN ChoiceGroupType CG ON CG.GroupTypeID = C.GroupTypeID "
            + "WHERE C.GroupTypeID LIKE '02%' ", [], function (tx, rs) {
                //console.log(rs.rows.length);
                if (rs.rows.length != 0) {
                    for (var j = 0; j < helpGuideStr.length; j++) {
                        var splitTxt = [];
                        for (var i = 0 ; i < rs.rows.length; i++) {
                            //splitTxt = helpGuideStr[j].split("/");
                            joinRes = rs.rows[i].ChoiceID + "/" + rs.rows[i].GroupTypeID;
                            if (joinRes == helpGuideStr[j]) {
                                var txt = rs.rows[i].GroupTypeDesc.split(" ");
                                bodyGuideStr.push(txt[1] + "//" + rs.rows[i].ChoiceDesc);
                                i = rs.rows.length - 1;
                            } else { }
                        }
                    }
                }
                //console.log(bodyGuideStr);
                var counter_1 = 0;
                for (var i = 0 ; i < bodyGuideStr.length; i++) {
                    counter_1++;
                    var splitBodyGuide = bodyGuideStr[i].split("//");
                    //$('#reportGuide').append('<li><h3 class="txtWrap">' + counter_1 + '. ' + splitBodyGuide[0] + '</h3><p class="txtWrap">' + splitBodyGuide[1] + '</p></li>').listview('refresh');
                    $('#reportGuide').append('<li><h3 class="txtWrap">' + counter_1 + '. ' + splitBodyGuide[1] + '</h3><p class="txtWrap">' + splitBodyGuide[0] + '</p></li>').listview('refresh');
                    //$('#reportGuide').append('<li>' + counter_1 + '. ' + bodyGuideStr[i] + '</li>').listview('refresh');
                }
                $('#spanGuide').empty().append($('#reportGuide li').length);
            }, errorCB);
    }, errorCB);

}

function getBodyGuide(bodyDmg, CID) {
    var helpGuide = [];

    var queryForGuide = "SELECT PO.PID, PO.OccupationID, PE.EduLevel, PF2.FamilyRole, PF2.PID AS PFamCID, PE2.EduLevel AS PFamEduLv "
    + "FROM PersonOccupation PO "
    + "LEFT JOIN PersonEducation PE ON PE.PID = PO.PID "
    + "LEFT JOIN PersonFamily PF ON PF.PID = PO.PID "

    + "LEFT JOIN PersonFamily PF2 ON PF2.FamilyID = PF.FamilyID "
    + "LEFT JOIN PersonOccupation PO2 ON PO2.PID = PF2.PID "
    + "LEFT JOIN PersonEducation PE2 ON PE2.PID = PF2.PID "
    + "WHERE (PO.PID = '" + CID + "' AND PF2.PID = '" + CID + "') "
    + "OR (PO.PID = '" + CID + "' AND PE2.EduStatus = '001') ";

    db.transaction(function (tx) {
        tx.executeSql(queryForGuide, [], function (tx, rs) {
            if (rs.rows.length != 0) {
                for (var i = 0; i < rs.rows.length; i++) {
                    if (rs.rows[i].PFamCID == CID) {
                        console.log(rs.rows[i]);
                        var res = checkBody(bodyDmg, rs.rows[i], '1');
                        console.log(res);

                        if (helpGuide.length == 0) {
                            helpGuide = res;
                        }
                        else {
                            for (var j = 0; j < res.length ; j++) {
                                var isDup = true;
                                for (var k = 0; k < helpGuide.length; k++) {
                                    if (res[j] != helpGuide[k]) {
                                        isDup = false;
                                    }
                                    else {
                                        isDup = true;
                                        return false;
                                    }
                                }
                                if (isDup == false) {
                                    helpGuide.push(res[j]);
                                } else { }
                            }
                        }
                    }
                    else if (rs.rows[i].PFamCID != CID) {
                        console.log(rs.rows[i]);
                        var res = checkBody(bodyDmg, rs.rows[i], '2');
                        console.log(res);

                        if (helpGuide.length == 0) {
                            helpGuide = res;
                        }
                        else {
                            for (var j = 0; j < res.length ; j++) {
                                var isDup = true;
                                for (var k = 0; k < helpGuide.length; k++) {
                                    if (res[j] != helpGuide[k]) {
                                        isDup = false;
                                    }
                                    else {
                                        isDup = true;
                                        return false;
                                    }
                                }
                                if (isDup == false) {
                                    helpGuide.push(res[j]);
                                } else { }
                            }
                        }

                    } else { }
                }
            } else { }
            console.log(helpGuide);
            return helpGuide;
        }, errorCB);
    }, errorCB);

}
function checkBody(dmg, rs, type) {
    // type | 1 = ผู้รับบริการ, 2 = ครอบครัวของผู้รับบริการ
    var bodyGuide = [];

    if (type == '1') {
        //บาดเจ็บเล็กน้อย
        if (dmg == "001") {
            bodyGuide = ["001/021", "001/022", "005/023", "006/023", "007/023"];

            if (rs.OccupationID != '4') {
                bodyGuide.push("001/024");
            }

            /*
            bodyGuide = [
                { "ChoiceID": "001", "GroupTypeID": "021" },
                { "ChoiceID": "001", "GroupTypeID": "022" },
                { "ChoiceID": "005", "GroupTypeID": "023" },
                { "ChoiceID": "006", "GroupTypeID": "023" },
                { "ChoiceID": "007", "GroupTypeID": "023" }
            ];

            if (rs.OccupationID != '4') {
                bodyGuide.push({ "ChoiceID": "001", "GroupTypeID": "024" });
            }
            */

        }
            //บาดเจ็บปานกลาง
        else if (dmg == "002") {
            bodyGuide = ["002/021", "001/022", "005/023", "006/023", "007/023"];

            if (rs.OccupationID != '4') {
                bodyGuide.push("001/024");
            }
        }
            //บาดเจ็บสาหัส
        else if (dmg == "003") {
            bodyGuide = ["003/021", "002/022", "005/023", "006/023", "007/023", "003/024"];

            if (rs.OccupationID != '4') {
                bodyGuide.push("001/024");
            } else { }

        }
            // ทุพพลภาพ/พิการ
        else if (dmg == "004") {
            bodyGuide = ["004/021", "003/024", "004/024", "003/022"];

            if (rs.OccupationID != '4') {
                bodyGuide.push("001/024");
            }
        }
            // เสียชีวิต
        else if (dmg == "006" || dmg == "007") {
            bodyGuide = ["005/021", "001/023", "002/023", "004/023"];

            // หัวหน้าครอบครัวเสีย
            if (dmg == "006") {
                bodyGuide.push("005/024", "003/023", "004/022");
            }
                // สมาชิกครอบครัวเสีย
            else if (dmg == "007") {
                bodyGuide.push("005/022");
            } else { }

            if (rs.OccupationID != '4') {
                bodyGuide.push("002/024");
            }
        }
        else { }
    }

    if ((rs.OccupationID == '7' && type == '1') || type == '2') {
        console.log('type = 2');
        if (dmg == "003" || dmg == "004" || dmg == "006" || dmg == "007") {
            if (rs.PFamEduLv == '03' || rs.PFamEduLv == '04' || rs.PFamEduLv == '05') { // อนุบาล/ประถม
                bodyGuide.push("001/026", "002/027");
            }
            else if (rs.PFamEduLv == '01' || rs.PFamEduLv == '02' || rs.PFamEduLv == '08' || rs.PFamEduLv == '06' || rs.PFamEduLv == '07' || rs.PFamEduLv == '09') { // ก่อนวัยเรียน/กศน./มัธยมศึกษา/ปวช.
                bodyGuide.push("002/026");

                if (rs.PFamEduLv == '08') { // กศน. / ปอเนาะ
                    bodyGuide.push("001/027");
                }
                else if (rs.PFamEduLv == '06' || rs.PFamEduLv == '07' || rs.PFamEduLv == '09') { // ม. / ปวช.
                    bodyGuide.push("003/027");
                } else { }
            }
            else if (rs.PFamEduLv == '10' || rs.PFamEduLv == '11' || rs.PFamEduLv == '12' || rs.PFamEduLv == '13') { // ปวส./อุดมศึกษา
                bodyGuide.push("003/026", "004/027");
            }
            else { }

        } else { }
    }

    return bodyGuide;
}

function loadGoogleMaps(divPosition, aLat, aLng) {
    $('#mapAboveTable, #mapBelowTable, #mapFam, #mapAcci').html("");

    var isEdit = false;

    if (aLat == 0.0 && aLng == 0.0) {
        isEdit = false;
        aLat = 13.76486874;
        aLng = 100.5382767; //bkk
    } else { isEdit = true; }


    var mapOptions = {
        center: new google.maps.LatLng(aLat, aLng), //{ lat: -34.397, lng: 150.644 },
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.HYBRID
    };

    var mapObj;
    if (divPosition == "mapAboveTable") {
        mapObj = document.getElementById("mapAboveTable");
        document.getElementById("divMapAbove").style.display = "block";
    }
    else if (divPosition == "mapBelowTable") {

        mapObj = document.getElementById("mapBelowTable");
        document.getElementById("divMapBelow").style.display = "block";
    }
    else if (divPosition == "mapFam") {
        mapObj = document.getElementById("mapFam");
        document.getElementById("divMapFam").style.display = "block";
    }
    else if (divPosition == "mapAcci") {
        mapObj = document.getElementById("mapAcci");
        document.getElementById("divMapAcci").style.display = "block";
    }
    else { }

    var map = new google.maps.Map(mapObj, mapOptions);
    //map.setTilt(45);

    if (isEdit) {
        var aMarker = new google.maps.Marker({
            position: new google.maps.LatLng(aLat, aLng),
            icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
        });
        aMarker.setMap(map);
    } else { }

}

//----------- profile --------------

//----------- profileFam --------------

$(document).on("pagebeforeshow", "#profileFam", function () {
    //db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);

    famCid = sessionStorage.getItem('FamilyCID');
    //console.log(sessionStorage.getItem('MenuID'));
    sessionStorage.removeItem('EditType');

    db.transaction(function (tx) {
        //tx.executeSql("SELECT P.FirstName, P.LastName FROM Person P WHERE P.PID = '" + famCid + "'", [], function (tx, results) {
        tx.executeSql("SELECT P.FirstName, P.LastName, R.RelationDescription FROM Person P LEFT JOIN PersonFamily PF ON PF.PID = P.PID LEFT JOIN Relation R ON R.RelationID = PF.FamilyRole WHERE P.PID = '" + famCid + "'", [], function (tx, results) {
            if (results.rows.length != 0) {
                if (sessionStorage.getItem("MenuID") == "personFamily") {
                    $("#headerProfileFam").empty().append(results.rows[0].FirstName + " " + results.rows[0].LastName + " (" + results.rows[0].RelationDescription + ")");
                }
                else if (sessionStorage.getItem("MenuID") == "personMate") {
                    $("#headerProfileFam").empty().append(results.rows[0].FirstName + " " + results.rows[0].LastName + " (คู่สมรส)");
                }
                else {
                    $("#headerProfileFam").empty().append(results.rows[0].FirstName + " " + results.rows[0].LastName);
                }

            } else { }
        }, errorCB);
    }, errorCB);

    $('#menuUlFam li').each(function (countN) {
        //console.log(countN);        
        if (sessionStorage.getItem("MenuID_2") != null && $(this).attr("id") == sessionStorage.getItem("MenuID_2")) {
            $('#menuUlFam #' + $(this).attr("id")).click();
            return false;
        }
        else if (countN == $('#menuUlFam li').length - 1) {
            $('#menuUlFam #personProfileFam').click();
        }
        else {
            //console.log("ไม่พบ Menu");
        }
    });

});

$(document).on("pageshow", "profileFam", function () {

});

$(document).on("pagecreate", "#profileFam", function () {

    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);
    //alert('create');    

    //$(document).on("swiperight", "#profileFam", function (e) {

    //    if ($(".ui-page-active").jqmData("panel") !== "open") {
    //        if (e.type === "swiperight") {
    //            $("#left-panel_fam").panel("open");
    //        }
    //    }
    //});

    var menuName = "person";
    //$('#addBtn').closest('.ui-btn').hide();

    $('#menuUlFam li').click(function () {
        $('#imgFamDiv, #divMapFam').css("display", "none");

        var id = this.id; // get id of clicked li
        $('#dataTablePerson').empty();
        //console.log(id);
        sessionStorage.setItem("MenuID_2", id);

        $('#menuUlFam li a').removeClass('ui-btn-active');
        $('#menuUlFam #' + id + ' a').addClass('ui-btn-active');

        if (id == 'personProfileFam') {
            menuName = "person";
            //$('#dataHeaderFam').empty(); $('#dataHeaderFam').append('ข้อมูลส่วนบุคคล');
            $('#dataTableFam thead').empty(); $('#dataTableFam tbody').empty();
            //document.getElementById("imgFamDiv").style.display = "inline";

            db.transaction(queryShownFamProfile, errorCB);

        }
        else if (id == 'personAddressFam') {
            menuName = "address";
            //$('#dataHeaderFam').empty(); $('#dataHeaderFam').append('ที่อยู่');
            $('#dataTableFam thead').empty(); $('#dataTableFam tbody').empty();
            db.transaction(queryShownFamAddress, errorCB);

        }
        else if (id == 'personalTypeFam') {
            menuName = "personal";
            //$('#dataHeaderFam').empty(); $('#dataHeaderFam').append('อาชีพ การศึกษา และอื่นๆ');
            $('#dataTableFam thead').empty(); $('#dataTableFam tbody').empty();
            db.transaction(queryShownFamType, errorCB);

        }
        else {
            console.log('ไม่พบ menuUl id');
        }
    });

    $('#editBtnFam').click(function () {
        if (menuName == "person") {
            sessionStorage.setItem('EditType', 'famPerson');
            window.location.href = 'person_data.html#info';

        } else if (menuName == "address") {
            sessionStorage.setItem('EditType', 'famAddress');
            window.location.href = 'person_data.html#address_info';

        } else if (menuName == "personal") {
            sessionStorage.setItem('EditType', 'famPersonalType');
            window.location.href = 'person_data.html#person_type';

        } else {
            console.log('ไม่พบ menuName');
        }
    });

});

function showFamList(tx) {
    $("#dataTablePerson").empty();

    tx.executeSql("SELECT * FROM PersonFamily WHERE PID = '" + cid + "' ", [],
        (function (tx, results) {
            if (results.rows.length == 0) {
                //console.log('have no familyID');                
            }
            else {
                //console.log('have');                                
                var queryGetFamily = "SELECT P.FirstName, P.LastName, P.PicPath, F.FamilyRole, F.PID, Rel.RelationDescription "
                    + "FROM Person P LEFT JOIN PersonFamily F ON P.PID = F.PID "
                    + "LEFT JOIN Relation Rel ON Rel.RelationID = F.FamilyRole "
                    + "WHERE F.FamilyID = '" + results.rows[0].FamilyID + "' ";
                //console.log(queryGetFamily);
                // "SELECT * FROM PersonFamily WHERE FamilyID = '" + results.rows[0].FamilyID + "' "
                tx.executeSql(queryGetFamily, [],
                (function (tx, rs) {
                    if (rs.rows.length == 0) {
                        //console.log('have no family');
                    }
                    else {
                        //console.log('have');
                        //$('#currentPerson').empty(); $('#currentPerson').append(1);
                        $('#maxPerson').empty().append(rs.rows.length - 1);

                        for (var i = 0; i < rs.rows.length; i++) {
                            if (rs.rows[i].PID != cid) {
                                //$("#dataTablePerson").append("<li class='ui-li-has-thumb' id='" + rs.rows[i].PID + "'><a class='ui-btn' data-transition='slide'><img id='" + rs.rows[i].PID + "-pic' style='width:100px; height:100px;' class='ui-thumbnail ui-thumbnail-circular' src='" + rs.rows[i].PicPath + "'><h2>"
                                //    + rs.rows[i].FirstName + " " + rs.rows[i].LastName + "</h2><p>" + rs.rows[i].PID + " " + rs.rows[i].RelationDescription + "</p></a></li>");

                                //imgID = "#" + rs.rows[i].PID + "-pic"; loadDefaultPic(imgID);

                                $("#dataTablePerson").append("<li id='" + rs.rows[i].PID + "'><a><h2>"
                                + rs.rows[i].FirstName + " " + rs.rows[i].LastName + " (" + rs.rows[i].RelationDescription + ") " + "</h2><p>" + rs.rows[i].PID + "</p></a></li>");

                            }
                        }
                        $("#dataTablePerson").listview('refresh');
                    }
                }), errorCB);
            }
        }), errorCB);
}
function showMateList(tx) {
    $("#dataTablePerson").empty();
    var queryGetMate = "SELECT P.FirstName, P.LastName, P.PicPath, M.MatePID "
                    + "FROM Person P INNER JOIN PersonMate M ON P.PID = M.MatePID "
                    + "WHERE M.PID = '" + cid + "' ";

    tx.executeSql(queryGetMate, [], (function (tx, rs) {
        if (rs.rows.length != 0) {
            //console.log('have');

            $('#maxPerson').empty(); $('#maxPerson').append(rs.rows.length);

            for (var i = 0; i < rs.rows.length; i++) {
                if (rs.rows[i].PID != cid) {
                    //$("#dataTablePerson").append("<li class='ui-li-has-thumb' id='" + rs.rows[i].MatePID + "'><a class='ui-btn' data-transition='slide'><img id='" + rs.rows[i].PID + "-pic' style='width:100px; height:100px;' class='ui-thumbnail ui-thumbnail-circular' src='" + rs.rows[i].PicPath + "'><h2>"
                    //    + rs.rows[i].FirstName + " " + rs.rows[i].LastName + "</h2><p>" + rs.rows[i].MatePID + "</p></a></li>");
                    //imgID = "#" + rs.rows[i].PID + "-pic"; loadDefaultPic(imgID);

                    $("#dataTablePerson").append("<li id='" + rs.rows[i].MatePID + "'><a><h2>"
                    + rs.rows[i].FirstName + " " + rs.rows[i].LastName + "</h2><p>" + rs.rows[i].MatePID + "</p></a></li>");

                }
            }
            $("#dataTablePerson").listview('refresh');

        }
        else {
            //console.log('have no mateID');                
        }
    }), errorCB);

}

function queryShownFamProfile(tx) {
    var queryPerson = "SELECT P.*, T.TitleDESC, N.NationalityDescription, R.RaceDescription, Re.Religion, M.MariageStatusDescription, Prov.ProvinceDescription"
    + " FROM Person P"
    + " LEFT JOIN Title T ON P.Title = T.TitleID"
    + " LEFT JOIN PersonVSAddress PVA ON P.PID = PVA.PID"
    + " LEFT JOIN Address A ON A.AddressID = PVA.AddressID"
    + " LEFT JOIN Nationality N ON N.NationalityID = P.Nation"
    + " LEFT JOIN Race R ON R.RaceID = P.Race"
    + " LEFT JOIN Religion Re ON Re.Id = P.Religion"
    + " LEFT JOIN MariageStatus M ON M.MariageStatusID = P.MarriageStatus"
    + " LEFT JOIN Province Prov ON Prov.ProvinceID = P.BirthProvince"
    + " WHERE P.PID = '" + famCid + "' ";

    tx.executeSql(queryPerson, [], function (tx, results) {

        //$("#headerProfileFam").empty();
        $('#dataHeaderFam').empty(); $('#dataHeaderFam').append('ข้อมูลส่วนบุคคล');
        if (results.rows.length != 0) {
            //console.log(results.rows[0]);
            $("#dataTableFam").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
                + "<tbody>"
                + "<tr><td><b>เลขบัตรประชาชน</b></td><td>" + results.rows[0].PID + "</td></tr>"
                + "<tr><td><b>ชื่อ-นามสกุล</b></td><td>" + results.rows[0].TitleDESC + " " + results.rows[0].FirstName + " " + results.rows[0].LastName + "</td></tr>"
                + "<tr><td><b>เบอร์โทร</b></td><td>" + results.rows[0].Phone + "</td></tr>"
                + "<tr><td><b>หมายเลขหนังสือเดินทาง</b></td><td>" + results.rows[0].Passport + "</td></tr>"
                + "<tr><td><b>วันเกิด</b></td><td>" + results.rows[0].DOB + "</td></tr>"

                + "<tr><td><b>สัญชาติ</b></td><td>" + results.rows[0].NationalityDescription + "</td></tr>"
                + "<tr><td><b>เชื้อชาติ</b></td><td>" + results.rows[0].RaceDescription + "</td></tr>"
                + "<tr><td><b>ศาสนา</b></td><td>" + results.rows[0].Religion + "</td></tr>"
                + "<tr><td><b>สถานภาพทางการสมรส</b></td><td>" + results.rows[0].MariageStatusDescription + "</td></tr>"
                + "<tr><td><b>ความพอใจในที่อยู่</b></td><td>" + getSatisfyStatus(results.rows[0].AddressSatisfy) + "</td></tr>"

                + "<tr><td><b>ภูมิลำเนา</b></td><td>" + results.rows[0].ProvinceDescription + "</td></tr>"
                + "</tbody>").closest("table#dataTableFam").table("refresh").trigger("create");

            //$("#headerProfileFam").append(results.rows[0].FirstName + " " + results.rows[0].LastName);
            //setColorTable("dataTableFam");
            //name = results.rows[0].FirstName + " " + results.rows[0].LastName;
            //$("#personicfRecord").append
        } else {
            $("#dataTableFam").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
                + "<tbody>"
                + "<tr><td><b>ไม่พบข้อมูล</b></td></tr>"
                + "</tbody>").closest("table#dataTableFam").table("refresh").trigger("create");
        }

    }, errorCB);
}
function queryShownFamAddress(tx) {

    var queryAddr = "SELECT A.*, P.ProvinceDescription, T.TumbonDescription, C.CityDescription, L.LiveHomeStatusDescription "
    + " FROM Address A"
    + " LEFT JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID"
    + " LEFT JOIN Province P ON P.ProvinceID = A.Province"
    + " LEFT JOIN Tumbon T ON T.TumbonID = A.Tumbon"
    + " LEFT JOIN City C ON C.CityID = A.Amphor"
    + " LEFT JOIN LiveHomeStatus L ON L.LiveHomeStatusID = A.LiveHomeStatusID"
    + " WHERE PVA.PID = '" + famCid + "' ";
    //console.log(queryAddr);

    //tx.executeSql('SELECT A.* FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = "' + cid + '" ', [], function (tx, results) {
    tx.executeSql(queryAddr, [], function (tx, results) {
        //console.log(results.rows[0]);
        if (results.rows.length != 0) {
            $("#dataTableFam").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
            + "<tbody>"
            + "<tr><td><b>รหัสบ้าน</b></td><td>" + results.rows[0].HomeCode + "</td></tr>"
            + "<tr><td><b>ประเภทบ้าน</b></td><td>" + results.rows[0].LiveHomeStatusDescription + "</td></tr>"
            + "<tr><td><b>บ้านเลขที่</b></td><td>" + results.rows[0].HouseNumber + "</td></tr>"
            + "<tr><td><b>หมู่ที่</b></td><td>" + results.rows[0].MooNumber + "</td></tr>"
            + "<tr><td><b>หมู่บ้าน</b></td><td>" + results.rows[0].VillageName + "</td></tr>"
            + "<tr><td><b>ซอย</b></td><td>" + results.rows[0].Alley + "</td></tr>"
            + "<tr><td><b>ถนน</b></td><td>" + results.rows[0].StreetName + "</td></tr>"
            + "<tr><td><b>ตำบล</b></td><td>" + results.rows[0].TumbonDescription + "</td></tr>"
            + "<tr><td><b>อำเภอ</b></td><td>" + results.rows[0].CityDescription + "</td></tr>"
            + "<tr><td><b>จังหวัด</b></td><td>" + results.rows[0].ProvinceDescription + "</td></tr>"
            + "<tr><td><b>รหัสไปรษณีย์</b></td><td>" + results.rows[0].Postcode + "</td></tr>"
            + "</tbody>").closest("table#dataTableFam").table("refresh").trigger("create");


            //if (navigator.onLine) {
            //    loadGoogleMaps("mapFam");
            //} else { }

            //setColorTable("dataTableFam");
        } else {
            $("#dataTableFam").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
                + "<tbody>"
                + "<tr><td><b>ไม่พบข้อมูล</b></td></tr>"
                + "</tbody>").closest("table#dataTableFam").table("refresh").trigger("create");
        }
    }, errorCB);
}
function queryShownFamType(tx) {

    var qrStr = 'SELECT PO.*, Edu.*, Fina.*, Bank.*, POO.OccupationDescription, POP.ProvinceDescription AS POP, POC.CityDescription AS POC, POT.TumbonDescription AS POT, ' +
                'EduL.EducationLevelDescription AS EduL, EduP.ProvinceDescription AS EduP, EduC.CityDescription AS EduC, EduT.TumbonDescription AS EduT, BankB.BankIDDesc, ' +
                //---- for student parent -----
                'Per.PID, PerT.TitleDESC, Per.FirstName, Per.LastName, Per.Phone AS PerPhone, PPR.RelationDescription, ' +
                'A.HouseNumber, A.MooNumber, A.VillageName, A.Alley, A.StreetName, A.PostCode AS PerPostCode, AddrP.ProvinceDescription AS AddrP, AddrT.TumbonDescription AS AddrT, AddrC.CityDescription AS AddrC ' +
                //------------------------
                //'SELECT Per.PID, PerT.TitleDESC, Per.FirstName, Per.LastName, PPR.RelationDescription ' +

                'FROM PersonOccupation PO ' +
                'LEFT JOIN Occupation POO ON POO.OccupationID = PO.OccupationID ' +
                'LEFT JOIN Province POP ON POP.ProvinceID = PO.Province ' +
                'LEFT JOIN City POC ON POC.CityID = PO.Amphor ' +
                'LEFT JOIN Tumbon POT ON POT.TumbonID = PO.Tumbon ' +

                //---- for student parent -----
                'LEFT JOIN PersonParent PP ON PP.PID = PO.PID ' +
                'LEFT JOIN Person Per ON Per.PID = PP.ParentPID ' +
                'LEFT JOIN Title PerT ON PerT.TitleID = Per.Title ' +
                'LEFT JOIN Relation PPR ON PPR.RelationID = PP.ParentRole ' +

                'LEFT JOIN PersonVSAddress PVA ON PVA.PID = Per.PID ' +
                'LEFT JOIN Address A ON A.AddressID = PVA.AddressID ' +
                'LEFT JOIN Province AddrP ON AddrP.ProvinceID = A.Province ' +
                'LEFT JOIN Tumbon AddrT ON AddrT.TumbonID = A.Tumbon ' +
                'LEFT JOIN City AddrC ON AddrC.CityID = A.Amphor ' +
                //----------------------------

                'LEFT JOIN PersonEducation Edu ON PO.PID = Edu.PID ' +
                'LEFT JOIN EducationLevel EduL ON EduL.EducationLevelID = Edu.EduLevel ' +
                'LEFT JOIN Province EduP ON EduP.ProvinceID = Edu.Province ' +
                'LEFT JOIN City EduC ON EduC.CityID = Edu.Amphor ' +
                'LEFT JOIN Tumbon EduT ON EduT.TumbonID = Edu.Tumbon ' +


                'LEFT JOIN PersonFinance Fina ON PO.PID = Fina.PID ' +

                'LEFT JOIN PersonBankAccount Bank ON PO.PID = Bank.PID ' +
                'LEFT JOIN Bank BankB ON BankB.BankID = Bank.BankName ' +

                'WHERE PO.PID = "' + famCid + '" ';
    //console.log(qrStr);

    tx.executeSql(qrStr, [], function (tx, results) {
        //console.log(results.rows.length);

        if (results.rows.length != 0) {
            //console.log(results.rows[0]);
            if (results.rows[0].OccupationID != 7) {

                $("#dataTableFam").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
                + "<tbody>"
                + "<tr><td><b>อาชีพ</b></td><td>" + results.rows[0].OccupationDescription + "</td></tr>"
                + "<tr><td><b>ตำแหน่ง</b></td><td>" + results.rows[0].Position + "</td></tr>"
                + "<tr><td><b>ชื่อสถานที่ทำงาน</b></td><td>" + results.rows[0].Office + "</td></tr>"
                + "<tr><td><b>ตำบล</b></td><td>" + results.rows[0].POT + "</td></tr>"
                + "<tr><td><b>อำเภอ</b></td><td>" + results.rows[0].POC + "</td></tr>"
                + "<tr><td><b>จังหวัด</b></td><td>" + results.rows[0].POP + "</td></tr>"
                + "<tr><td><b>รหัสไปรษณีย์</b></td><td>" + results.rows[0].Postcode + "</td></tr>"
                + "<tr><td><b>เบอร์โทรศัพท์</b></td><td>" + results.rows[0].Phone + "</td></tr>"
                + "<tr><td><b>โทรสาร</b></td><td>" + results.rows[0].Fax + "</td></tr>"

                + "<tr><td><b>---- การศึกษา ----</b></td></tr>"
                + "<tr><td><b>สถานภาพทางการศึกษา</b></td><td>" + getEduStatus(results.rows[0].EduStatus) + "</td></tr>"
                + "<tr><td><b>ระดับชั้น</b></td><td>" + results.rows[0].EduL + "</td></tr>"
                + "<tr><td><b>ปีการศึกษา</b></td><td>" + results.rows[0].EduYear + "</td></tr>"
                + "<tr><td><b>ชื่อสถานบันการศึกษา</b></td><td>" + results.rows[0].Academy + "</td></tr>"
                + "<tr><td><b>ตำบล</b></td><td>" + results.rows[0].EduT + "</td></tr>"
                + "<tr><td><b>อำเภอ</b></td><td>" + results.rows[0].EduC + "</td></tr>"
                + "<tr><td><b>จังหวัด</b></td><td>" + results.rows[0].EduP + "</td></tr>"

                + "<tr><td><b>---- การเงิน ----</b></td></tr>"
                + "<tr><td><b>รายได้</b></td><td>" + results.rows[0].Income + "</td></tr>"
                + "<tr><td><b>รายจ่าย</b></td><td>" + results.rows[0].Outgoing + "</td></tr>"
                + "<tr><td><b>หนี้ในระบบ</b></td><td>" + results.rows[0].InDebt + "</td></tr>"
                + "<tr><td><b>หนี้นอกระบบ</b></td><td>" + results.rows[0].ExDebt + "</td></tr>"

                + "<tr><td><b>---- บัญชีธนาคาร ----</b></td></tr>"
                + "<tr><td><b>ชื่อธนาคาร</b></td><td>" + results.rows[0].BankIDDesc + "</td></tr>"
                + "<tr><td><b>สาขา</b></td><td>" + results.rows[0].BankBranch + "</td></tr>"
                + "<tr><td><b>เลขบัญชี</b></td><td>" + results.rows[0].AccountNo + "</td></tr>"
                + "<tr><td><b>ชื่อบัญชี</b></td><td>" + results.rows[0].AccountName + "</td></tr>"

                + "</tbody>").closest("table#dataTableFam").table("refresh").trigger("create");
            }
            else {
                $("#dataTableFam").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
                + "<tbody>"

                + "<tr><td><b>อาชีพ</b></td><td>" + results.rows[0].OccupationDescription + "</td></tr>"
                + "<tr><td><b>สถานภาพทางการศึกษา</b></td><td>" + getEduStatus(results.rows[0].EduStatus) + "</td></tr>"
                + "<tr><td><b>ระดับชั้น</b></td><td>" + results.rows[0].EduL + "</td></tr>"
                + "<tr><td><b>ปีการศึกษา</b></td><td>" + results.rows[0].EduYear + "</td></tr>"
                + "<tr><td><b>ชื่อสถานบันการศึกษา</b></td><td>" + results.rows[0].Academy + "</td></tr>"
                + "<tr><td><b>ตำบล</b></td><td>" + results.rows[0].EduT + "</td></tr>"
                + "<tr><td><b>อำเภอ</b></td><td>" + results.rows[0].EduC + "</td></tr>"
                + "<tr><td><b>จังหวัด</b></td><td>" + results.rows[0].EduP + "</td></tr>"

                + "<tr><td><b>---- ผู้ปกครอง ----</b></td></tr>"
                + "<tr><td><b>เลขบัตรประชาชน</b></td><td>" + results.rows[0].PID + "</td></tr>"
                + "<tr><td><b>ชื่อ-นามสกุล</b></td><td>" + results.rows[0].TitleDESC + " " + results.rows[0].FirstName + " " + results.rows[0].LastName + "</td></tr>"
                + "<tr><td><b>เบอร์โทร</b></td><td>" + results.rows[0].PerPhone + "</td></tr>"
                + "<tr><td><b>เกี่ยวข้องเป็น</b></td><td>" + results.rows[0].RelationDescription + "</td></tr>"

                + "<tr><td><b>บ้านเลขที่</b></td><td>" + results.rows[0].HouseNumber + "</td></tr>"
                + "<tr><td><b>หมู่ที่</b></td><td>" + results.rows[0].MooNumber + "</td></tr>"
                + "<tr><td><b>หมู่บ้าน</b></td><td>" + results.rows[0].VillageName + "</td></tr>"
                + "<tr><td><b>ซอย</b></td><td>" + results.rows[0].Alley + "</td></tr>"
                + "<tr><td><b>ถนน</b></td><td>" + results.rows[0].StreetName + "</td></tr>"
                + "<tr><td><b>ตำบล</b></td><td>" + results.rows[0].AddrT + "</td></tr>"
                + "<tr><td><b>อำเภอ</b></td><td>" + results.rows[0].AddrC + "</td></tr>"
                + "<tr><td><b>จังหวัด</b></td><td>" + results.rows[0].AddrP + "</td></tr>"
                + "<tr><td><b>รหัสไปรษณีย์</b></td><td>" + results.rows[0].PerPostCode + "</td></tr>"

                + "</tbody>").closest("table#dataTableFam").table("refresh").trigger("create");
            }
            //setColorTable("dataTableFam");

        } else {
            $("#dataTableFam").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
                + "<tbody>"
                + "<tr><td><b>ไม่พบข้อมูล</b></td></tr>"
                + "</tbody>").closest("table#dataTableFam").table("refresh").trigger("create");
        }
    }, errorCB);

}

//----------- profileFam --------------

//------------- accidentProfile ---------------- pagebeforecreate
$(document).on("pagebeforeshow", "#accidentProfile", function () {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);

    var countN = 0;
    $('#menuUlAccid li').each(function () {
        //console.log($(this).attr("id") + "//" + sessionStorage.getItem("MenuID_2") + " N = " + countN);
        if (sessionStorage.getItem("MenuID_2") != null && $(this).attr("id") == sessionStorage.getItem("MenuID_2")) {
            $('#menuUlAccid #' + $(this).attr("id")).click();
            return false;
        }
        else if (countN == $('#menuUlAccid li').length - 1) {
            $('#menuUlAccid #AccidProfile').click();
        }
        else {
            countN++;
            //console.log("ไม่พบ Menu");
        }
    });

    db.transaction(function (tx) {
        tx.executeSql("SELECT AccidDesc FROM PersonAccident WHERE AccidID = '" + sessionStorage.getItem("AccidentID") + "'", [], function (tx, results) {
            if (results.rows.length != 0) {
                $("#headerAccidProfile").empty().append(results.rows[0].AccidDesc);
            } else { }
        }, errorCB);
    }, errorCB);


});

$(document).on("pageshow", "#accidentProfile", function () {

});

$(document).on("pagecreate", "#accidentProfile", function (event) {
    //db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);

    /*
    $(document).on("swiperight", "#accidentProfile", function (e) {
        if ($(".ui-page-active").jqmData("panel") !== "open") {
            if (e.type === "swiperight") { $("#left-panel_accid").panel("open"); }
        }
    });
    */

    var menuName;
    $('#menuUlAccid li').click(function () {
        var id = this.id; // get id of clicked li
        sessionStorage.setItem("MenuID_2", id);
        $('#dataTableAccid').empty(); $('#setAccid').hide(); $('#assistRequire').empty();
        $('#divMapAcci').css("display", "none");
        //console.log(id);

        $('#menuUlAccid li a').removeClass('ui-btn-active');
        $('#menuUlAccid #' + id + ' a').addClass('ui-btn-active');

        if (id == 'AccidProfile') {
            //menuName = id;
            db.transaction(showAccidProfile, errorCB);

        }
        else if (id == 'AccidDamage') {
            //menuName = id;
            $('#setAccid').show();
            db.transaction(shownAccidDamage, errorCB);

        }
        else if (id == 'AccidAssist') {
            db.transaction(showAssistRequire, errorCB);
        }
        else {
            console.log('ไม่พบ menuUl id');
        }
    });

    $("#editBtnAccid").click(function () {
        sessionStorage.setItem('EditType', 'AccidProfile');
        window.location.href = 'accident_data.html#accident';
    });

});

function showAccidProfile(tx) {
    var queryAccident = "SELECT PA.*, P.ProvinceDescription, T.TumbonDescription, C.CityDescription " +
        "FROM PersonAccident PA " +
        "LEFT JOIN Province P ON P.ProvinceID = PA.AccidProvince " +
        "LEFT JOIN Tumbon T ON T.TumbonID = PA.AccidTumbon " +
        "LEFT JOIN City C ON C.CityID = PA.AccidCity " +
        "WHERE AccidID = '" + sessionStorage.getItem("AccidentID") + "' ";

    $("#headerAccidProfile").empty();
    $('#dataHeaderAccid').empty(); $('#dataHeaderAccid').append('ข้อมูลเหตุการณ์');
    $('#dataTableAccid thead').empty(); $('#dataTableAccid tbody').empty();

    tx.executeSql(queryAccident, [], function (tx, results) {
        if (results.rows.length != 0) {
            //console.log(results.rows[0]);
            $("#dataTableAccid").append("<thead><tr><th data-priority='1' width='40%'><abbr title='ชื่อคอลัมน์'/></th><th width='60%'><abbr title='ช้อมูล'/></th></tr></thead>"
                + "<tbody>"
                + "<tr><td><b>วันที่ประสบเหตุ</b></td><td>" + results.rows[0].AccidDate + "</td></tr>"
                + "<tr><td><b>เหตุการณ์</b></td><td>" + results.rows[0].AccidDesc + "</td></tr>"
                + "<tr><td><b>ผู้ระบุเหตุการณ์</b></td><td>" + results.rows[0].AccidTeller + "</td></tr>"
                + "<tr><td><b>ประเภทเหตุการณ์</b></td><td>" + results.rows[0].AccidType + "</td></tr>"

                + "<tr><td><b>บ้านเลขที่</b></td><td>" + results.rows[0].AccidHouseNo + "</td></tr>"
                + "<tr><td><b>หมู่ที่</b></td><td>" + results.rows[0].AccidMoo + "</td></tr>"
                + "<tr><td><b>หมู่บ้าน</b></td><td>" + results.rows[0].AccidVillage + "</td></tr>"
                + "<tr><td><b>ซอย</b></td><td>" + results.rows[0].AccidAlley + "</td></tr>"
                + "<tr><td><b>ถนน</b></td><td>" + results.rows[0].AccidStreet + "</td></tr>"
                + "<tr><td><b>ตำบล</b></td><td>" + results.rows[0].TumbonDescription + "</td></tr>"
                + "<tr><td><b>อำเภอ</b></td><td>" + results.rows[0].CityDescription + "</td></tr>"
                + "<tr><td><b>จังหวัด</b></td><td>" + results.rows[0].ProvinceDescription + "</td></tr>"
                + "<tr><td><b>รหัสไปรษณีย์</b></td><td>" + results.rows[0].AccidPostCode + "</td></tr>"

                + "<tr><td><b>สถานีตำรวจที่ลงบันทึก</b></td><td>" + results.rows[0].PoliceStation + "</td></tr>"

                + "</tbody>").closest("table#dataTableAccid").table("refresh").trigger("create");

            $("#headerAccidProfile").append(results.rows[0].AccidDesc);
            $("#AccidIDtxt").empty(); $("#AccidIDtxt").val(results.rows[0].AccidID);

            if (navigator.onLine) { loadGoogleMaps("mapAcci", results.rows[0].AccidLat, results.rows[0].AccidLng); } else { }
        }
    }, errorCB);

}
function shownAccidDamage(tx) {
    //var queryAccidDmg = "SELECT * FROM AccidentDamageAndAssist "
    //    + "WHERE AccidID = '" + sessionStorage.getItem("AccidentID") + "' "
    //    + "AND (GroupType = '001' OR GroupType = '002' OR GroupType = '003')";

    var queryAccidDmg = "SELECT ADA.*, C.ChoiceDesc FROM AccidentDamageAndAssist ADA "
        + "LEFT JOIN Choice C ON ADA.Value = C.ChoiceID "
        + "WHERE AccidID = '" + sessionStorage.getItem("AccidentID") + "' "
        + "AND ( GroupType = '002' OR GroupType = '003') "
        + "AND (ADA.GroupType = C.GroupTypeID) "
        + "OR (AccidID = '" + sessionStorage.getItem("AccidentID") + "' AND ADA.GroupType = '001') ";

    $('#dataHeaderAccid').empty(); $('#dataHeaderAccid').append('ข้อมูลความเสียหาย');

    tx.executeSql(queryAccidDmg, [], function (tx, results) {
        if (results.rows.length != 0) {
            $("table#data-prop tbody").empty(); $('#data-body').empty(); $('#data-other').empty();

            for (var i = 0; i < results.rows.length; i++) {
                if (results.rows[i].GroupType == '001') {
                    $("table#data-prop tbody").append(""
                    + "<tr>"
                    + "<td>" + results.rows[i].Note + "</td>"
                    + "<td>" + results.rows[i].Value + "</td>"
                    + "</tr>"
                    + "").closest("table#data-prop").table("refresh").trigger("create");
                }
                else if (results.rows[i].GroupType == '002') {
                    $('#data-body').append(results.rows[i].ChoiceDesc);
                }
                else if (results.rows[i].GroupType == '003') {
                    $('#data-other').append(
                        '<li>' + results.rows[i].Note + '</li>'
                        ).listview('refresh');
                }
                else {
                    //console.log('No answer');
                }
            }
        }

    }, errorCB);

}
function showAssistRequire(tx) {
    //var queryAssistReq = "SELECT * FROM AccidentDamageAndAssist "
    //    + "WHERE AccidID = '" + sessionStorage.getItem("AccidentID") + "' "
    //    + "AND GroupType = '004' ";

    var queryAssistReq = "SELECT ADA.*, C.ChoiceDesc FROM AccidentDamageAndAssist ADA "
        + "LEFT JOIN Choice C ON ADA.Value = C.ChoiceID "
        + "WHERE AccidID = '" + sessionStorage.getItem("AccidentID") + "' "
        + "AND GroupType = '004' AND (ADA.GroupType = C.GroupTypeID) ";

    $('#dataHeaderAccid').empty(); $('#dataHeaderAccid').append('ความช่วยเหลือที่ต้องการ');

    tx.executeSql(queryAssistReq, [], function (tx, results) {
        if (results.rows.length != 0) {
            $('#dataTableAccid tbody').empty();
            $("#dataTableAccid").append("<thead><tr><th></th></tr></thead>");
            var counter = 0, txt = "";
            for (var i = 0; i < results.rows.length; i++) {
                counter = i + 1;

                if (results.rows[i].Value != '999') {
                    txt = results.rows[i].ChoiceDesc;
                }
                else {
                    txt = results.rows[i].Note;
                }

                $("#dataTableAccid").append(
                + "<tbody>"
                + "<tr><td style='text-align: left'>" + counter + ". " + txt + "</td></tr>"
                + "</tbody>");
            }
            $("#dataTableAccid").closest("table#dataTableAccid").table("refresh").trigger("create");
        }
        else {
            //console.log('No answer');
        }
    }, errorCB);
}

//------------- accidentProfile ----------------

function showAccidList(tx) {
    tx.executeSql("SELECT AccidID, AccidDate, AccidType FROM PersonAccident WHERE PID = '" + cid + "' ", [],
    function (tx, results) {
        if (results.rows.length != 0) {
            $('#maxPerson').empty(); $('#maxPerson').append(results.rows.length);
            for (var i = 0; i < results.rows.length; i++) {
                $("#dataTableAccident").append("<li class='ui-li-has-thumb' id='" + results.rows[i].AccidID + "'><a class='ui-btn' data-transition='slide'><img class='ui-thumbnail ui-thumbnail-circular' src='img/tribe-related-events-placeholder.png'><h2>"
                    + getQuestionJSDesc("AccidType", results.rows[i].AccidType) + "</h2><p>" + results.rows[i].AccidDate + "</p></a></li>");
            }
            $("#dataTableAccident").listview('refresh');
        }
        else {
            //console.log('have no familyID');
        }
    }, errorCB);
}

//----------- assistance -------------- pagebeforecreate
$(document).on("pagebeforeshow", "#assistance", function () {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);

    var countN = 0;
    $('#menuUlAccidAssist li').each(function () {
        //console.log($(this).attr("id") + "//" + sessionStorage.getItem("MenuID_2") + " N = " + countN);
        if (sessionStorage.getItem("MenuID_2") != null && $(this).attr("id") == sessionStorage.getItem("MenuID_2")) {
            $('#menuUlAccidAssist #' + $(this).attr("id")).click();
            return false;
        }
        else if (countN == $('#menuUlAccidAssist li').length - 1) {
            $('#menuUlAccidAssist #assistProp').click();
        }
        else {
            countN++;
            //console.log("ไม่พบ Menu");
        }
    });

    db.transaction(function (tx) {
        tx.executeSql("SELECT AccidDesc FROM PersonAccident WHERE AccidID = '" + sessionStorage.getItem("AccidentID") + "'", [], function (tx, results) {
            if (results.rows.length != 0) {
                $("#headerAccidAssist").empty().append(results.rows[0].AccidDesc);
            } else { }
        }, errorCB);
    }, errorCB);

});

$(document).on("pageshow", "#assistance", function () {

});

$(document).on("pagecreate", "#assistance", function (event) {
    $('#editBtnAssistance').click(function () {
        sessionStorage.setItem('EditType', 'Assistance');
        window.location.href = 'accident_data.html#assistance';
    });

    $('#menuUlAccidAssist li').click(function () {
        var id = this.id; // get id of clicked li
        sessionStorage.setItem("MenuID_2", id);

        $('#menuUlAccidAssist li a').removeClass('ui-btn-active');
        $('#menuUlAccidAssist #' + id + ' a').addClass('ui-btn-active');

        if (id == "assistProp") {
            db.transaction(shownAssistProp, errorCB);
        }
        else if (id == "assistBody") {
            db.transaction(shownAssistBody, errorCB);
        }
        else if (id == "assistOther") {
            db.transaction(shownAssistOther, errorCB);
        }
        else {
            console.log('ไม่พบ menuUl id');
        }

    });

});

function shownAssistProp(tx) {
    var queryAssistProp = "SELECT * FROM AccidentDamageAndAssist "
        + "WHERE AccidID = '" + sessionStorage.getItem("AccidentID") + "' "
        + "AND (GroupType LIKE '01%')";

    $('#headerAccidAssist2').empty(); $('#headerAccidAssist2').append('ความช่วยเหลือทางด้านทรัพย์สิน');
    $('#categories').empty();

    tx.executeSql(queryAssistProp, [], function (tx, results) {
        if (results.rows.length != 0) {
            for (var i = 0; i < results.rows.length; i++) {
                var selectID = "", selectText, valueTxt = results.rows[i].Value, noteTxt = results.rows[i].Note;
                if (results.rows[i].GroupType == '011') {
                    selectID = '001';
                    selectText = 'ศูนย์เยียวยาจังหวัด';
                }
                else if (results.rows[i].GroupType == '012') {
                    selectID = '002';
                    selectText = 'สำนักงานป้องกันและบรรเทาสาธารณภัย';
                }
                else if (results.rows[i].GroupType == '013') {
                    selectID = '003';
                    selectText = 'สำนักงานพัฒนาสังคมและความมั่นคงของมนุษย์';
                }
                else if (results.rows[i].GroupType == '014') {
                    selectID = '004';
                    selectText = 'กระทรวงศึกษาธิการ';
                }
                else if (results.rows[i].GroupType == '015') {
                    selectID = '005';
                    selectText = 'กระทรวงสาธารณสุข/กรมสุขภาพจิต';
                }
                else if (results.rows[i].GroupType == '016') {
                    selectID = '006';
                    selectText = 'กระทรวงยุติธรรม';
                }
                else if (results.rows[i].GroupType == '017') {
                    selectID = '007';
                    selectText = 'อื่นๆ';
                }
                else { }

                if ($('div#collapsible-' + selectID).length == 0) {
                    //console.log(selectID);
                    //create collapsible -> create table -> append table to collapsible

                    //----- create collapsible -------
                    $('#categories').append('<div id="collapsible-' + selectID + '" data-role="collapsible" class="custom-collapsible-z" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-collapsed="false"></div>');
                    $("#collapsible-" + selectID).append('<h3>' + selectText + '</h3>');
                    //--------------------------------

                    //------------ create table --------------
                    var service_table = $('<table data-role="table" id="table-' + selectID + '" class="ui-body-d ui-shadow table-stripe ui-responsive" data-mode="columntoggle" data-column-btn-text="คอลัมน์..." data-column-btn-theme="z" data-column-popup-theme="a"></table><br />');
                    var service_tr_th = $("<thead><tr><th>การช่วยเหลือ</th><th>จำนวนเงิน</th></tr></thead>");
                    var service_tbody = $('<tbody></tbody>');
                    var service_tr = $('<tr></tr>');
                    var service_name_td = $('<td>' + noteTxt + '</td><td>' + valueTxt + '</td>');
                    service_name_td.appendTo(service_tr);
                    service_tr_th.appendTo(service_table);
                    service_tr.appendTo(service_tbody);
                    service_tbody.appendTo(service_table);
                    //----------------------------------------

                    //------- append table to collapsible -------
                    service_table.appendTo($("#collapsible-" + selectID));
                    service_table.table();

                    $('#categories').trigger('create');
                    //-------------------------------------------
                }
                else if ($('div#collapsible-' + selectID).length != 0) {
                    //console.log("already have");

                    var service_name_td = $('<tr><td>' + noteTxt + '</td><td>' + valueTxt + '</td></tr>');
                    service_name_td.appendTo($('table#table-' + selectID + ' tbody'));
                    $('table#table-' + selectID).table();
                }
                else {
                    //alert("กรุณาเลือกหน่วยงาน");
                    return false;
                }
            }
        }

    }, errorCB);

}
function shownAssistBody(tx) {

    $('#headerAccidAssist2').empty(); $('#headerAccidAssist2').append('ความช่วยเหลือทางด้านร่างกาย');
    $('#categories').empty();

    var queryAssistProp = "SELECT ADA.*, C.ChoiceDesc FROM AccidentDamageAndAssist ADA "
        + "LEFT JOIN Choice C ON (ADA.Value = C.ChoiceID) "
        + "WHERE ADA.AccidID = '" + sessionStorage.getItem("AccidentID") + "' AND (ADA.GroupType LIKE '02%') AND (C.GroupTypeID LIKE '02%')"
        + "AND (ADA.GroupType = C.GroupTypeID) OR ((ADA.GroupType = '025' OR ADA.GroupType = '028') AND ADA.AccidID = '" + sessionStorage.getItem("AccidentID") + "' ) ";

    tx.executeSql(queryAssistProp, [], function (tx, results) {
        if (results.rows.length != 0) {

            var temp = '<div data-role="collapsible-set">'
            + '<div data-role="collapsible" class="custom-collapsible-z" data-iconpos="right" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-mini="true" data-collapsed="false">'
            + '     <h3>ศูนย์เยียวยาจังหวัด</h3>'
            + '     <ul id="coll-1" data-role="listview"></ul>'
            + '</div>'
            + '<div data-role="collapsible" class="custom-collapsible-z" data-iconpos="right" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-mini="true">'
            + '     <h3>สำนักงานป้องกันและบรรเทาสาธารณภัย</h3>'
            + '     <ul id="coll-2" data-role="listview"></ul>'
            + '</div>'
            + '<div data-role="collapsible" class="custom-collapsible-z" data-iconpos="right" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-mini="true">'
            + '     <h3>กระทรวงยุติธรรม</h3>'
            + '     <ul id="coll-3" data-role="listview"></ul>'
            + '</div>'
            + '<div data-role="collapsible" class="custom-collapsible-z" data-iconpos="right" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-mini="true">'
            + '     <h3>สานักงานพัฒนาสังคมและความมั่นคงของมนุษย์จังหวัด</h3>'
            + '     <ul id="coll-4" data-role="listview"</ul>'
            + '</div>'
            + '<div data-role="collapsible" class="custom-collapsible-z" data-iconpos="right" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-mini="true">'
            + '     <h3>อื่นๆ</h3>'
            + '     <ul id="coll-5" data-role="listview"></ul>'
            + '</div>'

            + '<h3>กรณีผู้เสียหายเป็นนักเรียน/นักศึกษา</h3>'

            + '<div data-role="collapsible" class="custom-collapsible-z" data-iconpos="right" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-mini="true">'
            + '     <h3>ศูนย์เยียวยาจังหวัด</h3>'
            + '     <ul id="coll-6" data-role="listview"></ul>'
            + '</div>'
            + '<div data-role="collapsible" class="custom-collapsible-z" data-iconpos="right" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-mini="true">'
            + '     <h3>สำนักงานป้องกันและบรรเทาสาธารณภัย</h3>'
            + '     <ul id="coll-7" data-role="listview"></ul>'
            + '</div>'
            + '<div data-role="collapsible" class="custom-collapsible-z" data-iconpos="right" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-mini="true">'
            + '     <h3>อื่นๆ</h3>'
            + '     <ul id="coll-8" data-role="listview"></ul>'
            + '</div>'

            + '</div>';

            $('#categories').append(temp).trigger('create');

            //console.log(results.rows);
            for (var i = 0; i < results.rows.length ; i++) {

                var cbName = "", isOther = 0;
                if (results.rows[i].GroupType == '021') {
                    cbName = '1'
                }
                else if (results.rows[i].GroupType == '022') {
                    cbName = '2'
                }
                else if (results.rows[i].GroupType == '023') {
                    cbName = '3'
                }
                else if (results.rows[i].GroupType == '024') {
                    cbName = '4'
                }
                else if (results.rows[i].GroupType == '025') {
                    cbName = '5';
                    isOther = 1;
                }
                else if (results.rows[i].GroupType == '026') {
                    cbName = '6'
                }
                else if (results.rows[i].GroupType == '027') {
                    cbName = '7'
                }
                else if (results.rows[i].GroupType == '028') {
                    cbName = '8'
                    isOther = 1;

                }
                else { }

                if (isOther == 0) {
                    var $el = $('<li>' + results.rows[i].ChoiceDesc + '</li>');
                    $('#coll-' + cbName).append($el).listview("refresh");
                }
                else {
                    var $el = $('<li>' + results.rows[i].Note + " จำนวน: " + results.rows[i].Value + ' บาท</li>');
                    $('#coll-' + cbName).append($el).listview("refresh");
                }

            }
        }
    }, errorCB);

}
function shownAssistOther(tx) {
    $('#headerAccidAssist2').empty(); $('#headerAccidAssist2').append('ความช่วยเหลืออื่นๆ');
    $('#categories').empty();

    var temp = '<div><ul id="li-other" data-role="listview" data-inset="true"></ul></div>';

    $('#categories').append(temp).trigger('create');

    //var queryOtherAssist = "SELECT * FROM AccidentDamageAndAssist WHERE AccidID = '" + sessionStorage.getItem("AccidentID") + "' AND (GroupType LIKE '03%')";

    var queryOtherAssist = "SELECT ADA.*, C.ChoiceDesc FROM AccidentDamageAndAssist ADA "
        + "LEFT JOIN Choice C ON (ADA.Value = C.ChoiceID) "
        + "WHERE ADA.AccidID = '" + sessionStorage.getItem("AccidentID") + "' AND (ADA.GroupType LIKE '03%') AND (C.GroupTypeID LIKE '03%')"
        + "AND (ADA.GroupType = C.GroupTypeID) ";

    tx.executeSql(queryOtherAssist, [], function (tx, results) {
        if (results.rows.length != 0) {
            var counter = 0;
            for (var i = 0; i < results.rows.length ; i++) {
                counter = i + 1;
                if (results.rows[i].Value != "999") {
                    temp = '<li>' + counter + '. ' + results.rows[i].ChoiceDesc + '</li>';
                    $("#li-other").append(temp).listview("refresh");
                }
                else {
                    temp = '<li>' + counter + '. ' + results.rows[i].Note + '</li>';
                    $("#li-other").append(temp).listview("refresh");
                }
            }
        }
        else {
            //console.log('No answer');
        }
    }, errorCB);

}

//----------- assistance --------------

//----------- upload --------------
$(document).on("pagebeforecreate", "#upload", function () {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);

    var service_name_td = "";

    db.transaction(function (tx) {
        $("#uploadPersonTable").empty();
        tx.executeSql("SELECT P.PID, P.FirstName, P.LastName, P.DateTimeUpdate FROM Person P INNER JOIN PersonRecordBy PRB ON P.PID=PRB.PID WHERE P.RecordStaffPID = '" + sessionStorage.getItem('CID') + "' ORDER BY P.DateTimeUpdate DESC", [],
            (function (tx, results) {
                //console.log(results.rows.length);
                if (results.rows.length != 0) {
                    $("#uploadPersonTable").append(""
                        + "<thead>"
                        + "<tr class='ui-bar-d'>"
                        + "<th style='width:25px;'></th>"
                        + "<th data-priority='2'>CID</th>"
                        + "<th >Name</th>"
                        + "<th data-priority='1'>Last Upload</th>"
                        + "</tr>"
                        + "</thead>"
                        + "<tbody>"
                        + "</tbody>").closest("table#uploadPersonTable").table("refresh").trigger("create");
                    for (var i = 0; i < results.rows.length; i++) {

                        service_name_td = $('<tr>'
                                + '<td><input id="checkbox' + i + '" type="checkbox"></td>'
                                + '<td>' + results.rows[i].PID + '</td>'
                                + '<td>' + results.rows[i].FirstName + ' ' + results.rows[i].LastName + '</td>'
                                + '<td>' + showDateTimeFormat(results.rows[i].DateTimeUpdate) + '</td>'
                                + '</tr>');
                        //.bind("click", function () {
                        //        var row = $(this).index();
                        //        $('#checkbox' + row).click();
                        //    });
                        service_name_td.appendTo($('table#uploadPersonTable tbody'));
                        $("table#uploadPersonTable").table("refresh").trigger("create");
                    }


                } else {
                    $("#uploadPersonTable").append(""
                        + "<thead>"
                        + "<tr class='ui-bar-d'>"
                        + "<th style='width:25px;'></th>"
                        + "<th data-priority='2'>CID</th>"
                        + "<th >Name</th>"
                        + "<th data-priority='1'>Last Upload</th>"
                        + "</tr>"
                        + "</thead>"

                        + "<tbody>"
                        + "<tbody>"
                        + "<tr>"
                        + "<td></td>"
                        + "<td></td>"
                        + "<td>ไม่พบข้อมูล</td>"
                        + "<td></td>"
                        + "</tr>"
                        + "</tbody>").closest("table#uploadPersonTable").table("refresh").trigger("create");
                }
            }), errorCB);
    }, errorCB, function () {
    });

});

$(document).on("pagecreate", "#upload", function (event) {
    //db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);

    $('#searchUpdateData').on('keyup', function (e) {
        var theEvent = e || window.event;
        var keyPressed = theEvent.keyCode || theEvent.which;

        var service_name_td = "";

        fetchUploadTable();

    });

    $('#uploadBtn').click(function () {
        //startAjaxLoader();

        if (navigator.onLine) {
            var r = confirm('ต้องการอัพโหลดข้อมูล ใช่หรือไม่');
            if (r == true) {
                var uploadCID = [];
                $('input:checked').each(function () {
                    var checked = $(this).closest("tr").find("td:not(:has(input))");
                    $.each(checked, function () {
                        //console.log($(this).text());
                        uploadCID.push($(this).text());
                        return false;
                    });
                });
                //console.log(uploadCID);

                for (var i = 0; i < uploadCID.length; i++) {
                    //console.log(uploadCID[i]);
                    getInvolvePerson(uploadCID[i]);
                }

                /*
                upload table list
                - Person
                - PersonBankAccount
                - PersonEducation
                - PersonFinance
                - PersonOccupation
                - Address
                - PersonVSAddress
        
                - PersonEmergencyContact (InvolvePerson)
                - PersonFamily (InvolvePerson)
                - PersonMate (InvolvePerson)
                - PersonStandin (InvolvePerson)
                - PersonParent (InvolvePerson)
                - PersonRecordBy (InvolvePerson)
                */
            }
            else {
                console.log('cancel');
            }
        }
        else {
            alert("กรุณาเชื่อมต่อ Internet");
        }
    });

    $('#uploadAllBtn').click(function () {

        if (navigator.onLine) {
            $('#uploadPersonTable input[type="checkbox"]').prop('checked', true);
            var r = confirm('ต้องการอัพโหลดข้อมูลทั้งหมด ใช่หรือไม่');
            if (r == true) {
                uploadAllPerson();
            }
            else {
                $('#uploadPersonTable input[type="checkbox"]').prop('checked', false);
                console.log('cancel');
            }
        }
        else {
            alert("กรุณาเชื่อมต่อ Internet");
        }

    });
});

function uploadAllPerson() {
    var Person = [], Bank = [], Edu = [], Fina = [], Occu = [], PVA = [], Addr = [], Accident = [], DmgAndAssist = [];
    var PersonEmergencyContact = [], PersonFamily = [], PersonMate = [], PersonStandin = [], PersonParent = [], PersonRecordBy = [];
    var data = {}, Base64Img = [];

    db.transaction(function (tx) {
        //console.log(uploadCID);
        tx.executeSql("SELECT * FROM Person", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    Person.push((rs.rows[i]));
                    //var ele = {
                    //    "Base64Img": getBase64Img(rs.rows[0].PicPath),
                    //    "PID": rs.rows[i].PID
                    //};
                    //Base64Img.push(ele);
                }
            } else {
                var element = {
                    "PID": ""
                };
                Person.push(element);

                //var element2 = {
                //    "PID": ""
                //};
                //Base64Img.push(element2);
            }

        }, errorCB);

        tx.executeSql("SELECT * FROM PersonBankAccount", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    Bank.push((rs.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                Bank.push(element);
            }
        }, errorCB);

        tx.executeSql("SELECT * FROM PersonEducation", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    Edu.push((rs.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                Edu.push(element);
            }
        }, errorCB);

        tx.executeSql("SELECT * FROM PersonFinance", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    Fina.push((rs.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                Fina.push(element);
            }
        }, errorCB);

        tx.executeSql("SELECT * FROM PersonOccupation", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    Occu.push((rs.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                Occu.push(element);
            }
        }, errorCB);

        tx.executeSql("SELECT * FROM PersonVSAddress", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    PVA.push((rs.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                PVA.push(element);
            }
            tx.executeSql("SELECT * FROM Address", [], function (tx, rs2) {
                if (rs2.rows.length != 0) {
                    //console.log(rs);
                    for (var i = 0; i < rs2.rows.length; i++) {
                        Addr.push((rs2.rows[i]));
                    }
                } else {
                    var element = {
                        "PID": ""
                    };
                    Addr.push(element);
                }
            }, errorCB);
        }, errorCB);

        tx.executeSql("SELECT * FROM PersonAccident", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                for (var i = 0; i < rs.rows.length ; i++) {
                    Accident.push(rs.rows[i]);
                }
            } else {
                var element = {
                    "PID": ""
                };
                Accident.push(element);
            }
        }, errorCB);

        tx.executeSql("SELECT * FROM AccidentDamageAndAssist", [], function (tx, rs2) {
            //console.log(rs2.rows.length);
            if (rs2.rows.length != 0) {
                for (var j = 0; j < rs2.rows.length; j++) {
                    DmgAndAssist.push(rs2.rows[j]);
                }
            } else {
                var element2 = {
                    "AccidID": ""
                };
                DmgAndAssist.push(element2);
            }
        }, errorCB);

        tx.executeSql("SELECT * FROM PersonEmergencyContact", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    PersonEmergencyContact.push((rs.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                PersonEmergencyContact.push(element);
            }
        }, errorCB);

        tx.executeSql("SELECT * FROM PersonFamily", [], function (tx, rs2) {
            if (rs2.rows.length != 0) {
                for (var i = 0; i < rs2.rows.length; i++) {
                    PersonFamily.push((rs2.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                PersonFamily.push(element);
            }
        }, errorCB);

        tx.executeSql("SELECT * FROM PersonMate", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    PersonMate.push((rs.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                PersonMate.push(element);
            }
        }, errorCB);

        tx.executeSql("SELECT * FROM PersonStandin", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    PersonStandin.push((rs.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                PersonStandin.push(element);
            }
        }, errorCB);

        tx.executeSql("SELECT * FROM PersonParent", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    PersonParent.push((rs.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                PersonParent.push(element);
            }
        }, errorCB);

        tx.executeSql("SELECT * FROM PersonRecordBy", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    PersonRecordBy.push((rs.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                PersonRecordBy.push(element);
            }
        }, errorCB);

    }, errorCB, function () {
        //console.log(Person, Bank, Edu, Fina, Occu, PVA, Addr);

        data = {
            bank: Bank, edu: Edu, person: Person, fina: Fina, occu: Occu,
            pva: PVA, addr: Addr, fam: PersonFamily, pec: PersonEmergencyContact, mate: PersonMate,
            stan: PersonStandin, parent: PersonParent, rec: PersonRecordBy, accident: Accident, ada: DmgAndAssist
            //base64Img: Base64Img
        };

        console.log(data);
        //console.log(PersonFamily);
        //console.log(Accident);
        //console.log(DmgAndAssist);

        startAjaxLoader();
        $.ajax({
            type: "POST",
            //url: "http://localhost:1722/Service1.svc/UploadPerson",
            url: "http://seniorservice.azurewebsites.net/Service1.svc/UploadPerson",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dateType: 'json',
            success: function (msg) {
                //saveData_callback(msg);
                console.log(msg);

                db.transaction(function (tx) {
                    tx.executeSql("UPDATE Person SET DateTimeUpdate = '" + msg + "'", null, function () {
                        stopAjaxLoader();
                        fetchUploadTable();
                    }, errorCB);
                }, errorCB);

            },
            error: function (request, status, error) {
                stopAjaxLoader();
                //console.log(error);
                alert("Upload fail กรุณาลองใหม่อีกครั้ง\nError: " + error);
                //fetchUploadTable();
            }
        });

    });

}

function fetchUploadTable() {
    db.transaction(function (tx) {
        $("#uploadPersonTable").empty();
        tx.executeSql("SELECT P.PID, P.FirstName, P.LastName, P.DateTimeUpdate FROM Person P INNER JOIN PersonRecordBy PRB ON P.PID=PRB.PID WHERE P.RecordStaffPID = '" + sessionStorage.getItem('CID') + "' AND (P.PID LIKE '%" + $('#searchUpdateData').val() + "%' OR P.FirstName LIKE '%" + $('#searchUpdateData').val() + "%' OR P.LastName LIKE '%" + $('#searchUpdateData').val() + "%') ORDER BY P.DateTimeUpdate DESC", [],
            function (tx, results) {
                //console.log(results.rows.length);
                if (results.rows.length != 0) {
                    $("#uploadPersonTable").append(""
                        + "<thead>"
                        + "<tr class='ui-bar-d'>"
                        + "<th style='width:25px;'></th>"
                        + "<th data-priority='2'>CID</th>"
                        + "<th >Name</th>"
                        + "<th data-priority='1'>Last Upload</th>"
                        + "</tr>"
                        + "</thead>"
                        + "<tbody>"
                        + "</tbody>").closest("table#uploadPersonTable").table("refresh").trigger("create");
                    for (var i = 0; i < results.rows.length; i++) {

                        service_name_td = $('<tr>'
                            + '<td><input type="checkbox"></td>'
                            + '<td>' + results.rows[i].PID + '</td>'
                            + '<td>' + results.rows[i].FirstName + ' ' + results.rows[i].LastName + '</td>'
                            + '<td>' + showDateTimeFormat(results.rows[i].DateTimeUpdate) + '</td>'
                            + '</tr>');
                        service_name_td.appendTo($('table#uploadPersonTable tbody'));
                        $("table#uploadPersonTable").table("refresh").trigger("create");
                    }
                } else {
                    $("#uploadPersonTable").append(""
                        + "<thead>"
                        + "<tr class='ui-bar-d'>"
                        + "<th style='width:25px;'></th>"
                        + "<th data-priority='2'>CID</th>"
                        + "<th >Name</th>"
                        + "<th data-priority='1'>Last Upload</th>"
                        + "</tr>"
                        + "</thead>"

                        + "<tbody>"
                        + "<tbody>"
                        + "<tr>"
                        + "<td></td>"
                        + "<td></td>"
                        + "<td>ไม่พบข้อมูล</td>"
                        + "<td></td>"
                        + "</tr>"
                        + "</tbody>").closest("table#uploadPersonTable").table("refresh").trigger("create");
                }
            }, errorCB);
    }, errorCB, function () {
        $("table#uploadPersonTable").table("refresh").trigger("create");
    });
}

function showDateTimeFormat(dt) {
    var result = "-";
    if (dt != "-") {
        var a = dt.split(":");
        result = a[0] + ":" + a[1];
    }
    return result;
}

function setUploadHourToLocal() {
    var d = new Date();
    var x = d.getHours();
    if (x >= 0 && x < 10) { x = "0" + x; }
    else { x = "" + x; }
    return x;
}

function uploadPerson(uploadCID, type) {
    //--- type ---
    // 1 = Person
    // 2 = Involve Person
    //------------
    var Person = [], Bank = [], Edu = [], Fina = [], Occu = [], PVA = [], Addr = [], Accident = [], DmgAndAssist = [];
    var PersonEmergencyContact = [], PersonFamily = [], PersonMate = [], PersonStandin = [], PersonParent = [], PersonRecordBy = [];
    var Object = [], Base64Img = [];
    var data = {};

    db.transaction(function (tx) {
        //console.log(uploadCID);
        tx.executeSql("SELECT * FROM Person WHERE PID = '" + uploadCID + "' ", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    Person.push((rs.rows[i]));
                    //var ele = {
                    //    "Base64Img": getBase64Img(rs.rows[0].PicPath),
                    //    "PID": rs.rows[i].PID
                    //};
                    //Base64Img.push(ele);
                }
            } else {
                var element = {
                    "PID": ""
                };
                Person.push(element);

                //var element2 = {
                //    "PID": ""
                //};
                //Base64Img.push(element2);
            }

        }, errorCB);

        tx.executeSql("SELECT * FROM PersonBankAccount WHERE PID = '" + uploadCID + "' ", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    Bank.push((rs.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                Bank.push(element);
            }
        }, errorCB);

        tx.executeSql("SELECT * FROM PersonEducation WHERE PID = '" + uploadCID + "' ", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    Edu.push((rs.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                Edu.push(element);
            }
        }, errorCB);

        tx.executeSql("SELECT * FROM PersonFinance WHERE PID = '" + uploadCID + "' ", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    Fina.push((rs.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                Fina.push(element);
            }
        }, errorCB);

        tx.executeSql("SELECT * FROM PersonOccupation WHERE PID = '" + uploadCID + "' ", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    Occu.push((rs.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                Occu.push(element);
            }
        }, errorCB);

        tx.executeSql("SELECT * FROM PersonVSAddress WHERE PID = '" + uploadCID + "' ", [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    PVA.push((rs.rows[i]));
                }
            } else {
                var element = {
                    "PID": ""
                };
                PVA.push(element);
            }
            tx.executeSql("SELECT * FROM Address WHERE AddressID = '" + rs.rows[0].AddressID + "' ", [], function (tx, rs2) {
                if (rs2.rows.length != 0) {
                    //console.log(rs);
                    for (var i = 0; i < rs2.rows.length; i++) {
                        Addr.push((rs2.rows[i]));
                    }
                } else {
                    var element = {
                        "PID": ""
                    };
                    Addr.push(element);
                }
            }, errorCB);
        }, errorCB);

        if (type == "1") {
            tx.executeSql("SELECT * FROM PersonAccident WHERE PID = '" + uploadCID + "' ", [], function (tx, rs) {
                if (rs.rows.length != 0) {
                    for (var i = 0; i < rs.rows.length ; i++) {
                        Accident.push(rs.rows[i]);
                        tx.executeSql("SELECT * FROM AccidentDamageAndAssist WHERE AccidID = '" + rs.rows[i].AccidID + "' ", [], function (tx, rs2) {
                            //console.log(rs2.rows.length);
                            if (rs2.rows.length != 0) {
                                for (var j = 0; j < rs2.rows.length; j++) {
                                    DmgAndAssist.push(rs2.rows[j]);
                                }
                            }
                        }, errorCB);
                    }
                } else {
                    var element = {
                        "PID": ""
                    };
                    Accident.push(element);

                    var element2 = {
                        "AccidID": ""
                    };
                    DmgAndAssist.push(element2);
                }
            }, errorCB);
        }
        else {
            var element = {
                "PID": ""
            };
            Accident.push(element);

            var element2 = {
                "AccidID": ""
            };
            DmgAndAssist.push(element2);
        }


        if (type == "1" || type == "2") {

            tx.executeSql("SELECT * FROM PersonEmergencyContact WHERE PID = '" + uploadCID + "' ", [], function (tx, rs) {
                if (rs.rows.length != 0) {
                    //console.log(rs);
                    for (var i = 0; i < rs.rows.length; i++) {
                        PersonEmergencyContact.push((rs.rows[i]));
                    }
                } else {
                    var element = {
                        "PID": ""
                    };
                    PersonEmergencyContact.push(element);
                }
            }, errorCB);

            tx.executeSql("SELECT FamilyID FROM PersonFamily WHERE PID = '" + uploadCID + "' ", [], function (tx, rs) {
                if (rs.rows.length != 0) {
                    //console.log(rs);
                    if (type == "1") {
                        tx.executeSql("SELECT * FROM PersonFamily WHERE FamilyID = '" + rs.rows[0].FamilyID + "' ", [], function (tx, rs2) {
                            if (rs2.rows.length != 0) {
                                for (var i = 0; i < rs2.rows.length; i++) {
                                    PersonFamily.push((rs2.rows[i]));
                                }
                            }
                        }, errorCB);
                    }
                    else if (type == "2") {
                        var element = {
                            "PID": ""
                        };
                        PersonFamily.push(element);
                    } else { /*foo*/ }
                } else {
                    var element = {
                        "PID": ""
                    };
                    PersonFamily.push(element);
                }
            }, errorCB);

            tx.executeSql("SELECT * FROM PersonMate WHERE PID = '" + uploadCID + "' ", [], function (tx, rs) {
                if (rs.rows.length != 0) {
                    //console.log(rs);
                    for (var i = 0; i < rs.rows.length; i++) {
                        PersonMate.push((rs.rows[i]));
                    }
                } else {
                    var element = {
                        "PID": ""
                    };
                    PersonMate.push(element);
                }
            }, errorCB);

            tx.executeSql("SELECT * FROM PersonStandin WHERE PID = '" + uploadCID + "' ", [], function (tx, rs) {
                if (rs.rows.length != 0) {
                    //console.log(rs);
                    for (var i = 0; i < rs.rows.length; i++) {
                        PersonStandin.push((rs.rows[i]));
                    }
                } else {
                    var element = {
                        "PID": ""
                    };
                    PersonStandin.push(element);
                }
            }, errorCB);

            tx.executeSql("SELECT * FROM PersonParent WHERE PID = '" + uploadCID + "' ", [], function (tx, rs) {
                if (rs.rows.length != 0) {
                    //console.log(rs);
                    for (var i = 0; i < rs.rows.length; i++) {
                        PersonParent.push((rs.rows[i]));
                    }
                } else {
                    var element = {
                        "PID": ""
                    };
                    PersonParent.push(element);
                }
            }, errorCB);

            tx.executeSql("SELECT * FROM PersonRecordBy WHERE PID = '" + uploadCID + "' ", [], function (tx, rs) {
                if (rs.rows.length != 0) {
                    //console.log(rs);
                    for (var i = 0; i < rs.rows.length; i++) {
                        PersonRecordBy.push((rs.rows[i]));
                    }
                } else {
                    var element = {
                        "PID": ""
                    };
                    PersonRecordBy.push(element);
                }
            }, errorCB);
        }

    }, errorCB, function () {
        //console.log(Person, Bank, Edu, Fina, Occu, PVA, Addr);
        if (type == "1" || type == "2") {
            data = {
                bank: Bank, edu: Edu, person: Person, fina: Fina, occu: Occu,
                pva: PVA, addr: Addr, fam: PersonFamily, pec: PersonEmergencyContact, mate: PersonMate,
                stan: PersonStandin, parent: PersonParent, rec: PersonRecordBy, accident: Accident, ada: DmgAndAssist
                //base64Img: Base64Img
            };
        }
        else {
            data = { bank: Bank, edu: Edu, person: Person, fina: Fina, occu: Occu, pva: PVA, addr: Addr };
        }

        console.log(data);
        //console.log(PersonFamily);
        //console.log(Accident);
        //console.log(DmgAndAssist);

        $.ajax({
            type: "POST",
            //url: "http://localhost:1722/Service1.svc/UploadPerson",
            url: "http://seniorservice.azurewebsites.net/Service1.svc/UploadPerson",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dateType: 'json',
            success: function (msg) {
                //saveData_callback(msg);
                console.log(msg);

                db.transaction(function (tx) {
                    tx.executeSql("UPDATE Person SET DateTimeUpdate = '" + msg + "' WHERE PID = '" + uploadCID + "'", null, function () {
                        if (type == "1") {
                            stopAjaxLoader();
                            fetchUploadTable();
                            //window.location.reload();
                        }
                        else { }
                    }, errorCB);
                }, errorCB);

            },
            error: function (request, status, error) {
                stopAjaxLoader();
                //console.log(error);
                alert("Upload fail กรุณาลองใหม่อีกครั้ง\nError: " + error);
            }
        });

    });

}

function getInvolvePerson(uploadCID) {
    var involvePerson = [];
    var uniqueInvolvePerson = [];

    var selectInvolvePerson = "SELECT PEC.EmergencyContactPID AS PEC, PS.StandIn AS PS, PP.ParentPID AS PP, PRB.RecordPID AS PRB "
        + "FROM Person P "
        + "LEFT JOIN PersonEmergencyContact PEC ON PEC.PID = P.PID "
        //+ "LEFT JOIN PersonFamily PF ON PF.PID = P.PID "
        //+ "LEFT JOIN PersonMate PM ON PM.PID = P.PID "
        + "LEFT JOIN PersonStandin PS ON PS.PID = P.PID "
        + "LEFT JOIN PersonParent PP ON PP.PID = P.PID "
        + "LEFT JOIN PersonRecordBy PRB ON PRB.PID = P.PID "
        + "WHERE P.PID = '" + uploadCID + "'";

    var selectFamilyID = "SELECT FamilyID FROM PersonFamily WHERE PID = '" + uploadCID + "' ";

    var selectMateID = "SELECT MatePID FROM PersonMate WHERE PID = '" + uploadCID + "' ";

    db.transaction(function (tx) {
        tx.executeSql(selectInvolvePerson, [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                if (rs.rows[0].PEC != uploadCID) { involvePerson.push(rs.rows[0].PEC); }
                if (rs.rows[0].PS != uploadCID) { involvePerson.push(rs.rows[0].PS); }
                if (rs.rows[0].PP != uploadCID) { involvePerson.push(rs.rows[0].PP); }
                if (rs.rows[0].PRB != uploadCID) { involvePerson.push(rs.rows[0].PRB); }
            }

            tx.executeSql(selectFamilyID, [], function (tx, rs2) {
                if (rs2.rows.length != 0) {
                    tx.executeSql("SELECT PID FROM PersonFamily WHERE FamilyID = '" + rs2.rows[0].FamilyID + "' ", [], function (tx, rs3) {
                        //console.log(rs3);
                        if (rs3.rows.length != 0) {
                            for (var i = 0; i < rs3.rows.length; i++) {
                                if (rs3.rows[i].PID != uploadCID) {
                                    involvePerson.push(rs3.rows[i].PID);
                                    tx.executeSql("SELECT ParentPID FROM PersonParent WHERE PID = '" + rs3.rows[i].PID + "' ", [], function (tx, res) {
                                        if (res.rows.length != 0 && res.rows[0].ParentPID != uploadCID) {
                                            involvePerson.push(res.rows[0].ParentPID);
                                        }
                                    }, errorCB);
                                }
                            }
                        }
                    }, errorCB);
                }
            }, errorCB);

            tx.executeSql(selectMateID, [], function (tx, rs4) {
                //console.log(rs4);
                if (rs4.rows.length != 0) {
                    for (var i = 0; i < rs4.rows.length; i++) {
                        if (rs4.rows[i].PID != uploadCID) {
                            involvePerson.push(rs4.rows[i].MatePID);
                        }
                    }
                }
            }, errorCB);

        }, errorCB);
    }, errorCB, function () {
        //console.log(involvePerson);
        startAjaxLoader();
        $.each(involvePerson, function (i, el) {
            if (($.inArray(el, uniqueInvolvePerson) === -1) && (involvePerson[i] != "null" && involvePerson[i] != null && involvePerson[i] != "")) {
                uniqueInvolvePerson.push(el);
            }
        });
        uniqueInvolvePerson.push(uploadCID);
        //console.log(uniqueInvolvePerson);

        for (var i = 0; i < uniqueInvolvePerson.length; i++) {
            if (uniqueInvolvePerson[i] == uploadCID && i == uniqueInvolvePerson.length - 1) {
                uploadPerson(uniqueInvolvePerson[i], "1");
                console.log(uniqueInvolvePerson[i]);
            }
            else {
                uploadPerson(uniqueInvolvePerson[i], "2");
                console.log(uniqueInvolvePerson[i]);
            }

        }

    });
}

function getBase64Img(ImgPath) {
    var myImage = document.getElementById("avatar");
    myImage.src = ImgPath;

    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    var img = document.getElementById("avatar");
    ctx.drawImage(img, 0, 0, 100, 100);

    var dataURL = c.toDataURL("image/png");

    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

function getImage() {
    var myImage = document.getElementById("avatar");
    myImage.src = "img/avatar-placeholder.png";
}

function myCanvas() {
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    var img = document.getElementById("avatar");
    ctx.drawImage(img, 0, 0);
    getBase64Image(c);
}

function getBase64Image(img) {
    // Create an empty canvas element
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;

    // Copy the image contents to the canvas
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);

    // Get the data-URL formatted image
    // Firefox supports PNG and JPEG. You could check img.src to
    // guess the original format, but be aware the using "image/jpg"
    // will re-encode the image.
    var dataURL = canvas.toDataURL("image/png");

    console.log(dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));

    //return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
}

//----------- upload --------------

function imgError(image) {
    console.log("err ");
    image.onerror = "";
    image.src = "img/avatar-placeholder.png";
    return true;
}

function getSatisfyStatus(value) {
    var returnDesc = "";
    if (value == '1') {
        returnDesc = "พอใจในที่อยู่อาศัยปัจจุบัน";
    }
    else if (value == '2') {
        returnDesc = "ต้องการย้ายที่อยู่อาศัยปัจจุบัน";
    }
    else {
        returnDesc = "ไม่ระบุ";
    }
    return returnDesc;
}
function getEduStatus(value) {
    var returnDesc = "";
    if (value == '001') {
        returnDesc = "กำลังศึกษา";
    }
    else if (value == '002') {
        returnDesc = "สำเร็จการษึกษา";
    }
    else {
        returnDesc = "ไม่ระบุ";
    }
    return returnDesc;
}

function getQuestionJSDesc(type, value) {
    var msg = getAnswer(type);
    var response = value;
    var sortedKeys = Object.keys(msg[0]).sort();
    if (value == "999") {
        response = value;
    }
    else {
        for (var i = 0; i < msg.length; i++) {
            if (msg[i][sortedKeys[1]] == value) {
                response = msg[i][sortedKeys[0]];
            }
        }
    }
    return response;
}

function loadDefaultPic(imgID) {
    $(imgID).on('load', function () {
        console.log("image loaded correctly");
    }).on('error', function () {
        console.log("error loading image");
        $(imgID).attr("src", 'img/avatar-placeholder.png');
    });//.attr("src", 'img/avatar-placeholder.png');
}

function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

function setColorTable(tableName) {
    var table = document.getElementById(tableName);
    var rows = table.getElementsByTagName("tr");
    for (var a = 1; a < rows.length; a++) {
        rows[a].style.backgroundColor = '#EEEEEE';
        a++;
    }
}

// Template SELECT
function templateSelect() {
    var queryString = "";

    db.transaction((function (tx) {
        tx.executeSql(queryString, [], (function (tx, results) {
            if (results.rows.length != 0) {

            }
            else {

            }
        }), errorCB);
    }), errorCB);

}
