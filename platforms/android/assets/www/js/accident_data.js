﻿var db;

var QTID = [];
var QID = [];

function errorCB(err) {
    alert("Error processing SQL: " + err.code);
}

function successCB() {
    //alert("success!");
}

var currentQ;
var maxQ;

document.addEventListener("backbutton", function (e) {
    if ($.mobile.activePage.is('#blank')) {
        e.preventDefault();
        window.location.href = 'home.html';
        //navigator.app.exitApp();
    }
    else {
        navigator.app.backHistory()
    }
}, false);

$(document).on("pagecreate", "#blank", function (event) {
    $('#blank_next').click(function () {
        if (sessionStorage.getItem('AccidentID') === null) {
            console.log('AccidentID null');
            window.location.href = 'home.html#profile';
        }
        else if (sessionStorage.getItem('AccidentID') !== null) {
            console.log('AccidentID not null');
            if (sessionStorage.getItem('isDamagePage') == '1') {
                sessionStorage.removeItem('isDamagePage');
                window.location.href = 'home.html#accidentProfile';
            }
            else {
                sessionStorage.removeItem('isDamagePage');
                window.location.href = 'home.html#assistance';
            }

        }
        else {
            console.log('else');
        }
    });
});

//--------- Accident ----------
var addrLat = 0.0, addrLng = 0.0;
$(document).on("pagebeforecreate", "#accident", function (event) {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);
    maxQ = $('.Accident').length;
    $("#slider-1").attr("max", maxQ);

    $("#accidentHead").empty();
    hideNoteText();
    if ($('#Province').children('option').length < 3) { insertDropdown('accident'); lvCreate('accident'); }

    if (sessionStorage.getItem('AccidentID') === null) {
        console.log('AccidentID null');
        document.getElementById("navbarEditAccident").style.display = "none";
        $('#accidentHead').append('เพิ่มข้อมูลเหตุการณ์');
        currentQ = 1;
        showClassAccident(1, 0);

        if (navigator.onLine) { initMap(0.0, 0.0); } else { }
    }
    else if (sessionStorage.getItem('AccidentID') !== null && sessionStorage.getItem('EditType') == 'AccidProfile') {
        console.log('AccidentID not null');

        //document.getElementById("navbarAccident").style.display = "none";
        document.getElementById("navbarEditAccident").style.display = "none";
        $('#accidentHead').append('แก้ไขข้อมูลเหตุการณ์');

        loadAccideProfile(sessionStorage.getItem('AccidentID'));
        currentQ = 1;
        showClassAccident(1, 0);
    }
    else {
        console.log('else');
    }
});

$(document).on("pagecreate", "#accident", function () {
    var AccidData = [], propDmg = [], bodyDmg, otherDmg = [], assistReq = [], AccidType;

    $("#slider-1").on('slidestop', function (event) {
        //console.log($(this).slider().val());
        currentQ = $(this).slider().val();
        showClassAccident(currentQ, 1);
    });

    $("#accident_prev").click(function () {
        //console.log(currentQ);
        if (currentQ > 1) {
            currentQ--;
            //console.log(currentQ);
            showClassAccident(currentQ, 1);
        }
    });

    $("#accident_next").click(function () {
        //console.log(currentQ);
        if (currentQ == 1) { }
        else if (currentQ == 2) { }
        else if (currentQ == 3) { }
        else if (currentQ == 4) { }
        else if (currentQ == 5) {
            //--------------------------------------------------------
            AccidData = [];

            if ($('#AccidType').val() == "999") {
                AccidType = $('#AccidTypeNote').val();
            }
            else {
                AccidType = $('#AccidType :selected').html();
            }

            replaceNull("AccidentDetail");

            var guid = "";
            if (sessionStorage.getItem('AccidentID') === null) {
                //console.log('AccidentID null');
                guid = guidGenerator();
            }
            else if (sessionStorage.getItem('AccidentID') !== null) {
                //console.log('AccidentID not null');
                guid = sessionStorage.getItem('AccidentID');
            }
            else {
                console.log('else');
            }

            AccidData.push(guid);
            AccidData.push($('#AccidDate').val());
            AccidData.push($('#AccidDesc').val());
            AccidData.push($('#AccidTeller').val());
            AccidData.push(AccidType);

            AccidData.push($('#HouseNumber').val());
            AccidData.push($('#MooNumber').val());
            AccidData.push($('#VillageName').val());
            AccidData.push($('#Alley').val());
            AccidData.push($('#Street').val());

            AccidData.push($('#Tumbon').val());
            AccidData.push($('#City').val());
            AccidData.push($('#Province').val());
            AccidData.push($('#Postcode').val());
            AccidData.push($('#PoliceStation').val());

            AccidData.push(addrLat);
            AccidData.push(addrLng);

            AccidData.push(sessionStorage.getItem('PersonCID'));

            //-------------------------------------------------------

            propDmg = [];
            for (var i = 1; i < $('#propDmgTable tr').length; i++) {
                if ($('#checkbox' + i + ':checked').val() == "on") {
                    if ($('#number' + i).val() != "") {
                        //console.log($('#label' + i).text() + "//" + $('#number' + i).val());
                        propDmg.push($('#number' + i).val() + "', '" + $('#label' + i).text());
                    }
                    else {
                        alert("กรุณากรอกมูลค่าให้ครบ");
                        currentQ = 2;
                        showClassAccident(currentQ, 1);
                        return false;
                    }
                }
                else {
                }
            }

            //----------------------------------------------------------

            bodyDmg = "";
            if ($('input[name=radio-a]:checked').val()) {
                var radioID = $('input[name=radio-a]:checked').attr("id");
                bodyDmg = $('input[name=radio-a]:checked').val() + "', '-";
                //console.log(bodyDmg);
            }
            else {
                alert('กรุณาเลือกความเสียหาย');
                currentQ = 3;
                showClassAccident(currentQ, 1);
                return false;
            }

            //----------------------------------------------------------

            otherDmg = [];
            for (var i = 0; i < $('#my-controlgroup a').length; i++) {
                otherDmg.push($('#my-controlgroup a')[i].text);
            }

            //----------------------------------------------------------
            assistReq = []; var txtLabel;
            if ($('input[name=checkbox-a]:checked').val()) {
                $.each($("input[name='checkbox-a']:checked"), function () {
                    //console.log($(this).val());
                    if ($(this).val() == '999') {
                        txtLabel = $('#lb-' + $(this).attr("id")).text();
                    }
                    else {
                        txtLabel = "-";
                    }

                    assistReq.push($(this).val() + "', '" + txtLabel);
                });
            }
            else {
                //alert('กรุณาเลือกความช่วยเหลือที่ต้องการ');
                //return false;
            }

            //-------------------------------------------------------------

            console.log(AccidData);
            console.log(propDmg);
            console.log(bodyDmg);
            console.log(otherDmg);
            console.log(assistReq);

            var r = confirm("ต้องการบันทึกข้อมูล ใช่หรือไม่");
            if (r == true) {
                db.transaction(queryAccidDetail, errorCB, successCB);
            } else { console.log("cancle"); }


        }
        else {
            //console.log('have no Q');
        }

        if (currentQ <= maxQ - 1) {
            currentQ++;
            //console.log(currentQ);
            showClassAccident(currentQ, 1);
        }
        //replaceNull(currentQ); //หาวิธิ Replace Null ด้วย

        /*
        var AccidType;
        if ($('#AccidType').val() == "999") {
            AccidType = $('#AccidTypeNote').val();
        }
        else {
            AccidType = $('#AccidType :selected').html();
        }
        
        var queryInsertAccid = "INSERT INTO PersonAccident "
            + "( AccidID, AccidDate, AccidDesc, AccidTeller, AccidType, "
            + "AccidHouseNo, AccidMoo, AccidVillage, AccidAlley, AccidStreet, "
            + "AccidTumbon, AccidCity, AccidProvince, AccidPostCode, PoliceStation, "
            + "PID ) "
            + "VALUES ( "
            + "'" + guidGenerator() + "', "
            + "'" + $('#AccidDate').val() + "', "
            + "'" + $('#AccidDesc').val() + "', "
            + "'" + $('#AccidTeller').val() + "', "
            + "'" + AccidType + "', "
        
            + "'" + $('#HouseNumber').val() + "', "
            + "'" + $('#MooNumber').val() + "', "
            + "'" + $('#VillageName').val() + "', "
            + "'" + $('#Alley').val() + "', "
            + "'" + $('#Street').val() + "', "
        
            + "'" + $('#Tumbon').val() + "', "
            + "'" + $('#City').val() + "', "
            + "'" + $('#Province').val() + "', "
            + "'" + $('#Postcode').val() + "', "
            + "'" + $('#PoliceStation').val() + "', "
        
            + "'" + sessionStorage.getItem('PersonCID') + "' ) ";
        
        var r = confirm("ต้องการเพิ่มเหตุการณ์ใหม่ใช่หรือไม่");
        if (r == true) {
            //console.log(queryInsertAccid);
            db.transaction(function (tx) {
                tx.executeSql(queryInsertAccid);
            }, errorCB, function () {
                window.location.href = "home.html#profile";
            });
        }
        else {
            console.log("ยกเลิก");
        }
        */
    });

    $('#propDmgTable td').click(function () {
        var row = $(this).parent().index() + 1;
        var col = $(this).index();

        if (col == 2) {
            if ($('#checkbox' + row + ':checked').val() != "on") {
                $('#checkbox' + row).click();
            }
        }
        else if (col == 0) {

        }
        else {
            $('#checkbox' + row).click();
        }

    });

    $("#accident_edit").click(function () {
        replaceNull("accident");

        var AccidType;
        if ($('#AccidType').val() == "999") {
            AccidType = $('#AccidTypeNote').val();
        }
        else {
            AccidType = $('#AccidType :selected').html();
        }
        console.log(AccidType);

        var updateAccid = "UPDATE PersonAccident SET "
            + "AccidDate = '" + $('#AccidDate').val() + "', "
            + "AccidDesc = '" + $('#AccidDesc').val() + "', "
            + "AccidTeller = '" + $('#AccidTeller').val() + "', "
            + "AccidType = '" + AccidType + "', "

            + "AccidHouseNo = '" + $('#HouseNumber').val() + "', "
            + "AccidMoo = '" + $('#MooNumber').val() + "', "
            + "AccidVillage = '" + $('#VillageName').val() + "', "
            + "AccidAlley = '" + $('#Alley').val() + "', "
            + "AccidStreet = '" + $('#Street').val() + "', "

            + "AccidTumbon = '" + $('#Tumbon').val() + "', "
            + "AccidCity = '" + $('#City').val() + "', "
            + "AccidProvince = '" + $('#Province').val() + "', "
            + "AccidPostCode = '" + $('#Postcode').val() + "', "
            + "PoliceStation = '" + $('#PoliceStation').val() + "' "

            + "WHERE AccidID = '" + sessionStorage.getItem('AccidentID') + "' ";

        var r = confirm("ต้องการอัพเดทข้อมูลเหตุการณ์ใช่หรือไม่");
        if (r == true) {
            //console.log(queryInsertAccid);
            db.transaction(function (tx) {
                tx.executeSql(updateAccid);
            }, errorCB, function () {
                window.location.href = "home.html#profile";
            });
        }
        else {
            console.log("ยกเลิก");
        }

    });

    $('#addOther').click(function () {
        var length = $('#propDmgTable tr').length;
        console.log(length);

        for (var i = 0; i < $('#popupOther input').length; i++) {
            if ($('#popupOther input')[i].value == "") {
                alert("กรุณาใส่ข้อมูลให้ครบ");
                return false;
            }
            else if ($('#popupOther input')[i].value != "" && i == $('#popupOther input').length - 1) {
                $("table#propDmgTable tbody").append(""
                + "<tr>"
                + "<td><input id='checkbox" + length + "' type='checkbox' style='margin-top: 1px' checked></td>"
                + "<td><label id='label" + length + "' style='margin-top: 10px'>" + $('#propOther').val() + "</label></td>"
                + "<td><input id='number" + length + "' type='number' data-mini='true' value=" + $('#totalValOther').val() + "></td>"
                + "</tr>"
                + "").closest("table#propDmgTable").table("refresh").trigger("create");

                $('#popupOther input').val("");
                $('#popupOther').popup("close");

                console.log(length);
            }
            else { //foo
            }
        }
    });

    (function ($, undefined) {
        var counter = 0;
        $(document).bind("pageinit", function (e) {
            $("#append", e.target).on("click", function (e) {
                //counter++;
                var group = $("#my-controlgroup"), $el, action = function () {
                    var action = "remove";
                    $el[action]();
                    group.controlgroup("refresh");
                };

                //$el = $("<a href='#'>Link " + counter + "</a>").bind("click", action);
                if ($('#otherDmgTxt').val() != "") {
                    $el = $("<a href='#' class='ui-btn ui-btn-icon-right ui-icon-delete ui-shadow ui-corner-all'>" + $('#otherDmgTxt').val() + "</a>").bind("click", action);
                    $("#my-controlgroup").controlgroup("container")[$(this).attr("id")]($el);
                    //console.log($(this));
                    $el.buttonMarkup();

                    $('#otherDmgTxt').val("");
                    group.controlgroup("refresh");
                }
                else {
                    //console.log("กรุณาใส่ข้อมูล");
                }
            });
        });
    })(jQuery);

    $('#otherAssistBtn').click(function () {
        var counter = $('[name="checkbox-a"]').length + 1;
        if ($('#otherAssistTxt').val() != "") {
            //var temp = "<input type='checkbox' name='checkbox-a' id='checkbox-" + counter + "-a' value='" + $('#otherAssistTxt').val() + "' checked><label for='checkbox-" + counter + "-a'>" + $('#otherAssistTxt').val() + "</label>";
            var temp = "<input type='checkbox' name='checkbox-a' id='checkbox-" + counter + "-a' value='999' checked><label id='lb-checkbox-" + counter + "-a' for='checkbox-" + counter + "-a'>" + $('#otherAssistTxt').val() + "</label>";
            $('#ctrlGroupAssist').controlgroup("container").append(temp);
            $('#ctrlGroupAssist').enhanceWithin().controlgroup("refresh");
            $('#otherAssistTxt').val("");
        }
        else {
            //console.log("กรุณาใส่ข้อมูล");
        }
    });

    document.getElementById("AccidType").onchange = function () {
        if ($(this).val() == "999") {
            document.getElementById("AccidTypeNote").style.display = "inline";
        }
        else {
            document.getElementById("AccidTypeNote").style.display = "none";
        }
    };

    document.getElementById('Province').onchange = function () {
        var txtProvinceID = $(this).val();
        $('#City').empty();
        $('#City').append($("<option></option>").val(000).html('ไม่ระบุ'));
        $('#City').selectmenu("refresh");
        $('#Tumbon').empty();
        $('#Tumbon').append($("<option></option>").val(000).html('ไม่ระบุ'));
        $('#Tumbon').selectmenu("refresh");
        insertCity(txtProvinceID, 'City');
    };

    document.getElementById('City').onchange = function () {
        var txtAmphorID = $(this).val();
        $('#Tumbon').empty();
        $('#Tumbon').append($("<option></option>").val(000).html('ไม่ระบุ'));
        $('#Tumbon').selectmenu("refresh");
        insertTumbon(txtAmphorID, 'Tumbon');
    };

    document.getElementById('Tumbon').onchange = function () {
        if (navigator.onLine) {
            db.transaction(function (tx) {
                tx.executeSql("SELECT * FROM Tumbon WHERE TumbonID = '" + $('#Tumbon').val() + "'", [], function (tx, rs) {
                    if (rs.rows.length != 0) {
                        map.setCenter({ lat: rs.rows[0].Latitude, lng: rs.rows[0].Longtitude });

                        if (myMarker.length != 0) {
                            myMarker[0].setMap(null);
                        }
                        myMarker = [];
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(rs.rows[0].Latitude, rs.rows[0].Longtitude)
                        });
                        marker.addListener('click', function () {
                            this.setMap(null);
                            addrLat = 0.0; addrLng = 0.0;
                        });
                        addrLat = rs.rows[0].Latitude; addrLng = rs.rows[0].Longtitude;
                        myMarker.push(marker);
                        myMarker[0].setMap(map);

                    } else { }
                }, errorCB);
            }, errorCB);
        } else { }
    }

    function queryAccidDetail(tx) {
        tx.executeSql("SELECT AccidID FROM PersonAccident WHERE AccidID = '" + AccidData[0] + "' ", [], function (tx, rs) {
            console.log(rs.rows.length);
            if (rs.rows.length == 0) {
                //console.log('NEED Insert');
                db.transaction(insertAccidTable, errorCB, function () {
                    sessionStorage.setItem('isDamagePage', '1');
                    $(":mobile-pagecontainer").pagecontainer("change", "#blank", { transition: "flip" });
                });
            }
            else {
                //console.log('NEED Update');
                db.transaction(updateAccidTable, errorCB, function () {
                    sessionStorage.setItem('isDamagePage', '1');
                    $(":mobile-pagecontainer").pagecontainer("change", "#blank", { transition: "flip" });
                });
            }
        }, errorCB);
    }

    function insertAccidTable(tx) {
        var queryPersonAccident = "INSERT INTO PersonAccident VALUES ( ";
        for (var i = 0 ; i < AccidData.length; i++) {
            if (i == AccidData.length - 1) { queryPersonAccident += "'" + AccidData[i] + "') "; }
            else { queryPersonAccident += "'" + AccidData[i] + "', "; }
        }
        console.log(queryPersonAccident);
        tx.executeSql(queryPersonAccident, [], function (tx) {

            var queryPropDamage = "";
            for (var i = 0 ; i < propDmg.length; i++) {
                queryPropDamage = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + AccidData[0] + "', "
                + "'" + propDmg[i] + "', '001' ) ";
                console.log(queryPropDamage);
                tx.executeSql(queryPropDamage, null, successCB, errorCB);
            }

            var queryBodyDamage = "";
            queryBodyDamage = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + AccidData[0] + "', "
            + "'" + bodyDmg + "', '002' ) ";
            console.log(queryBodyDamage);
            tx.executeSql(queryBodyDamage, null, successCB, errorCB);

            var queryOtherDamage = "";
            for (var i = 0 ; i < otherDmg.length; i++) {
                queryOtherDamage = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + AccidData[0] + "', "
                + " '999', '" + otherDmg[i] + "', '003' ) ";
                console.log(queryOtherDamage);
                tx.executeSql(queryOtherDamage, null, successCB, errorCB);
            }

            var queryAssistRequire = "";
            for (var i = 0 ; i < assistReq.length; i++) {
                queryAssistRequire = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + AccidData[0] + "', "
                + "'" + assistReq[i] + "', '004' ) ";
                console.log(queryAssistRequire);
                tx.executeSql(queryAssistRequire, null, successCB, errorCB);
            }

        }, errorCB);

    }

    function updateAccidTable(tx) {
        var queryPersonAccident = "UPDATE PersonAccident SET "
            + "AccidDate = '" + AccidData[1] + "', "
            + "AccidDesc = '" + AccidData[2] + "', "
            + "AccidTeller = '" + AccidData[3] + "', "
            + "AccidType = '" + AccidData[4] + "', "
            + "AccidHouseNo = '" + AccidData[5] + "', "

            + "AccidMoo = '" + AccidData[6] + "', "
            + "AccidVillage = '" + AccidData[7] + "', "
            + "AccidAlley = '" + AccidData[8] + "', "
            + "AccidStreet = '" + AccidData[9] + "', "
            + "AccidTumbon = '" + AccidData[10] + "', "

            + "AccidCity = '" + AccidData[11] + "', "
            + "AccidProvince = '" + AccidData[12] + "', "
            + "AccidPostCode = '" + AccidData[13] + "', "
            + "PoliceStation = '" + AccidData[14] + "' ";
        if (addrLat != 0.0 && addrLng != 0.0) {
            queryPersonAccident += ', AccidLat = ' + AccidData[15] + ', AccidLng = ' + AccidData[16] + ' ';
        } else { }

        queryPersonAccident += "WHERE AccidID = '" + AccidData[0] + "' ";
        + "AND PID = '" + AccidData[AccidData.length - 1] + "' ";

        console.log(queryPersonAccident);

        var queryDeleteForUpdate = "DELETE FROM AccidentDamageAndAssist WHERE AccidID = '" + AccidData[0] + "' "
        + "AND (GroupType = '001' OR GroupType = '002' OR GroupType = '003' OR GroupType = '004') "

        tx.executeSql(queryPersonAccident, [], function (tx) {
            tx.executeSql(queryDeleteForUpdate, null, function () {
                var queryPropDamage = "";
                for (var i = 0 ; i < propDmg.length; i++) {
                    queryPropDamage = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + AccidData[0] + "', "
                    + "'" + propDmg[i] + "', '001' ) ";
                    console.log(queryPropDamage);
                    tx.executeSql(queryPropDamage, null, successCB, errorCB);
                }

                var queryBodyDamage = "";
                queryBodyDamage = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + AccidData[0] + "', "
                + "'" + bodyDmg + "', '002' ) ";
                console.log(queryBodyDamage);
                tx.executeSql(queryBodyDamage, null, successCB, errorCB);

                var queryOtherDamage = "";
                for (var i = 0 ; i < otherDmg.length; i++) {
                    queryOtherDamage = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + AccidData[0] + "', "
                    + " '999', '" + otherDmg[i] + "', '003' ) ";
                    console.log(queryOtherDamage);
                    tx.executeSql(queryOtherDamage, null, successCB, errorCB);
                }

                var queryAssistRequire = "";
                for (var i = 0 ; i < assistReq.length; i++) {
                    queryAssistRequire = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + AccidData[0] + "', "
                    + "'" + assistReq[i] + "', '004' ) ";
                    console.log(queryAssistRequire);
                    tx.executeSql(queryAssistRequire, null, successCB, errorCB);
                }


            }, errorCB);
        }, errorCB);
    }

});

function showClassAccident(curQ, type) {
    // type: 0 = not refresh // 1 = refresh
    for (var i = 0; i < $('.Accident').length ; i++) {
        var id = $('.Accident')[i].id;
        $('#' + id).hide();
    }
    if (type == 1) {
        $("#slider-1").val(curQ + "").slider('refresh');
    }
    else {
        $("#slider-1").val(curQ + "");
    }
    curQ = curQ - 1;
    var id = $('.Accident')[curQ].id;
    $('#' + id).show();
}

function loadAccideProfile(AccidID) {
    var selectAccident = "SELECT * FROM PersonAccident WHERE AccidID = '" + AccidID + "' ";
    db.transaction(function (tx) {
        tx.executeSql(selectAccident, [], function (tx, rs) {
            if (rs.rows.length != 0) {
                $('#AccidDate').val(rs.rows[0].AccidDate);
                $('#AccidDesc').val(rs.rows[0].AccidDesc);
                $('#AccidTeller').val(rs.rows[0].AccidTeller);
                $('#AccidType').val(checkDropdownValue("AccidType", rs.rows[0].AccidType));

                $('#AccidTypeNote').val(rs.rows[0].AccidType);

                $('#HouseNumber').val(rs.rows[0].AccidHouseNo);
                $('#MooNumber').val(rs.rows[0].AccidMoo);
                $('#VillageName').val(rs.rows[0].AccidVillage);
                $('#Alley').val(rs.rows[0].AccidAlley);
                $('#Street').val(rs.rows[0].AccidStreet);

                $('#Province').val(rs.rows[0].AccidProvince);
                autoLoadCityAndTumbon('accident', 'Tumbon', 'City', rs.rows[0].AccidTumbon, rs.rows[0].AccidCity, rs.rows[0].AccidProvince);
                $('#Postcode').val(rs.rows[0].AccidPostCode);
                $('#PoliceStation').val(rs.rows[0].PoliceStation);

                if (navigator.onLine) { initMap(rs.rows[0].AccidLat, rs.rows[0].AccidLng); } else { }

            }
        }, errorCB);
    }, errorCB, function () {
        $('#accident select').selectmenu("refresh");
        loadDmgAndAssist(AccidID);
    });

}
function loadDmgAndAssist(AccidID) {
    var selectAccidentDamageAndAssist = "SELECT Value, Note, GroupType FROM AccidentDamageAndAssist WHERE AccidID = '" + AccidID + "' ";
    db.transaction(function (tx) {
        tx.executeSql(selectAccidentDamageAndAssist, [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    //console.log(rs.rows[i]);
                    if (rs.rows[i].GroupType == '001') {
                        loadPropDmg(tx, rs.rows[i]);
                    }
                    else if (rs.rows[i].GroupType == '002') {
                        loadBodyDmg(tx, rs.rows[i]);
                    }
                    else if (rs.rows[i].GroupType == '003') {
                        loadOtherDmg(tx, rs.rows[i]);
                    }
                    else if (rs.rows[i].GroupType == '004') {
                        loadAssistReq(tx, rs.rows[i]);
                    }
                    else { }
                }
            }
        }, errorCB);
    }, errorCB, successCB);
}
function loadPropDmg(tx, rs) {
    //console.log(rs);
    var propTbLength = $('#propDmgTable tr').length;
    for (var j = 0; j < propTbLength; j++) {
        if ($('#label' + j).text() == rs.Note) {
            //console.log($('#label' + j).text() + "===" + rs.Note);

            $('#checkbox' + j).click();
            $('#number' + j).val(rs.Value);
            j = propTbLength - 1;
        }
        else if (($('#label' + j).text() != rs.Note) && (j == propTbLength - 1)) {
            $("table#propDmgTable tbody").append(""
                + "<tr>"
                + "<td><input id='checkbox" + propTbLength + "' type='checkbox' style='margin-top: 1px' checked></td>"
                + "<td><label id='label" + propTbLength + "' style='margin-top: 10px'>" + rs.Note + "</label></td>"
                + "<td><input id='number" + propTbLength + "' type='number' data-mini='true' value=" + rs.Value + "></td>"
                + "</tr>"
                + "").closest("table#propDmgTable").table("refresh").trigger("create");
        }
        else { }
    }
}
function loadBodyDmg(tx, rs) {
    var bodyLength = $('input[name=radio-a]').length;
    for (var i = 0; i < bodyLength; i++) {
        if ($('input[name=radio-a]')[i].value == rs.Value) {
            //console.log($('input[name=radio-a]')[i].value + " === " + rs.Value + " // radio-" + i + "-a");
            $('#radio-' + i + '-a').attr('checked', true);
            $('#body-controlgroup').controlgroup('refresh');
            i = bodyLength - 1;
        }
    }
}
function loadOtherDmg(tx, rs) {
    var $el = $("<a href='#' class='ui-btn ui-btn-icon-right ui-icon-delete ui-shadow ui-corner-all'>" + rs.Note + "</a>").bind("click", action = function () {
        var r = confirm('ต้องการลบใช่หรือไม่');
        if (r == true) {
            $el["remove"]();
            $("#my-controlgroup").controlgroup("refresh");
        }
        else { }
    });
    $("#my-controlgroup").controlgroup("container").append($el);
    $el.buttonMarkup();
    $("#my-controlgroup").controlgroup("refresh");

}
function loadAssistReq(tx, rs) {
    var assistTBLength = $('input[name=checkbox-a]').length;
    for (var i = 0; i < assistTBLength; i++) {
        if ($('input[name=checkbox-a]')[i].value == rs.Value && (rs.Value != '999')) {
            var checkID = i + 1;
            //console.log($('input[name=checkbox-a]')[i].value + " === " + rs.Value + " // checkbox-" + checkID + "-a");
            $('#checkbox-' + checkID + '-a').attr('checked', true);
            $('#ctrlGroupAssist').controlgroup('refresh');
            i = assistTBLength - 1;
        }
        else if ((rs.Value == '999') && (i == assistTBLength - 1)) {
            //console.log(rs.Value + " // " + rs.Note);
            var checkID = assistTBLength + 1;
            var temp = "<input type='checkbox' name='checkbox-a' id='checkbox-" + checkID + "-a' value='999' checked='checked'><label id='lb-checkbox-" + checkID + "-a' for='checkbox-" + checkID + "-a'>" + rs.Note + "</label>";
            $('#ctrlGroupAssist').controlgroup("container").append(temp);
            $('#ctrlGroupAssist').enhanceWithin().controlgroup("refresh");
        }
        else { }
    }

}

var map; var myMarker = [];
function initMap(aLat, aLng) {

    var isEdit = false;

    if (aLat == 0.0 && aLng == 0.0) {
        isEdit = false;
        aLat = 13.76486874;
        aLng = 100.5382767; //bkk
    } else { isEdit = true; }

    document.getElementById('mapAcci').innerHTML = "";
    map = new google.maps.Map(document.getElementById('mapAcci'), {
        center: new google.maps.LatLng(aLat, aLng),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.HYBRID
    });

    // Create the search box and link it to the UI element.
    var input = document.getElementById('accidAddr');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

        // Clear out the old markers.
        markers.forEach(function (marker) {
            marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            var icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
                map: map,
                icon: icon,
                title: place.name,
                position: place.geometry.location
            }));

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
    });

    google.maps.event.addListener(map, 'click', function (event) {
        if (myMarker.length != 0) {
            myMarker[0].setMap(null);
        }
        myMarker = [];
        var marker = new google.maps.Marker({
            position: event.latLng,
            //draggable: true
        });
        marker.addListener('click', function () {
            this.setMap(null);
            addrLat = 0.0; addrLng = 0.0;
        });

        addrLat = event.latLng.lat(); addrLng = event.latLng.lng();

        //console.log(event.latLng.lat() + " " + event.latLng.lng());
        myMarker.push(marker);
        myMarker[0].setMap(map);
        //map.setCenter(event.latLng);
    });

    if (isEdit) {
        var aMarker = new google.maps.Marker({
            position: new google.maps.LatLng(aLat, aLng),
            icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
        });
        aMarker.addListener('click', function () {
            this.setMap(null);
            addrLat = 0.0; addrLng = 0.0;
        });
        aMarker.setMap(map);
    } else { }

}
//------------------------------

//-------- Assistance ----------
$(document).on("pagebeforecreate", "#assistance", function () {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);

    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);
    maxQ = $('.Assist').length;
    $("#slider-2").attr("max", maxQ);

    $("#assistanceHead").empty();

    if (sessionStorage.getItem('AccidentID') === null) {
        console.log('AccidentID null');
        $('#assistanceHead').append('เพิ่มความช่วยเหลือ');
        currentQ = 1;
        showClassAssist(1, 0);
    }
    else if (sessionStorage.getItem('AccidentID') !== null && sessionStorage.getItem('EditType') == 'Assistance') {
        console.log('AccidentID not null');
        $('#assistanceHead').append('แก้ไขความช่วยเหลือ');
        loadAssistProfile(sessionStorage.getItem('AccidentID'));
        currentQ = 1;
        showClassAssist(1, 0);
    }
    else {
        console.log('else');
        $('#assistanceHead').append('ความช่วยเหลือ');
    }

});

$(document).on("pagecreate", "#assistance", function () {
    var propAssist = [], bodyAssist = [], studentAssist = [], otherAssist = [];
    var otherType = 0;

    $("#slider-2").on('slidestop', function (event) {
        //console.log($(this).slider().val());
        currentQ = $(this).slider().val();
        showClassAssist(currentQ, 1);
    });

    $("#assistance_prev").click(function () {
        //console.log(currentQ);
        if (currentQ > 1) {
            currentQ--;
            //console.log(currentQ);
            showClassAssist(currentQ, 1);
        }
    });

    $("#assistance_next").click(function () {

        if (currentQ == 1) { }
        else if (currentQ == 2) { }
        else if (currentQ == 3) { }
        else if (currentQ == 4) {
            //-------------------------------------------------------------------------

            propAssist = []; var data = [];
            if ($('#categories table').length != 0) {
                $('#categories table').each(function () {
                    //console.log($(this).attr("id"));
                    var id = $(this).attr("id");
                    if (id == 'table-001') {
                        data = [];
                        for (var i = 0; i < $('#' + id + ' td').length ; i++) {
                            data.push($('#' + id + ' td')[i].innerText);
                        }
                        for (var i = 0; i < data.length ; i++) {
                            propAssist.push(data[i + 1] + "', '" + data[i] + "', '011");
                            i = i + 1;
                        }
                    }
                    else if (id == 'table-002') {
                        data = [];
                        for (var i = 0; i < $('#' + id + ' td').length ; i++) {
                            data.push($('#' + id + ' td')[i].innerText);
                        }
                        for (var i = 0; i < data.length ; i++) {
                            propAssist.push(data[i + 1] + "', '" + data[i] + "', '012");
                            i = i + 1;
                        }
                    }
                    else if (id == 'table-003') {
                        data = [];
                        for (var i = 0; i < $('#' + id + ' td').length ; i++) {
                            data.push($('#' + id + ' td')[i].innerText);
                        }
                        for (var i = 0; i < data.length ; i++) {
                            propAssist.push(data[i + 1] + "', '" + data[i] + "', '013");
                            i = i + 1;
                        }
                    }
                    else if (id == 'table-004') {
                        data = [];
                        for (var i = 0; i < $('#' + id + ' td').length ; i++) {
                            data.push($('#' + id + ' td')[i].innerText);
                        }
                        for (var i = 0; i < data.length ; i++) {
                            propAssist.push(data[i + 1] + "', '" + data[i] + "', '014");
                            i = i + 1;
                        }
                    }
                    else if (id == 'table-005') {
                        data = [];
                        for (var i = 0; i < $('#' + id + ' td').length ; i++) {
                            data.push($('#' + id + ' td')[i].innerText);
                        }
                        for (var i = 0; i < data.length ; i++) {
                            propAssist.push(data[i + 1] + "', '" + data[i] + "', '015");
                            i = i + 1;
                        }
                    }
                    else if (id == 'table-006') {
                        data = [];
                        for (var i = 0; i < $('#' + id + ' td').length ; i++) {
                            data.push($('#' + id + ' td')[i].innerText);
                        }
                        for (var i = 0; i < data.length ; i++) {
                            propAssist.push(data[i + 1] + "', '" + data[i] + "', '016");
                            i = i + 1;
                        }
                    }
                    else if (id == 'table-007') {
                        data = [];
                        for (var i = 0; i < $('#' + id + ' td').length ; i++) {
                            data.push($('#' + id + ' td')[i].innerText);
                        }
                        for (var i = 0; i < data.length ; i++) {
                            propAssist.push(data[i + 1] + "', '" + data[i] + "', '017");
                            i = i + 1;
                        }
                    }
                    else { }
                });
            }
            else { console.log("0"); }

            //return false;

            //-------------------------------------------------------------------------

            bodyAssist = []; var cb = ['a', 'b', 'c', 'd'], data = [];

            for (var i = 0; i < cb.length ; i++) {
                $.each($("input[name='checkbox-body-" + cb[i] + "']:checked"), function () {
                    data = [];
                    //console.log($(this).val());
                    if (i == 0) {
                        //data.push($(this).val() + "', '-', '021");
                        bodyAssist.push($(this).val() + "', '-', '021");
                    }
                    else if (i == 1) {
                        //data.push($(this).val() + "', '-', '022");
                        bodyAssist.push($(this).val() + "', '-', '022");
                    }
                    else if (i == 2) {
                        //data.push($(this).val() + "', '-', '023");
                        bodyAssist.push($(this).val() + "', '-', '023");
                    }
                    else if (i == 3) {
                        //data.push($(this).val() + "', '-', '024");
                        bodyAssist.push($(this).val() + "', '-', '024");
                    }
                    else { }
                });
            }

            for (var i = 0; i < $('#bodyAssistOtherList li').length; i++) {
                data = [];
                var str = $('#bodyAssistOtherList li')[i].innerText;
                var res = str.split(" จำนวน: ");
                res[1] = res[1].replace("\n", "");
                res[1] = res[1].replace(" บาท", "");
                //data.push(res[1] + "', '" + res[0] + "', '025");
                bodyAssist.push(res[1] + "', '" + res[0] + "', '025");
            }

            //-------------------------------------------------------------------------

            studentAssist = []; var cb = ['e', 'f'];

            for (var i = 0; i < cb.length ; i++) {
                $.each($("input[name='checkbox-body-" + cb[i] + "']:checked"), function () {
                    if (i == 0) {
                        studentAssist.push($(this).val() + "', '-', '026");
                    }
                    else if (i == 1) {
                        studentAssist.push($(this).val() + "', '-', '027");
                    }
                    else { }
                });
            }

            for (var i = 0; i < $('#studentAssistList li').length; i++) {
                var str = $('#studentAssistList li')[i].innerText;
                var res = str.split(" จำนวน: ");
                res[1] = res[1].replace("\n", "");
                res[1] = res[1].replace(" บาท", "");
                studentAssist.push(res[1] + "', '" + res[0] + "', '028");
            }

            //-------------------------------------------------------------------------

            otherAssist = [];

            $.each($("input[name='checkbox-g']:checked"), function () {
                //console.log($(this).val());
                var txtLabel = $('#lb-' + $(this).attr("id")).text();
                if ($(this).val() == '999') {
                    otherAssist.push($(this).val() + "', '" + txtLabel + "', '031");
                }
                else {
                    otherAssist.push($(this).val() + "', '-', '031");
                }

            });

            //-------------------------------------------------------------------------

            console.log(propAssist);
            console.log(bodyAssist);
            console.log(studentAssist);
            console.log(otherAssist);

            var r = confirm("ต้องการบันทึกข้อมูล ใช่หรือไม่");
            if (r == true) {
                db.transaction(queryAssistance, errorCB, successCB);
            } else { console.log("cancle"); }

        }
        else {
            //console.log('have no Q');
        }

        if (currentQ <= maxQ - 1) {
            currentQ++;
            //console.log(currentQ);
            showClassAssist(currentQ, 1);
        }
    });

    $('#addAssistBtn').click(function () {
        var selectID = $("#department").val();
        var selectText = $("#department option:selected").text();

        for (var i = 0; i < $('#popupAddAssist input').length; i++) {
            if ($('#popupAddAssist input')[i].value == "") {
                alert("กรุณาใส่ข้อมูลให้ครบ");
                return false;
            }
            else if ($('#popupAddAssist input')[i].value != "" && i == $('#popupAddAssist input').length - 1) {

                if ($('div#collapsible-' + selectID).length == 0 && $("#department").val() != '000') {
                    console.log(selectID);
                    //create collapsible -> create table -> append table to collapsible

                    //----- create collapsible -------
                    $('#categories').append('<div id="collapsible-' + selectID + '" data-role="collapsible" class="custom-collapsible-z" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-collapsed="false"></div>');
                    $("#collapsible-" + selectID).append('<h3>' + selectText + '</h3>');
                    //--------------------------------

                    //------------ create table --------------
                    var service_table = $('<table data-role="table" id="table-' + selectID + '" class="ui-body-d ui-shadow table-stripe ui-responsive" data-mode="columntoggle" data-column-btn-text="คอลัมน์..." data-column-btn-theme="z" data-column-popup-theme="a"></table><br />');
                    var service_tr_th = $("<thead><tr><th>การช่วยเหลือ</th><th>จำนวนเงิน</th></tr></thead>");
                    var service_tbody = $('<tbody></tbody>');
                    var service_tr = $('<tr></tr>').bind("click", function () {
                        var r = confirm('ต้องการลบใช่หรือไม่');
                        if (r == true) {
                            $(this)["remove"]()
                        }
                        else { }
                    });
                    var service_name_td = $('<td>' + $("#assistName").val() + '</td><td>' + $("#assistVal").val() + '</td>');
                    service_name_td.appendTo(service_tr);
                    service_tr_th.appendTo(service_table);
                    service_tr.appendTo(service_tbody);
                    service_tbody.appendTo(service_table);
                    //----------------------------------------

                    //------- append table to collapsible -------
                    service_table.appendTo($("#collapsible-" + selectID));
                    service_table.table();

                    $('#categories').trigger('create');
                    //-------------------------------------------
                }
                else if ($('div#collapsible-' + selectID).length != 0 && $("#department").val() != '000') {
                    console.log("already have");

                    var service_name_td = $('<tr><td>' + $("#assistName").val() + '</td><td>' + $("#assistVal").val() + '</td></tr>').bind("click", function () {
                        var r = confirm('ต้องการลบใช่หรือไม่');
                        if (r == true) {
                            $(this)["remove"]()
                        }
                        else { }
                    });
                    service_name_td.appendTo($('table#table-' + selectID + ' tbody'));
                    $('table#table-' + selectID).table();
                }
                else {
                    alert("กรุณาเลือกหน่วยงาน");
                    return false;
                }

                $('#popupAddAssist input').val("");
                //$('#popupAddAssist select').val("000").selectmenu("refresh");
                $('#popupAddAssist').popup("close");

            }
            else { //foo
            }
        }

    });

    $("#bodyOtherAssistBtn").click(function () { otherType = 1; });
    $("#stuAssistBtn").click(function () { otherType = 2; });
    $("#addBodyAssistBtn").click(function () {
        for (var i = 0; i < $('#popupAddBodyAssist input').length; i++) {
            if ($('#popupAddBodyAssist input')[i].value == "") {
                alert("กรุณาใส่ข้อมูลให้ครบ");
                return false;
            }
            else if ($('#popupAddBodyAssist input')[i].value != "" && i == $('#popupOther input').length - 1) {

                var $el = $('<li data-icon="delete"><a>' + $('#bodyAssistName').val() + " จำนวน: " + $('#bodyAssistVal').val() + ' บาท</a></li>').bind("click", action = function () {
                    var r = confirm('ต้องการลบใช่หรือไม่');
                    if (r == true) {
                        $el["remove"]();
                        //$("#studentAssistList").listview("refresh");
                    }
                    else { }
                });

                if (otherType == 1) {
                    //$('#bodyAssistOtherList').append('<li>' + $('#bodyAssistName').val() + " จำนวน: " + $('#bodyAssistVal').val() + ' บาท</li>').listview("refresh");
                    $('#bodyAssistOtherList').append($el).listview("refresh");
                }
                else if (otherType == 2) {
                    //$('#studentAssistList').append('<li>' + $('#bodyAssistName').val() + " จำนวน: " + $('#bodyAssistVal').val() + ' บาท</li>').listview("refresh");
                    $('#studentAssistList').append($el).listview("refresh");

                }
                else { }
                $('#popupAddBodyAssist input').val("");
                $('#popupAddBodyAssist').popup("close");
            }
            else { //foo
            }
        }

    });

    $("#otherAssistBtn2").click(function () {
        var counter = $('[name="checkbox-g"]').length + 1;
        if ($('#otherAssistTxt2').val() != "") {
            var temp = "<input type='checkbox' name='checkbox-g' id='checkbox-" + counter + "-g' value='999' checked><label id='lb-checkbox-" + counter + "-g' for='checkbox-" + counter + "-g'>" + $('#otherAssistTxt2').val() + "</label>";
            $('#ctrlGroupAssist2').controlgroup("container").append(temp);
            $('#ctrlGroupAssist2').enhanceWithin().controlgroup("refresh");
            $('#otherAssistTxt2').val("");
        }
        else {
            //console.log("กรุณาใส่ข้อมูล");
        }

    });


    function queryAssistance(tx) {

        tx.executeSql("SELECT AccidID FROM AccidentDamageAndAssist WHERE AccidID = '" + sessionStorage.getItem('AccidentID') + "' ", [], function (tx, rs) {
            console.log(rs.rows.length);
            if (rs.rows.length == 0) {
                alert("ไม่พบข้อมูลความเสียหาย และความช่วยเหลือที่ต้องการ");
            }
            else {
                db.transaction(checkAlready, errorCB, successCB);
            }
        }, errorCB);

    }
    function checkAlready(tx) {
        var GroupType = ["011", "012", "013", "014", "015", "016", "017"
        , "021", "022", "023", "024", "025", "026", "027", "028", "031"];
        var queryStr = "SELECT * FROM AccidentDamageAndAssist WHERE AccidID = '" + sessionStorage.getItem('AccidentID') + "' AND "
        + "(GroupType = '" + GroupType[0] + "'";
        for (var i = 1; i < GroupType.length - 1 ; i++) {
            queryStr += " OR GroupType = '" + GroupType[i] + "'";
        }
        queryStr += " OR GroupType = '" + GroupType[GroupType.length - 1] + "') ";

        //console.log(queryStr);

        tx.executeSql(queryStr, [], function (tx, rs) {
            console.log(rs.rows.length);
            if (rs.rows.length == 0) {
                //console.log("Insert");
                db.transaction(insertAssistance, errorCB, function () {
                    sessionStorage.setItem('isDamagePage', '0');
                    $(":mobile-pagecontainer").pagecontainer("change", "#blank", { transition: "flip" });
                });
            }
            else {
                //console.log("Update");
                db.transaction(updateAssistance, errorCB, function () {
                    sessionStorage.setItem('isDamagePage', '0');
                    $(":mobile-pagecontainer").pagecontainer("change", "#blank", { transition: "flip" });
                });
            }
        }, errorCB);

    }
    function insertAssistance(tx) {
        var queryPropAssist = "";
        for (var i = 0 ; i < propAssist.length; i++) {
            queryPropAssist = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + sessionStorage.getItem('AccidentID') + "', "
            + "'" + propAssist[i] + "' ) ";
            console.log(queryPropAssist);
            tx.executeSql(queryPropAssist, null, successCB, errorCB);
        }

        var queryBodyAssist = "";
        for (var i = 0 ; i < bodyAssist.length; i++) {
            queryBodyAssist = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + sessionStorage.getItem('AccidentID') + "', "
            + "'" + bodyAssist[i] + "' ) ";
            console.log(queryBodyAssist);
            tx.executeSql(queryBodyAssist, null, successCB, errorCB);
        }

        var queryStudentAssist = "";
        for (var i = 0 ; i < studentAssist.length; i++) {
            queryStudentAssist = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + sessionStorage.getItem('AccidentID') + "', "
            + "'" + studentAssist[i] + "' ) ";
            console.log(queryStudentAssist);
            tx.executeSql(queryStudentAssist, null, successCB, errorCB);
        }

        var queryOtherAssist = "";
        for (var i = 0 ; i < otherAssist.length; i++) {
            queryOtherAssist = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + sessionStorage.getItem('AccidentID') + "', "
            + "'" + otherAssist[i] + "' ) ";
            console.log(queryOtherAssist);
            tx.executeSql(queryOtherAssist, null, successCB, errorCB);
        }

    }
    function updateAssistance(tx) {
        var GroupType = ["011", "012", "013", "014", "015", "016", "017"
        , "021", "022", "023", "024", "025", "026", "027", "028", "031"];
        var queryDelete = "DELETE FROM AccidentDamageAndAssist WHERE AccidID = '" + sessionStorage.getItem('AccidentID') + "' AND "
        + "(GroupType = '" + GroupType[0] + "'";
        for (var i = 1; i < GroupType.length - 1 ; i++) {
            queryDelete += " OR GroupType = '" + GroupType[i] + "'";
        }
        queryDelete += " OR GroupType = '" + GroupType[GroupType.length - 1] + "') ";

        console.log(queryDelete);

        tx.executeSql(queryDelete, null, function () {

            var queryPropAssist = "";
            for (var i = 0 ; i < propAssist.length; i++) {
                queryPropAssist = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + sessionStorage.getItem('AccidentID') + "', "
                + "'" + propAssist[i] + "' ) ";
                console.log(queryPropAssist);
                tx.executeSql(queryPropAssist, null, successCB, errorCB);
            }

            var queryBodyAssist = "";
            for (var i = 0 ; i < bodyAssist.length; i++) {
                queryBodyAssist = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + sessionStorage.getItem('AccidentID') + "', "
                + "'" + bodyAssist[i] + "' ) ";
                console.log(queryBodyAssist);
                tx.executeSql(queryBodyAssist, null, successCB, errorCB);
            }

            var queryStudentAssist = "";
            for (var i = 0 ; i < studentAssist.length; i++) {
                queryStudentAssist = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + sessionStorage.getItem('AccidentID') + "', "
                + "'" + studentAssist[i] + "' ) ";
                console.log(queryStudentAssist);
                tx.executeSql(queryStudentAssist, null, successCB, errorCB);
            }

            var queryOtherAssist = "";
            for (var i = 0 ; i < otherAssist.length; i++) {
                queryOtherAssist = "INSERT INTO AccidentDamageAndAssist VALUES ( '" + sessionStorage.getItem('AccidentID') + "', "
                + "'" + otherAssist[i] + "' ) ";
                console.log(queryOtherAssist);
                tx.executeSql(queryOtherAssist, null, successCB, errorCB);
            }

        }, errorCB);
    }

});

function showClassAssist(curQ, type) {
    for (var i = 0; i < $('.Assist').length ; i++) {
        var id = $('.Assist')[i].id;
        $('#' + id).hide();
    }
    if (type == 1) {
        $("#slider-2").val(curQ + "").slider('refresh');
    }
    else {
        $("#slider-2").val(curQ + "");
    }
    curQ = curQ - 1;
    var id = $('.Assist')[curQ].id;
    $('#' + id).show();

}

function loadAssistProfile(AccidID) {
    var GroupType = ["011", "012", "013", "014", "015", "016", "017"
        , "021", "022", "023", "024", "025", "026", "027", "028", "031"];
    var queryStr = "SELECT * FROM AccidentDamageAndAssist WHERE AccidID = '" + sessionStorage.getItem('AccidentID') + "' AND "
    + "(GroupType = '" + GroupType[0] + "'";
    for (var i = 1; i < GroupType.length - 1 ; i++) {
        queryStr += " OR GroupType = '" + GroupType[i] + "'";
    }
    queryStr += " OR GroupType = '" + GroupType[GroupType.length - 1] + "') ";

    db.transaction(function (tx) {
        tx.executeSql(queryStr, [], function (tx, rs) {
            if (rs.rows.length != 0) {
                //console.log(rs);
                for (var i = 0; i < rs.rows.length; i++) {
                    //console.log(rs.rows[i]);
                    if (rs.rows[i].GroupType.match(/01.*/)) {
                        loadPropAssi(tx, rs.rows[i]);
                    }
                    else if (rs.rows[i].GroupType.match(/02.*/)) {
                        loadBodyAssi(tx, rs.rows[i]);
                    }
                    else if (rs.rows[i].GroupType.match(/03.*/)) {
                        loadOtherAssi(tx, rs.rows[i]);
                    }
                    else { }
                }
            }
        }, errorCB);
    }, errorCB, successCB);
}
function loadPropAssi(tx, rs) {
    //console.log(rs);
    var selectID = "", selectText, valueTxt = rs.Value, noteTxt = rs.Note;
    if (rs.GroupType == '011') {
        selectID = '001';
        selectText = 'ศูนย์เยียวยาจังหวัด';
    }
    else if (rs.GroupType == '012') {
        selectID = '002';
        selectText = 'สำนักงานป้องกันและบรรเทาสาธารณภัย';
    }
    else if (rs.GroupType == '013') {
        selectID = '003';
        selectText = 'สำนักงานพัฒนาสังคมและความมั่นคงของมนุษย์';
    }
    else if (rs.GroupType == '014') {
        selectID = '004';
        selectText = 'กระทรวงศึกษาธิการ';
    }
    else if (rs.GroupType == '015') {
        selectID = '005';
        selectText = 'กระทรวงสาธารณสุข/กรมสุขภาพจิต';
    }
    else if (rs.GroupType == '016') {
        selectID = '006';
        selectText = 'กระทรวงยุติธรรม';
    }
    else if (rs.GroupType == '017') {
        selectID = '007';
        selectText = 'อื่นๆ';
    }
    else { }

    if ($('div#collapsible-' + selectID).length == 0) {
        //console.log(selectID);
        //create collapsible -> create table -> append table to collapsible

        //----- create collapsible -------
        $('#categories').append('<div id="collapsible-' + selectID + '" data-role="collapsible" class="custom-collapsible-z" data-collapsed-icon="carat-r" data-expanded-icon="carat-d" data-collapsed="false"></div>');
        $("#collapsible-" + selectID).append('<h3>' + selectText + '</h3>');
        //--------------------------------

        //------------ create table --------------
        var service_table = $('<table data-role="table" id="table-' + selectID + '" class="ui-body-d ui-shadow table-stripe ui-responsive" data-mode="columntoggle" data-column-btn-text="คอลัมน์..." data-column-btn-theme="z" data-column-popup-theme="a"></table><br />');
        var service_tr_th = $("<thead><tr><th>การช่วยเหลือ</th><th>จำนวนเงิน</th></tr></thead>");
        var service_tbody = $('<tbody></tbody>');
        var service_tr = $('<tr></tr>').bind("click", function () {
            var r = confirm('ต้องการลบใช่หรือไม่');
            if (r == true) {
                $(this)["remove"]()
            }
            else { }
        });
        var service_name_td = $('<td>' + noteTxt + '</td><td>' + valueTxt + '</td>');
        service_name_td.appendTo(service_tr);
        service_tr_th.appendTo(service_table);
        service_tr.appendTo(service_tbody);
        service_tbody.appendTo(service_table);
        //----------------------------------------

        //------- append table to collapsible -------
        service_table.appendTo($("#collapsible-" + selectID));
        service_table.table();

        $('#categories').trigger('create');
        //-------------------------------------------
    }
    else if ($('div#collapsible-' + selectID).length != 0) {
        //console.log("already have");

        var service_name_td = $('<tr><td>' + noteTxt + '</td><td>' + valueTxt + '</td></tr>').bind("click", function () {
            var r = confirm('ต้องการลบใช่หรือไม่');
            if (r == true) {
                $(this)["remove"]()
            }
            else { }
        });
        service_name_td.appendTo($('table#table-' + selectID + ' tbody'));
        $('table#table-' + selectID).table();
    }
    else {
        //alert("กรุณาเลือกหน่วยงาน");
        return false;
    }

}
function loadBodyAssi(tx, rs) {
    var cbName = "", isOther = 0;
    if (rs.GroupType == '021') {
        cbName = 'a'
    }
    else if (rs.GroupType == '022') {
        cbName = 'b'
    }
    else if (rs.GroupType == '023') {
        cbName = 'c'
    }
    else if (rs.GroupType == '024') {
        cbName = 'd'
    }
    else if (rs.GroupType == '025') {
        isOther = 1;
    }
    else if (rs.GroupType == '026') {
        cbName = 'e'
    }
    else if (rs.GroupType == '027') {
        cbName = 'f'
    }
    else if (rs.GroupType == '028') {
        isOther = 1;
    }
    else { }

    if (isOther == 0) {
        var assistTBLength = $("input[name='checkbox-body-" + cbName + "']").length;
        for (var i = 0; i < assistTBLength; i++) {
            if ($("input[name='checkbox-body-" + cbName + "']")[i].value == rs.Value) {
                var checkID = i + 1;
                $('#checkbox-body-' + checkID + '-' + cbName).attr('checked', true);
                $('#bodyAssist fieldset[data-role="controlgroup"]').controlgroup('refresh');
                $('#studentAssist fieldset[data-role="controlgroup"]').controlgroup('refresh');
                i = assistTBLength - 1;
            }
            else { }
        }
    }
    else {
        var $el = $('<li data-icon="delete"><a>' + rs.Note + " จำนวน: " + rs.Value + ' บาท</a></li>').bind("click", action = function () {
            var r = confirm('ต้องการลบใช่หรือไม่');
            if (r == true) {
                $el["remove"]();
                //$("#studentAssistList").listview("refresh");
            }
            else { }
        });

        if (rs.GroupType == '025') {
            $('#bodyAssistOtherList').append($el).listview("refresh");
        }
        else if (rs.GroupType == '028') {
            $('#studentAssistList').append($el).listview("refresh");

        }
        else { }
    }


}
function loadOtherAssi(tx, rs) {
    var assistTBLength = $('input[name=checkbox-g]').length;
    for (var i = 0; i < assistTBLength; i++) {
        if ($('input[name=checkbox-g]')[i].value == rs.Value && (rs.Value != '999')) {
            var checkID = i + 1;

            $('#checkbox-' + checkID + '-g').attr('checked', true);
            $('#ctrlGroupAssist2').controlgroup('refresh');
            i = assistTBLength - 1;
        }
        else if ((rs.Value == '999') && (i == assistTBLength - 1)) {
            //console.log(rs.Value + " // " + rs.Note);
            var checkID = assistTBLength + 1;
            var temp = "<input type='checkbox' name='checkbox-g' id='checkbox-" + checkID + "-g' value='999' checked='checked'><label id='lb-checkbox-" + checkID + "-g' for='checkbox-" + checkID + "-g'>" + rs.Note + "</label>";
            $('#ctrlGroupAssist2').controlgroup("container").append(temp);
            $('#ctrlGroupAssist2').enhanceWithin().controlgroup("refresh");
        }
        else { }
    }

}
//------------------------------


function checkDropdownValue(type, value) {
    var response;
    //console.log("value = " + value);
    for (var i = 0; i < document.getElementById(type).options.length ; i++) {
        //console.log(document.getElementById(type).options.item(i).value + " / " + value);
        //console.log(document.getElementById(type).options[i].text + " / " + value);

        if (document.getElementById(type).options.item(i).text == value) {
            response = document.getElementById(type).options.item(i).value;
            document.getElementById("AccidTypeNote").style.display = "none";
            return response;
        }
        else if (document.getElementById(type).options.item(i).text != value && i == document.getElementById(type).options.length - 1) {
            response = "999";
            $('.NoteText').each(function () {
                document.getElementById($(this).attr('id')).style.display = "inline";
            });
        }
        else {
            //console.log("else");
        }

    }
    return response;
}

function insertDropdown(page) {

    $('#' + page + ' select').empty();
    //$('#' + page + ' select').append($("<option></option>").val("0").html("เลือก"));

    if (page == "accident") {

        document.getElementById('AccidDate').valueAsDate = new Date();

        var msg = getAnswer("AccidType");
        for (var i = 0; i < msg.length ; i++) {
            $('#AccidType').append($("<option></option>").val(msg[i].AnswerID).html(msg[i].AnswerDesc));
        }
        //$('#' + page + ' select').selectmenu('refresh');
    }

    var queryProvince = "SELECT ProvinceID, ProvinceDescription FROM Province ORDER BY ProvinceDescription";

    db.transaction(function (tx) {

        tx.executeSql(queryProvince, [], (function (tx, response) {
            for (var i = 0; i < response.rows.length ; i++) {
                $('#Province').append($("<option></option>").val(response.rows.item(i).ProvinceID).html(response.rows.item(i).ProvinceDescription));
            }
        }), errorCB);

    }, errorCB, function () {
        console.log('insert dropdown');
        //$('select').selectmenu().selectmenu('refresh', true);
        $('#' + page + ' select').selectmenu('refresh');
    });

}

function insertCity(provinceID, dropdownID) {

    var queryGetCityByProvince = "SELECT C.CityID, C.CityDescription "
        + "FROM City C INNER JOIN Province P ON C.ProvinceID = P.ProvinceID "
        + "WHERE P.ProvinceID = '" + provinceID + "' ";

    db.transaction(function (tx) {
        tx.executeSql(queryGetCityByProvince, [], (function (tx, response) {

            if (response.rows.length != 0) {
                for (var i = 0; i < response.rows.length ; i++) {
                    $('#' + dropdownID + '').append($("<option></option>").val(response.rows[i].CityID).html(response.rows[i].CityDescription));
                }
            }

        }), errorCB);
    }, errorCB, function () {
        $('#' + dropdownID + '').selectmenu("refresh");
    });
}
function insertTumbon(cityID, dropdownID) {

    var queryGetCityByProvince = "SELECT T.TumbonID, T.TumbonDescription "
        + "FROM Tumbon T INNER JOIN City C ON T.CityID = C.CityID "
        + "WHERE C.CityID = '" + cityID + "' ";

    db.transaction(function (tx) {
        tx.executeSql(queryGetCityByProvince, [], (function (tx, response) {

            if (response.rows.length != 0) {
                for (var i = 0; i < response.rows.length ; i++) {

                    $('#' + dropdownID + '').append($("<option></option>").val(response.rows[i].TumbonID).html(response.rows[i].TumbonDescription));
                }
            }

        }), errorCB);
    }, errorCB, function () {
        $('#' + dropdownID + '').selectmenu("refresh");
    });

}
function autoLoadCityAndTumbon(page, tumbonTB, cityTB, tumbonVal, cityVal, provinceVal) {

    db.transaction(function (tx) {
        tx.executeSql("SELECT CityID, CityDescription FROM City WHERE ProvinceID = '" + provinceVal + "' ", [],
            function (tx, result) {
                if (result.rows.length != 0) {
                    for (var i = 0; i < result.rows.length ; i++) {
                        $('#' + cityTB).append($("<option></option>").val(result.rows[i].CityID).html(result.rows[i].CityDescription));
                    }
                }
            }, errorCB);
    }, errorCB, function () {
        $('#' + cityTB).val(cityVal);

        db.transaction(function (tx) {
            tx.executeSql("SELECT TumbonID, TumbonDescription FROM Tumbon WHERE CityID = '" + cityVal + "' ", [],
                function (tx, result) {
                    if (result.rows.length != 0) {
                        for (var i = 0; i < result.rows.length ; i++) {
                            $('#' + tumbonTB).append($("<option></option>").val(result.rows[i].TumbonID).html(result.rows[i].TumbonDescription));
                        }
                    }
                }, errorCB);
        }, errorCB, function () {
            //console.log(page);
            $('#' + tumbonTB).val(tumbonVal);
            $('#' + page + ' select').selectmenu('refresh');
        });
    });
}

function hideNoteText() {
    $('.NoteText').each(function () {
        document.getElementById($(this).attr('id')).style.display = "none";
    });
}

function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

function replaceNull(pageID) {
    $('#' + pageID + ' input[type=text]').each(function () {
        if ($(this).val() == "") {
            $(this).val("-");
        }
        else {
            //inputNotNull = true;            
        }
    });

    $('#' + pageID + ' input[type=tel], #' + pageID + ' input[type=number]').each(function () {
        if ($(this).val() == "") {
            $(this).val("0");
        }
        else {
            //inputNotNull = true;            
        }
    });

    $('#' + pageID + ' select').each(function () {

        if ($(this).val() == "" || $(this).val() == null || $(this).val() == "null") {
            $(this).empty();
            $(this).append($("<option></option>").val("0000").html('ไม่ระบุ')).selectmenu('refresh');
            //console.log($(this).val());
        }
        else {
            //inputNotNull = true;            
        }
    });
}

function maxLengthPostCode(object) {
    if (object.value.length > 5)
        object.value = object.value.slice(0, 5)
}

function lvCreate(page) {

    var selectID = [];

    $('#' + page + ' .add-filter').each(function () {
        //console.log($(this).attr('id'));
        selectID.push($(this).attr('id'));
    });

    for (var i = 0; i < selectID.length; i++) {
        addFilterToSelect(selectID[i]);
    }

}

function addFilterToSelect(selectID) {
    $.mobile.document
        .on("listviewcreate", "#" + selectID + "-menu", function (e) {
            var input,
                listbox = $("#" + selectID + "-listbox"),
                form = listbox.jqmData("filter-form"),
                listview = $(e.target);
            if (!form) {
                input = $("<input data-type='search'></input>");
                form = $("<form></form>").append(input);
                input.textinput();
                $("#" + selectID + "-listbox")
                    .prepend(form)
                    .jqmData("filter-form", form);
            }
            listview.filterable({ input: input });
        })
        .on("pagebeforeshow pagehide", "#" + selectID + "-dialog", function (e) {
            var form = $("#" + selectID + "-listbox").jqmData("filter-form"),
                placeInDialog = (e.type === "pagebeforeshow"),
                destination = placeInDialog ? $(e.target).find(".ui-content") : $("#" + selectID + "-listbox");
            form
                .find("input")
                .textinput("option", "inset", !placeInDialog)
                .end()
                .prependTo(destination);
        });
}

/*
$(document).on("pagebeforecreate", "#accident", function (event) {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);
    $("#accidentHead").empty();

    if (sessionStorage.getItem('PersonCID') !== null) {
        console.log('PersonCID not null');
        $('#accidentHead').append('ข้อมูลเหตุการณ์');

        db.transaction(getDistinctQTID, errorCB, function () {
            //console.log('success getQTID');
            $('#maxQ').empty(); $('#maxQ').append(QTID.length + "");
            $('#currentQ').empty(); $('#currentQ').append(1 + "");
            $("#slider-1").attr("max", QTID.length);
            queryShowQuestion(QTID[0]);
        });        

    }
    else {
        console.log('else');        
    }

});

$(document).on("pagecreate", "#accident", function () {    
    var currentQ = 0;
    
    //$('#questionText').empty(); $('#questionText').append(question[currentQ-1]["QDesc"]);

    $('#nextBtn').click(function () {
        if (currentQ < QTID.length-1) { currentQ++; }
        //console.log(currentQ + "");
        $("#slider-1").val(currentQ + 1 + "").slider('refresh');
        queryShowQuestion(QTID[currentQ]);
        //$('#questionText').empty(); $('#questionText').append(question[$('#slider-1').val() - 1]["QDesc"]);
    });

    $('#backBtn').click(function () {
        if (currentQ > 0) { currentQ--; }
        //console.log(currentQ + "");
        $("#slider-1").val(currentQ + 1 + "").slider('refresh');
        queryShowQuestion(QTID[currentQ]);
        //$('#questionText').empty(); $('#questionText').append(question[$('#slider-1').val() - 1]["QDesc"]);
    });
    

});

function getDistinctQTID(tx) {
    tx.executeSql("SELECT QTID FROM QuestionType ", [], function (tx, response) {
        if (response.rows.length != 0) {
            QTID = [];
            //console.log(response);
            for (var i = 0; i < response.rows.length; i++) {
                QTID.push(response.rows[i].QTID);
            }            
        }
    }, errorCB);
}

function queryShowQuestion(QTID) {
    //DROP TABLE IF EXISTS Question
    //"SELECT * FROM Questionaire INNER JOIN Answer ON (Questionaire.QID = Answer.QID AND Questionaire.QTID = Answer.QTID) WHERE Questionaire.QTID = '" + sectionQTID[currentQTID] + "' AND Questionaire.QID = '" + sectionQID[currentQID] + "'"
    var queryQuestion = "SELECT * "
        + "FROM Question Q "
        + "WHERE QTID = '" + QTID + "' ";

    db.transaction(function (tx) {
        tx.executeSql(queryQuestion, [], showQuestion, errorCB);
    }, errorCB, successCB);
}

function showQuestion(tx, response) {
    $("#questionForm").empty();
    
    if (response.rows.length != 0) {

        //var tempAppend = "<div class='ui-controlgroup-controls'>";
        var tempAppend = "<div class='ui-field-contain'>";

        for (var i = 0; i < response.rows.length; i++) {
            //tempAppend += "<p>" + response.rows[i].QDesc + " :</p>";
            if (response.rows[i].UIControl == "10020") {
                tempAppend += "<div class='ui-field-contain'>";
                tempAppend += "<label for='" + response.rows[i].QID + "'>" + response.rows[i].QDesc + " : </label><input type='date' name='date-01' id='" + response.rows[i].QID + "'>";
                tempAppend += "</div>";
            }
            else if (response.rows[i].UIControl == "10010") {
                tempAppend += "<div class='ui-field-contain'>";
                tempAppend += "<label for='" + response.rows[i].QID + "'>" + response.rows[i].QDesc + " : </label><input type='text' name='text-01' id='" + response.rows[i].QID + "'>";
                tempAppend += "</div>";
            }
            else if (response.rows[i].UIControl == "20020") {
                tempAppend += "<div class='ui-field-contain'>";
                tempAppend += "<label for='" + response.rows[i].QID + "'>" + response.rows[i].QDesc + " : </label><input type='text' name='text-01' id='" + response.rows[i].QID + "'>";
                tempAppend += "</div>";
            }
            else {
                tempAppend += "<div class='ui-field-contain'>";
                tempAppend += "<label for='" + response.rows[i].QID + "'>" + response.rows[i].QDesc + " : </label><input type='text' name='text-01' id='" + response.rows[i].QID + "'>";
                tempAppend += "</div>";
            }
        }

        tempAppend += "</div>";
        $("#questionForm").append(tempAppend);


        $("#questionForm").trigger("create");

    }
    else {

    }
}
*/