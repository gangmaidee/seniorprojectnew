﻿//startAjaxLoader(); isOnlineMode = true; createDatabase();
var db;

var Address;
var isOnlineMode;
var isAccess;

function populateDB(tx) {

    tx.executeSql("CREATE TABLE IF NOT EXISTS Person ("
        + " PID VARCHAR(13) PRIMARY KEY, Title VARCHAR(4), FirstName VARCHAR(50), LastName VARCHAR(50), Phone VARCHAR(20), Passport VARCHAR(50),"
        + " DOB DATE, Nation VARCHAR(50), Race VARCHAR(50), Religion VARCHAR(10), MarriageStatus VARCHAR(10),"
        + " AddressSatisfy VARCHAR(4), BirthProvince VARCHAR(4), RecordStaffPID VARCHAR(13), DateTimeUpdate DATETIME, PicPath VARCHAR(255)"
        + " )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS Staff ("
        + " StaffID VARCHAR(13) PRIMARY KEY, Password VARCHAR(50),Title VARCHAR(4), FirstName VARCHAR(50), LastName VARCHAR(50),"
        + " Phone VARCHAR(20) ,OccupationID VARCHAR(4), Position VARCHAR(50), Office VARCHAR(255),"
        + " Tumbon VARCHAR(50), Amphor VARCHAR(50), Province VARCHAR(4)"
        + " )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS Address ("
        + " AddressID VARCHAR(50) PRIMARY KEY, LiveHomeStatusID VARCHAR(4), HouseNumber VARCHAR(5),"
        + " MooNumber VARCHAR(5), VillageName VARCHAR(255), Alley VARCHAR(255), StreetName VARCHAR(255),"
        + " Tumbon VARCHAR(50), Amphor VARCHAR(50), Province VARCHAR(4), Postcode VARCHAR(5), HomeCode VARCHAR(20),"
        + " addrLat float, addrLng float"
        + " )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonVSAddress ( PID VARCHAR(13) PRIMARY KEY, AddressID VARCHAR(50) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonOccupation ( PID VARCHAR(13) PRIMARY KEY, OccupationID VARCHAR(4),"
        + " Position varchar(50), Office varchar(255), Tumbon VARCHAR(50), Amphor VARCHAR(50), Province VARCHAR(4), Postcode VARCHAR(5), "
        + " Phone VARCHAR(20), Fax VARCHAR(20)"
        + " )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonFinance ( PID VARCHAR(13) PRIMARY KEY, Income INT, Outgoing INT, InDebt int, ExDebt int )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonBankAccount ( PID VARCHAR(13) PRIMARY KEY, BankName varchar(50), BankBranch varchar(50), AccountNo varchar(50), AccountName varchar(50) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonEducation ( PID VARCHAR(13) PRIMARY KEY, EduStatus varchar(50), EduLevel varchar(10), EduYear varchar(50), Academy varchar(50), "
        + " Tumbon VARCHAR(50), Amphor VARCHAR(50), Province VARCHAR(4) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonRecordBy ( PID VARCHAR(13) PRIMARY KEY, RecordPID VARCHAR(13), Relation varchar(4) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonStandIn ( PID VARCHAR(13) PRIMARY KEY, StandIn VARCHAR(13), StandInRole varchar(4) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonFamily ( FamilyID varchar(50), FamilyRole varchar(4), PID VARCHAR(13) PRIMARY KEY )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonMate ( PID VARCHAR(13), MatePID VARCHAR(13) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonParent ( PID VARCHAR(13) PRIMARY KEY, ParentPID VARCHAR(13), ParentRole varchar(4) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonEmergencyContact ( PID VARCHAR(13) PRIMARY KEY, EmergencyContactPID VARCHAR(13), EmergencyContactRole varchar(4) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS PersonAccident "
        + "( AccidID VARCHAR(50) PRIMARY KEY, AccidDate DATE, AccidDesc VARCHAR(255), AccidTeller VARCHAR(50), AccidType VARCHAR(50), "
        + "AccidHouseNo VARCHAR(5), AccidMoo VARCHAR(5), AccidVillage VARCHAR(255), AccidAlley VARCHAR(255), AccidStreet VARCHAR(255), "
        + "AccidTumbon VARCHAR(50), AccidCity VARCHAR(50), AccidProvince VARCHAR(4), AccidPostCode VARCHAR(5), PoliceStation VARCHAR(50), "
        + "AccidLat float, AccidLng float, PID VARCHAR(13) ) ");

    tx.executeSql("CREATE TABLE IF NOT EXISTS AccidentDamageAndAssist "
        + "( AccidID VARCHAR(50), Value VARCHAR(50), Note VARCHAR(255), GroupType VARCHAR(5) )");

    tx.executeSql("CREATE TABLE IF NOT EXISTS Choice ( ChoiceID VARCHAR(5), ChoiceDesc VARCHAR(255), GroupTypeID VARCHAR(5) )", null, function () {
        insertTable('Choice');
    }, function (tx, error) {
        alert('Create Choice table error: ' + error.message);
    });

    tx.executeSql("CREATE TABLE IF NOT EXISTS ChoiceGroupType ( GroupTypeID VARCHAR(5), GroupTypeDesc VARCHAR(255) )", null, function () {
        insertTable('ChoiceGroupType');
    }, function (tx, error) {
        alert('Create ChoiceGroupType table error: ' + error.message);
    });

    //---------------- DROP DOWN -------------------

    tx.executeSql("CREATE TABLE IF NOT EXISTS Title ("
        + " TitleID VARCHAR(4) PRIMARY KEY, TitleDESC VARCHAR(50)"
        + " )", null, function () {
            //console.log('Create Title table success');
            insertTable('Title');
        }, function (tx, error) {
            alert('Create Title table error: ' + error.message);
        });

    tx.executeSql("CREATE TABLE IF NOT EXISTS Province ("
        + " ProvinceID VARCHAR(2) PRIMARY KEY, ProvinceDescription VARCHAR(150), Latitude float(53), Longtitude float(53)"
        + " )", null, function () {
            //console.log('Create Title table success');
            insertTable('Province');
        }, function (tx, error) {
            alert('Create Province table error: ' + error.message);
        });

    tx.executeSql("CREATE TABLE IF NOT EXISTS MariageStatus ( MariageStatusID VARCHAR(10) PRIMARY KEY, MariageStatusDescription VARCHAR(255) )", null, function () {
        //console.log('Create Title table success');
        insertTable('MariageStatus');
    }, function (tx, error) {
        alert('Create MariageStatus table error: ' + error.message);
    });

    tx.executeSql("CREATE TABLE IF NOT EXISTS Nationality ( NationalityID VARCHAR(50) PRIMARY KEY, NationalityDescription VARCHAR(255), Zone VARCHAR(50) )", null, function () {
        //console.log('Create Title table success');
        insertTable('Nationality');
    }, function (tx, error) {
        alert('Create Nationality table error: ' + error.message);
    });

    tx.executeSql("CREATE TABLE IF NOT EXISTS Race ( RaceID VARCHAR(50) PRIMARY KEY, RaceDescription VARCHAR(255), Zone VARCHAR(50) )", null, function () {
        //console.log('Create Title table success');
        insertTable('Race');
    }, function (tx, error) {
        alert('Create Race table error: ' + error.message);
    });

    tx.executeSql("CREATE TABLE IF NOT EXISTS Religion ( Id VARCHAR(10) PRIMARY KEY, Religion VARCHAR(50) )", null, function () {
        //console.log('Create Title table success');
        insertTable('Religion');
    }, function (tx, error) {
        alert('Create Religion table error: ' + error.message);
    });

    tx.executeSql("CREATE TABLE IF NOT EXISTS Relation ( RelationID VARCHAR(4) PRIMARY KEY, RelationDescription VARCHAR(50) )", null, function () {
        //console.log('Create Title table success');
        insertTable('Relation');
    }, function (tx, error) {
        alert('Create Relation table error: ' + error.message);
    });

    tx.executeSql("CREATE TABLE IF NOT EXISTS Occupation ( OccupationID VARCHAR(4) PRIMARY KEY, OccupationDescription VARCHAR(255) )", null, function () {
        //console.log('Create Title table success');
        insertTable('Occupation');
    }, function (tx, error) {
        alert('Create Occupation table error: ' + error.message);
    });

    tx.executeSql("CREATE TABLE IF NOT EXISTS LiveHomeStatus ( LiveHomeStatusID VARCHAR(4) PRIMARY KEY, LiveHomeStatusDescription VARCHAR(50) )", null, function () {
        //console.log('Create Title table success');
        insertTable('LiveHomeStatus');
    }, function (tx, error) {
        alert('Create LiveHomeStatus table error: ' + error.message);
    });

    tx.executeSql("CREATE TABLE IF NOT EXISTS EducationLevel ( EducationLevelID VARCHAR(10) PRIMARY KEY, EducationLevelDescription VARCHAR(255) )", null, function () {
        //console.log('Create Title table success');
        insertTable('EducationLevel');
    }, function (tx, error) {
        alert('Create EducationLevel table error: ' + error.message);
    });

    tx.executeSql("CREATE TABLE IF NOT EXISTS Bank ( BankID VARCHAR(4) PRIMARY KEY, BankIDDesc VARCHAR(50), Swift VARCHAR(5) )", null, function () {
        //console.log('Create Title table success');
        insertTable('Bank');
    }, function (tx, error) {
        alert('Create Bank table error: ' + error.message);
    });

    tx.executeSql("CREATE TABLE IF NOT EXISTS Tumbon (TumbonID varchar(6) PRIMARY KEY, TumbonDescription varchar(150), CityID varchar(4), Latitude float(53), Longtitude float(53) )", null, function () {
        //console.log('Create Title table success');
        insertTable('Tumbon');
    }, function (tx, error) {
        alert('Create Tumbon table error: ' + error.message);
    });

    tx.executeSql("CREATE TABLE IF NOT EXISTS City (CityID varchar(4) PRIMARY KEY, CityDescription varchar(150), ProvinceID varchar(2), Latitude float(53), Longtitude float(53) )", null, function () {
        //console.log('Create Title table success');
        insertTable('City');
    }, function (tx, error) {
        alert('Create City table error: ' + error.message);
    });

    //insertTable(tx);
    //addListView();
}

function insertTable(Table) {

    db.transaction(function (tx) {
        tx.executeSql('SELECT * FROM "' + Table + '" ', [],
            (function (tx, response) {
                if (response.rows.length == 0) {
                    console.log('insert : ' + Table);

                    var msg = getDropdown(Table);
                    var a = msg["RECORDS"];

                    if (Table == "Bank") {
                        for (var i = 0; i < a.length ; i++) {
                            tx.executeSql("INSERT INTO Bank ( BankID, BankIDDesc, Swift ) VALUES ( "
                            + " '" + a[i]["BankID"] + "', "
                            + " '" + a[i]["BankIDDesc"] + "', "
                            + " '" + a[i]["Swift"] + "' "
                            + " ) ");
                        }
                    }
                    else if (Table == "City") {
                        for (var i = 0; i < a.length ; i++) {
                            tx.executeSql("INSERT INTO City ( CityID, CityDescription, ProvinceID, Latitude, Longtitude ) VALUES ( "
                            + " '" + a[i]["CityID"] + "', "
                            + " '" + a[i]["CityDescription"] + "', "
                            + " '" + a[i]["ProvinceID"] + "', "
                            + " '" + a[i]["Latitude"] + "', "
                            + " '" + a[i]["Longtitude"] + "' "
                            + " ) ");
                        }
                    }
                    else if (Table == "EducationLevel") {
                        for (var i = 0; i < a.length ; i++) {
                            tx.executeSql("INSERT INTO EducationLevel ( EducationLevelID, EducationLevelDescription ) VALUES ( "
                            + " '" + a[i]["EducationLevelID"] + "', "
                            + " '" + a[i]["EducationLevelDescription"] + "' "
                            + " ) ");
                        }
                    }
                    else if (Table == "LiveHomeStatus") {
                        for (var i = 0; i < a.length ; i++) {
                            tx.executeSql("INSERT INTO LiveHomeStatus ( LiveHomeStatusID, LiveHomeStatusDescription ) VALUES ( "
                            + " '" + a[i]["LiveHomeStatusID"] + "', "
                            + " '" + a[i]["LiveHomeStatusDescription"] + "' "
                            + " ) ");
                        }
                    }
                    else if (Table == "MariageStatus") {
                        for (var i = 0; i < a.length ; i++) {
                            tx.executeSql("INSERT INTO MariageStatus ( MariageStatusID, MariageStatusDescription ) VALUES ( "
                            + " '" + a[i]["MariageStatusID"] + "', "
                            + " '" + a[i]["MariageStatusDescription"] + "' "
                            + " ) ");
                        }
                    }
                    else if (Table == "Nationality") {
                        for (var i = 0; i < a.length ; i++) {
                            tx.executeSql("INSERT INTO Nationality ( NationalityID, NationalityDescription ) VALUES ( "
                            + " '" + a[i]["NationalityID"] + "', "
                            + " '" + a[i]["NationalityDescription"] + "' "
                            + " ) ");
                        }
                    }
                    else if (Table == "Occupation") {
                        for (var i = 0; i < a.length ; i++) {
                            tx.executeSql("INSERT INTO Occupation ( OccupationID, OccupationDescription ) VALUES ( "
                            + " '" + a[i]["OccupationID"] + "', "
                            + " '" + a[i]["OccupationDescription"] + "' "
                            + " ) ");
                        }
                    }
                    else if (Table == "Province") {
                        for (var i = 0; i < a.length ; i++) {
                            tx.executeSql("INSERT INTO Province (ProvinceID, ProvinceDescription, Latitude, Longtitude) VALUES ( "
                            + " '" + a[i]['ProvinceID'] + "', "
                            + " '" + a[i]['ProvinceDescription'] + "', "
                            + " '" + a[i]['Latitude'] + "', "
                            + " '" + a[i]['Longtitude'] + "' "
                            + " ) ");
                        }
                    }
                    else if (Table == "Race") {
                        for (var i = 0; i < a.length ; i++) {
                            tx.executeSql("INSERT INTO Race ( RaceID, RaceDescription ) VALUES ( "
                            + " '" + a[i]["RaceID"] + "', "
                            + " '" + a[i]["RaceDescription"] + "' "
                            + " ) ");
                        }
                    }
                    else if (Table == "Relation") {
                        for (var i = 0; i < a.length ; i++) {
                            tx.executeSql("INSERT INTO Relation ( RelationID, RelationDescription ) VALUES ( "
                            + " '" + a[i]["RelationID"] + "', "
                            + " '" + a[i]["RelationThaiDescription"] + "' "
                            + " ) ");
                        }
                    }
                    else if (Table == "Religion") {
                        for (var i = 0; i < a.length ; i++) {
                            tx.executeSql("INSERT INTO Religion ( Id, Religion ) VALUES ( "
                            + " '" + a[i]["Id"] + "', "
                            + " '" + a[i]["Religion"] + "' "
                            + " ) ");
                        }
                    }
                    else if (Table == "Title") {
                        for (var i = 0; i < a.length ; i++) {
                            tx.executeSql("INSERT INTO Title (TitleID, TitleDESC) VALUES ( "
                            + " '" + a[i]['TitleID'] + "', "
                            + " '" + a[i]['TitleDescription'] + "' "
                            + " ) ");
                        }
                    }
                    else if (Table == "Tumbon") {
                        for (var i = 0; i < a.length ; i++) {
                            tx.executeSql("INSERT INTO Tumbon ( TumbonID, TumbonDescription, CityID, Latitude, Longtitude ) VALUES ( "
                            + " '" + a[i]["TumbonID"] + "', "
                            + " '" + a[i]["TumbonDescription"] + "', "
                            + " '" + a[i]["CityID"] + "', "
                            + " '" + a[i]["Latitude"] + "', "
                            + " '" + a[i]["Longtitude"] + "' "
                            + " ) ");
                        }
                    }
                    else if (Table == "Choice") {
                        for (var i = 0; i < a.length ; i++) {
                            tx.executeSql("INSERT INTO Choice ( ChoiceID, ChoiceDesc, GroupTypeID ) VALUES ( "
                            + " '" + a[i]["ChoiceID"] + "', "
                            + " '" + a[i]["ChoiceDesc"] + "', "
                            + " '" + a[i]["GroupTypeID"] + "' "
                            + " ) ");
                        }
                    }
                    else if (Table == "ChoiceGroupType") {
                        for (var i = 0; i < a.length ; i++) {
                            tx.executeSql("INSERT INTO ChoiceGroupType ( GroupTypeID, GroupTypeDesc ) VALUES ( "
                            + " '" + a[i]["GroupTypeID"] + "', "
                            + " '" + a[i]["GroupTypeDesc"] + "' "
                            + " ) ");
                        }
                    }
                    else {
                        console.log('Not found : ' + Table);
                    }



                    //titleService();
                    //provinceService();
                    //mariageService();
                    //nationService();
                    //raceService();
                    //religionService();
                    //relationService();
                    //educationLevelService();
                    //occupationService();
                    //liveHomeStatusService();

                } else {
                    console.log('not insert : ' + Table);
                }
            }));
    }, errorCB);

}

function insertQandA(Table) {

    db.transaction(function (tx) {
        tx.executeSql('SELECT * FROM "' + Table + '" ', [], function (tx, response) {
            if (response.rows.length == 0) {
                console.log('insert : ' + Table);

                var msg = getJsonData(Table);
                var a = msg["RECORDS"];

                if (Table == "Question") {
                    for (var i = 0; i < a.length ; i++) {
                        tx.executeSql("INSERT INTO Question VALUES ( "
                        + " '" + a[i]["QID"] + "', "
                        + " '" + a[i]["QDesc"] + "', "
                        + " '" + a[i]["QTID"] + "', "
                        + " '" + a[i]["UIControl"] + "' "
                        + " ) ");
                    }
                }
                else if (Table == "QuestionType") {
                    for (var i = 0; i < a.length ; i++) {
                        tx.executeSql("INSERT INTO QuestionType VALUES ( "
                        + " '" + a[i]["QTID"] + "', "
                        + " '" + a[i]["QTDesc"] + "' "
                        + " ) ");
                    }
                }
                else if (Table == "QuestionVSAnswer") {
                    for (var i = 0; i < a.length ; i++) {
                        tx.executeSql("INSERT INTO QuestionVSAnswer VALUES ( "
                        + " '" + a[i]["QID"] + "', "
                        + " '" + a[i]["AID"] + "', "
                        + " '" + a[i]["QTID"] + "' "
                        + " ) ");
                    }
                }
                else if (Table == "Answer") {
                    for (var i = 0; i < a.length ; i++) {
                        tx.executeSql("INSERT INTO Answer VALUES ( "
                        + " '" + a[i]["AID"] + "', "
                        + " '" + a[i]["ADesc"] + "', "
                        + " '" + a[i]["QID"] + "', "
                        + " '" + a[i]["QTID"] + "' "
                        + " ) ");
                    }
                }
                else if (Table == "AnswerGroup") {
                    for (var i = 0; i < a.length ; i++) {
                        tx.executeSql("INSERT INTO AnswerGroup VALUES ( "
                        + " '" + a[i]["AnswerGroupID"] + "', "
                        + " '" + a[i]["AnswerGroupDesc"] + "' "
                        + " ) ");
                    }
                }
                else if (Table == "UIControl") {
                    for (var i = 0; i < a.length ; i++) {
                        tx.executeSql("INSERT INTO UIControl VALUES ( "
                        + " '" + a[i]["UIControlID"] + "', "
                        + " '" + a[i]["UIControlDesc"] + "' "
                        + " ) ");
                    }
                }
            } else {
                console.log('not insert : ' + Table);
            }
        });
    });

}

function errorCB(err) {
    alert("Error processing SQL: " + err.code);
}

function successCB() {
    //alert("success!");
}

document.addEventListener("backbutton", function (e) {
    if ($.mobile.activePage.is('#login')) {
        e.preventDefault();
        navigator.app.exitApp();
    }
    else {
        navigator.app.backHistory()
    }
}, false);

$(document).on("pageshow", "#login", function (event) {
    queryCheckingInterNet();
});

$(document).on("pagecreate", "#login", function (event) {
    isOnlineMode = false;
    //document.addEventListener("deviceready", queryCheckingInterNet, false);
    //document.addEventListener("deviceready", onDeviceReady, false);
    sessionStorage.clear();

    var inputNotNull = true;

    $("#loginBtn").click(function (e) {

        isOnlineMode = checkInternet();

        //console.log("http://www.servicessenior.somee.com/Service1.svc/Login?cid=" + $("#loginCID").val() + "&pwd=" + $("#loginPassword").val());

        var inputNotNull = true;

        $('#userbox :input').each(function () {
            if ($(this).val() == "") {
                inputNotNull = false;
            }
            else {
                //inputNotNull = true;
            }
        });

        if (inputNotNull == true) {

            if (isOnlineMode) {
                startAjaxLoader(); $('#login h1').empty().append("กำลังเข้าสู่ระบบ . . .");
                $.ajax({
                    type: "GET",
                    //url: "http://www.servicessenior.somee.com/Service1.svc/Login?cid=" + $("#loginCID").val() + "&pwd=" + $("#loginPassword").val(),
                    url: "http://seniorservice.azurewebsites.net/Service1.svc/Login?cid=" + $("#loginCID").val() + "&pwd=" + $("#loginPassword").val(),
                    success: function (response) {
                        stopAjaxLoader();
                        isAccess = false;
                        var a = JSON.parse(response);
                        if (a != "") {
                            $('#login h1').empty().append("ลงชื่อเข้าสู่ระบบ");
                            isAccess = true;
                            if (isAccess === true) {
                                var queryStr = 'INSERT INTO Staff ( StaffID, Password, Title, FirstName, LastName, Phone, OccupationID, '
                                    + 'Position, Office, Tumbon, Amphor, Province ) '
                                    + 'VALUES ( "' + $('#loginCID').val() + '", "' + a[0]["Password"] + '", "' + a[0]["Title"] + '", '
                                    + '"' + a[0]["FirstName"] + '", "' + a[0]["LastName"] + '", "' + a[0]["Phone"] + '", '
                                    + '"' + a[0]["OccupationID"] + '", "' + a[0]["Position"] + '", "' + a[0]["Office"] + '", '
                                    + '"' + a[0]["Tumbon"] + '", "' + a[0]["Amphor"] + '", "' + a[0]["Province"] + '" )';
                                //console.log(queryStr);
                                db.transaction(function (tx) {
                                    tx.executeSql(queryStr);
                                }, errorCB,
                                    function () {
                                        localStorage.setItem("staffID", $('#loginCID').val());
                                        sessionStorage.setItem('CID', localStorage.getItem("staffID"));
                                        sessionStorage.setItem('isFirstLogin', "1");
                                        window.location.href = 'home.html';
                                        //loadPerson($('#loginCID').val());
                                        //$.mobile.changePage("#home", { transition: "slide" });
                                    }
                                );
                            }
                        } else {
                            stopAjaxLoader();
                            $('#login h1').empty().append("ลงชื่อเข้าสู่ระบบ");
                            alert("username หรือ passwod ไม่ถูกต้อง");
                        }
                    },
                    error: function (request, status, error) {
                        stopAjaxLoader();
                        alert("เข้าสู่ระบบไม่สำเร็จ กรุณาลองใหม่อีกครั้ง\n" + error);
                        console.log(error);
                        $('#login h1').empty().append("ลงชื่อเข้าสู่ระบบ");
                    }
                });

            }
            else {
                //Locking
                alert("โปรดต่อ Internet เพื่อ Login");
            }

        } else {
            alert("กรุณากรอกใส่ username และ password");
            e.preventDefault();
        }

    });

    $('#goRegisterBtn').click(function () {
        isOnlineMode = checkInternet();
        if (isOnlineMode) {
            $(":mobile-pagecontainer").pagecontainer("change", "#register", { transition: "flip" });
        }
        else {
            //Locking
            alert("โปรดต่อ Internet เพื่อ Register");
        }
    });

    $('#checkOnline').click(function () {
        if (navigator.onLine) {
            isOnlineMode = true;
            alert(isOnlineMode);
        }
        else {
            isOnlineMode = false;
            alert(isOnlineMode);

        }
    });

});

function queryCheckingInterNet() {
    startAjaxLoader();
    //alert("deviceready");
    //if(navigator.connection.type==0)
    //{
    //	isOnlineMode = false;
    //}
    //else if(navigator.connection.type=='none')
    //{
    //	isOnlineMode = false;
    //}
    //else
    //{
    //	isOnlineMode = true;
    //}

    //document.addEventListener("backbutton", function (e) {
    //    if ($.mobile.activePage.is('home.html#home')) {
    //        e.preventDefault();
    //        //navigator.app.exitApp();
    //    }
    //    else {
    //        navigator.app.backHistory()
    //    }
    //}, false);

    if (navigator.onLine) {
        isOnlineMode = true;
    }
    else {
        isOnlineMode = false;
    }

    createDatabase();

}

function createDatabase() {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);
    db.transaction(function (tx) {
        //Create Table
        //tx.executeSql("CREATE TABLE IF NOT EXISTS Staff ("
        //+ " StaffID VARCHAR(13) PRIMARY KEY, Password VARCHAR(50),Title VARCHAR(4), FirstName VARCHAR(50), LastName VARCHAR(50),"
        //+ " Phone VARCHAR(20) ,OccupationID VARCHAR(4), Position VARCHAR(50)"
        //+ " )");
        populateDB(tx);
    }, errorCB,
       function (tx) {
           //isOnlineMode = true;
           //insertTable(tx);
           db.transaction(checkingQueryStaff, errorCB);
       });
}

function checkingQueryStaff(tx) {
    tx.executeSql("SELECT * FROM Staff", [],
        function (tx, results) {
            //Login with current user
            //isLock = false;
            if (results.rows.length == 1) {
                //Success Function Query Person
                isAccess = true;
                //localStorage.setItem("hostID", results.rows[0].HostID);
                localStorage.setItem("staffID", results.rows[0].StaffID);
                sessionStorage.setItem('CID', localStorage.getItem("staffID"));
                //$.mobile.changePage("#home", { transition: "slide" });
                window.location.href = 'home.html';
            }
            else {
                if (isOnlineMode === false) {
                    isAccess = false;
                    alert("โปรดต่อ Internet เพื่อ Login");
                    //isLock = true;
                }
            }
        }
        , errorCB);
    stopAjaxLoader();
}

/*
var recordReponse;
function loadPerson(StaffPID) {
    console.log(StaffPID); $('#login h1').empty().append("โหลดข้อมูลผู้รับบริการ . . .");
    $.ajax({
        type: "GET",
        url: "http://seniorservice.azurewebsites.net/Service1.svc/GetPerson?staffID=" + StaffPID,
        success: function (response) {
            isAccess = false;
            //var a = JSON.parse(response);            
            recordReponse = JSON.parse(response);
            db.transaction(insertRecordData, errorCB);
        },
        error: function (request, status, error) {
            stopAjaxLoader();
            console.log(error);
            $('#login h1').empty().append("ลงชื่อเข้าสู่ระบบ");
            window.location.href = 'home.html';
        }
    });
}

function insertRecordData(tx) {
    //console.log(recordReponse);
    var tableName;
    var tableData;
    var tableColumnName = [];
    var tableValueArray = [];
    var queryStr = "";

    for (var name in recordReponse) {
        if (recordReponse.hasOwnProperty(name)) {
            //console.log("name = " + name);

            switch (name) {
                case "PBank":
                    tableName = "PersonBankAccount";
                    break;
                case "PEdu":
                    tableName = "PersonEducation";
                    break;
                case "PData":
                    tableName = "Person";
                    break;
                case "PEmer":
                    tableName = "PersonEmergencyContact";
                    break;
                case "POcc":
                    tableName = "PersonOccupation";
                    break;

                case "PFin":
                    tableName = "PersonFinance";
                    break;
                case "PPar":
                    tableName = "PersonParent";
                    break;
                case "PRec":
                    tableName = "PersonRecordBy";
                    break;
                case "PStan":
                    tableName = "PersonStandIn";
                    break;
                case "PMate":
                    tableName = "PersonMate";
                    break;

                case "PFam":
                    tableName = "PersonFamily";
                    break;
                case "PVA":
                    tableName = "PersonVSAddress";
                    break;
                case "Addr":
                    tableName = "Address";
                    break;
                case "Accid":
                    tableName = "PersonAccident";
                    break;
                case "ADA":
                    tableName = "AccidentDamageAndAssist";
                    break;
            }
            if (recordReponse[name].length != 0) {
                //console.log(tableName);
                //console.log(recordReponse[name]);
                //console.log(recordReponse[name].length);                
                if (recordReponse[name].length != 0) {
                    //queryStr = "INSERT INTO " + tableName + " VALUES (" + + ")";
                    for (var i = 0; i < recordReponse[name].length; i++) {
                        //console.log(recordReponse[name][i]);
                        tableColumnName = [];
                        tableValueArray = [];
                        for (var name2 in recordReponse[name][i]) {
                            if (recordReponse[name][i].hasOwnProperty(name2)) {
                                //console.log("name = " + name2 + " //value = " + recordReponse[name][i][name2]);
                                tableColumnName.push(name2);
                                tableValueArray.push(recordReponse[name][i][name2]);
                            }
                        }
                        //console.log(tableColumnName);
                        //console.log(tableValueArray);
                        queryStr = "INSERT INTO " + tableName + " (" + tableColumnName[0];
                        for (var j = 1; j < tableColumnName.length - 1; j++) {
                            queryStr += ", " + tableColumnName[j];
                        }
                        queryStr += ", " + tableColumnName[tableColumnName.length - 1] + ") VALUES ('" + tableValueArray[0] + "'";
                        for (var j = 1; j < tableValueArray.length - 1; j++) {
                            queryStr += ", '" + tableValueArray[j] + "'";
                        }
                        queryStr += ", '" + tableValueArray[tableValueArray.length - 1] + "')";
                        //console.log(queryStr);

                        tx.executeSql(queryStr, null, function () {
                            stopAjaxLoader();
                            $('#login h1').empty().append("ลงชื่อเข้าสู่ระบบ");
                            window.location.href = 'home.html';
                        }, function (err) {
                            console.log("error on Table: " + tableName + " code:" + err.code);
                        });
                    }
                }
            }
            else { }
        }
        else {
            console.log(name); // toString or something else
        }
    }

    //var len = Object.keys(recordReponse).length;
    //for (var i = 0; i < len; i++) {
    //    recordReponse.
    //}

}
*/

//ยังไม่ใช้
function onDeviceReady() {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);
    db.transaction(populateDB, errorCB, function (tx) {
        insertTable(tx);
    });
}

//ยังไม่ใช้
function login(username, password) {
    var queryStr = "SELECT StaffID, Password FROM Staff WHERE StaffID = '" + username + "' ";
    db.transaction(function (tx) {
        tx.executeSql(queryStr, [], (function (tx, response) {
            if ((response.rows.length != 0) && (password == response.rows.item(0).Password)) {
                //alert("สำเร็จ");
                return true;
            } else {
                //alert("username หรือ password ไม่ถูกต้อง");                
                return false;
            }
        }), errorCB, successCB);
    });
}

function comparePassword(password, confirmPassword) {
    if (password == confirmPassword) {
        return true;
    } else {
        return false;
    }
}

/*
function register() {


    var staff = {
        CID: $('#staIDNumber').val(),
        Password: $('#staPassword').val(),
        Title: $('#staTitle').val(),
        FName: $('#staFirstName').val(),
        LName: $('#staLastName').val(),

        Tel: $('#staPhone').val(),
        Job: $('#staJob').val(),
        Position: $('#staPosition').val(),
        Address: $('#staAddress').val(),
        Province: $('#staProvince').val(),

        Amphur: $('#staAmphor').val(),
        Tumbon: $('#staTumbon').val()
    };

    

    
    ////http://www.servicessenior.somee.com/Service1.svc/InsertStaff?StaffID=9876&Password=1234&Title=Mr&FirstName=Nine&LastName=Mahoy&Phone=0816844&OccupationID=01&Position=eat
    //var urlStr = "http://www.servicessenior.somee.com/Service1.svc/InsertStaff?StaffID=" + $('#staIDNumber').val()
    //    + "&Password=" + $('#staPassword').val() + "&Title=" + $('#staTitle').val() + "&FirstName=" + $('#staFirstName').val()
    //    + "&LastName=" + $('#staLastName').val() + "&Phone=" + $('#staPhone').val() + "&OccupationID=" + $('#staJob').val()
    //    + "&Position=" + $('#staPosition').val() + " ";
    //console.log(urlStr);
    //startAjaxLoader();
    //$.ajax({
    //    type: "GET",
    //    url: urlStr,
    //    success: function () {
    //        stopAjaxLoader();
    //        $(":mobile-pagecontainer").pagecontainer("change", "#login", { transition: "flip", changeHash: false });
    //    }
    //});   
    
        
    var queryStrSta = "SELECT StaffID FROM Staff WHERE StaffID = '" + staff.CID + "' ";
    db.transaction(function (tx) {
        tx.executeSql(queryStrSta, [], function (tx, response) {

            if (response.rows.length == 0) {

                console.log("ยังไม่มีข้อมูลเจ้าหน้าที่คนนี้");

                var queryInsertStaff = 'INSERT INTO Staff (StaffID, Password, Title, FirstName, LastName, Phone, OccupationID, Position, Office, Tumbon, Amphor, Province) '
                        + 'VALUES ( ' + staff.CID + ', "' + staff.Password + '", "' + staff.Title + '", "' + staff.FName + '", '
                        + ' "' + staff.LName + '", "' + staff.Tel + '", "' + staff.Job + '", "' + staff.Position + '", "' + staff.Address + '", '
                        + ' "' + staff.Tumbon + '", "' + staff.Amphur + '", "' + staff.Province + '" )';
                console.log(queryInsertStaff);

                //กรอกข้อมูลเจ้าหน้าที่
                db.transaction(function (tx) {
                    tx.executeSql(queryInsertStaff);
                }, errorCB, function () {
                    alert("สมัครสมาชิกเรียบร้อย");
                    $(":mobile-pagecontainer").pagecontainer("change", "#login", { transition: "flip", changeHash: false });
                });

            } else {
                alert("พบเลขบัตรประชาชนแล้ว");
            }

        }, errorCB);
    });
    

}
*/

function register() {
    /*
    var staff = {
        id: $('#staIDNumber').val(),
        pwd: $('#staPassword').val(),
        title: $('#staTitle').val(),
        fn: $('#staFirstName').val(),
        ln: $('#staLastName').val(),

        phone: $('#staPhone').val(),
        occid: $('#staJob').val(),
        pos: $('#staPosition').val(),
        of: $('#staAddress').val(),
        pv: $('#staProvince').val(),

        ap: $('#staAmphor').val(),
        tb: $('#staTumbon').val()
    };
    */

    var staff = {
        "id": "" + $('#staIDNumber').val() + "",
        "pwd": "" + $('#staPassword').val() + "",
        "title": "" + $('#staTitle').val() + "",
        "fn": "" + $('#staFirstName').val() + "",
        "ln": "" + $('#staLastName').val() + "",

        "phone": "" + $('#staPhone').val() + "",
        "occid": "" + $('#staJob').val() + "",
        "pos": "" + $('#staPosition').val() + "",
        "of": "" + $('#staAddress').val() + "",
        "pv": "" + $('#staProvince').val() + "",

        "ap": "" + $('#staAmphor').val() + "",
        "tb": "" + $('#staTumbon').val() + ""
    };

    console.log(staff);
    startAjaxLoader();
    $.ajax({
        type: "POST",
        url: "http://seniorservice.azurewebsites.net/Service1.svc/InsertStaff",
        data: JSON.stringify(staff),
        contentType: "application/json",
        dateType: 'json', success: function (msg) {
            saveData_callback(msg);
        },
        error: function (request, status, error) {
            stopAjaxLoader();
            console.log(error);
        }
    });

    function saveData_callback(d) {
        stopAjaxLoader();
        alert('Complete');
        $(":mobile-pagecontainer").pagecontainer("change", "#login", { transition: "flip", changeHash: false });
    }
}

function loadStaffData(staffCID) {
    //var staffCID = sessionStorage.getItem('CID');
    var queryStr = "SELECT * FROM Staff WHERE StaffID = '" + staffCID + "' ";
    db.transaction(function (tx) {
        tx.executeSql(queryStr, [], (function (tx, response) {
            //console.log(response.rows[0]);
            $('#staIDNumber').val(response.rows.item(0).StaffID);
            $('#staPassword').val(response.rows.item(0).Password);
            $('#staConfirmPassword').val(response.rows.item(0).Password);
            $('#staTitle').val(response.rows.item(0).Title);
            $('#staFirstName').val(response.rows.item(0).FirstName);

            $('#staLastName').val(response.rows.item(0).LastName);
            $('#staPhone').val(response.rows.item(0).Phone);
            $('#staJob').val(response.rows.item(0).OccupationID);
            $('#staPosition').val(response.rows.item(0).Position);
            $('#staAddress').val(response.rows.item(0).Office);

            $('#staProvince').val(response.rows.item(0).Province);
            //insertCity(response.rows.item(0).Province, 'staAmphor');

            db.transaction(function (tx) {
                tx.executeSql("SELECT CityID, CityDescription FROM City WHERE ProvinceID = '" + response.rows.item(0).Province + "' ", [],
                    function (tx, result) {
                        if (result.rows.length != 0) {
                            for (var i = 0; i < result.rows.length ; i++) {
                                $('#staAmphor').append($("<option></option>").val(result.rows[i].CityID).html(result.rows[i].CityDescription));
                            }
                        }
                    }, errorCB);
            }, errorCB, function () {
                $('#staAmphor').val(response.rows.item(0).Amphor);

                db.transaction(function (tx) {
                    tx.executeSql("SELECT TumbonID, TumbonDescription FROM Tumbon WHERE CityID = '" + response.rows.item(0).Amphor + "' ", [],
                        function (tx, result) {
                            if (result.rows.length != 0) {
                                for (var i = 0; i < result.rows.length ; i++) {
                                    $('#staTumbon').append($("<option></option>").val(result.rows[i].TumbonID).html(result.rows[i].TumbonDescription));
                                }
                            }
                        }, errorCB);
                }, errorCB, function () {
                    $('#staTumbon').val(response.rows.item(0).Tumbon);
                    $('#register select').selectmenu('refresh');
                });
            });

        }), errorCB);
    }, errorCB);

    /*
    //หา AddressID ของ Person
    //var AddressID;
    var queryFindAddressID = 'SELECT A.AddressID FROM Address A INNER JOIN PersonVSAddress PVA ON A.AddressID = PVA.AddressID WHERE PVA.PID = ' + staffCID + ' ';
    db.transaction(function (tx) { 
        tx.executeSql(queryFindAddressID, [],
            (function (tx, response) {                
                //AddressID = response.rows.item(0).AddressID;

                Address = response.rows.item(0).AddressID;

                var queryFindAddressData = 'SELECT * FROM Address WHERE AddressID = "' + Address + '" '; //หาข้อมูล Address
                

                db.transaction(function (txData) {
                    txData.executeSql(queryFindAddressData, [],
                        (function (txData, rpData) {

                            //console.log("AID = " + response.rows.item(0).AddressID);

                            $('#staAddress').val(rpData.rows.item(0).AddressData);
                            $('#staProvince').val(rpData.rows.item(0).Province);
                            insertCity( rpData.rows.item(0).Province, 'staAmphor');
                            $('#staAmphor').val(rpData.rows.item(0).Amphor);
                            insertTumbon(rpData.rows.item(0).Amphor, 'staTumbon');
                            $('#staTumbon').val(rpData.rows.item(0).Tumbon);
                            $('#staPostcode').val(rpData.rows.item(0).Postcode);
                            
                            $('select').selectmenu().selectmenu('refresh', true);

                        }), errorCB, successCB);
                });
                


            }), errorCB, successCB);
    });    
    */

    return false;
}

function updateStaffData(staffID, staffAddress) {
    console.log("ID : " + staffID + "\nAddress : " + staffAddress);

    var staff = {
        id: $('#staIDNumber').val(),
        pwd: $('#staPassword').val(),
        title: $('#staTitle').val(),
        fn: $('#staFirstName').val(),
        ln: $('#staLastName').val(),

        phone: $('#staPhone').val(),
        occid: $('#staJob').val(),
        pos: $('#staPosition').val(),
        of: $('#staAddress').val(),
        pv: $('#staProvince').val(),

        ap: $('#staAmphor').val(),
        tb: $('#staTumbon').val()
    };

    isOnlineMode = checkInternet();
    console.log(isOnlineMode);

    var r = confirm("ต้องการอัพเดทข้อมูล หรือไม่");
    if (r == true) {

        //--------- อัพเดทตาราง Staff ---------------
        db.transaction(function (tx) {
            tx.executeSql('UPDATE Staff SET Password = "' + staff.pwd + '", Title = "' + staff.title + '", FirstName = "' + staff.fn + '", '
                + 'LastName = "' + staff.ln + '", Phone = "' + staff.phone + '", OccupationID = "' + staff.occid + '", Position = "' + staff.pos + '", '
                + 'Office = "' + staff.of + '", Tumbon = "' + staff.tb + '", Amphor = "' + staff.ap + '", Province = "' + staff.pv + '" '
                + 'WHERE StaffID = "' + staffID + '" ');
        }, errorCB, function () {
            if (isOnlineMode == true) {
                console.log("both online and offline update.");
                startAjaxLoader();
                $.ajax({
                    type: "POST",
                    url: "http://seniorservice.azurewebsites.net/Service1.svc/InsertStaff",
                    data: JSON.stringify(staff),
                    contentType: "application/json",
                    dateType: 'json', success: function (msg) {
                        saveData_callback(msg);
                    },
                    error: function (request, status, error) {
                        stopAjaxLoader();
                        console.log(error);
                    }
                });

                function saveData_callback(d) {
                    stopAjaxLoader();
                    //alert('Complete');
                    window.location.replace('home.html');
                }
            }
            else {
                console.log("offline update.");
                window.location.replace('home.html');
            }

        });
        //-----------------------------------------
        console.log("อัพเดท");
    } else {
        console.log("ยกเลิก");
    }

}

$(document).on("pagebeforecreate", "#register", function (event) {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);
    if ($('#staTitle').children('option').length < 3) {
        insertDropdown(); lvCreate('register');
    }
    //var username = sessionStorage.getItem('CID');
    //alert("Welcome : " + username);

    if (sessionStorage.getItem('CID') === null) {
        //console.log('null');
        document.getElementById("navbarEditList").style.display = "none";
        $("#staffInfoH1").empty();
        $("#staffInfoH1").append("ลงชื่อเจ้าหน้าที่");
        //document.getElementById("staffInfoH1").innerHTML = "ลงชื่อเจ้าหน้าที่คนใหม่";
    } else {
        //console.log('else');
        document.getElementById("navbarList").style.display = "none";
        $("#staffInfoH1").empty();
        $("#staffInfoH1").append("แก้ไขเจ้าหน้าที่");
        //document.getElementById("staffInfoH1").innerHTML = "แก้ไขข้อมูลเจ้าหน้าที่";
        loadStaffData(sessionStorage.getItem('CID'));
    }
});

$(document).on("pagecreate", "#register", function (event) {
    //db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);    

    $('#staIDNumber').on('keyup', function (e) {        
        var fn = document.getElementById('staIDNumber').value;
        if (fn == "") {
            $('#staIDNumber').css('border', '1px solid #ff0000');
            return false;
        } else {
            $('#staIDNumber').css('border', '1px solid #00ff00');
        }
        if (fn.length < 13) {
            $('#staIDNumber').css('border', '1px solid #ff0000');
            return false;
        } else {
            if (/^\d+$/.test($('#staIDNumber').val())) {
                $('#staIDNumber').css('border', '1px solid #00ff00');
            }
            else {
                $('#staIDNumber').css('border', '1px solid #ff0000');
            }
        }
    });

    $('#staPassword, #staConfirmPassword').on('keyup', function (e) {
        //var fn = document.getElementById('staIDNumber').value;
        var fn = $('#staPassword').val(), fn2 = $('#staConfirmPassword').val();
        if (fn != fn2) {
            $('#staPassword, #staConfirmPassword').css("border", "");
            $(this).css('border', '1px solid #ff0000');
            return false;
        }
        else if (fn == fn2 && fn.length != 0 && fn2.length != 0) {
            $('#staPassword, #staConfirmPassword').css('border', '1px solid #00ff00');
        } else { }
    });

    $('#registerBtn').click(function (e) {
        
        var inputNotNull = true;
        //$("#regForm :text, :file, :checkbox, select, textarea").each(function () {
        $('.regForm input').each(function () {
            $(this).css('border', '');
            if ($(this).val() == "") {
                inputNotNull = false;
                $(this).css('border', '1px solid #ff0000');                
                $('html, body').animate({ scrollTop: $(this).offset().top - 100 }, 'slow');
                return false;
            }
            else {
                //inputNotNull = true;
            }
        });

        if (inputNotNull == true && $('#staIDNumber').val().length == 13 && /^\d+$/.test($('#staIDNumber').val())) {
            var checkPassword = comparePassword($('#staPassword').val(), $('#staConfirmPassword').val());
            if (checkPassword == true) {
                register();
            }
            else {
                alert("รหัสผ่านไม่ตรงกัน");
                e.preventDefault();
            }
        } else {
            alert("กรุณากรอกข้อมูลให้ครบและถูกต้อง");
            e.preventDefault();
        }


    });

    $('#editListBtn').click(function (e) {
        var staffID = sessionStorage.getItem('CID');

        var inputNotNull = true;
        //$("#regForm :text, :file, :checkbox, select, textarea").each(function () {
        $('.regForm input').each(function () {
            if ($(this).val() == "") {
                inputNotNull = false;
                return false;
            }
            else {
                //inputNotNull = true;            
            }
        });

        if (inputNotNull == true) {
            var checkPassword = comparePassword($('#staPassword').val(), $('#staConfirmPassword').val());
            if (checkPassword == true) {
                //register();
                updateStaffData(staffID, Address); //Address = Global variable
            }
            else {
                alert("รหัสผ่านไม่ตรงกัน");
                e.preventDefault();
            }
        } else {
            alert("กรุณากรอกข้อมูลให้ครบ");
            e.preventDefault();
        }
    });

    document.getElementById('staProvince').onchange = function () {
        var txtProvinceID = $(this).val();
        //var txtProvinceID = $(this).find('option:selected').text(); // get text
        //console.log(txtProvinceID);
        $('#staAmphor').empty();
        $('#staAmphor').append($("<option></option>").val(000).html('ไม่ระบุ'));
        $('#staAmphor').selectmenu("refresh");
        $('#staTumbon').empty();
        $('#staTumbon').append($("<option></option>").val(000).html('ไม่ระบุ'));
        $('#staTumbon').selectmenu("refresh");
        insertCity(txtProvinceID, 'staAmphor');

    };

    document.getElementById('staAmphor').onchange = function () {
        var txtAmphorID = $(this).val();
        //var txtProvinceID = $(this).find('option:selected').text(); // get text
        //console.log(txtProvinceID);
        $('#staTumbon').empty();
        $('#staTumbon').append($("<option></option>").val(000).html('ไม่ระบุ'));
        $('#staTumbon').selectmenu("refresh");
        insertTumbon(txtAmphorID, 'staTumbon');
    };


});


function startAjaxLoader() {
    if ($('#ajaxLoadScreen') != null) {
        var loader = '<div id="ajaxLoadScreen"><img id="loadingImg" src="img/ajax-loader-bar.gif"></div>';
        $(loader).appendTo('body');
    }
}

function stopAjaxLoader() {
    $('#ajaxLoadScreen').remove();
}

function insertCity(provinceID, dropdownID) {

    var queryGetCityByProvince = "SELECT C.CityID, C.CityDescription "
        + "FROM City C INNER JOIN Province P ON C.ProvinceID = P.ProvinceID "
        + "WHERE P.ProvinceID = '" + provinceID + "' ";

    db.transaction(function (tx) {
        tx.executeSql(queryGetCityByProvince, [], (function (tx, response) {
            if (response.rows.length != 0) {

                for (var i = 0; i < response.rows.length ; i++) {
                    $('#' + dropdownID + '').append($("<option></option>").val(response.rows[i].CityID).html(response.rows[i].CityDescription));
                }
            }
        }), errorCB);
    }, errorCB, function () {
        $('#' + dropdownID + '').selectmenu("refresh");
    });

    /*
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/getCityListFromProvince?provinceID=" + provinceID + "",
        dataType: 'jsonp',
        success: function (msg) {
            var a = JSON.parse(msg);
            //console.log(a);
            
            $('#' + dropdownID + '').empty();
            $('#' + dropdownID + '').append($("<option></option>").val(000).html('ไม่ระบุ'));
            for (var i = 0; i < a.length ; i++) {
                $('#' + dropdownID + '').append($("<option></option>").val(a[i]['cityID']).html(a[i]['cityDes']));
            }
            $('#' + dropdownID + '').selectmenu("refresh");

        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
    */

}

function insertTumbon(cityID, dropdownID) {

    var queryGetCityByProvince = "SELECT T.TumbonID, T.TumbonDescription "
        + "FROM Tumbon T INNER JOIN City C ON T.CityID = C.CityID "
        + "WHERE C.CityID = '" + cityID + "' ";

    db.transaction(function (tx) {
        tx.executeSql(queryGetCityByProvince, [], (function (tx, response) {

            if (response.rows.length != 0) {
                for (var i = 0; i < response.rows.length ; i++) {

                    $('#' + dropdownID + '').append($("<option></option>").val(response.rows[i].TumbonID).html(response.rows[i].TumbonDescription));
                }
            }
        }), errorCB);
    }, errorCB, function () {
        $('#' + dropdownID + '').selectmenu("refresh");
    });

    /*
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/getTumbonListFromCity?cityID=" + cityID + "",
        dataType: 'jsonp',
        success: function (msg) {
            var a = JSON.parse(msg);
            //console.log(a);
            
            $('#' + dropdownID + '').empty();
            $('#' + dropdownID + '').append($("<option></option>").val(000).html('ไม่ระบุ'));
            for (var i = 0; i < a.length ; i++) {
                $('#' + dropdownID + '').append($("<option></option>").val(a[i]['tumbonID']).html(a[i]['tumbonDes']));
            }
            $('#' + dropdownID + '').selectmenu("refresh");

        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
    */
}

function insertDropdown() {

    //Title dropdown
    $('#staTitle').empty(); $('#title_info').empty();
    var queryTitle = "SELECT * FROM Title";
    db.transaction(function (tx) {
        tx.executeSql(queryTitle, [], (function (tx, response) {
            for (var i = 0; i < response.rows.length ; i++) {
                $('#staTitle').append($("<option></option>").val(response.rows.item(i).TitleID).html(response.rows.item(i).TitleDESC));
            }
        }), errorCB);
    });

    //Province dropdown     
    $('#staProvince').empty();
    var queryProvince = "SELECT ProvinceID, ProvinceDescription FROM Province ORDER BY ProvinceDescription";
    db.transaction(function (tx) {
        tx.executeSql(queryProvince, [],
            (function (tx, response) {
                for (var i = 0; i < response.rows.length ; i++) {
                    $('#staProvince').append($("<option></option>").val(response.rows.item(i).ProvinceID).html(response.rows.item(i).ProvinceDescription));
                }
            }), errorCB);
    });

    //Occupation dropdown
    var queryOccupation = "SELECT * FROM Occupation";
    $('#staJob').empty();
    db.transaction(function (tx) {
        tx.executeSql(queryOccupation, [],
            (function (tx, response) {
                for (var i = 0; i < response.rows.length ; i++) {
                    $('#staJob').append($("<option></option>").val(response.rows.item(i).OccupationID).html(response.rows.item(i).OccupationDescription));
                }
                $('#register select').selectmenu('refresh');
            }), errorCB);
    });



}

function checkInternet() {
    /*
    if (navigator.connection.type == 0) {
        //alert('This application requires internet. Please connect to the internet.');
        return false;
    }
    else if (navigator.connection.type == 'none') {
        //alert('This application requires internet. Please connect to the internet.');
        return false;
    }
    else {
        //alert("Hurray I'm online");//Hurray I'm online
        return true;
    }
    */
    /*
    var result = false;
    document.addEventListener("online", function () {
        result = true;
    }, false);

    document.addEventListener("offline", function () {
        result = false;
    }, false);
    */

    var result = false;
    if (navigator.onLine) {
        result = true;
    }
    else {
        result = false;
    }

    return result;

}

function guidGenerator() {
    var S4 = function () {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
}

//---------- Deprecate --------------

function titleService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=TitleID&desc=TitleDescription&table=Title&condition",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);     

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO Title (TitleID, TitleDESC) VALUES ( "
                    + " '" + a[i]['TitleID'] + "', "
                    + " '" + a[i]['TitleDescription'] + "' "
                    + " ) ");
                }
            });

        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function provinceService() {
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/getProvinceList",
        dataType: 'jsonp',
        success: function (msg) {
            var a = JSON.parse(msg);

            //insertProvinceTable(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO Province (ProvinceID, ProvinceDescription) VALUES ( "
                    + " '" + a[i]['provinceID'] + "', "
                    + " '" + a[i]['provinceDes'] + "' "
                    + " ) ");
                }
            });

        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function mariageService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=MariageStatusID&desc=MariageStatusDescription&table=MariageStatus&condition=",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO MariageStatus ( MariageStatusID, MariageStatusDescription ) VALUES ( "
                    + " '" + a[i]["MariageStatusID"] + "', "
                    + " '" + a[i]["MariageStatusDescription"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function nationService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=NationalityID&desc=NationalityDescription&table=Nationality&condition=",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO Nationality ( NationalityID, NationalityDescription ) VALUES ( "
                    + " '" + a[i]["NationalityID"] + "', "
                    + " '" + a[i]["NationalityDescription"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function raceService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=RaceID&desc=NationalityDescription&table=Race&condition=",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO Race ( RaceID, RaceDescription ) VALUES ( "
                    + " '" + a[i]["RaceID"] + "', "
                    + " '" + a[i]["NationalityDescription"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function religionService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=Id&desc=Religion&table=Religion&condition=",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO Religion ( Id, Religion ) VALUES ( "
                    + " '" + a[i]["Id"] + "', "
                    + " '" + a[i]["Religion"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function relationService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=RelationID&desc=RelationThaiDescription&table=Relation&condition=",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO Relation ( RelationID, RelationDescription ) VALUES ( "
                    + " '" + a[i]["RelationID"] + "', "
                    + " '" + a[i]["RelationThaiDescription"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function educationLevelService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=EducationLevelID&desc=EducationLevelDescription&table=EducationLevel&condition=",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO EducationLevel ( EducationLevelID, EducationLevelDescription ) VALUES ( "
                    + " '" + a[i]["EducationLevelID"] + "', "
                    + " '" + a[i]["EducationLevelDescription"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function occupationService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=OccupationID&desc=OccupationDescription&table=Occupation&condition",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);            

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO Occupation ( OccupationID, OccupationDescription ) VALUES ( "
                    + " '" + a[i]["OccupationID"] + "', "
                    + " '" + a[i]["OccupationDescription"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

function liveHomeStatusService() {
    var a;
    $.ajax({
        url: "http://newtestnew.azurewebsites.net/ServiceControl/Service.svc/GetDropdownProfileDesc?id=LiveHomeStatusID&desc=LiveHomeStatusDescription&table=LiveHomeStatus&condition",
        dataType: 'jsonp',
        success: function (msg) {
            a = JSON.parse(msg);
            //console.log(a);

            db.transaction(function (tx) {
                for (var i = 0; i < a.length ; i++) {
                    tx.executeSql("INSERT INTO LiveHomeStatus ( LiveHomeStatusID, LiveHomeStatusDescription ) VALUES ( "
                    + " '" + a[i]["LiveHomeStatusID"] + "', "
                    + " '" + a[i]["LiveHomeStatusDescription"] + "' "
                    + " ) ");
                }
            });


        },
        error: function (request, status, error) {
            alert('login error ' + error);
        }
    });
} //

//---------- END Deprecate ------------

function deleteAllTable() {
    db = window.openDatabase("Database", "1.0", "Survey", 2 * 1024 * 1024);
    db.transaction(function (tx) {
        tx.executeSql("DROP TABLE IF EXISTS Person");
        tx.executeSql("DROP TABLE IF EXISTS Staff");
        tx.executeSql("DROP TABLE IF EXISTS Address");
        tx.executeSql("DROP TABLE IF EXISTS PersonVSAddress");
        tx.executeSql("DROP TABLE IF EXISTS PersonOccupation");

        tx.executeSql("DROP TABLE IF EXISTS PersonFinance");
        tx.executeSql("DROP TABLE IF EXISTS PersonBankAccount");
        tx.executeSql("DROP TABLE IF EXISTS PersonEducation");
        tx.executeSql("DROP TABLE IF EXISTS PersonRecordBy");
        tx.executeSql("DROP TABLE IF EXISTS PersonStandIn");

        tx.executeSql("DROP TABLE IF EXISTS PersonMate");
        tx.executeSql("DROP TABLE IF EXISTS PersonFamily");
        tx.executeSql("DROP TABLE IF EXISTS PersonEmergencyContact");
        tx.executeSql("DROP TABLE IF EXISTS PersonParent");
        tx.executeSql("DROP TABLE IF EXISTS PersonAccident");

        tx.executeSql("DROP TABLE IF EXISTS AccidentDamageAndAssist");
        tx.executeSql("DROP TABLE IF EXISTS Choice");
        tx.executeSql("DROP TABLE IF EXISTS ChoiceGroupType");

        //<--------------- Dropdown Table --------------->
        tx.executeSql("DROP TABLE IF EXISTS Title");
        tx.executeSql("DROP TABLE IF EXISTS Province");
        tx.executeSql("DROP TABLE IF EXISTS MariageStatus");
        tx.executeSql("DROP TABLE IF EXISTS Nationality");
        tx.executeSql("DROP TABLE IF EXISTS Race");

        tx.executeSql("DROP TABLE IF EXISTS Religion");
        tx.executeSql("DROP TABLE IF EXISTS Relation");
        tx.executeSql("DROP TABLE IF EXISTS EducationLevel");
        tx.executeSql("DROP TABLE IF EXISTS Occupation");
        tx.executeSql("DROP TABLE IF EXISTS LiveHomeStatus");

        tx.executeSql("DROP TABLE IF EXISTS City");
        tx.executeSql("DROP TABLE IF EXISTS Tumbon");
        tx.executeSql("DROP TABLE IF EXISTS Bank");
        //<---------------------------------------------->

    }, errorCB,
       function (tx) {
           alert("All database are drop.")
       });
}

function runOnBrowser() {
    startAjaxLoader(); isOnlineMode = true; createDatabase();
}

function lvCreate(page) {

    var selectID = [];

    $('#' + page + ' .add-filter').each(function () {
        //console.log($(this).attr('id'));
        selectID.push($(this).attr('id'));
    });

    for (var i = 0; i < selectID.length; i++) {
        addFilterToSelect(selectID[i]);
    }

}

function addFilterToSelect(selectID) {
    $.mobile.document
        .on("listviewcreate", "#" + selectID + "-menu", function (e) {
            var input,
                listbox = $("#" + selectID + "-listbox"),
                form = listbox.jqmData("filter-form"),
                listview = $(e.target);
            if (!form) {
                input = $("<input data-type='search'></input>");
                form = $("<form></form>").append(input);
                input.textinput();
                $("#" + selectID + "-listbox")
                    .prepend(form)
                    .jqmData("filter-form", form);
            }
            listview.filterable({ input: input });
        })
        .on("pagebeforeshow pagehide", "#" + selectID + "-dialog", function (e) {
            var form = $("#" + selectID + "-listbox").jqmData("filter-form"),
                placeInDialog = (e.type === "pagebeforeshow"),
                destination = placeInDialog ? $(e.target).find(".ui-content") : $("#" + selectID + "-listbox");
            form
                .find("input")
                .textinput("option", "inset", !placeInDialog)
                .end()
                .prependTo(destination);
        });
}


